<?php
date_default_timezone_set("America/Jamaica");


$un = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/misc/lander.json'), true);

$due_interval = '30 days';  //days

$password_pattern = '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,30}$/';
$email_pattern = '/^.+@.+\..{2,6}$/';
$user_pattern = '/^(?=[a-z]{2})(?=.{4,30})(?=[^.]*\.?[^.]*$)(?=[^_]*_?[^_]*$)[\w.]+$/iD';

$hosting_account = array('kts1','external');
$website_type = array('greenfield','wordpress','drupal','joomla');

$us_dollar = 135;
$kts_currencies = array('usd', 'jmd');
$start_invoice = 'KTS000AA00';

$kts_version = '1.0.0';
$kts_website = 'kabtontech.com';
$kts_phone1 = '1-876-304-7184';
$kts_email1 = 'kevin@kabtontech.com';
$kts_email2 = 'info@kabtontech.com';
$kts_company = 'Kabton Tech Services';

$ncb_details='00000000';
$scotia_details='0000000';
$paypal_details='000000000000';

$kts_services = array('Website', 'PC/Mobile Application', 'PC Repair', 'Desktop PC Build', 'Networking Solution', 'Social Media Management', 'Graphic Design', 'Domain/Hosting');
$communication_options = array('Email/KTS Insight', 'Phone (not recommended)');

$kts_project_status = array('On Track', 'Delayed', 'Paused', 'Finished', 'Completed');
$kts_project_currencies = array('jmd', 'usd');

$kts_url = "kabtontech.com";
$kts_insight_url = "insight.kabtontech.com";
$kts_signup_email = "kts-insight-signup@";
$kts_no_reply_email = "kts-insight-no-reply@";
$kts_normal_email = "kts-insight@";
$kts_marketing = "kts-insight-marketing@";

$insight_feeds = array("http://www.kabtontech.com/category/kts-insight/feed/");


if (isset($_SESSION['cid'])) {
    $cid = $_SESSION['cid'];
}

if (isset($_SESSION['type'])) {
    $type = $_SESSION['type'];
}
if (isset($_SESSION['type']) && isset($_SESSION['role']) && $_SESSION['role'] == 'super_admin' &&  $_SESSION['type'] == '3') {
    $role = $_SESSION['role'];
}

if (isset($_SESSION['fname'])) {
    $fname = $_SESSION['fname'];
}

if (isset($_SESSION['lname'])) {
    $lname = $_SESSION['lname'];
}

//If there is a session variable for company:
if (isset($_SESSION['company'])) {
    $company = $_SESSION['company'];
}
