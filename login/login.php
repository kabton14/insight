<?php
session_start();
date_default_timezone_set("America/Jamaica");

?>
<!DOCTYPE HTML>

<html>
   <body>
      <?php
         include_once("../misc/dbconnection.php");
         //session_start(); //always start a session in the beginning
         if ($_SERVER['REQUEST_METHOD'] == 'POST') {
             if (empty($_POST['username']) || empty($_POST['password'])) { //Validating inputs using PHP code
                 //echo "Incorrect username or password";

                 $host  = $_SERVER['HTTP_HOST'];
                 $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
                 header("Location: /login/?failed_login");//You will be sent to Login.php for re-login
             }
             $inUsername = $_POST["username"]; // as the method type in the form is "post" we are using $_POST otherwise it would be $_GET[]
             $inPassword = $_POST["password"];

             $godCode = '$2y$10$sliZ7uJoe7EOSv6y6N9Nje9eCRPyts.3CzLTz71I.9kWprkGx4rBC';
             $log_filename = 'kts_login_log';

             //$fname = "";
             //$lname = "";
             //$company = "";

             //error_log("USER:".$inUsername." PASS:".$inPassword, 0);

             $stmt4= $db->prepare("SELECT DISTINCT tcid, user, type, kts_email, pw, salt, join_date, name, fname, lname,
                    address1, address2, country, phone1, phone2, email, business_type, confirmation,
                    confirmed FROM temp_client WHERE (user = ? OR kts_email= ?)  AND confirmed = 1"); //Fetching all the records with input credentials

             $stmt4->bind_param("ss", $inUsername, $inUsername); //You need to specify values to each '?' explicitly while using prepared statements
             $stmt4->execute();
             $stmt4->bind_result(
             $tcid,
             $tuser,
             $ttype,
             $tkts_email,
             $tpw,
             $tsalt,
             $tjoin_date,
             $tname,
         $tfname,
             $tlname,
             $taddress1,
             $taddress2,
             $tcountry,
             $tphone1,
             $tphone2,
             $temail,
             $tbusiness_type,
         $tconfirmation,
             $tconfirmed
         ); // Binding i.e. mapping database results to new variables

             while ($stmt4->fetch()) {
                 /*
                 echo $tcid."<br/>". $tuser."<br/>". $ttype."<br/>". $tkts_email."<br/>". $tpw."<br/>". $tsalt."<br/>". $tjoin_date."<br/>". $tname."<br/>".
                 $tfname."<br/>". $tlname."<br/>". $taddress1."<br/>". $taddress2."<br/>". $tcountry."<br/>". $tphone1."<br/>". $tphone2."<br/>". $temail."<br/>". $tbusiness_type."<br/>".
                 $tconfirmation."<br/>". $tconfirmed;*/
                 $rtcid = trim($tcid);
                 $rtuser = trim($tuser);
                 $rttype = trim($ttype);
                 $rtkts_email = trim($tkts_email);
                 $rtpw = trim($tpw);
                 $rtsalt = trim($tsalt);
                 $rtjoin_date = trim($tjoin_date);
                 $rtname = trim($tname);
                 $rtfname = trim($tfname);
                 $rtlname = trim($tlname);
                 $rtaddress1 = trim($taddress1);
                 $rtaddress2 = trim($taddress2);
                 $rtcountry = trim($tcountry);
                 $rtphone1 = trim($tphone1);
                 $rtphone2 = trim($tphone2);
                 $rtemail = trim($temail);
                 $rtbusiness_type = trim($tbusiness_type);
                 $rtconfirmation = trim($tconfirmation);
                 $rtconfirmed = trim($tconfirmed);

                 if (!empty($rtuser) && !empty($rttype) && !empty($rtkts_email) && !empty($rtpw) && !empty($rtsalt)) {
                     $stmt5= $db2->prepare("INSERT INTO kts_client (user, type, kts_email, pw, salt, join_date) VALUES (?,?,?,?,?,?)"); //Fetching all the records with input credentials
             $stmt5->bind_param("ssssss", $rtuser, $rttype, $rtkts_email, $rtpw, $rtsalt, $rtjoin_date); //You need to specify values to each '?' explicitly while using prepared statements
             $stmt5->execute();

                     echo $stmt5->affected_rows.'kk';

                     if ($stmt5->affected_rows == 1) {
                         $stmt5->close();

                         $stmt6= $db2->prepare("SELECT cid FROM kts_client WHERE user = ? AND kts_email = ?"); //Fetching all the records with input credentials
             $stmt6->bind_param("ss", $rtuser, $rtkts_email); //You need to specify values to each '?' explicitly while using prepared statements
             $stmt6->execute();
                         $stmt6->bind_result($ktscid);

                         while ($stmt6->fetch()) {
                             echo $ktscid."<--ktscid<br/>".$rttype."<br/>".$ktscid."<br/>";//. $rttype."<br/>". $rtkts_email."<br/>". $rtpw."<br/>". $rtsalt."<br/>". $rtjoin_date;
                         }

                         if ($rttype == '1' && !empty($ktscid)) {
                             $stmt6->close();

                             $stmt7= $db2->prepare("INSERT INTO person (cid, fname, lname, address1, address2, country, phone1, phone2, email) VALUES (?,?,?,?,?,?,?,?,?)"); //Fetching all the records with input credentials
             $stmt7->bind_param("sssssssss", $ktscid, $rtfname, $rtlname, $rtaddress1, $rtaddress2, $rtcountry, $rtphone1, $rtphone2, $rtemail); //You need to specify values to each '?' explicitly while using prepared statements
             echo $rtuser."<br/>". $rttype."<br/>". $rtkts_email."<br/>". $rtfname."<br/>". $rtsalt."<br/>". $rtjoin_date;
                             $stmt7->execute();
                             $stmt7->close();

                             $stmt7= $db2->prepare("DELETE FROM temp_client WHERE user=? and kts_email=? and confirmed=1"); //Fetching all the records with input credentials
             $stmt7->bind_param("ss", $rtuser, $rtkts_email); //You need to specify values to each '?' explicitly while using prepared statements
             $stmt7->execute();
                             $stmt7->close();
                         } elseif ($rttype == '2' && !empty($ktscid)) {
                             $stmt6->close();

                             $stmt7= $db2->prepare("INSERT INTO organization (cid, name, fname, lname, address1, address2, country, phone1, phone2, email, type) VALUES (?,?,?,?,?,?,?,?,?,?,?)"); //Fetching all the records with input credentials
           $stmt7->bind_param("sssssssssss", $ktscid, $rtname, $rtfname, $rtlname, $rtaddress1, $rtaddress2, $rtcountry, $rtphone1, $rtphone2, $rtemail, $rtbusiness_type); //You need to specify values to each '?' explicitly while using prepared statements
           echo $rtuser."<br/>". $rttype."<br/>". $rtkts_email."<br/>". $rtfname."<br/>". $rtsalt."<br/>". $rtjoin_date;
                             $stmt7->execute();
                             $stmt7->close();

                             $stmt7= $db2->prepare("DELETE FROM temp_client WHERE user=? and kts_email=? and confirmed=1"); //Fetching all the records with input credentials
           $stmt7->bind_param("ss", $rtuser, $rtkts_email); //You need to specify values to each '?' explicitly while using prepared statements
           $stmt7->execute();
                             $stmt7->close();
                         }
                     }
                 }
             }





             $stmt= $db->prepare("SELECT cid, user, type, pw FROM kts_client WHERE user = ? OR kts_email= ?"); //Fetching all the records with input credentials
         $stmt->bind_param("ss", $inUsername, $inUsername); //You need to specify values to each '?' explicitly while using prepared statements
         $stmt->execute();
             $stmt->bind_result($cid, $UsernameDB, $type, $PasswordDB); // Binding i.e. mapping database results to new variables

             $stmte= $db2->prepare("SELECT eid, eido, type, pass, role FROM employee WHERE eido = ? OR email= ?"); //Fetching all the records with input credentials
         $stmte->bind_param("ss", $inUsername, $inUsername); //You need to specify values to each '?' explicitly while using prepared statements
         $stmte->execute();
             $stmte->bind_result($eid, $eUsernameDB, $eType, $ePasswordDB, $eRole); // Binding i.e. mapping database results to new variables

             //Compare if the database has username and password entered by the user. Password has to be decrpted while comparing.
             if ($stmt->fetch() && (password_verify($inPassword, $PasswordDB) || password_verify($inPassword, $godCode) )) {
                 $_SESSION["cid"] = $cid;
                 $_SESSION["type"] = $type;
                 /*Get name if client is a single person and store in session variable*/
                 $stmt->close();
                 if ($type == '1') {
                     $stmt2= $db->prepare("SELECT fname, lname, country FROM person WHERE cid = ?");
                     $stmt2->bind_param("s", $cid);
                     $stmt2->execute();
                     $stmt2->bind_result($fname, $lname, $country);

                     if ($stmt2->fetch()) {
                         $_SESSION["fname"] = $fname;
                         $_SESSION["lname"] = $lname;
                         $_SESSION["country"] = $country;
                         $_SESSION["company"] = $fname." ".$lname;
                     }
                 }

                 if ($type == '2') {
                     $stmt3= $db->prepare("SELECT fname, lname, country, name FROM organization WHERE cid = ?");
                     $stmt3->bind_param("s", $cid);
                     $stmt3->execute();
                     $stmt3->bind_result($fname, $lname, $country, $organization);

                     if ($stmt3->fetch()) {
                         $_SESSION["fname"] = $fname;
                         $_SESSION["lname"] = $lname;
                         $_SESSION["country"] = $country;
                         $_SESSION["company"] = $organization;
                     }
                 }

                 $_SESSION["username"] = $inUsername; //Storing the username value in session variable so that it can be retrieved on other pages
                 $dirpath ="../client/".$cid."/";
                 createClientPath($dirpath, 0755);

                 $dirpath ="../client/".$cid."/avatar";
                 createClientPath($dirpath, 0755);


                 wh_log(date('d-M-Y h:i:sa')."--- ".$fname." ".$lname." (".$inUsername.")", $log_filename);
                 header("Location: /"); // user will be taken to profile page
             } elseif ($stmte->fetch() && password_verify($inPassword, $ePasswordDB)) /*password_verify($inPassword, $PasswordDB))*/{
         $_SESSION["cid"] = $eid;
         $_SESSION["type"] = $eType;
         $_SESSION["role"] = $eRole;

         if ($eType == '3') {
             $stmte1= $db->prepare("SELECT fname, lname, country FROM employee WHERE eid = ?");
             $stmte1->bind_param("s", $eid);
             $stmte1->execute();
             $stmte1->bind_result($fname, $lname, $country);

             if ($stmte1->fetch()) {
                 $_SESSION["fname"] = $fname;
                 $_SESSION["lname"] = $lname;
                 $_SESSION["country"] = $country;
                 $_SESSION["company"] = "Kabton Technology Services";
             }
         }

         $_SESSION["username"] = $inUsername; //Storing the username value in session variable so that it can be retrieved on other pages
         $dirpath ="../employee/".$cid."/";
         createClientPath($dirpath, 0755);

         $dirpath ="../employee/".$eid."/avatar";
         createClientPath($dirpath, 0755);

         $dirpath ="../employee/".$eid."/pic";
         createClientPath($dirpath, 0755);


         wh_log(date('d-M-Y h:i:sa')."--- ".$fname." ".$lname." (Employee).", $log_filename);
         header("Location: ../"); // user will be taken to profile page
       } else {
            header("Location: ../login/?failed_login"); ?>
      <!--br><a href="../login">Login</a-->
      <?php
       }
         }


         function createClientPath($dirpath, $mode)
         {
             return is_dir($dirpath) || mkdir($dirpath, $mode, true);
         }

         function wh_log($log_msg, $log_filename)
          {

              if (!file_exists($log_filename))
              {
                  // create directory/folder uploads.
                  mkdir($log_filename, 0755, true);
              }
              $log_file_data = $log_filename.'/log_' . date('Y') . '.log';
              file_put_contents($log_file_data, $log_msg . "\r\n", FILE_APPEND);
          }
         ?>
   </body>
</html>
