<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Insight| Kabton Tech Services | </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../vendors/animate.css/animate.min.css" rel="stylesheet">



    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <link href="../build/css/snackbar.css" rel="stylesheet">
  </head>

  <body class="login">
  <!-- The actual snackbar -->

    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form id="login_form" action="login.php" method="POST">
              <h1>User Login</h1>
              <div id="user_f" class="col-sm-12">
                <input type="text" class="form-control" name="username" placeholder="Username" required="" />
              </div>
              <div id="pass_f" class="col-sm-12">
                <input type="password" class="form-control" name="password" placeholder="Password" required="" />
              </div>
              <div id="login_button" class="col-sm-12" >
                <a class="btn btn-default submit" onclick="document.getElementById('login_form').submit()">Log in</a>
              </div>
              <div id="reset_pass" class="col-sm-12">
                <a class="reset_pass_w"  href="../reset">Lost your password?</a>
              </div>
              <div id="reset_pass" class="col-sm-12">
                <p class="change_link">New here?
                  <a href="../signup" class="to_register"> Create Account </a>
                </p>
              </div>

              <div class="clearfix"></div>

              <div class="separator">


                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><span><img src="../images/logoicon.png"/></span> Kabton Tech Services Insight</h1>
                  <p>©<?php echo date("Y");?> All Rights Reserved. <a href="https://kabtontech.com" target="_blank">Kabton Technology Services<a/></p>
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form>
              <h1>Create Account</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="email" class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <a class="btn btn-default submit" href="index.html">Submit</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Already a member ?
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1> Kabton Tech Services Insight </h1>
                  <p>©<?php echo date("Y");?> All Rights Reserved. Kabton Technology Services</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>


    <div id="snackbar">Congratulations! You have successfully activated your account. You can now log in.</div>
    <div id="snackbar2">There was a problem logging in. Make sure your log in details are correct.</div>
    <div id="snackbar3">Your password was successfully changed. You can now log into your account using the new login details.</div>
    <div id="snackbar4">Your email was successfully changed. You can now log into your account using the new login details.</div>
    <?php

if (isset($_REQUEST['welcome'])) {
    echo "
<script type='text/javascript'>


function myActivation() {
    // Get the snackbar DIV
    var x = document.getElementById('snackbar');

    // Add the 'show' class to DIV
    x.className = 'show';

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace('show', ''); }, 30000);
}

document.onload = myActivation();




</script>
";
}

if (isset($_REQUEST['failed_login'])) {
    echo "

<script>
function myActivation2() {
    // Get the snackbar DIV
    var x = document.getElementById('snackbar2');

    // Add the 'show' class to DIV
    x.className = 'show';

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace('show', ''); }, 30000);
}


document.onload = myActivation2();

</script>

";
}

if (isset($_REQUEST['pw_reset_confirmed'])) {
    echo "

<script>
function myActivation3() {
    // Get the snackbar DIV
    var x = document.getElementById('snackbar3');

    // Add the 'show' class to DIV
    x.className = 'show';

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace('show', ''); }, 30000);
}


document.onload = myActivation3();

</script>

";
}

if (isset($_REQUEST['email_confirmed'])) {
    echo "

<script>
function myActivation4() {
    // Get the snackbar DIV
    var x = document.getElementById('snackbar4');

    // Add the 'show' class to DIV
    x.className = 'show';

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace('show', ''); }, 30000);
}


document.onload = myActivation4();

</script>

";
}
?>

  </body>
</html>
