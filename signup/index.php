<?php

function generateRandomString($length)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

 ?>

<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>

    <title>KTS Insight | Sign Up</title>

    <!-- Bootstrap -->
    <link href='../vendors/bootstrap/dist/css/bootstrap.min.css' rel='stylesheet'>
    <!-- Font Awesome -->
    <link href='../vendors/font-awesome/css/font-awesome.min.css' rel='stylesheet'>
    <!-- NProgress -->
    <link href='../vendors/nprogress/nprogress.css' rel='stylesheet'>

    <!--SmartWizard-->
    <link href='../vendors/SmartWizarda/dist/css/smart_wizard.css' rel='stylesheet' type='text/css' />

    <!-- intlTelInput -->
    <link rel='stylesheet' href='../vendors/intl-tel-input-12.1.0/build/css/intlTelInput.css' />

    <!-- PNotify -->
    <link href='../vendors/pnotify/dist/pnotify.css' rel='stylesheet'>
    <link href='../vendors/pnotify/dist/pnotify.buttons.css' rel='stylesheet'>
    <link href='../vendors/pnotify/dist/pnotify.nonblock.css' rel='stylesheet'>

    <!-- Custom Theme Style -->
    <link href='../build/css/custom.min.css' rel='stylesheet'>
  </head>

  <body class='nav-md'>
    <div class='container body'>
      <div class='main_container col-md-12 col-sm-5 col-xs-12'>


        <!-- top navigation -->
        <div class='top_nav signup-top-nav' id="get-with-it">
          <div class='nav_menu'>
            <nav>
              <div class='nav toggle'>
                <a id='menu_toggle'><i class='fa fa-bars'></i></a>
              </div>

              <ul class='nav navbar-nav navbar-right'>

                <li class=''>
                  <a class='user-profile dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>
                    <a href='/'><i class='fa fa-home'></i> Home</a>
                  </a>

                </li>



              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class='right_col' id='signup-content' role='main'>
          <div class=''>
            <div class='page-title'>
              <div class='title_left'>
                <h3>Kabton Tech Services Insight&#8482;</h3>
              </div>

              <div class='title_right'>
                <div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'>

                </div>
              </div>
            </div>
            <div class='clearfix'></div>

            <div class='row'>

              <div class='col-md-12 col-sm-12 col-xs-12'>
                <div class='x_panel'>
                  <div class='x_title'>
                    <h2>Welcome<small></small></h2>

                    <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>



                    <h2></h2>
                    <!-- Tabs -->
                    <div id='wizard_horizontal' class=' form_wizard wizard_horizontal'>


                      <ul id='signup-anchor' class='wizard_steps anchor'>
                        <li>
                          <a href='#KTSI<?php echo $step1 = generateRandomString(5);  ?>' class='selected'>
                            <span class='step_no'>1</span>
                            <span class="step_descr">Step 1<br><small>About KTS Insight</small></span>
                          </a>
                        </li>
                        <li>
                          <a href='#KTSI<?php echo $step2 = generateRandomString(5);  ?>' class='disabled'>
                            <span class='step_no'>2</span>
                            <span class="step_descr">Step 2<br><small>Sign Up</small></span>
                          </a>
                        </li>
                        <li>
                          <a href='#KTSI<?php echo $step3 = generateRandomString(5);  ?>' class='disabled'>
                            <span class='step_no'>3</span>
                            <span class="step_descr">Step 3<br><small>Confirm Details</small></span>
                          </a>
                        </li>

                      </ul>



                      <div class='stepContainer'>
                      <div id='KTSI<?php echo $step1; ?>'>
                        <br/><br/>
                        <div class='signup-kts-logo'></div>
                        <br/><br/>
                        <h2 class='StepTitle'>About KTS Insight&#8482;</h2>
                        <p id='signup-intro'>Welcome to Kabton Tech Services (KTS) Insight&#8482;. This is where you will be able to request our services which includes computer repairs, software/website development etc. You will also be able to view the status of all your  projects with us.
                          To get started, simply click next below.
                        </p>
                      </div>
                      <div id='KTSI<?php echo $step2; ?>'>

                        <form id='signupform' class='form-horizontal form-label-left' novalidate>
                          <br/><br/>

                          <span class='section'>Personal Info</span>

                          <div class='item form-group'>
                            <label class='control-label col-md-3 col-sm-3' for='first-name'>First Name <span class='required'>*</span>
                            </label>
                            <div class='col-md-6 col-sm-6'>
                              <input id='first-name' class='form-control col-md-7 col-xs-12' data-validate-length-range='1,20' data-validate-words='1' name='first-name'  required='required' type='text'>
                            </div>
                          </div>
                          <div class='item form-group'>
                            <label class='control-label col-md-3 col-sm-3' for='last-name'>Last Name <span class='required'>*</span>
                            </label>
                            <div class='col-md-6 col-sm-6'>
                              <input id='last-name' class='form-control col-md-7 col-xs-12' data-validate-length-range='1,20' data-validate-words='1' name='last-name'  required='required' type='text'>
                            </div>
                          </div>

                          <span class='section'>Contact Info</span>

                          <div class='item form-group'>
                            <label class='control-label col-md-3 col-sm-3 col-xs-12' for='email'>Email <span class='required'>*</span>
                            </label>
                            <div class='col-md-6 col-sm-6 col-xs-12'>
                              <input type='email' id='email' name='email' required='required' class='form-control col-md-7 col-xs-12'>
                            </div>
                          </div>
                          <div class='item form-group'>
                            <label class='control-label col-md-3 col-sm-3 col-xs-12' for='email'>Confirm Email <span class='required'>*</span>
                            </label>
                            <div class='col-md-6 col-sm-6 col-xs-12'>
                              <input type='email' id='email2' autocomplete='false' name='confirm-email' data-validate-linked='email' required='required' class='form-control col-md-7 col-xs-12'>
                            </div>
                          </div>
                          <div class='item form-group'>
                            <label class='control-label col-md-3 col-sm-3 col-xs-12' for='phone1'>Phone 1 <span class='required'>*</span>
                            </label>
                            <div class='col-md-6 col-sm-6 col-xs-12'>
                                <input type='tel' id='phone1' name='phone1' required='required' data-validate-length-range='8,20' class='form-control col-md-7 col-xs-12'>
                            </div>
                          </div>
                          <div class='item form-group'>
                            <label class='control-label col-md-3 col-sm-3 col-xs-12' for='phone2'>Phone 2
                            </label>
                            <div class='col-md-6 col-sm-6 col-xs-12'>
                              <input type='tel' id='phone2' name='phone2'  data-validate-length-range='8,20' class='form-control col-md-7 col-xs-12 optional'>
                            </div>
                          </div>
                          <div class='item form-group'>
                            <label class='control-label col-md-3 col-sm-3' for='address1' >Address Line 1 <span class='required'>*</span>
                            </label>
                            <div class='col-md-6 col-sm-6'>
                              <input id='address1' class='form-control col-md-7 col-xs-12' data-validate-length-range='5,75' data-validate-words='2' name='address1'  required='required' type='text'>
                            </div>
                          </div>
                          <div class='item form-group'>
                            <label class='control-label col-md-3 col-sm-3' for='address2' >Address Line 2
                            </label>
                            <div class='col-md-6 col-sm-6'>
                              <input id='address2' class='form-control col-md-7 col-xs-12 optional' data-validate-length-range='5, 105' data-validate-words='1' name='address2' type='text'>
                            </div>
                          </div>
                          <div class='item form-group'>
                        <label class='control-label col-md-3 col-sm-3'>Country <span class='required'>*</span> </label>
                          <div class='col-md-6 col-sm-6'>
                            <select id='country' name='country' required='required' class='form-control'>
                              <?php
                                    $countries = file_get_contents('../misc/lander.json');
                                    $json_country = json_decode($countries, true);
                                    ksort($json_country);?>

                              <option value=''>Choose option</option>

                              <option value='JM'>Jamaica</option>
                              <option value='US'>United States</option>
                              <option value='DE'>Germany</option>

                              <?php foreach ($json_country as $key => $value) {
                                        echo "<option value='.$key.'>".$value."</option>";
                                    } ?>

                              <!--option value='JM'>Option one</option-->

                            </select>
                          </div>
                      </div>
                      <span class='section'>Catering to Your Needs</span>

                      <div class='item form-group'>
                        <label class='control-label col-md-3 col-sm-3' for='type' >Who are you? <span class='required'>*</span>
                        </label>
                        <div class=''>
                      <div class='btn-group col-md-6 col-sm-6 ' data-toggle='buttons'>
                        <label class='btn  btn-default client-type-i-button active'>
                          <input type='radio' value='1' name='client-type' id='option1' checked='checked'> Individual
                        </label>
                        <label class='btn btn-default client-type-o-button '>
                          <input type='radio' value='2' name='client-type' id='option2'> Business/Organization
                        </label>
                      </div>
                    </div>
                      </div>
                      <div id='client-type-on' class='item form-group'>
                        <label class='control-label col-md-3 col-sm-3' for='organization-name'>Organization Name<span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <input id='organization-name' class='form-control col-md-7 col-xs-12' data-validate-length-range='50' name='organization-name'  type='text'>
                        </div>
                      </div>

                      <div id='client-type-ot' class='item form-group'>
                        <label class='control-label col-md-3 col-sm-3' for='organization-type'>Organization Type<span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                            <select id='organization-type' name='organization-type' class='form-control'>
                              <option value=''>Choose option</option>
                              <option value='Church'>Church</option>
                              <option value='Child Care'>Child Care</option>
                              <option value='Entertainment'>Entertainment</option>
                              <option value='Techology'>Technology</option>
                              <option value='Other Government Sector'>Other Government Sector</option>
                              <option value='Other Private Sector'>Other Private Sector</option>
                            </select>
                        </div>
                      </div>



                      <span class='section'>Log In Info</span>
                      <div class='item form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='user-name'>User Name <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input id='user-name' class='form-control col-md-7 col-xs-12' autocomplete='false' data-validate-length-range='4,15' data-validate-words='1' name='user-name'  required='required' type='text'>
                        </div>
                      </div>

                      <div class='item form-group'>
                        <label for='password' class='control-label col-md-3'>Password <span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input id='password' type='password' autocomplete='false' name='password' data-validate-length-range='7,35' class='form-control col-md-7 col-xs-12' required='required'>
                        </div>
                      </div>
                      <div class='item form-group'>
                        <label for='password2' class='control-label col-md-3 col-sm-3 col-xs-12'>Repeat Password <span class='required'>*</span></label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input id='password2' type='password' autocomplete='false' name='password2' data-validate-length-range='7,35' data-validate-linked='password' class='form-control col-md-7 col-xs-12' required='required'>
                        </div>
                      </div>

                        </form>

                      </div>
                      <div id='KTSI<?php echo $step3;  ?>' >
                        <div class='confirm-content'>
                        <h2 class='StepTitle' id='step-title-3'>Confirm Your Email Address</h2>

                        <p id='step-description-3'>
                          Please check your email for a message with subject, '<strong>KTS Insight Confirmation</strong>'. Click the confirmation link or copy the
                          confirmation code and enter it on this page. (leave this page open). Remember to check your spam folder if email is not seen within 10 mins.

                        </p>

                        <div class='col-md-11' align='center' id='step-image-3'>
                          <br/><br/><br/>
                        <img src='../images/insightspinner.gif' />
                        <br/><br/><br/><br/>


                        <form id='step-form-3' class='form-horizontal form-label-left' >

                          <div class='form-group'>
                            <label class='col-md-6 control-label'>Enter Confirmation Code Here</label>

                            <div class='col-md-3 '>
                              <div class='input-group'>
                                <input id='step-confirm-3' type='text' class='form-control'>
                                <span class='input-group-btn'>
                                                  <button id='step-confirm-go-3' type='button' class='btn btn-primary'>Go!</button>
                                              </span>
                              </div>
                            </div>
                          </div>


                          <div class='divider-dashed'></div>

                        </form>
                      </div>






                      </div>
                    </div>

                    </div>
                    </div>
                    <!-- End SmartWizard Content -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer class='signup-footer'>
          <div class='pull-right signup-footer'>
            Insight - Project Viewer by <a href='https://kabtontech.com'>Kabton Tech Services</a>
          </div>
          <div class='clearfix'></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>


    <!-- Include jQuery -->
<!--script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script-->

    <!-- jQuery -->
    <script src='../vendors/jquery/dist/jquery.min.js'></script>
    <!-- Bootstrap -->
    <script src='../vendors/bootstrap/dist/js/bootstrap.min.js'></script>
    <!-- FastClick -->
    <script src='../vendors/fastclick/lib/fastclick.js'></script>
    <!-- NProgress -->
    <script src='../vendors/nprogress/nprogress.js'></script>
    <!-- jQuery Smart Wizard -->
    <!--script src='../vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'></script-->
    <script src='../vendors/SmartWizarda/dist/js/jquery.smartWizard.js'></script>
    <!-- validator -->
    <script src='../vendors/validator/validator.js'></script>
    <script src='../vendors/validator/validator2.js'></script>

    <!--intl-tel-input-->
    <script src='../vendors/intl-tel-input-12.1.0/build/js/intlTelInput.min.js'></script>

    <!-- PNotify -->
    <script src='../vendors/pnotify/dist/pnotify.js'></script>
    <script src='../vendors/pnotify/dist/pnotify.buttons.js'></script>
    <script src='../vendors/pnotify/dist/pnotify.nonblock.js'></script>

    <!-- Custom Theme Scripts -->
    <!--script src='../build/js/custom.min.js'></script-->
    <script src='../build/js/ktssignup.js'></script>


  </body>
</html>
