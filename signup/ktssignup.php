<?php
require($_SERVER['DOCUMENT_ROOT'].'/misc/dbconnection.php');
require($_SERVER['DOCUMENT_ROOT'].'/misc/variables.php');

$confirmation;


$first_name; $last_name; $email; $confirm_email; $phone1; $phone2;
$address1; $address2; $country; $client_type; $user_name; $password; $password2; $salt;
$organization_name; $organization_type; $confirmation; $confirmation_salt;


if (isset($_REQUEST['confirmation'])) {
    if (trim($_REQUEST['confirmation']) != null && trim($_REQUEST['confirmation']) != "") {
        $confirmation = mysqli_real_escape_string($db, $_REQUEST['confirmation']);
        $confirmed = 1;

        $stmt5= $db->prepare("UPDATE temp_client SET confirmed =? WHERE confirmation = ? LIMIT 1"); //Fetching all the records with input credentials
  $stmt5->bind_param("ss", $confirmed, $confirmation); //You need to specify values to each '?' explicitly while using prepared statements
  $stmt5->execute();

        if ($stmt5->affected_rows == 1) {
            $status['code'] = 1;
            $status['msg'] = "The user was confirmed.";
            echo json_encode($status);
            $stmt5->close();
            header("location: ../login/?welcome");
            return true;
        } else {
            $status['code'] = 2;
            $status['msg'] = "Records updated- ".$stmt5->affected_rows." (Confirmation)";
            echo json_encode($status);
            $stmt5->close();
            return false;
        }

        $stmt5->close();
    } else {
        $status['code'] = 2;
        $status['msg'] = "The input did not meet requirements. (Confirmation)";
        echo json_encode($status);
        return false;
    }
} elseif (isset($_REQUEST['option']) && isset($_REQUEST['user-name']) && isset($_REQUEST['email'])) {
    if ($_REQUEST['option'] != null && trim($_REQUEST['option']) == "poll" && $_REQUEST['user-name'] != null && trim($_REQUEST['user-name']) != ""
     && $_REQUEST['email'] != null && trim($_REQUEST['email']) != "") {
        $user = mysqli_real_escape_string($db, $_REQUEST['user-name']);
        $email = mysqli_real_escape_string($db, $_REQUEST['email']);



        $stmt5= $db->prepare("SELECT user, email from temp_client WHERE user = ? AND email = ? AND confirmed = 1"); //Fetching all the records with input credentials
       $stmt5->bind_param("ss", $user, $email); //You need to specify values to each '?' explicitly while using prepared statements
       $stmt5->execute();


        $stmt5->bind_result($result1, $result2);
        $count = 0;

        while ($stmt5->fetch()) {
            $count++;
        }



        if ($count == 1) {
            $status['code'] = 1;
            $status['msg'] = "The user was confirmed.";
            echo json_encode($status);
            $stmt5->close();
            return true;
        } else {
            $status['code'] = 2;
            $status['msg'] = "Records updated- ".$count." (Confirmation Poll)";
            echo json_encode($status);
            return false;
        }
    } else {
        $status['code'] = 2;
        $status['msg'] = "The input did not meet requirements. (Polling)";
        echo json_encode($status);
        return false;
    }
} elseif (isset($_REQUEST['first-name'])
&& isset($_REQUEST['last-name'])
&& isset($_REQUEST['email'])
&& isset($_REQUEST['confirm-email'])
&&isset($_REQUEST['phone1'])
&& isset($_REQUEST['address1'])
&& isset($_REQUEST['country'])
&& isset($_REQUEST['client-type'])
&& isset($_REQUEST['user-name'])
&& isset($_REQUEST['password'])
&& isset($_REQUEST['password2'])) {
    if (trim($_REQUEST['first-name']) != null && trim($_REQUEST['first-name']) != "") {
        $first_name = mysqli_real_escape_string($db, $_REQUEST['first-name']);
    } else {
        $status['code'] = 2;
        $status['msg'] = "The input did not meet requirements. (First Name)";
        echo json_encode($status);
        return false;
    }

    if (trim($_REQUEST['last-name']) != null && trim($_REQUEST['last-name']) != "") {
        $last_name = mysqli_real_escape_string($db, $_REQUEST['last-name']);
    } else {
        $status['code'] = 2;
        $status['msg'] = "The input did not meet requirements. (Last Name)";
        echo json_encode($status);
        return false;
    }

    if (trim($_REQUEST['email']) != null && trim($_REQUEST['email']) != "") {
        if ($_REQUEST['email'] != $_REQUEST['confirm-email']) {
            $status['code'] = 2;
            $status['msg'] = "The inputs do not match. (Email)";
            echo json_encode($status);
            return false;
        } else {
            $email = mysqli_real_escape_string($db, $_REQUEST['email']);
        }
    } else {
        $status['code'] = 2;
        $status['msg'] = "The input did not meet requirements. (Email)";
        echo json_encode($status);
        return false;
    }

    if (trim($_REQUEST['phone1']) != null && trim($_REQUEST['phone1']) != "") {
        $phone1 = mysqli_real_escape_string($db, $_REQUEST['phone1']);

        if (isset($_REQUEST['phone2']) && (trim($_REQUEST['phone2']) != null && trim($_REQUEST['phone2']) != "")) {
            $phone2 = mysqli_real_escape_string($db, $_REQUEST['phone2']);
        } else {
            $phone2 = "";
        }
    } else {
        $status['code'] = 2;
        $status['msg'] = "The input did not meet requirements. (Phone 1)";
        echo json_encode($status);
        return false;
    }

    if (trim($_REQUEST['address1']) != null && trim($_REQUEST['address1']) != "") {
        $address1 = $_REQUEST['address1'];

        if (isset($_REQUEST['address2']) && (trim($_REQUEST['address2']) != null && trim($_REQUEST['address2']) != "")) {
            $address2 = mysqli_real_escape_string($db, $_REQUEST['address2']);
        } else {
            $address2 = "";
        }
    } else {
        $status['code'] = 2;
        $status['msg'] = "The input did not meet requirements. (Last Name)";
        echo json_encode($status);
        return false;
    }

    if (trim($_REQUEST['country']) != null && trim($_REQUEST['country']) != "") {
        $country = mysqli_real_escape_string($db, $_REQUEST['country']);
    } else {
        $status['code'] = 2;
        $status['msg'] = "The input did not meet requirements. (Country)";
        echo json_encode($status);
        return false;
    }

    if (trim($_REQUEST['client-type']) != null && trim($_REQUEST['client-type']) != "") {
        $client_type = mysqli_real_escape_string($db, $_REQUEST['client-type']);

        if ($client_type == '2') {//organizatiion
            if (isset($_REQUEST['organization-name']) && isset($_REQUEST['organization-type']) && trim($_REQUEST['organization-name']) != null && trim($_REQUEST['organization-name']) != "" &&
      trim($_REQUEST['organization-type']) != null && trim($_REQUEST['organization-type']) != "") {
                $organization_name = mysqli_real_escape_string($db, $_REQUEST['organization-name']);
                $organization_type = mysqli_real_escape_string($db, $_REQUEST['organization-type']);
            } else {
                $status['code'] = 2;
                $status['msg'] = "The input did not meet requirements. (Organization Name-Organization Type)";
                echo json_encode($status);
                return false;
            }
        } else {
            $organization_name = "";
            $organization_type = "";
        }

        ////////////////////
    } else {
        $status['code'] = 2;
        $status['msg'] = "The input did not meet requirements. (Client Type)";
        echo json_encode($status);
        return false;
    }

    if (trim($_REQUEST['user-name']) != null && trim($_REQUEST['user-name']) != "") {
        $user_name = mysqli_real_escape_string($db, $_REQUEST['user-name']);
    } else {
        $status['code'] = 2;
        $status['msg'] = "The input did not meet requirements. (User Name)";
        echo json_encode($status);
        return false;
    }

    if (trim($_REQUEST['password']) != null && trim($_REQUEST['password']) != "") {
        if (trim($_REQUEST['password2']) != null && trim($_REQUEST['password2']) != "" && $_REQUEST['password'] == $_REQUEST['password2']) {
            $password = mysqli_real_escape_string($db, $_REQUEST['password']);
            $hash = password_hash($password, PASSWORD_DEFAULT);
            $salt = generateRandomString(10);
            $confirmation = generateRandomString(5);
            $confirmed = 0;
        } else {
            $status['code'] = 2;
            $status['msg'] = "The input did not meet requirements.";
            echo json_encode($status);
            return false;
        }
    } else {
        $status['code'] = 2;
        $status['msg'] = "The input did not meet requirements.";
        echo json_encode($status);
        return false;
    }





    $stmt= $db->prepare("SELECT user, kts_email  FROM temp_client where user = ? or kts_email = ?"); //Fetching all the records with input credentials
  $stmt->bind_param("ss", $user_name, $email); //You need to specify values to each '?' explicitly while using prepared statements
  $stmt->execute();

    $stmt2= $db2->prepare("SELECT user, kts_email  FROM kts_client where user = ? or kts_email = ?"); //Fetching all the records with input credentials
  $stmt2->bind_param("ss", $user_name, $email); //You need to specify values to each '?' explicitly while using prepared statements
  $stmt2->execute();


    if ($stmt->fetch() == null and $stmt2->fetch() == null) {
        $stmt->close();
        $stmt2->close();
        //User does not exist so add new user to temporary table:
   $stmt3= $db->prepare("INSERT INTO temp_client (user, type, kts_email, pw, salt, join_date, name, fname, lname, address1, address2, country, phone1, phone2, email, business_type, confirmation, confirmed)
   values (?, ?, ?, ?, ?, now(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"); //Fetching all the records with input credentials
     $stmt3->bind_param("sssssssssssssssss", $user_name, $client_type, $email, $hash, $salt, $organization_name, $first_name, $last_name, $address1, $address2, $country, $phone1, $phone2, $email, $organization_type, $confirmation, $confirmed); //You need to specify values to each '?' explicitly while using prepared statements
     $stmt3->execute();
        $stmt3->close();

        $status['code'] = 1;
        $status['msg'] = "Fields were successfully entered.";
        $status['payload'] = "DATA :)";
        echo json_encode($status);

        sendConfirmationEmail($kts_signup_email.$kts_url, $email, 'KTS Insight Confirmation', $confirmation, $first_name);


        return true;
    } else {
        $stmt->close();
        $stmt2->close();
        $status['code'] = 4;
        $status['msg'] = "Username or email already exists";
        echo json_encode($status);
        return false;
    }
} else {
    $status['code'] = 2;
    $status['msg'] = "The required fields were not filled.";

    echo json_encode($status);
    return false;
}


/////////////functions

function generateRandomString($length)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

/////////////////////////////EMAIL MESSAGES/////////////////////////////////////////////////

function sendConfirmationEmail($sender, $receiver, $subject, $code, $name)
{

  // Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    // More headers
    $headers .= 'From: <'.$sender.'>' . "\r\n";
    //$headers .= 'Cc: myboss@example.com' . "\r\n";

    $message = "
  <!DOCTYPE HTML PUBLIC '-//W3C//DTD XHTML 1.0 Transitional //EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml' xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office'><head>
      <!--[if gte mso 9]><xml>
       <o:OfficeDocumentSettings>
        <o:AllowPNG/>
        <o:PixelsPerInch>96</o:PixelsPerInch>
       </o:OfficeDocumentSettings>
      </xml><![endif]-->
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
      <meta name='viewport' content='width=device-width'>
      <!--[if !mso]><!--><meta http-equiv='X-UA-Compatible' content='IE=edge'><!--<![endif]-->
      <title></title>
      <!--[if !mso]><!-- -->
  	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
  	<!--<![endif]-->

      <style type='text/css' id='media-query'>
        body {
    margin: 0;
    padding: 0; }

  table, tr, td {
    vertical-align: top;
    border-collapse: collapse; }

  .ie-browser table, .mso-container table {
    table-layout: fixed; }

  * {
    line-height: inherit; }

  a[x-apple-data-detectors=true] {
    color: inherit !important;
    text-decoration: none !important; }

  [owa] .img-container div, [owa] .img-container button {
    display: block !important; }

  [owa] .fullwidth button {
    width: 100% !important; }

  [owa] .block-grid .col {
    display: table-cell;
    float: none !important;
    vertical-align: top; }

  .ie-browser .num12, .ie-browser .block-grid, [owa] .num12, [owa] .block-grid {
    width: 600px !important; }

  .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
    line-height: 100%; }

  .ie-browser .mixed-two-up .num4, [owa] .mixed-two-up .num4 {
    width: 200px !important; }

  .ie-browser .mixed-two-up .num8, [owa] .mixed-two-up .num8 {
    width: 400px !important; }

  .ie-browser .block-grid.two-up .col, [owa] .block-grid.two-up .col {
    width: 300px !important; }

  .ie-browser .block-grid.three-up .col, [owa] .block-grid.three-up .col {
    width: 200px !important; }

  .ie-browser .block-grid.four-up .col, [owa] .block-grid.four-up .col {
    width: 150px !important; }

  .ie-browser .block-grid.five-up .col, [owa] .block-grid.five-up .col {
    width: 120px !important; }

  .ie-browser .block-grid.six-up .col, [owa] .block-grid.six-up .col {
    width: 100px !important; }

  .ie-browser .block-grid.seven-up .col, [owa] .block-grid.seven-up .col {
    width: 85px !important; }

  .ie-browser .block-grid.eight-up .col, [owa] .block-grid.eight-up .col {
    width: 75px !important; }

  .ie-browser .block-grid.nine-up .col, [owa] .block-grid.nine-up .col {
    width: 66px !important; }

  .ie-browser .block-grid.ten-up .col, [owa] .block-grid.ten-up .col {
    width: 60px !important; }

  .ie-browser .block-grid.eleven-up .col, [owa] .block-grid.eleven-up .col {
    width: 54px !important; }

  .ie-browser .block-grid.twelve-up .col, [owa] .block-grid.twelve-up .col {
    width: 50px !important; }

  @media only screen and (min-width: 620px) {
    .block-grid {
      width: 600px !important; }
    .block-grid .col {
      vertical-align: top; }
      .block-grid .col.num12 {
        width: 600px !important; }
    .block-grid.mixed-two-up .col.num4 {
      width: 200px !important; }
    .block-grid.mixed-two-up .col.num8 {
      width: 400px !important; }
    .block-grid.two-up .col {
      width: 300px !important; }
    .block-grid.three-up .col {
      width: 200px !important; }
    .block-grid.four-up .col {
      width: 150px !important; }
    .block-grid.five-up .col {
      width: 120px !important; }
    .block-grid.six-up .col {
      width: 100px !important; }
    .block-grid.seven-up .col {
      width: 85px !important; }
    .block-grid.eight-up .col {
      width: 75px !important; }
    .block-grid.nine-up .col {
      width: 66px !important; }
    .block-grid.ten-up .col {
      width: 60px !important; }
    .block-grid.eleven-up .col {
      width: 54px !important; }
    .block-grid.twelve-up .col {
      width: 50px !important; } }

  @media (max-width: 620px) {
    .block-grid, .col {
      min-width: 320px !important;
      max-width: 100% !important;
      display: block !important; }
    .block-grid {
      width: calc(100% - 40px) !important; }
    .col {
      width: 100% !important; }
      .col > div {
        margin: 0 auto; }
    img.fullwidth, img.fullwidthOnMobile {
      max-width: 100% !important; }
    .no-stack .col {
      min-width: 0 !important;
      display: table-cell !important; }
    .no-stack.two-up .col {
      width: 50% !important; }
    .no-stack.mixed-two-up .col.num4 {
      width: 33% !important; }
    .no-stack.mixed-two-up .col.num8 {
      width: 66% !important; }
    .no-stack.three-up .col.num4 {
      width: 33% !important; }
    .no-stack.four-up .col.num3 {
      width: 25% !important; } }

      </style>
  </head>
  <body class='clean-body' style='margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #e2eace'>
    <style type='text/css' id='media-query-bodytag'>
      @media (max-width: 520px) {
        .block-grid {
          min-width: 320px!important;
          max-width: 100%!important;
          width: 100%!important;
          display: block!important;
        }

        .col {
          min-width: 320px!important;
          max-width: 100%!important;
          width: 100%!important;
          display: block!important;
        }

          .col > div {
            margin: 0 auto;
          }

        img.fullwidth {
          max-width: 100%!important;
        }
  			img.fullwidthOnMobile {
          max-width: 100%!important;
        }
        .no-stack .col {
  				min-width: 0!important;
  				display: table-cell!important;
  			}
  			.no-stack.two-up .col {
  				width: 50%!important;
  			}
  			.no-stack.mixed-two-up .col.num4 {
  				width: 33%!important;
  			}
  			.no-stack.mixed-two-up .col.num8 {
  				width: 66%!important;
  			}
  			.no-stack.three-up .col.num4 {
  				width: 33%!important
  			}
  			.no-stack.four-up .col.num3 {
  				width: 25%!important
  			}
      }
    </style>
    <!--[if IE]><div class='ie-browser'><![endif]-->
    <!--[if mso]><div class='mso-container'><![endif]-->
    <table class='nl-container' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #e2eace;width: 100%' cellpadding='0' cellspacing='0'>
  	<tbody>
  	<tr style='vertical-align: top'>
  		<td style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
      <!--[if (mso)|(IE)]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td align='center' style='background-color: #e2eace;'><![endif]-->

      <div style='background-color:transparent;'>
        <div style='Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
          <div style='border-collapse: collapse;display: table;width: 100%;background-color:transparent;'>
            <!--[if (mso)|(IE)]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='background-color:transparent;' align='center'><table cellpadding='0' cellspacing='0' border='0' style='width: 600px;'><tr class='layout-full-width' style='background-color:transparent;'><![endif]-->

                <!--[if (mso)|(IE)]><td align='center' width='600' style=' width:600px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;' valign='top'><![endif]-->
              <div class='col num12' style='min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;'>
                <div style='background-color: transparent; width: 100% !important;'>
                <!--[if (!mso)&(!IE)]><!--><div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;'><!--<![endif]-->


                      <div align='center' class='img-container center  autowidth  fullwidth' style='padding-right: 0px;  padding-left: 0px;'>
  <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 0px; padding-left: 0px;' align='center'><![endif]-->
  <div style='line-height:25px;font-size:1px'>&#160;</div>  <img class='center  autowidth  fullwidth' align='center' border='0' src='https://www.kabtontech.com/wp-content/uploads/2018/01/rounder-up.png' alt='Image' title='Image' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 600px' width='600'>
  <!--[if mso]></td></tr></table><![endif]-->
  </div>


                <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                </div>
              </div>
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
          </div>
        </div>
      </div>    <div style='background-color:transparent;'>
        <div style='Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;' class='block-grid '>
          <div style='border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;'>
            <!--[if (mso)|(IE)]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='background-color:transparent;' align='center'><table cellpadding='0' cellspacing='0' border='0' style='width: 600px;'><tr class='layout-full-width' style='background-color:#FFFFFF;'><![endif]-->

                <!--[if (mso)|(IE)]><td align='center' width='600' style=' width:600px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;' valign='top'><![endif]-->
              <div class='col num12' style='min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;'>
                <div style='background-color: transparent; width: 100% !important;'>
                <!--[if (!mso)&(!IE)]><!--><div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'><!--<![endif]-->


                      <div align='center' class='img-container center  autowidth ' style='padding-right: 0px;  padding-left: 0px;'>
  <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 0px; padding-left: 0px;' align='center'><![endif]-->
    <img class='center  autowidth ' align='center' border='0' src='https://www.kabtontech.com/wp-content/uploads/2018/01/kts-logo-dark.png' alt='Kabton Tech Services - Insight' title='Kabton Tech Services - Insight' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 250px' width='250'>
  <!--[if mso]></td></tr></table><![endif]-->
  </div>



                      <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'><![endif]-->
  <div style='font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:150%;color:#555555; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
  	<div style='font-size:12px;line-height:18px;color:#555555;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 21px;text-align: center'>Welcome</p></div>
  </div>
  <!--[if mso]></td></tr></table><![endif]-->


                <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                </div>
              </div>
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
          </div>
        </div>
      </div>    <div style='background-color:transparent;'>
        <div style='Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;' class='block-grid '>
          <div style='border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;'>
            <!--[if (mso)|(IE)]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='background-color:transparent;' align='center'><table cellpadding='0' cellspacing='0' border='0' style='width: 600px;'><tr class='layout-full-width' style='background-color:#FFFFFF;'><![endif]-->

                <!--[if (mso)|(IE)]><td align='center' width='600' style=' width:600px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;' valign='top'><![endif]-->
              <div class='col num12' style='min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;'>
                <div style='background-color: transparent; width: 100% !important;'>
                <!--[if (!mso)&(!IE)]><!--><div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'><!--<![endif]-->


                      <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'><![endif]-->
  <div style='font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:120%;color:#0D0D0D; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
  	<div style='font-size:12px;line-height:14px;color:#0D0D0D;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 17px;text-align: center'><span style='font-size: 28px; line-height: 33px;'><strong><span style='line-height: 33px; font-size: 28px;'>Hello ".$name.",</span></strong></span><br><span style='font-size: 28px; line-height: 33px;'>Registration completed</span></p></div>
  </div>
  <!--[if mso]></td></tr></table><![endif]-->



                      <div align='center' class='img-container center  autowidth ' style='padding-right: 0px;  padding-left: 0px;'>
  <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 0px; padding-left: 0px;' align='center'><![endif]-->
    <img class='center  autowidth ' align='center' border='0' src='https://www.kabtontech.com/wp-content/uploads/2018/01/divider.png' alt='Image' title='Image' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 316px' width='316'>
  <!--[if mso]></td></tr></table><![endif]-->
  </div>



                      <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'><![endif]-->
  <div style='font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:150%;color:#555555; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
  	<div style='font-size:12px;line-height:18px;color:#555555;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 21px;text-align: center'>Thanks so much for joining KTS Insight!<span style='color: rgb(168, 191, 111); font-size: 14px; line-height: 21px;'><strong><br></strong></span></p></div>
  </div>
  <!--[if mso]></td></tr></table><![endif]-->



                      <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 10px;'><![endif]-->
  <div style='font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:150%;color:#0D0D0D; padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 10px;'>
  	<div style='font-size:12px;line-height:18px;color:#0D0D0D;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 21px;text-align: center'>TO FINISH SIGNING UP,&#160; ACTIVATE YOUR ACCOUNT</p></div>
  </div>
  <!--[if mso]></td></tr></table><![endif]-->



<a href='http://insight.kabtontech.com/signup/ktssignup.php?confirmation=".$code."'>
  <div align='center' class='button-container center' style='padding-right: 10px; padding-left: 10px; padding-top:25px; padding-bottom:10px;'>

    <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0' style='border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;'><tr><td style='padding-right: 10px; padding-left: 10px; padding-top:25px; padding-bottom:10px;' align='center'><v:roundrect xmlns:v='urn:schemas-microsoft-com:vml' xmlns:w='urn:schemas-microsoft-com:office:word' href='' style='height:46pt; v-text-anchor:middle; width:172pt;' arcsize='7%' strokecolor='#EEE9B3' fillcolor='#EEE9B3'><w:anchorlock/><v:textbox inset='0,0,0,0'><center style='color:#0D0D0D; font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif; font-size:16px;'><![endif]-->
      <div style='color: #0D0D0D; background-color: #EEE9B3; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; max-width: 230px; width: 200px;width: auto; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent; padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px; font-family: 'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif; text-align: center; mso-border-alt: none;'>
        <span style='font-size:16px;line-height:32px;'>ACTIVATE MY ACCOUNT</span>
      </div>
    <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->

  </div>
  </a>



                      <table border='0' cellpadding='0' cellspacing='0' width='100%' class='divider' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
      <tbody>
          <tr style='vertical-align: top'>
              <td class='divider_inner' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-right: 10px;padding-left: 10px;padding-top: 30px;padding-bottom: 10px;min-width: 100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
                  <table class='divider_content' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 0px solid transparent;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
                      <tbody>
                          <tr style='vertical-align: top'>
                              <td style='word-break: break-word;border-collapse: collapse !important;vertical-align: top;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
                                  <span></span>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </td>
          </tr>
      </tbody>
  </table>


                      <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 10px;'><![endif]-->
  <div style='font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:150%;color:#0D0D0D; padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 10px;'>
  	<div style='font-size:12px;line-height:18px;color:#0D0D0D;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 21px;text-align: center'>OR ENTER CODE BELOW INTO KTS REGISTRATION FORM</p></div>
  </div>
  <!--[if mso]></td></tr></table><![endif]-->



                      <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'><![endif]-->
  <div style='font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:120%;color:#0D0D0D; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
  	<div style='line-height:14px;font-size:12px;color:#0D0D0D;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;'><p style='margin: 0;line-height: 14px;text-align: center;font-size: 12px'><span style='font-size: 28px; line-height: 33px;'><b>".$code."</b></span></p></div>
  </div>
  <!--[if mso]></td></tr></table><![endif]-->


                <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                </div>
              </div>
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
          </div>
        </div>
      </div>    <div style='background-color:transparent;'>
        <div style='Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #525252;' class='block-grid three-up '>
          <div style='border-collapse: collapse;display: table;width: 100%;background-color:#525252;'>
            <!--[if (mso)|(IE)]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='background-color:transparent;' align='center'><table cellpadding='0' cellspacing='0' border='0' style='width: 600px;'><tr class='layout-full-width' style='background-color:#525252;'><![endif]-->

                <!--[if (mso)|(IE)]><td align='center' width='200' style=' width:200px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;' valign='top'><![endif]-->
              <div class='col num4' style='max-width: 320px;min-width: 200px;display: table-cell;vertical-align: top;'>
                <div style='background-color: transparent; width: 100% !important;'>
                <!--[if (!mso)&(!IE)]><!--><div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;'><!--<![endif]-->



  <div align='center' style='padding-right: 0px; padding-left: 0px; padding-bottom: 0px;'>
    <div style='line-height:15px;font-size:1px'>&#160;</div>
    <div style='display: table; max-width:131px;'>
    <!--[if (mso)|(IE)]><table width='131' cellpadding='0' cellspacing='0' border='0'><tr><td style='border-collapse:collapse; padding-right: 0px; padding-left: 0px; padding-bottom: 0px;'  align='center'><table width='100%' cellpadding='0' cellspacing='0' border='0' style='border-collapse:collapse; mso-table-lspace: 0pt;mso-table-rspace: 0pt; width:131px;'><tr><td width='32' style='width:32px; padding-right: 5px;' valign='top'><![endif]-->
      <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px'>
        <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
          <a href='https://www.facebook.com/' title='Facebook' target='_blank'>
            <img src='https://www.kabtontech.com/wp-content/uploads/2018/01/facebook2x.png' alt='Facebook' title='Facebook' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
          </a>
        <div style='line-height:5px;font-size:1px'>&#160;</div>
        </td></tr>
      </tbody></table>
        <!--[if (mso)|(IE)]></td><td width='32' style='width:32px; padding-right: 5px;' valign='top'><![endif]-->
      <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px'>
        <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
          <a href='https://twitter.com/' title='Twitter' target='_blank'>
            <img src='https://www.kabtontech.com/wp-content/uploads/2018/01/twitter2x.png' alt='Twitter' title='Twitter' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
          </a>
        <div style='line-height:5px;font-size:1px'>&#160;</div>
        </td></tr>
      </tbody></table>
        <!--[if (mso)|(IE)]></td><td width='32' style='width:32px; padding-right: 0;' valign='top'><![endif]-->
      <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 0'>
        <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
          <a href='https://plus.google.com/' title='Google+' target='_blank'>
            <img src='https://www.kabtontech.com/wp-content/uploads/2018/01/googleplus2x.png' alt='Google+' title='Google+' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
          </a>
        <div style='line-height:5px;font-size:1px'>&#160;</div>
        </td></tr>
      </tbody></table>
      <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
    </div>
  </div>

                <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                </div>
              </div>
                <!--[if (mso)|(IE)]></td><td align='center' width='200' style=' width:200px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;' valign='top'><![endif]-->
              <div class='col num4' style='max-width: 320px;min-width: 200px;display: table-cell;vertical-align: top;'>
                <div style='background-color: transparent; width: 100% !important;'>
                <!--[if (!mso)&(!IE)]><!--><div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'><!--<![endif]-->


                      <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 0px; padding-left: 0px; padding-top: 20px; padding-bottom: 0px;'><![endif]-->
  <div style='font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:120%;color:#a8bf6f; padding-right: 0px; padding-left: 0px; padding-top: 20px; padding-bottom: 0px;'>
  	<div style='font-size:12px;line-height:14px;color:#a8bf6f;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px;text-align: center'><span style='color: #ffffff; font-size: 12px; line-height: 14px;'><span style='font-size: 12px; line-height: 14px; color: #ffffff;'>:</span> +".$kts_phone1."</span><br></p></div>
  </div>
  <!--[if mso]></td></tr></table><![endif]-->


                <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                </div>
              </div>
                <!--[if (mso)|(IE)]></td><td align='center' width='200' style=' width:200px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;' valign='top'><![endif]-->
              <div class='col num4' style='max-width: 320px;min-width: 200px;display: table-cell;vertical-align: top;'>
                <div style='background-color: transparent; width: 100% !important;'>
                <!--[if (!mso)&(!IE)]><!--><div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'><!--<![endif]-->


                      <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 0px; padding-left: 0px; padding-top: 20px; padding-bottom: 0px;'><![endif]-->
  <div style='font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:120%;color:#a8bf6f; padding-right: 0px; padding-left: 0px; padding-top: 20px; padding-bottom: 0px;'>
  	<div style='font-size:12px;line-height:14px;color: #ffffff;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px;text-align: center'>)<span style='color: #ffffff; font-size: 12px; line-height: 14px;'>".$kts_email2."</span></p></div>
  </div>
  <!--[if mso]></td></tr></table><![endif]-->


                <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                </div>
              </div>
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
          </div>
        </div>
      </div>    <div style='background-color:transparent;'>
        <div style='Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
          <div style='border-collapse: collapse;display: table;width: 100%;background-color:transparent;'>
            <!--[if (mso)|(IE)]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='background-color:transparent;' align='center'><table cellpadding='0' cellspacing='0' border='0' style='width: 600px;'><tr class='layout-full-width' style='background-color:transparent;'><![endif]-->

                <!--[if (mso)|(IE)]><td align='center' width='600' style=' width:600px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;' valign='top'><![endif]-->
              <div class='col num12' style='min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;'>
                <div style='background-color: transparent; width: 100% !important;'>
                <!--[if (!mso)&(!IE)]><!--><div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'><!--<![endif]-->


                      <div align='center' class='img-container center  autowidth  fullwidth' style='padding-right: 0px;  padding-left: 0px;'>
  <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 0px; padding-left: 0px;' align='center'><![endif]-->
    <img class='center  autowidth  fullwidth' align='center' border='0' src='https://www.kabtontech.com/wp-content/uploads/2018/01/rounder-dwn.png' alt='Image' title='Image' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 600px' width='600'>
  <!--[if mso]></td></tr></table><![endif]-->
  </div>



                      <table border='0' cellpadding='0' cellspacing='0' width='100%' class='divider' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
      <tbody>
          <tr style='vertical-align: top'>
              <td class='divider_inner' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-right: 30px;padding-left: 30px;padding-top: 30px;padding-bottom: 30px;min-width: 100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
                  <table class='divider_content' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 0px solid transparent;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
                      <tbody>
                          <tr style='vertical-align: top'>
                              <td style='word-break: break-word;border-collapse: collapse !important;vertical-align: top;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
                                  <span></span>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </td>
          </tr>
      </tbody>
  </table>

                <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                </div>
              </div>
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
          </div>
        </div>
      </div>   <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
  		</td>
    </tr>
    </tbody>
    </table>
    <!--[if (mso)|(IE)]></div><![endif]-->


  </body></html>


  ";

    mail($receiver, $subject, $message, $headers);
}
