<?php
require($_SERVER['DOCUMENT_ROOT'] . "/serve/functions.php");

// if ($_SERVER['REQUEST_METHOD'] == 'POST')
// {
if (empty($_REQUEST['wort']) || empty($_REQUEST['email'])) { //Validating inputs using PHP code
    //header("location: /login");//You will be sent to Login.php for re-login
    header("location: /login"); //You will be sent to Login.php for re-login
} else {
    $wort = mysqli_real_escape_string($db, $_REQUEST['wort']);
    $email = mysqli_real_escape_string($db, $_REQUEST['email']);

    //function recordExists($table, $column, $item, $multiple, $db)
    if (recordExists('kts_client', 'confirm_email', $wort, '0', $db)) {
        if ((isset($_REQUEST['email'])) && (!empty($_REQUEST['email'])) && preg_match($email_pattern, $email)) {

                // updateTable($id, $id_column, $table, $column, $value, $multiple, $db)
            if (updateTable($wort, 'confirm_email', 'kts_client', 'kts_email', $email, 0, $db) ['code'] == 1) {
                header("location: /login/?email_confirmed"); //You will be sent to Login.php for re-login
            }
        } else {
            header("location: /reset/email/naricht?incorrect_format&wort=$wort"); //You will be sent to Login.php for re-login
        }
    }
}

//}
// else{
//   //header("location: /login");//You will be sent to Login.php for re-login
//   echo "hy";
//
// }
