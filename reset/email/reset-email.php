<?php
// require_once ($_SERVER['DOCUMENT_ROOT'] . "/serve/functions.php");
require($_SERVER['DOCUMENT_ROOT'] . "/serve/harbinger.php");

//session_start(); //always start a session in the beginning
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (empty($_REQUEST['email'])) {

        header("location: /reset/email/?empty_email"); //You will be sent to Login.php for re-login
    } else {
        $email = mysqli_real_escape_string($db, $_REQUEST['email']);
        //function recordExists($table, $column, $item, $multiple, $db)
        if (recordExists('kts_client', 'kts_email', $email, '0', $db)) {

            // updateTable($id, $id_column, $table, $column, $value, $multiple, $db)
            $code = generateRandomString(7);
            $link = 'http://insight.kabtontech.com/reset/email/naricht?wort=' . $code;
            updateTable($email, 'kts_email', 'kts_client', 'confirm_email', $code, 1, $db);

            //sendEmail($sender, $receiver, $subject, $sub_subject, $code, $name)
            @sendEmail(
                $kts_normal_email . $kts_url,
                $email,
                'Email Reset',
                'Confirm Email Change',
            confirm_email($link, $kts_company, $kts_website),
                'Insight User'
            );
            header("location: /reset/email/?confirm");
        } else {
            header("location: /reset/email/?empty_email"); //You will be sent to Login.php for re-login
        }
    }
} else {
    header("location: /email/reset");
}
