<?php
// require_once ($_SERVER['DOCUMENT_ROOT'] . "/serve/functions.php");
require($_SERVER['DOCUMENT_ROOT'] . "/serve/harbinger.php");

//session_start(); //always start a session in the beginning
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (empty($_REQUEST['email'])) { //Validating inputs using PHP code
        echo "Incorrect username or password";
        header("location: ../reset/?empty_email"); //You will be sent to Login.php for re-login
    } else {
        $email = mysqli_real_escape_string($db, $_REQUEST['email']);
        //function recordExists($table, $column, $item, $multiple, $db)
        if (recordExists('kts_client', 'kts_email', $email, '0', $db)) {

            // updateTable($id, $id_column, $table, $column, $value, $multiple, $db)
            $code = generateRandomString(7);
            $link = 'http://insight.kabtontech.com/reset/password?wort=' . $code;
            updateTable($email, 'kts_email', 'kts_client', 'confirm_pass', $code, 1, $db);

            //sendEmail($sender, $receiver, $subject, $sub_subject, $code, $name)
            @sendEmail(
                $kts_normal_email . $kts_url,
                $email,
                'Password Reset',
                'Here are instructions for password reset',
            confirm_pass($link, $kts_company, $kts_website),
                'Insight User'
            );
            header("location: /reset/?confirm");
        } else {
            header("location: /reset/?empty_email"); //You will be sent to Login.php for re-login
        }
    }
} else {
    header("location: /reset");
}
