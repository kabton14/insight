<?php
require($_SERVER['DOCUMENT_ROOT'] . "/serve/functions.php");

// if ($_SERVER['REQUEST_METHOD'] == 'POST')
// {
if (empty($_REQUEST['wort']) || !isset($_REQUEST['wort'])) { //Validating inputs using PHP code
    header("location: /login"); //You will be sent to Login.php for re-login
} else {
    $wort = mysqli_real_escape_string($db, $_REQUEST['wort']);

    //function recordExists($table, $column, $item, $multiple, $db)
    if (!recordExists('kts_client', 'confirm_pass', $wort, '0', $db)) {
        header("location: /login");
    }
}

// }
// else{
//   //header("location: /login");//You will be sent to Login.php for re-login
//   echo "stringpost";
//
// }



?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Insight| Kabton Tech Services | </title>

    <!-- Bootstrap -->
    <link href="../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../../vendors/animate.css/animate.min.css" rel="stylesheet">



    <!-- Custom Theme Style -->
    <link href="../../build/css/custom.min.css" rel="stylesheet">
    <link href="../../build/css/snackbar.css" rel="stylesheet">
  </head>

  <body class="login">
  <!-- The actual snackbar -->

    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form id="confirm-pass-form" action="confirm/index.php" method="POST">
              <h1>Reset Password</h1>
              <div class="alert alert-info" style="text-align: left;">
                <strong>Note</strong><br>
                Password must contain at least:<br>
                <ul>
                <li>1 uppercase letter</li>
                <li>1 lowercase letter</li>
                <li>1 numeral</li>
                <li>1 special character</li>
              </ul>
              </div>
              <div>
                <input type="password" class="form-control" name="pass" placeholder="Enter New Password" required="" />
              </div>
              <div>
                <input type="password" class="form-control" name="pass2" placeholder="Enter Password Again" required="" />
              </div>
              <div>
                <input type="hidden" class="form-control" name="wort" value="<?php echo $wort; ?>" required="" />
              </div>
              <!--div>
                <input type="password" class="form-control" name="password" placeholder="Password" required="" />
              </div-->
              <div>
                <a class="btn btn-default submit" onclick="document.getElementById('confirm-pass-form').submit()">Go</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">
                  <a href="../../login" class="to_register"> Back to login </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><span><img src="../../images/logoicon.png"/></span> Kabton Tech Services Insight</h1>
                  <p>©<?php echo date("Y"); ?> All Rights Reserved. Kabton Technology Services</p>
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form>
              <h1>Create Account</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="email" class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <a class="btn btn-default submit" href="index.html">Submit</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Already a member ?
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1> Kabton Tech Services Insight </h1>
                  <p>©<?php echo date("Y"); ?> All Rights Reserved. Kabton Technology Services</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>

              <div id="snackbar">Please enter the email associated with your account.</div>
              <div id="snackbar2">There was a problem with the password format. Please make sure your password satisfies the conditions highlighted above.</div>
              <div id="snackbar3">Passwords do not match.</div>
    </div>
    <?php
if (isset($_REQUEST['empty_email'])) {
    echo "
<script type='text/javascript'>


function myActivation() {
    // Get the snackbar DIV
    var x = document.getElementById('snackbar');

    // Add the 'show' class to DIV
    x.className = 'show';

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace('show', ''); }, 30000);
}

document.onload = myActivation();




</script>
";
}

if (isset($_REQUEST['incorrect_format'])) {
    echo "

<script>
function myActivation2() {
    // Get the snackbar DIV
    var x = document.getElementById('snackbar2');

    // Add the 'show' class to DIV
    x.className = 'show';

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace('show', ''); }, 30000);
}


document.onload = myActivation2();

</script>

";
}

if (isset($_REQUEST['p_match'])) {
    echo "

<script>
function myActivation3() {
    // Get the snackbar DIV
    var x = document.getElementById('snackbar3');

    // Add the 'show' class to DIV
    x.className = 'show';

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace('show', ''); }, 30000);
}


document.onload = myActivation3();

</script>

";
}
?>

  </body>
</html>
