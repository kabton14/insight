<?php
require($_SERVER['DOCUMENT_ROOT'] . "/serve/functions.php");

// if ($_SERVER['REQUEST_METHOD'] == 'POST')
// {
if (empty($_REQUEST['wort'])) { //Validating inputs using PHP code
    //header("location: /login");//You will be sent to Login.php for re-login
    header("location: /reset/password/?incorrect_format"); //You will be sent to Login.php for re-login
} else {
    $wort = mysqli_real_escape_string($db, $_REQUEST['wort']);

    //function recordExists($table, $column, $item, $multiple, $db)
    if (recordExists('kts_client', 'confirm_pass', $wort, '0', $db)) {
        if ((isset($_REQUEST['pass']) && isset($_REQUEST['pass2'])) && (!empty($_REQUEST['pass']) && !empty($_REQUEST['pass2']))) {
            $pass = mysqli_real_escape_string($db, $_REQUEST['pass']);
            $pass2 = mysqli_real_escape_string($db, $_REQUEST['pass2']);

            if ($pass != $pass2) {
                header("location: /reset/password/?p_match&wort=$wort"); //You will be sent to Login.php for re-login
            }

            if (($pass == $pass2) && preg_match($password_pattern, $pass)) {

                // updateTable($id, $id_column, $table, $column, $value, $multiple, $db)
                $hash = password_hash($pass, PASSWORD_DEFAULT);
                if (updateTable($wort, 'confirm_pass', 'kts_client', 'pw', $hash, 0, $db) ['code'] == 1) {
                    header("location: /login/?pw_reset_confirmed"); //You will be sent to Login.php for re-login
                }
            } else {
                header("location: /reset/password/?incorrect_format&wort=$wort"); //You will be sent to Login.php for re-login
            }
        } else {
            header("location: /reset/password/?incorrect_format&wort=$wort"); //You will be sent to Login.php for re-login
        }
    }
}

//}
// else{
//   //header("location: /login");//You will be sent to Login.php for re-login
//   echo "hy";
//
// }
