<?php
session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

//require ("/../misc/dbconnection.php");
//require ("/../misc/variables.php");
require($_SERVER['DOCUMENT_ROOT'] . "/misc/dbconnection.php");
require($_SERVER['DOCUMENT_ROOT'] . "/misc/variables.php");

/*FUNCTION TO GENERATE INVOICE #:*/
/*function generateInvoiceNumber($nth){



}*/
/*EO FUNCTION TO GENERATE INVOICE #:*/
function genResult($code, $msg, $payload)
{
    $status['code'] = $code;
    $status['msg'] = $msg;
    $status['payload'] = $payload;

    return $status;
}

/*FUNCTION TO CREATE FOLDERS FOR CLIENT OR EMPLOYEE:*/
function createEmployeePath($dirpath, $dirpath2, $mode)
{
    return is_dir($dirpath) && is_dir($dirpath2) || mkdir($dirpath, $mode, true) && mkdir($dirpath2, $mode, true);
}

function createClientPath($dirpath, $dirpath2, $dirpath3, $mode)
{
    return is_dir($dirpath) && is_dir($dirpath2) && is_dir($dirpath3) || mkdir($dirpath, $mode, true) && mkdir($dirpath2, $mode, true) && mkdir($dirpath3, $mode, true);
}

function createProjectPath($dirpath, $dirpath2, $mode)
{
    return is_dir($dirpath) && is_dir($dirpath2) || mkdir($dirpath, $mode, true) && mkdir($dirpath2, $mode, true);
}
/*EO FUNCTION TO CREATE FOLDERS FOR CLIENT OR EMPLOYEE:*/

/*FUNCTION TO LIST FILES IN CLIENTS PROJECT:*/
function listFiles($path)
{
    $files = scandir($path);
    $result = array();
    //foreach (array_filter(glob($path.'*'), 'is_file') as $file)
    $count = 0;
    foreach ($files as $value) {
        if ($value === '.' || $value === '..') {
            continue;
        }
        if (!is_file($path . $value)) {
            continue;
        }
        //echo $value." --->".pathinfo($value, PATHINFO_EXTENSION)."<br/>";
        $arr['url'] = $path . $value;
        $arr['name'] = $value;
        $ext = pathinfo($value, PATHINFO_EXTENSION);
        $arr['icon'] = getIcon($ext);
        $arr['type'] = pathinfo($value, PATHINFO_EXTENSION);

        //echo $arr['url']."___ ".$arr['name']."___ ".$arr['type']."___".$arr['icon']."<br/>";
        $result[$count] = $arr;

        $count++;
    }

    if ($files == null || $files == false) {
        $result = false;
        return $result;
    } else {
        return $result;
    }
}
/*EO FUNCTION TO LIST FILES IN CLIENTS PROJECT*/

function calcTimeGone($then, $now)
{
    $then = new DateTime($then);

    $now = new DateTime();

    $sinceThen = $then->diff($now);

    //Combined
    // echo $sinceThen->y.' years have passed.<br>';
    // echo $sinceThen->m.' months have passed.<br>';
    // echo $sinceThen->format('%a').' days have passed.<br>';
    // echo $sinceThen->h.' hours have passed.<br>';
    // echo $sinceThen->i.' minutes have passed.<br>';
    if ($sinceThen->format('%a') > '365') {
        if ($sinceThen->y > '1') {
            return genResult('1', 'Success', $sinceThen->y . ' yrs ago');
        } else {
            return genResult('1', 'Success', $sinceThen->y . ' yr ago');
        }
    } elseif ($sinceThen->format('%a') >= '1') {
        if ($sinceThen->format('%a') > '1') {
            return genResult('1', 'Success', $sinceThen->format('%a') . ' days ago');
        } else {
            return genResult('1', 'Success', $sinceThen->format('%a') . ' day ago');
        }
    } elseif ($sinceThen->format('%a') < '1' && $sinceThen->h > '0') {
        if ($sinceThen->h > '1') {
            return genResult('1', 'Success', $sinceThen->h . ' hrs ago');
        } else {
            return genResult('1', 'Success', $sinceThen->h . ' hr ago');
        }
    } else {
        if ($sinceThen->i > '1') {
            return genResult('1', 'Success', $sinceThen->i . ' mins ago');
        } else {
            return genResult('1', 'Success', $sinceThen->i . ' min ago');
        }
    }
}

/*FUNCTION TO RETURN FA ICON FOR FILE TYPE:*/
function getIcon($type)
{
    if ($type == 'url') {
        return 'fa fa-external-link';
    }
    if ($type == 'doc' || $type == 'docx') {
        return 'fa fa-file-word-o';
    }
    if ($type == 'odt' || $type == 'rtf' || $type == 'tex' || $type == 'txt' || $type == 'wpd') {
        return 'fa fa-file-text';
    }
    if ($type == 'xlr' || $type == 'xls' || $type == 'xlsx' || $type == 'ods') {
        return 'fa fa-file-excel-o';
    }
    if ($type == 'pdf') {
        return 'fa fa-file-pdf-o';
    }
    if ($type == 'pps' || $type == 'ppt' || $type == 'pptx') {
        return 'fa fa-file-powerpoint-o';
    }
    if ($type == 'ai' || $type == 'bmp' || $type == 'ico' || $type == 'jpg' || $type == 'jpeg' || $type == 'png' || $type == 'ps' || $type == 'psd' || $type == 'svg' || $type == 'tif' || $type == 'tiff') {
        return 'fa fa-picture-o';
    }
    if ($type == 'aif' || $type == 'cda' || $type == 'mid' || $type == 'midi' || $type == 'mp3' || $type == 'mpa' || $type == 'ogg' || $type == 'wav' || $type == 'wma' || $type == 'wpl' || $type == 'flac') {
        return 'fa fa-file-audio-o';
    }
    if ($type == '7z' || $type == 'arj' || $type == 'deb' || $type == 'pkg' || $type == 'rar' || $type == 'rpm' || $type == 'tar.gz' || $type == 'tar' || $type == 'gz' || $type == 'z' || $type == 'zip') {
        return 'fa fa-file-archive-o';
    }
    if ($type == '3g2' || $type == '3gp' || $type == 'avi' || $type == 'flv' || $type == 'h264' || $type == 'm4v' || $type == 'mkv' || $type == 'mov' || $type == 'mp4' || $type == 'mpg' || $type == 'mpeg' || $type == 'rm' || $type == 'swf' || $type == 'wmv' || $type == 'vob') {
        return 'fa fa-file-video-o';
    }
    if ($type == 'c' || $type == 'class' || $type == 'cpp' || $type == 'cs' || $type == 'h' || $type == 'java' || $type == 'sh' || $type == 'swift' || $type == 'vb' || $type == 'py' || $type == 'html' || $type == 'js' || $type == 'css' || $type == 'css3' || $type == 'php') {
        return 'fa fa-file-code-o';
    }
    if ($type == 'csv' || $type == 'dat' || $type == 'db' || $type == 'dbf' || $type == 'log' || $type == 'mdb' || $type == 'sav' || $type == 'sql' || $type == 'tar' || $type == 'xml') {
        return 'fa fa-database';
    } else {
        return 'fa fa-file';
    }
}
/*EO FUNCTION TO RETURN FA ICON FOR FILE TYPE*/

function ktsEncode($s)
{
    $or = base64_encode($s);
    return $or;
}

function ktsDecode($s)
{
    $ro = base64_decode($s);
    return $ro;
}

/*FUNCTION TO RETRIEVE CURRENT PROJECTS FOR A CLIENT:*/
function getCurrentProjects($cid, $option, $db)
{
    $query = '';

    if ($option == '1') {
        $query = "SELECT cpid, cid, project_name, project_type, project_leader, start_date, projected_end, progress, status FROM current_project where cid=? AND status <> 'Completed'";
    } elseif ($option == '2') { //employee
        $query = "SELECT cpid, cid, project_name, project_type, project_leader, start_date, projected_end, progress, status FROM current_project where project_leader=? AND status <> 'Completed'";
    }elseif ($option == '3') { //employee past
        $query = "SELECT cpid, cid, project_name, project_type, project_leader, start_date, projected_end, progress, status FROM current_project where project_leader=? AND status = 'Completed'";
    } else {
        return genResult('2', 'Unknown operation', null);
    }

    $stmt = $db->prepare($query); //Fetching all the records with input credentials
    $stmt->bind_param("s", $cid); //You need to specify values to each '?' explicitly while using prepared statements
    $stmt->execute();
    $stmt->bind_result($cpid, $cid, $project_name, $project_type, $project_leader, $start_date, $projected_end, $progress, $status); // Binding i.e. mapping database results to new variables
    $result = array();
    $count = 0;

    while ($row = $stmt->fetch()) {
        $arr['cpid'] = $cpid;
        $arr['cid'] = $cid;
        $arr['project_name'] = $project_name;
        $arr['project_type'] = $project_type;
        $arr['project_leader'] = $project_leader;
        $arr['start_date'] = $start_date;
        $arr['projected_end'] = $projected_end;
        $arr['progress'] = $progress;
        $arr['status'] = $status;

        $result[$count] = $arr;
        $count++;
    }

    $stmt->close();

    return genResult('1', 'Success', $result);
}
/*EO FUNCTION TO RETRIEVE CURRENT PROJECTS FOR A CLIENT:*/

/*FUNCTION TO RETRIEVE CURRENT PROJECT *DETAILS* FOR A CLIENT:*/
function getCurrentProjectDetails($cpid, $db)
{
    $stmt = $db->prepare("SELECT cid, DATEDIFF (projected_end, CURDATE()  ) as days_remaining, project_name, project_type, project_leader, description, start_date, projected_end, progress, status, time_logged, deposit, pymnt_due, fname, lname FROM current_project LEFT JOIN employee ON current_project .project_leader = employee.eid where cpid=? LIMIT 1"); //Fetching all the records with input credentials
    $stmt->bind_param("s", $cpid); //You need to specify values to each '?' explicitly while using prepared statements
    if ($stmt->execute()) {
        $stmt->bind_result($cpcid, $days_remaining, $project_name, $project_type, $project_leader, $description, $start_date, $projected_end, $progress, $status, $hours, $down_payment, $pymnt_due, $pl_fname, $pl_lname); // Binding i.e. mapping database results to new variables
        $result = array();
        $count = 0;

        while ($row = $stmt->fetch()) {
            $arr['cpid'] = $cpid;
            $arr['cid'] = $cpcid;
            $arr['days_remaining'] = ($days_remaining < 0) ? '0' : $days_remaining;
            $arr['project_name'] = $project_name;
            $arr['project_type'] = $project_type;
            $arr['project_leader'] = $project_leader;
            $arr['description'] = $description;
            $arr['start_date'] = $start_date;
            $arr['projected_end'] = $projected_end;
            $arr['progress'] = $progress;
            $arr['status'] = $status;
            $arr['pl_fname'] = $pl_fname;
            $arr['pl_lname'] = $pl_lname;
            $arr['hours'] = $hours;
            $arr['deposit'] = $down_payment;
            $arr['pymnt_due'] = $pymnt_due;

            $result[$count] = $arr;
            $count++;
        }

        $stmt->close();
        return genResult('1', 'Success', $result);
    } else {
        return genResult('3', 'Could not retreive project items', null);
    }
}
/*EO FUNCTION TO RETRIEVE CURRENT PROJECT  *DETAILS* FOR A CLIENT:*/

function createProject($cid, $project_name, $project_type, $project_leader, $project_start_date, $project_end_date, $project_time_logged, $project_progress, $project_status, $project_description, $project_deposit, $db)
{
    $stmt = $db->prepare("INSERT INTO current_project (cid, project_name, project_type, project_leader, start_date, projected_end, time_logged, progress, status, description, deposit)
  VALUES (?,?,?,?,?,?,?,?,?,?,?) "); //Fetching all the records with input credentials
    $stmt->bind_param("sssssssssss", $cid, $project_name, $project_type, $project_leader, $project_start_date, $project_end_date, $project_time_logged, $project_progress, $project_status, $project_description, $project_deposit); //You need to specify values to each '?' explicitly while using prepared statements
    if ($stmt->execute()) {
        return genResult('1', 'Success', null);
    } else {
        return genResult('3', 'Project was not submitted.', null);
    }
}

function createProjectAssignment($cpid, $eid, $db)
{
    $stmt = $db->prepare("INSERT INTO project_assignment (pid, eid, assignment_date)
  VALUES (?,?,now()) "); //Fetching all the records with input credentials
    $stmt->bind_param("ss", $cpid, $eid); //You need to specify values to each '?' explicitly while using prepared statements
    if ($stmt->execute()) {
        return genResult('1', 'Success', null);
    } else {
        return genResult('3', 'Assignment was not submitted.', null);
    }
}

/*FUNCTION TO RETRIEVE CURRENT PROJECT UPDATES FOR A CLIENT:*/
function getProjectUpdates($cpid, $limit, $db)
{
    $stmt = $db->prepare("SELECT puid, date_added, EXTRACT(minute FROM date_added) as min_added, EXTRACT(hour FROM date_added) as hour_added, EXTRACT(day FROM date_added) as day_added, EXTRACT(month FROM date_added) as month_added, EXTRACT(year FROM date_added) as year_added, update_content, project_update.eid, fname, lname, avatar, update_pic FROM project_update
LEFT JOIN employee ON project_update.eid = employee.eid
WHERE cpid = ?  ORDER BY date_added  DESC LIMIT ?"); //Fetching all the records with input credentials
    $stmt->bind_param("ss", $cpid, $limit); //You need to specify values to each '?' explicitly while using prepared statements
    if ($stmt->execute()) {
        $stmt->bind_result($puid, $date_added, $min_added, $hour_added, $day_added, $month_added, $year_added, $update_content, $eid, $fname, $lname, $avatar, $update_pic); // Binding i.e. mapping database results to new variables
        $result = array();
        $count = 0;

        while ($row = $stmt->fetch()) {
            $arr['puid'] = $puid;
            $arr['day_added'] = $day_added;
            $arr['month_added'] = getMonth($month_added);
            $arr['year_added'] = $year_added;
            $arr['update_content'] = $update_content;
            $arr['eid'] = $eid;
            $arr['fname'] = $fname;
            $arr['lname'] = $lname;
            $arr['avatar'] = $avatar;
            $arr['update_pic'] = $update_pic;

            $result[$count] = $arr;
            $count++;
        }
        return genResult('1', 'Success', $result);
    } else {
        return genResult('3', 'Operation failed.', $result);
    }
}

function createProjectUpdate($cpid, $eid, $content, $db)
{
    $stmt = $db->prepare("INSERT INTO project_update (cpid, date_added, update_content, eid) values (?,now(),?, ?)"); //Fetching all the records with input credentials
    $stmt->bind_param("sss", $cpid, $content, $eid); //You need to specify values to each '?' explicitly while using prepared statements
    if ($stmt->execute()) {
        $stmt->close();
        return genResult('1', 'Success', null);
    } else {
        $stmt->close();
        return genResult('3', 'Operation was not successful', null);
    }
}

function deleteProjectUpdate($puid, $eid, $db)
{
    $stmt = $db->prepare("DELETE FROM project_update WHERE puid=? AND eid=? LIMIT 1 "); //Fetching all the records with input credentials
    $stmt->bind_param("ss", $puid, $eid); //You need to specify values to each '?' explicitly while using prepared statements
    if ($stmt->execute()) {
        $stmt->close();
        return genResult('1', 'Success', null);
    } else {
        $stmt->close();
        return genResult('3', 'Operation was not successful', null);
    }
}
/*EO FUNCTION TO RETRIEVE CURRENT PROJECT UPDATES FOR A CLIENT*/

/*FUNCTION TO RETRIEVE EMPLOYEES ASSIGNED TO A PROJECT:*/
function getEmployeeAssignment($cpid, $limit, $db)
{
    $stmt = $db->prepare("SELECT eid FROM project_assignment WHERE pid = ? ORDER BY eid ASC LIMIT ?"); //Fetching all the records with input credentials
    $stmt->bind_param("ss", $cpid, $limit); //You need to specify values to each '?' explicitly while using prepared statements
    if ($stmt->execute()) {
        $stmt->bind_result($eid); // Binding i.e. mapping database results to new variables
        $result = array();
        $count = 0;

        while ($row = $stmt->fetch()) {
            $arr['eid'] = $eid;

            $result[$count] = $arr;
            $count++;
        }
        $stmt->close();
        return genResult('1', 'Success', $result);
    } else {
        $stmt->close();
        return genResult('3', 'Operation was not successful', null);
    }
}
/*EO FUNCTION TO RETRIEVE EMPLOYEES ASSIGNED TO A PROJECT*/

function KTSLeap($num)
{
    $invoice = str_split($num);
    $count = count($invoice) - 1;
    $balan = 0;

    while ($invoice[$count] == '9' || $invoice[$count] == 'z' || $invoice[$count] == 'Z') {
        if ($count == 0) {
            echo "There are no more codes to be generated.";
        }

        if ($invoice[(count($invoice) - 1) - $balan] == 'z') {
            $invoice[(count($invoice) - 1) - $balan] = 'a';
        }

        if ($invoice[(count($invoice) - 1) - $balan] == 'Z') {
            $invoice[(count($invoice) - 1) - $balan] = 'A';
        }

        if ($invoice[(count($invoice) - 1) - $balan] == '9') {
            $invoice[(count($invoice) - 1) - $balan] = '0';
        }

        $count--;
        $balan++;
    }

    if ((ord($invoice[$count]) >= ord('a') and ord($invoice[$count]) < ord('z'))) {
        $invoice[$count] = chr(ord($invoice[$count]) + 1);
    }

    if ((ord($invoice[$count]) >= ord('A') and ord($invoice[$count]) < ord('Z'))) {
        $invoice[$count] = chr(ord($invoice[$count]) + 1);
    }

    if ((ord($invoice[$count]) >= ord('0') and ord($invoice[$count]) < ord('9'))) {
        $invoice[$count] = chr(ord($invoice[$count]) + 1);
    }
    $result = implode('', $invoice);

    return genResult(1, 'Success', $result);
}

function getInvoiceCode($n, $start_code)
{
    $count = 0;
    $invoice = $start_code;
    while ($count < $n) {
        $invoice = KTSLeap($invoice) ['payload'];
        $count++;
    }

    return genResult(1, 'Success', $invoice);
}

/*FUNCTION TO RETRIEVE CLIENT INVOICE:*/
function getInvoice($cid, $type, $db, $db2)
{
    if ($type == '1') { //Fetching all the billable records that have NOT been paid (for client)
        $stmt = $db->prepare("SELECT pid, A.cid, A.type, name, amount, currency, date_added, paid, invoice_number FROM

(SELECT distinct pid,invoice_item.cid, type, sum((amount * qty) + ((amount * qty) * (tax/100)) + shipping) as amount, MAX(date_added) as date_added, paid, invoice_number FROM invoice_item
                         LEFT JOIN kts_client ON invoice_item.cid = kts_client.cid WHERE invoice_item.cid = ? and paid = 0 group by pid, cid, invoice_number, date_added )
                         AS A

                         LEFT JOIN

(SELECT cid, lname, fname, name, currency FROM organization

union

SELECT cid, lname, fname, concat(fname,' ', lname) as name, currency from person)

AS B

ON A.cid = B.cid");
    } elseif ($type == '2') { //Fetching all the billable records that have NOT been paid (for admin)
        $stmt = $db->prepare("SELECT pid, A.cid, A.type, name, amount, currency, date_added, paid, invoice_number FROM

(SELECT distinct pid,invoice_item.cid, type, sum((amount * qty) + ((amount * qty) * (tax/100)) + shipping) as amount, MAX(date_added) as date_added, paid, invoice_number FROM invoice_item
                         LEFT JOIN kts_client ON invoice_item.cid = kts_client.cid WHERE paid = 0 group by pid, cid, invoice_number, date_added )
                         AS A

                         LEFT JOIN

(SELECT cid, lname, fname, name, currency FROM organization

union

SELECT cid, lname, fname, concat(fname,' ', lname) as name, currency from person)

AS B

ON A.cid = B.cid");
    }
    elseif ($type == '2a') { //Fetching all the billable records that have been paid (for admin)
        $stmt = $db->prepare("SELECT pid, A.cid, A.type, name, amount, currency, date_added, paid, invoice_number FROM

(SELECT distinct pid,invoice_item.cid, type, sum((amount * qty) + ((amount * qty) * (tax/100)) + shipping) as amount, MAX(date_added) as date_added, paid, invoice_number FROM invoice_item
                         LEFT JOIN kts_client ON invoice_item.cid = kts_client.cid WHERE paid = 1 group by pid, cid, invoice_number, date_added )
                         AS A

                         LEFT JOIN

(SELECT cid, lname, fname, name, currency FROM organization

union

SELECT cid, lname, fname, concat(fname,' ', lname) as name, currency from person)

AS B

ON A.cid = B.cid");
    }
     elseif ($type == '3') { //Fetching all the billable records that have NOT been paid (based on project id)
        $stmt = $db->prepare("SELECT pid, A.cid, A.type, name, amount, currency, date_added, paid, invoice_number FROM

(SELECT distinct pid,invoice_item.cid, type, sum((amount * qty) + ((amount * qty) * (tax/100)) + shipping) as amount, MAX(date_added) as date_added, paid, invoice_number FROM invoice_item
                         LEFT JOIN kts_client ON invoice_item.cid = kts_client.cid WHERE pid = ? and paid = 0 group by pid, cid, invoice_number, date_added )
                         AS A

                         LEFT JOIN

(SELECT cid, lname, fname, name, currency FROM organization

union

SELECT cid, lname, fname, concat(fname,' ', lname) as name, currency from person)

AS B

ON A.cid = B.cid"); //Fetching all the records that have been paid (receipts -- based on project id)
    }
    elseif ($type == '3a') {
       $stmt = $db->prepare("SELECT pid, A.cid, A.type, name, amount, currency, date_added, paid, invoice_number FROM

(SELECT distinct pid,invoice_item.cid, type, sum((amount * qty) + ((amount * qty) * (tax/100)) + shipping) as amount, MAX(date_added) as date_added, paid, invoice_number FROM invoice_item
                        LEFT JOIN kts_client ON invoice_item.cid = kts_client.cid WHERE pid = ? and paid = 1 group by pid, cid, invoice_number, date_added )
                        AS A

                        LEFT JOIN

(SELECT cid, lname, fname, name, currency FROM organization

union

SELECT cid, lname, fname, concat(fname,' ', lname) as name, currency from person)

AS B

ON A.cid = B.cid");
   }
   elseif ($type == '4') { //Fetching all the records that are estimates (proforma) for client (cid required)
      $stmt = $db->prepare("SELECT pid, A.cid, A.type, name, amount, currency, date_added, paid, invoice_number FROM

(SELECT distinct pid,invoice_item.cid, type, sum((amount * qty) + ((amount * qty) * (tax/100)) + shipping) as amount, MAX(date_added) as date_added, paid, invoice_number FROM invoice_item
                       LEFT JOIN kts_client ON invoice_item.cid = kts_client.cid WHERE invoice_item.cid = ? and estimate = 1 group by pid, cid, invoice_number, date_added )
                       AS A

                       LEFT JOIN

(SELECT cid, lname, fname, name, currency FROM organization

union

SELECT cid, lname, fname, concat(fname,' ', lname) as name, currency from person)

AS B

ON A.cid = B.cid");
  }
  elseif ($type == '4a') { //Fetching all the records that are estimates (proforma) for client (pid required)
     $stmt = $db->prepare("SELECT pid, A.cid, A.type, name, amount, currency, date_added, paid, invoice_number FROM

(SELECT distinct pid,invoice_item.cid, type, sum((amount * qty) + ((amount * qty) * (tax/100)) + shipping) as amount, MAX(date_added) as date_added, paid, invoice_number FROM invoice_item
                      LEFT JOIN kts_client ON invoice_item.cid = kts_client.cid WHERE pid = ? AND estimate = 1 AND paid = 0 group by pid, cid, invoice_number, date_added )
                      AS A

                      LEFT JOIN

(SELECT cid, lname, fname, name, currency FROM organization

union

SELECT cid, lname, fname, concat(fname,' ', lname) as name, currency from person)

AS B

ON A.cid = B.cid");
 }
  elseif ($type == '4b') { //Fetching all the records that are estimates (proforma) for admin
     $stmt = $db->prepare("SELECT pid, A.cid, A.type, name, amount, currency, date_added, paid, invoice_number FROM

(SELECT distinct pid,invoice_item.cid, type, sum((amount * qty) + ((amount * qty) * (tax/100)) + shipping) as amount, MAX(date_added) as date_added, paid, invoice_number FROM invoice_item
                      LEFT JOIN kts_client ON invoice_item.cid = kts_client.cid WHERE estimate = 1 AND paid = 0 group by pid, cid, invoice_number, date_added )
                      AS A

                      LEFT JOIN

(SELECT cid, lname, fname, name, currency FROM organization

union

SELECT cid, lname, fname, concat(fname,' ', lname) as name, currency from person)

AS B

ON A.cid = B.cid");
 }

    @$stmt->bind_param("s", $cid); //You need to specify values to each '?' explicitly while using prepared statements
    $stmt->execute();
    $stmt->bind_result($pid, $client, $ctype, $name, $amount, $currency, $date, $paid, $invoice); // Binding i.e. mapping database results to new variables
    //$stmt->close();


    $result = array();
    $count = 0;

    while ($row = $stmt->fetch()) {
        $arr['name'] = $name;
        $arr['pid'] = ktsEncode($pid);
        $arr['client'] = $client;
        $arr['type'] = $ctype;
        $arr['fakeid'] = generateRandomString(5) . $pid;
        $arr['amount'] = $amount;
        $arr['currency'] = $currency;
        $arr['date'] = $date;
        if ($paid == '1') {
            $arr['paid'] = "Paid";
        } else {
            $arr['paid'] = "Not Paid";
        }
        $arr['invoice'] = getInvoiceCode($pid, 'KTS000AA00') ['payload'];//old C

        $date_legacy = date('Y-m-j', strtotime('2019-02-21')); //date after which the invoice number format changes
        $date_now = date('Y-m-j', strtotime($date));// date current invoice was created

        $sub_number = $arr['invoice'];

        if($date_legacy < $date_now){
                  $arr['invoice'] = $sub_number.(substr($sub_number, -2)*7)%5; //new invoice number format
                }

        $result[$count] = $arr;
        $count++;
    }

    $stmt->close();
    return $result;
}

function getExpiredInvoice($length, $db)
{
    if ($length = '' || $length = '30') {
        $sql = ("SELECT kts_client.cid, kts_email, total FROM
(SELECT  cid, sum(amount) as total FROM invoice_item WHERE paid = 0 and datediff(now(), date_added) > ?  GROUP BY cid
)AS A
INNER JOIN kts_client
ON kts_client.cid = A.cid");
    }
    if ($length = '45') {
        $sql = ("SELECT kts_client.cid, kts_email, total FROM
(SELECT  cid, sum(amount) as total FROM invoice_item WHERE paid = 0 and datediff(now(), date_added) > ?  GROUP BY cid
)AS A
INNER JOIN kts_client
ON kts_client.cid = A.cid");
    }
    if ($length = '60') {
        $sql = ("SELECT kts_client.cid, kts_email, total FROM
(SELECT  cid, sum(amount) as total FROM invoice_item WHERE paid = 0 and datediff(now(), date_added) > ?  GROUP BY cid
)AS A
INNER JOIN kts_client
ON kts_client.cid = A.cid");
    } else {
        return genResult('3', 'There was an error retrieving info.', null);
    }

    $stmt = $db->prepare($sql);
    $stmt->bind_param('s', $length);

    $stmt->execute();
    $stmt->bind_result($client, $cemail, $amount);

    $result = array();

    while ($row = $stmt->fetch()) {
        $arr['cid'] = $client;
        $arr['kts_email'] = $cemail;
        $arr['amount'] = $amount;
        $result[] = $arr;
    }

    return genResult('1', 'Success.', $result);
}

function getInvoiceItems($cid, $cpid, $type, $db)
{

  if($type == '1'){
    $stmt = $db->prepare("SELECT iiid, pid, invoice_item.cid, description, type, qty, amount, date_added, tax, shipping, paid, estimate  FROM invoice_item
                       LEFT JOIN kts_client ON invoice_item.cid = kts_client.cid WHERE /*invoice_item.cid = ? and*/ pid = ? and paid = 0 ");
  }
  elseif($type == '2'){
    $stmt = $db->prepare("SELECT iiid, pid, invoice_item.cid, description, type, qty, amount, date_added, tax, shipping, paid, estimate  FROM invoice_item
                       LEFT JOIN kts_client ON invoice_item.cid = kts_client.cid WHERE /*invoice_item.cid = ? and*/ pid = ? and paid = 1 ");
  }
  elseif($type == '3'){
    $stmt = $db->prepare("SELECT iiid, pid, invoice_item.cid, description, type, qty, amount, date_added, tax, shipping, paid, estimate FROM invoice_item
                       LEFT JOIN kts_client ON invoice_item.cid = kts_client.cid WHERE /*invoice_item.cid = ? and*/ pid = ? and estimate = 1 ");
  }

    $stmt->bind_param("s", $cpid); //You need to specify values to each '?' explicitly while using prepared statements
    $stmt->execute();
    $stmt->bind_result($iiid, $pid, $icid, $description, $type, $qty, $amount, $date, $tax, $shipping, $paid, $estimate); // Binding i.e. mapping database results to new variables
    //$stmt->close();


    $result = array();
    $count = 0;

    while ($row = $stmt->fetch()) {
        $arr['iiid'] = $iiid;
        $arr['pid'] = $pid;
        $arr['cid'] = $icid;
        $arr['description'] = $description;
        $arr['type'] = $type;
        $arr['qty'] = $qty;
        $arr['amount'] = $amount;
        $arr['date'] = $date;
        $arr['tax'] = $tax;
        $arr['shipping'] = $shipping;
        $arr['paid'] = $paid;
        $arr['estimate'] = $estimate;

        $result[$count] = $arr;
        $count++;
    }

    $stmt->close();
    return $result;
}
/*EO FUNCTION TO RETRIEVE CLIENT INVOICE*/

function setInvoiceItem($pid, $cid, $description, $qty, $amount, $invoice_number, $paid, $estimate, $tax, $shipping, $currency, $db)
{
    $query = "INSERT INTO invoice_item (pid, cid, description, qty, amount, date_added, invoice_number, paid, estimate, tax, shipping, currency) values (?, ?,?,?,?, now(), ?, ?, ?, ?, ?, ?)";
    $stmt = $db->prepare($query);
    $stmt->bind_param("sssssssssss", $pid, $cid, $description, $qty, $amount, $invoice_number, $paid, $estimate, $tax, $shipping, $currency); //You need to specify values to each '?' explicitly while using prepared statements
    if ($stmt->execute()) {
        $stmt->close();

        return genResult('1', 'Success.', null);
    } else {
        $stmt->close();
        return genResult('3', 'Could not add item.', null);
    }
}

function deleteInvoiceItem($iiid, $pid, $db)
{
    $query = "DELETE FROM invoice_item WHERE iiid=? AND pid=?";
    $stmt = $db->prepare($query);
    $stmt->bind_param("ss", $iiid, $pid); //You need to specify values to each '?' explicitly while using prepared statements
    if ($stmt->execute()) {
        $stmt->close();

        return genResult('1', 'Success.', null);
    } else {
        $stmt->close();
        return genResult('3', 'Could not add item.', null);
    }
}

function setFX($date, $denom, $rate, $db)
{
    $query = "INSERT INTO fx (date, denom, rate, time_entered) values (?, ?, ?, now())";
    $stmt = $db->prepare($query);
    $stmt->bind_param("sss", $date, $denom, $rate); //You need to specify values to each '?' explicitly while using prepared statements
    if ($stmt->execute()) {
        $stmt->close();

        return genResult('1', 'Success.', null);
    } else {
        $stmt->close();
        return genResult('3', 'Could not add item.', null);
    }
}

function fxConvert($cid, $amount, $date, $base, $db)
{
    $stmt = $db->prepare("SELECT cid, currency FROM (SELECT cid, currency FROM person UNION SELECT cid, currency FROM organization) AS A
  WHERE cid = ?"); //Fetching all the records with input credentials
    $stmt->bind_param("s", $cid);
    if ($stmt->execute()) {
        $stmt->bind_result($this_cid, $currency);

        $result = array();
        $count = 0;

        while ($stmt->fetch()) {
            $arr['cid'] = $this_cid;
            $arr['currency'] = $currency;

            $result[$count] = $arr;
            $count++;
        }

        $stmt->close();

        if ($result[0]['currency'] == 'jmd') {
            return genResult('1', 'Success', $amount);
        } elseif ($result[0]['currency'] == 'usd') {
            $stmt = $db->prepare("SELECT date, denom, rate FROM fx WHERE date = ? LIMIT 1"); //Fetching all the records with input credentials
            $stmt->bind_param("s", $date);
            if ($stmt->execute()) {
                $stmt->bind_result($this_date, $denom, $rate);

                $result = array();
                $count = 0;

                while ($stmt->fetch()) {
                    $arr['date'] = $this_date;
                    $arr['denom'] = $denom;
                    $arr['rate'] = $rate;

                    $result[$count] = $arr;
                    $count++;
                }

                $stmt->close();
                if ($result != null) {
                    $rate = $result[0]['rate'];
                    return genResult('1', 'Success', $amount / $rate);
                } else {
                    return genResult('1', 'Success', $amount / $base);
                }
            }
        } else {
            return genResult('3', 'Could not retrieve converted value', null);
        }
    }
}
/*GET CLIENT OR COMPANY NAME*/
function getEntityName($cid, $type, $db2)
{
    $query2 = "SELECT name from organization where cid = ?";
    $query1 = "SELECT fname, lname from person where cid = ?";

    if ($type == '2') {
        $stmt2 = $db2->prepare($query2); //Fetching all the records with input credentials
        $stmt2->bind_param("s", $cid); //You need to specify values to each '?' explicitly while using prepared statements
        $stmt2->execute();
        $stmt2->bind_result($name); // Binding i.e. mapping database results to new variables
        while ($row = $stmt2->fetch()) {
            $heisse = $name;
        }

        $stmt2->close();
        return $heisse;
    }
    if ($type == '1') {
        $stmt2 = $db2->prepare("$query1"); //Fetching all the records with input credentials
        $stmt2->bind_param("s", $cid); //You need to specify values to each '?' explicitly while using prepared statements
        $stmt2->execute();
        $stmt2->bind_result($fname, $lname); // Binding i.e. mapping database results to new variables


        while ($stmt2->fetch()) {
            $heisse = $fname . " " . $lname;
        }

        $stmt2->close();
        return $heisse;
    }
}

/*EO GET CLIENT OR COMPANY NAME*/

/*FUNCTION TO RETRIEVE CLIENTS ADDRESS:*/
function getClientAddress($cid, $db)
{
    $stmt = $db->prepare("SELECT cid, fname, lname, country, phone1, email FROM

(SELECT cid, fname, lname, country, phone1, email FROM person
UNION
SELECT cid, fname, lname, country, phone1, email FROM organization) as combo

WHERE cid = ?"); //Fetching all the records with input credentials
    $stmt->bind_param("s", $cid); //You need to specify values to each '?' explicitly while using prepared statements
    $stmt->execute();
    $stmt->bind_result($_cid, $fname, $lname, $country, $phone1, $email); // Binding i.e. mapping database results to new variables
    $result = array();

    $stmt->fetch();
    $arr['cid'] = $cid;
    $arr['fname'] = $fname;
    $arr['lname'] = $lname;
    $arr['country'] = $country;
    $arr['phone1'] = $phone1;
    $arr['email'] = $email;

    $result[0] = $arr;

    return $result;
}

/*EO FUNCTION TO RETRIEVE CLIENTS ADDRESS*/

function getAllClients($db)
{
    $stmt = $db->prepare("SELECT cid, fname, lname, ('Personal') as name FROM person UNION
  Select cid, fname, lname, name FROM organization ORDER BY lname"); //Fetching all the records with input credentials
    if ($stmt->execute()) {
        $stmt->bind_result($this_cid, $this_fname, $this_lname, $this_name);

        $result = array();
        $count = 0;

        while ($stmt->fetch()) {
            $arr['cid'] = $this_cid;
            $arr['fname'] = $this_fname;
            $arr['lname'] = $this_lname;
            $arr['name'] = $this_name;

            $result[$count] = $arr;
            $count++;
        }

        return genResult('1', 'Success', $result);
    } else {
        return genResult('3', 'Could not retrieve clients', null);
    }
}

function getMonth($month)
{
    $months = array(
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    );

    if ($month < 13 && $month > 0) {
        return $months[$month - 1];
    } else {
        return "-";
    }
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0;$i < $length;$i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1) ];
    }
    return $randomString;
}

function getTaxRate($type)
{
    if (strtoupper($type) == 'GCT') {
        return 16.5;
    }
}

function notFound($type)
{
    if ($type == '1') { //No records found (generic)
        $title = "KTS- Insight";
        $sub = "Not Available";
        $msg = "Sorry, your request is not available at the moment. Check back later.";
    } elseif ($type == '2') { //No invoices found
        $title = "Invoices";
        $sub = "No Invoices at This Time";
        $msg = "... Looks like you're all caught up!";
    } elseif ($type == '2a') { //No proforma invoices found
        $title = "proforma Invoices";
        $sub = "No Estimates at This Time";
        $msg = "... Looks like you're all caught up!";
    } elseif ($type == '2b') { //No receipts found
        $title = "Reciepts";
        $sub = "No Receipts at This Time";
        $msg = "... Looks like you're all caught up!";
    } elseif ($type == '3') { //No projects found
        $title = "Current Project";
        $sub = "No Projects at This Time";
        $msg = "You have no projects in progress at the moment. If you would like to start a new project click here to let us know.";
    } elseif ($type == '4') { //No messages found
        $title = "Inbox";
        $sub = "No Messages";
        $msg = "You have no messages at this time. ";
    } elseif ($type == '5') { //No domains found
        $title = "Web Domains";
        $sub = "No Domains";
        $msg = "You have no domains at this time. ";
    } elseif ($type == '6') { //No web hosting found
        $title = "Web Hosting";
        $sub = "No Web Hosting";
        $msg = "You have no web hosting plans at this time. ";
    }

    return "<div class='col-md-12 col-sm-12 col-xs-12'>
    <div class='x_panel'>
      <div class='x_title'>
        <h2>" . $title . " <small></small></h2>
        <ul class='nav navbar-right panel_toolbox'>
          <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a>
          </li>
          <li class='dropdown'>
            <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'><i class='fa fa-wrench'></i></a>
            <ul class='dropdown-menu' role='menu'>
              <li><a href='#'>Settings 1</a>
              </li>
              <li><a href='#'>Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class='close-link'><i class='fa fa-close'></i></a>
          </li>
        </ul>
        <div class='clearfix'></div>
      </div>
      <div class='x_content'>

        <div class='bs-example' data-example-id='simple-jumbotron'>
          <div class='jumbotron'>
            <h1>" . $sub . "</h1>
            <p>" . $msg . "</p>
          </div>
        </div>

      </div>
    </div>
  </div>";
}

function serveLogout()
{
    session_destroy(); //destroy the session
    echo '<script>window.location.href = "login";</script>';
    exit();
}

####################################################MESSAGING##################################
function getInbox($type, $id, $db, $db2)
{
    $stmt = $db->prepare("SELECT msgid, sender_type, sender, sender_fname, sender_lname, receiver_type, receiver, fname as receiver_fname, lname as receiver_lname, subj, msg, date_received, reply_to, reply_from, root, msg_read  FROM

(SELECT msgid, sender_type, sender, fname as sender_fname, lname as sender_lname, receiver_type, receiver, subj, msg, date_received, reply_to, reply_from, root, msg_read FROM

(SELECT msgid, sender_type, sender, receiver_type, receiver, subj, msg, date_received, reply_to, reply_from, root, msg_read FROM message
LEFT JOIN receive_message ON message.msgid = receive_message.mid
WHERE ((receiver = ? AND receiver_type= ?) OR (sender = ? AND sender_type= ? AND reply_from <> 0))   AND show_receiver = 1 ) as A

LEFT JOIN

(SELECT  cid, fname, lname, type FROM

(SELECT person.cid, fname, lname, kts_client.type FROM person

LEFT JOIN kts_client ON person.cid = kts_client.cid

UNION

SELECT organization.cid, fname, lname, kts_client.type FROM organization

LEFT JOIN kts_client ON organization.cid = kts_client.cid

UNION

SELECT eid,  fname, lname, type FROM employee)

AS B) AS C

ON A.sender_type = C.type AND A.sender = C.cid and A.sender_type = C.type AND A.sender = C.cid) AS D

LEFT JOIN

(SELECT  cid, fname, lname, type FROM

(SELECT person.cid, fname, lname, kts_client.type FROM person

LEFT JOIN kts_client ON person.cid = kts_client.cid

UNION

SELECT organization.cid, fname, lname, kts_client.type FROM organization

LEFT JOIN kts_client ON organization.cid = kts_client.cid

UNION

SELECT eid,  fname, lname, type FROM employee)

AS E) AS F

ON D.receiver_type = F.type AND D.receiver = F.cid and D.receiver_type = F.type AND D.receiver = F.cid



order by date_received DESC, root"); //Fetching all the records with input credentials
    $stmt->bind_param("ssss", $id, $type, $id, $type); //You need to specify values to each '?' explicitly while using prepared statements
    $stmt->execute();
    $stmt->bind_result($msgid, $sender_type, $sender, $sender_fname, $sender_lname, $receiver_type, $receiver, $receiver_fname, $receiver_lname, $subj, $msg, $date_received, $reply_to, $reply_from, $root, $msg_read); // Binding i.e. mapping database results to new variables
    $result = array();
    $arr = array();
    $count = 0;
    $reply_count = 0;
    $reply_msg_count = 0;
    $reply_root = 0;
    $real_root = 0;

    while ($row = $stmt->fetch()) { //!EXTREMELY IMPORTANT THAT RECORDS^^^^ ARE SORTED IN DESCENDING ORDER!!!
        if ($count == 0 || (($msgid == $root) && ($reply_from == 0))) {
            if (!recordExists('receive_message', 'mid', '$reply_from', 0, $db)) {
                $stmt2 = $db2->prepare("UPDATE receive_message SET reply_from = 0, root = ? WHERE mid=?"); //Fetching all the records with input credentials
                $stmt2->bind_param("ss", $msgid, $msgid); //You need to specify values to each '?' explicitly while using prepared statements
                $stmt2->execute();
                $stmt2->close();

                $stmt3 = $db2->prepare("UPDATE receive_message SET root = ? WHERE root=?"); //Fetching all the records with input credentials
                $stmt3->bind_param("ss", $msgid, $reply_from); //You need to specify values to each '?' explicitly while using prepared statements
                if ($stmt3->execute()) {
                    $reply_from = 0;
                    $root = $msgid;
                    $real_root = $root;
                }
                $stmt3->close();
            }

            if ($reply_from == '0' && $root == $msgid) {
                $reply_count = 0;
                $reply_msg_count = 0;

                $arr['n'] = $count;
                $arr['msgid'] = $msgid;
                $arr['sender_type'] = $sender_type;
                $arr['sender'] = $sender;
                $arr['sender_fname'] = $sender_fname;
                $arr['sender_lname'] = $sender_lname;
                $arr['receiver_type'] = $receiver_type;
                $arr['receiver'] = $receiver;
                $arr['receiver_fname'] = $receiver_fname;
                $arr['receiver_lname'] = $receiver_lname;
                $arr['subj'] = $subj;
                $arr['msg'] = $msg;
                $arr['date_received'] = $date_received;
                $arr['reply_to'] = $reply_to;
                $arr['reply_from'] = $reply_from;
                $arr['root'] = $real_root;
                $arr['msg_read'] = $msg_read;

                $arr['replies'] = $reply_count;
                $arr['reply_msgs'] = array();

                $result[$count] = $arr;
                $count++;
            }
        } elseif ($reply_from != '0' && $root != $msgid) {
            $reply_count++;
            $result[$count - 1]['replies'] = $reply_count;

            $result[$count - 1]['reply_msgs'][$reply_msg_count]['msgid'] = $msgid;
            $result[$count - 1]['reply_msgs'][$reply_msg_count]['sender_type'] = $sender_type;
            $result[$count - 1]['reply_msgs'][$reply_msg_count]['sender'] = $sender;
            $result[$count - 1]['reply_msgs'][$reply_msg_count]['sender_fname'] = $sender_fname;
            $result[$count - 1]['reply_msgs'][$reply_msg_count]['sender_lname'] = $sender_lname;
            $result[$count - 1]['reply_msgs'][$reply_msg_count]['receiver_type'] = $receiver_type;
            $result[$count - 1]['reply_msgs'][$reply_msg_count]['receiver'] = $receiver;
            $result[$count - 1]['reply_msgs'][$reply_msg_count]['receiver_fname'] = $receiver_fname;
            $result[$count - 1]['reply_msgs'][$reply_msg_count]['receiver_lname'] = $receiver_lname;
            $result[$count - 1]['reply_msgs'][$reply_msg_count]['subj'] = $subj;
            $result[$count - 1]['reply_msgs'][$reply_msg_count]['msg'] = $msg;
            $result[$count - 1]['reply_msgs'][$reply_msg_count]['date_received'] = $date_received;
            $result[$count - 1]['reply_msgs'][$reply_msg_count]['reply_to'] = $reply_to;
            $result[$count - 1]['reply_msgs'][$reply_msg_count]['reply_from'] = $reply_from;
            $result[$count - 1]['reply_msgs'][$reply_msg_count]['root'] = $root;
            $result[$count - 1]['reply_msgs'][$reply_msg_count]['msg_read'] = $msg_read;

            $reply_msg_count++;

            continue;
        }
    }

    $stmt->close();
    return $result;
}

function sendMessage($id, $type, $to, $to_type, $mode, $reply_from, $oldmsgid, $oldroot, $subject, $msg, $db)
{ //mode = 1 for new message, mode = 2 for reply
    $result = array();
    if ($mode == '2' && (!recordExists('receive_message', 'receiver_type', $type, 1, $db) || !recordExists('receive_message', 'receiver', $id, 1, $db) || !recordExists('receive_message', 'mid', $oldmsgid, 0, $db) || !recordExists('receive_message', 'show_receiver', '1', 1, $db))) {
        return genResult('2', 'There was a problem. (4-2)', null);
    }

    if ($mode == '1' || $mode == '2') {
        $stmt = $db->prepare("INSERT INTO message (subj, msg) values (?, ?)"); //Fetching all the records with input credentials
        $stmt->bind_param("ss", $subject, $msg); //You need to specify values to each '?' explicitly while using prepared statements
        $stmt->execute();
        $stmt->close();

        $stmt2 = $db->prepare("SELECT msgid, subj, msg FROM message order by msgid DESC LIMIT 1"); //Fetching all the records with input credentials
        $stmt2->execute();
        $stmt2->bind_result($msgid2, $subj2, $msg2); //You need to specify values to each '?' explicitly while using prepared statements
        while ($stmt2->fetch()) {
            $result['msgid'] = $msgid2;
            $result['subj'] = $subj2;
            $result['msg'] = $msg2;
        }

        $stmt2->close();

        $msgid = $result['msgid'];

        $stmt3 = $db->prepare("INSERT INTO sent_message (mid, sender_type, sender, show_sender, date_sent, receiver_type, receiver)
VALUES (?,?,?,1,NOW(),?,?)"); //Fetching all the records with input credentials
        $stmt3->bind_param("sssss", $msgid, $type, $id, $to_type, $to); //You need to specify values to each '?' explicitly while using prepared statements
        $stmt3->execute();
        $stmt3->close();

        $stmt4 = $db->prepare("INSERT INTO receive_message (mid, receiver_type, receiver, show_receiver, date_received, sender_type, sender, show_sender, reply_from, reply_to, root, msg_read)
VALUES (?,?,?,1,NOW(),?,?,1,?,?,?,0)"); //Fetching all the records with input credentials
        $stmt4->bind_param("ssssssss", $msgid, $to_type, $to, $type, $id, $reply_from, $oldmsgid, $msgid); //You need to specify values to each '?' explicitly while using prepared statements
        $stmt4->execute();
        $stmt4->close();

        if ($mode == '2' && ($oldmsgid != 0 || $oldmsgid != null) && ($oldroot != 0 || $oldroot != null)) { // For replies
            $stmt5 = $db->prepare("UPDATE receive_message SET reply_from = ? WHERE mid = ? LIMIT 1"); //Fetching all the records with input credentials
            $stmt5->bind_param("ss", $msgid, $oldmsgid); //You need to specify values to each '?' explicitly while using prepared statements
            $stmt5->execute();
            $stmt5->close();

            $stmt6 = $db->prepare("UPDATE receive_message SET root = ? WHERE root = ?"); //Fetching all the records with input credentials
            $stmt6->bind_param("ss", $msgid, $oldmsgid); //You need to specify values to each '?' explicitly while using prepared statements
            $stmt6->execute();
            $stmt6->close();
        }

        return genResult('1', 'Success!', null);
    } else {
        return genResult('2', 'Message was not successfully sent. (4-2)', null);
    }
}

function deleteMessage($msgid, $cid, $type, $db, $db2)
{
    $stmt = $db->prepare("UPDATE receive_message SET show_receiver = 0 WHERE mid = ? AND receiver = ? AND receiver_type = ? LIMIT 1"); //Fetching all the records with input credentials
    $stmt->bind_param("sss", $msgid, $cid, $type); //You need to specify values to each '?' explicitly while using prepared statements
    if (!$stmt->execute()) {
        return genResult('2', 'There was an error. (4-2a)', null);
    }
    $stmt->close();

    $stmt2 = $db2->prepare("UPDATE receive_message SET show_sender = 0 WHERE mid = ? AND sender = ? AND sender_type = ? LIMIT 1"); //Fetching all the records with input credentials
    $stmt2->bind_param("sss", $msgid, $cid, $type); //You need to specify values to each '?' explicitly while using prepared statements
    if (!$stmt2->execute()) {
        return genResult('2', 'There was an error. (4-2b)', null);
    }

    $stmt2->close();

    return genResult('1', 'Success.', null);
}

function checkNewMessages($id, $type, $db)
{
    $result = array();
    $stmt = $db->prepare("SELECT COUNT(mid) FROM receive_message WHERE receiver = ? AND receiver_type = ?  AND msg_read = 0"); //Fetching all the records with input credentials
    $stmt->bind_param("ss", $id, $type); //You need to specify values to each '?' explicitly while using prepared statements
    $stmt->execute();
    $stmt->bind_result($unread_msg_count);

    while ($stmt->fetch()) {
        $result['unread_msg_count'] = $unread_msg_count;
    }

    $stmt->close();

    return $result;
}

function getEmployee($id, $db)
{
    $result = array();
    $stmt = $db->prepare("SELECT eid, fname, lname, email, phone1, address, role, avatar, pic FROM employee WHERE eid = ? LIMIT 1"); //Fetching all the records with input credentials
    $stmt->bind_param("s", $id); //You need to specify values to each '?' explicitly while using prepared statements
    $stmt->execute();
    $stmt->bind_result($eid, $fname, $lname, $email, $phone1, $address, $role, $avatar, $pic);

    while ($stmt->fetch()) {
        $result['eid'] = $eid;
        $result['fname'] = $fname;
        $result['lname'] = $lname;
        $result['email'] = $email;
        $result['phone1'] = $phone1;
        $result['role'] = $role;
        $result['avatar'] = $avatar;
        $result['pic'] = $pic;
    }

    $stmt->close();

    return $result;
}

function getAllEmployees($db)
{
    $result = array();
    $stmt = $db->prepare("SELECT eid, fname, lname, email, phone1, address, role, avatar, pic FROM employee"); //Fetching all the records with input credentials
    if ($stmt->execute()) {
        $stmt->bind_result($eid, $fname, $lname, $email, $phone1, $address, $role, $avatar, $pic);

        $count = 0;

        while ($stmt->fetch()) {
            $row['eid'] = $eid;
            $row['fname'] = $fname;
            $row['lname'] = $lname;
            $row['email'] = $email;
            $row['phone1'] = $phone1;
            $row['role'] = $role;
            $row['avatar'] = $avatar;
            $row['pic'] = $pic;

            $result[$count] = $row;
            $count++;
        }

        $stmt->close();

        return genResult('1', 'Success', $result);
    } else {
        return genResult('3', 'There was an error retreiving employees', null);
    }
}

function setRead($msgid, $reader, $type, $db)
{
    $stmt = $db->prepare("UPDATE receive_message SET msg_read = 1 WHERE mid = ? AND receiver = ? AND receiver_type = ?"); //Fetching all the records with input credentials
    $stmt->bind_param("sss", $msgid, $reader, $type); //You need to specify values to each '?' explicitly while using prepared statements
    $stmt->execute();
    $stmt->close();
}

function getDomain($id, $type, $db)
{
    $result = array();

    if ($type == '1') {
        $stmt = $db->prepare("SELECT did, cid, domain, parked, sub_domain, registrar, account, duration, buy_date, expire FROM domain WHERE cid = ?"); //Fetching all the records with input credentials
    } elseif ($type == '2') {
        $stmt = $db->prepare("SELECT did, cid, domain, parked, sub_domain, registrar, account, duration, buy_date, expire FROM domain"); //Fetching all the records with input credentials
    } elseif ($type == '3') {
        $stmt = $db->prepare("SELECT did, cid, domain, parked, sub_domain, registrar, account, duration, buy_date, expire FROM domain WHERE did = ?"); //Fetching all the records with input credentials
    } elseif ($type == '4-60') {
        $stmt = $db->prepare("SELECT did, cid, domain, parked, sub_domain, registrar, account, duration, buy_date, expire FROM domain WHERE  (datediff(expire, now()) = 60) and sub_domain <> 1 "); //Fetching all the records with input credentials
    } elseif ($type == '4-30') {
        $stmt = $db->prepare("SELECT did, cid, domain, parked, sub_domain, registrar, account, duration, buy_date, expire FROM domain WHERE  (datediff(expire, now()) = 30) and sub_domain <> 1 "); //Fetching all the records with input credentials
    } elseif ($type == '4-7') {
        $stmt = $db->prepare("SELECT did, cid, domain, parked, sub_domain, registrar, account, duration, buy_date, expire FROM domain WHERE  (datediff(expire, now()) = 7) and sub_domain <> 1 "); //Fetching all the records with input credentials
    } elseif ($type == '4-1') {
        $stmt = $db->prepare("SELECT did, cid, domain, parked, sub_domain, registrar, account, duration, buy_date, expire FROM domain WHERE  (datediff(expire, now()) = 1) and sub_domain <> 1 "); //Fetching all the records with input credentials
    }
    @$stmt->bind_param("s", $id); //You need to specify values to each '?' explicitly while using prepared statements
    if ($stmt->execute()) {
        $stmt->bind_result($did, $id, $domain, $parked, $sub_domain, $registrar, $account, $duration, $buy_date, $expire);

        $count = 0;

        while ($stmt->fetch()) {
            $arr['did'] = $did;
            $arr['cid'] = $id;
            $arr['domain'] = $domain;
            $arr['parked'] = $parked;
            $arr['sub_domain'] = $sub_domain;
            $arr['registrar'] = $registrar;
            $arr['account'] = $account;
            $arr['duration'] = $duration;
            $arr['buy_date'] = $buy_date;
            $arr['expire'] = $expire;

            $result[$count] = $arr;
            $count++;
        }

        $stmt->close();

        return genResult('1', 'Success', $result);
    } else {
        return genResult('3', 'There was a problem retreiving results', null);
    }
}

function createDomain($domain_client, $domain_name, $domain_registrar, $domain_account, $domain_parked, $domain_sub, $domain_duration, $domain_purchase, $domain_expiry, $db)
{
    $stmt = $db->prepare("INSERT INTO domain (cid, domain, registrar, account, parked, sub_domain, duration, buy_date, expire)
    VALUES (?,?,?,?,?,?,?,?,?)");
    $stmt->bind_param("sssssssss", $domain_client, $domain_name, $domain_registrar, $domain_account, $domain_parked, $domain_sub, $domain_duration, $domain_purchase, $domain_expiry); //You need to specify values to each '?' explicitly while using prepared statements
    if ($stmt->execute()) {
        $stmt->close();

        return genResult('1', 'Success', null);
    } else {
        $stmt->close();

        return genResult('3', 'There was an error submitting request. Try again later.', null);
    }
}

function createHosting($hosting_client, $hosting_name, $hosting_account, $hosting_registrar, $hosting_type, $hosting_duration, $hosting_purchase, $hosting_expiry, $db)
{
    if (recordExists('domain', 'did', $hosting_name, '1', $db)) {
        if (!recordExists('hosting', 'did', $hosting_name, '1', $db)) {
            $stmt = $db->prepare("INSERT INTO hosting (cid, did, account, registrar, website_type, duration, buy_date, expire)
    VALUES (?,?,?,?,?,?,?,?)");
            $stmt->bind_param("ssssssss", $hosting_client, $hosting_name, $hosting_account, $hosting_registrar, $hosting_type, $hosting_duration, $hosting_purchase, $hosting_expiry); //You need to specify values to each '?' explicitly while using prepared statements
            if ($stmt->execute()) {
                $stmt->close();

                return genResult('1', 'Success', null);
            } else {
                $stmt->close();

                return genResult('3', 'There was an error submitting request. Try again later.', null);
            }
        } else {
            return genResult('2', 'A hosting plan with that domain already exists.', null);
        }
    } else {
        return genResult('2', 'The domain you are trying to add does not exist.', null);
    }
}

function getHosting($id, $type, $db)
{
    $result = array();
    if ($type == '1') {
        $stmt = $db->prepare("SELECT hid, hosting.cid,  domain, hosting.did, hosting.account, hosting.registrar, website_type, hosting.duration, hosting.buy_date, hosting.expire FROM hosting left join domain on hosting.did = domain.did WHERE hosting.cid = ?"); //Fetching all the records with input credentials
    } elseif ($type == '2') {
        $stmt = $db->prepare("SELECT hid, hosting.cid,  domain, hosting.did, hosting.account, hosting.registrar, website_type, hosting.duration, hosting.buy_date, hosting.expire FROM hosting left join domain on hosting.did = domain.did"); //Fetching all the records with input credentials
    } elseif ($type == '3') {
        $stmt = $db->prepare("SELECT hid, hosting.cid,  domain, hosting.did, hosting.account, hosting.registrar, website_type, hosting.duration, hosting.buy_date, hosting.expire FROM hosting left join domain on hosting.did = domain.did WHERE hid=?"); //Fetching all the records with input credentials
    } elseif ($type == '4-60') {
        $stmt = $db->prepare("SELECT hid, hosting.cid,  domain, hosting.did, hosting.account, hosting.registrar, website_type, hosting.duration, hosting.buy_date, hosting.expire FROM hosting left join domain on hosting.did = domain.did WHERE  (datediff(hosting.expire, now()) = 60)"); //Fetching all the records with input credentials
    } elseif ($type == '4-30') {
        $stmt = $db->prepare("SELECT hid, hosting.cid,  domain, hosting.did, hosting.account, hosting.registrar, website_type, hosting.duration, hosting.buy_date, hosting.expire FROM hosting left join domain on hosting.did = domain.did WHERE  (datediff(hosting.expire, now()) = 30)"); //Fetching all the records with input credentials
    } elseif ($type == '4-7') {
        $stmt = $db->prepare("SELECT hid, hosting.cid,  domain, hosting.did, hosting.account, hosting.registrar, website_type, hosting.duration, hosting.buy_date, hosting.expire FROM hosting left join domain on hosting.did = domain.did WHERE  (datediff(hosting.expire, now()) = 7)"); //Fetching all the records with input credentials
    } elseif ($type == '4-1') {
        $stmt = $db->prepare("SELECT hid, hosting.cid,  domain, hosting.did, hosting.account, hosting.registrar, website_type, hosting.duration, hosting.buy_date, hosting.expire FROM hosting left join domain on hosting.did = domain.did WHERE  (datediff(hosting.expire, now()) = 1)"); //Fetching all the records with input credentials
    }
    @$stmt->bind_param("s", $id); //You need to specify values to each '?' explicitly while using prepared statements
    if ($stmt->execute()) {
        $stmt->bind_result($hid, $cid, $domain, $did, $account, $registrar, $website_type, $duration, $buy_date, $expire);

        $count = 0;

        while ($stmt->fetch()) {
            $arr['hid'] = $hid;
            $arr['cid'] = $cid;
            $arr['did'] = $did;
            $arr['domain'] = $domain;
            $arr['account'] = $account;
            $arr['registrar'] = $registrar;
            $arr['website_type'] = $website_type;
            $arr['duration'] = $duration;
            $arr['buy_date'] = $buy_date;
            $arr['expire'] = $expire;

            $result[$count] = $arr;
            $count++;
        }

        $stmt->close();

        return genResult('1', 'Success', $result);
    } else {
        return genResult('3', 'There was a problem retreiving results', null);
    }
}

function getClientDetails($id, $type, $db)
{
    $result = array();
    if ($type == '1' || $type == '2') { //Backwards compatibility. Each type had their own query. Now, 1 query to rule them all.
        if ($stmt = $db->prepare("SELECT kts_client.cid, user, kts_client.type, kts_email as email, join_date, name, fname, lname, address1, address2, country, phone1, phone2, sms, push, currency, A.type FROM

(SELECT cid, fname, lname, concat(fname,' ',lname) as name, currency, address1, address2, country, phone1, phone2, email, concat('n/a') as type FROM person
UNION
SELECT cid, fname, lname, name, currency, address1, address2, country, phone1, phone2, email, type FROM organization)

AS A

LEFT JOIN kts_client ON A.cid = kts_client.cid

WHERE A.cid = ?") //Fetching all the records with input credentials
        ) {
            $stmt->bind_param("s", $id); //You need to specify values to each '?' explicitly while using prepared statements
            $stmt->execute();
            $stmt->bind_result($uid, $user, $ctype, $cemail, $join_date, $name, $fname, $lname, $address1, $address2, $country, $phone1, $phone2, $sms, $push, $currency, $org_type);

            $count = 0;

            while ($stmt->fetch()) {
                $arr['cid'] = $uid;
                $arr['user'] = $user;
                $arr['ctype'] = $ctype;
                $arr['email'] = $cemail;
                $arr['join_date'] = $join_date;
                $arr['name'] = $name;
                $arr['fname'] = $fname;
                $arr['lname'] = $lname;
                $arr['address1'] = $address1;
                $arr['address2'] = $address2;
                $arr['country'] = $country;
                $arr['phone1'] = $phone1;
                $arr['phone2'] = $phone2;
                $arr['sms'] = $sms;
                $arr['push'] = $push;
                $arr['currency'] = $currency;
                $arr['organization_type'] = $org_type;

                $result[$count] = $arr;
                $count++;
            }

            $stmt->close();

            return genResult('1', 'Success', $result);
        } else {
            return genResult('3', 'There was an error accessing info.', null);
        }
    } else {
        return genResult('2', 'Unknown operation.', null);
    }
}

function recordExists($table, $column, $item, $multiple, $db)
{
    $query = "select " . $column . " from " . $table . " where " . $column . "=?";
    if ($stmt = $db->prepare($query) //Fetching all the records with input credentials
    ) {
        $stmt->bind_param("s", $item);
        if ($stmt->execute()) {
            $stmt->store_result();

            $rows = $stmt->num_rows;

            $stmt->close();

            //echo $rows."<br/>";
            if ($multiple == '1' && $rows >= '1') {
                //echo "true";
                return true;
            } elseif ($multiple == '0' && $rows == '1') {
                //echo "true";
                return true;
            } else {
                //echo "false";
                return false;
            }
        } else {
            //echo "falseb";
            return false;
        }
    } else {
        ///echo "falseb";
        return false;
    }
}

function updateTable($id, $id_column, $table, $column, $value, $multiple, $db)
{
    $query = "UPDATE " . $table . " SET " . $column . "=? WHERE " . $id_column . "=?";
    //$query = "UPDATE project_update SET update_content=? WHERE puid=?";
    if (recordExists($table, $id_column, $id, $multiple, $db)) {
        if ($stmt = $db->prepare($query) //Fetching all the records with input credentials
        ) {
            $stmt->bind_param("ss", $value, $id); //You need to specify values to each '?' explicitly while using prepared statements
            if ($stmt->execute()) {
                $stmt->close();
                return genResult('1', 'Success.', null);
            } else {
                $stmt->close();
                return genResult('2', 'Record was not added successfully.', null);
            }
        } else {
            $stmt->close();
            return genResult('2', 'Record was not added successfully.', null);
        }
    } else {
        return genResult('3', 'Record not found.', null);
    }
}

function updateKTS($operation, $cid, $value, $pattern, $db)
{
    if ($operation == 'avatar') {
        $target_dir = "../../client/" . $cid . "/avatar";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

        if (isset($_POST["avatar"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if ($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";

                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }

        if ($uploadOk == 1) {
            $status['code'] = 1;
            $status['msg'] = "Success!";
            $status['payload'] = null;

            return $status;
        } elseif ($uploadOk == 0) {
            $status['code'] = 0;
            $status['msg'] = "There was an error uploading your image.";
            $status['payload'] = null;

            return $status;
        }
    } elseif ($operation == 'username') {
        if (recordExists('kts_client', 'user', $value, '1', $db)) {
            $status['code'] = 4;
            $status['msg'] = "Username already exists.";
            $status['payload'] = null;

            return $status;
        } elseif (!preg_match($pattern, $value)) {
            $status['code'] = 4;
            $status['msg'] = "Username is not in correct format.\n Must: \n -be at least 4 characters long. \n -start with at least 2 letters.";
            $status['payload'] = null;

            return $status;
        }

        $query = "UPDATE kts_client SET user = ? WHERE cid = ?";
        $value = mysqli_real_escape_string($db, $value);
    } elseif ($operation == 'email') {
        if (recordExists('kts_client', 'kts_email', $value, '1', $db)) {
            $status['code'] = 4;
            $status['msg'] = "Email already exists.";
            $status['payload'] = null;

            return $status;
        } elseif (!preg_match($pattern, $value) || !filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $status['code'] = 4;
            $status['msg'] = "Email is not in correct format.";
            $status['payload'] = null;

            return $status;
        }

        $query = "UPDATE kts_client SET kts_email = ? WHERE cid = ?";
        $value = mysqli_real_escape_string($db, $value);
    } elseif ($operation == 'password') {

        //TODO check if password has been confirmed
        /*if(recordExists('kts_client', 'kts_email', $value, '1', $db)){

        $status['code'] = 4;
        $status['msg'] = "Email already exists.";
        $status['payload'] = null;

        echo json_encode($status);

        return false;

        }*/
        if (!preg_match($pattern, $value)) {
            $status['code'] = 4;
            $status['msg'] = "Password is not in correct format.\n Must contain at least:\n-1 uppercase letter\n-1 lowercase letter\n-1 numeral\n-1 special character\n";
            $status['payload'] = null;

            return $status;
        }

        $query = "UPDATE kts_client SET pw = ? WHERE cid = ?";
        $value = mysqli_real_escape_string($db, $value);
        $value = password_hash($value, PASSWORD_DEFAULT);
    } elseif ($operation == 'sms') {
        if ($value != 'on' && $value != 'off') {
            $status['code'] = 4;
            $status['msg'] = "Unknown format.";
            $status['payload'] = null;

            return $status;
        }
        $query = "UPDATE kts_client SET sms = ? WHERE cid = ?";
        $value = mysqli_real_escape_string($db, $value);
        if ($value == 'on') {
            $value = 1;
        } elseif ($value == 'off') {
            $value = 0;
        }
    } elseif ($operation == 'push') {
        if ($value != 'on' && $value != 'off') {
            $status['code'] = 4;
            $status['msg'] = "Unknown format.";
            $status['payload'] = null;

            return $status;
        }
        $query = "UPDATE kts_client SET push = ? WHERE cid = ?";
        $value = mysqli_real_escape_string($db, $value);
        if ($value == 'on') {
            $value = 1;
        } elseif ($value == 'off') {
            $value = 0;
        }
    } elseif ($operation == 'currency') {
        if ($pattern == '1') {
            $query = "UPDATE person SET currency = ? WHERE cid = ?";
        } elseif ($pattern == '2') {
            $query = "UPDATE organization SET currency = ? WHERE cid = ?";
        } else {
            $status['code'] = 4;
            $status['msg'] = "Unknown operation.";
            $status['payload'] = null;

            return $status;
        }
    } else {
        $status['code'] = 2;
        $status['msg'] = "Unknown operation.";
        $status['payload'] = null;

        return $status;
    }

    if ($stmt = $db->prepare($query) //Fetching all the records with input credentials
    ) {
        $stmt->bind_param("ss", $value, $cid); //You need to specify values to each '?' explicitly while using prepared statements
        if ($stmt->execute()) {
            $stmt->close();

            $status['code'] = 1;
            $status['msg'] = "Success!";
            $status['payload'] = null;

            return $status;
        } else {
            $status['code'] = 2;
            $status['msg'] = "Record was not added successfully.";
            $status['payload'] = null;

            return $status;
        }
    } else {
        $status['code'] = 2;
        $status['msg'] = "Record was not added successfully.";
        $status['payload'] = null;

        return $status;
    }
}

function getAvatar($client_pic, $initials_pic, $generic_pic)
{
    if (file_exists($client_pic)) {
        $scanned_directory = array_diff(scandir($client_pic), array(
            '..',
            '.'
        ));
        foreach ($scanned_directory as $d) {
            $ext = pathinfo($d, PATHINFO_EXTENSION);
            if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') {
                return $client_pic . "/" . $d;
            }
        }
        return getAvatar('doesntexist', $initials_pic, $generic_pic);
    } elseif (UR_exists($initials_pic)) {
        return $initials_pic;
    } else {
        return $generic_pic;
    }
}

function UR_exists($url)
{
    $headers = get_headers($url);
    return stripos($headers[0], "200 OK") ? true : false;
}

function getProjectRequest($cid, $received, $max, $db)
{
    $query = '';

    if ($received == '0') {
        $query = "SELECT prid, cid, type, description, start_date, phone, email, preferred_method, date_requested, received FROM
      project_request WHERE cid = ? and received = '0' ORDER BY date_requested DESC LIMIT ?";
    } else {
        $query = "SELECT prid, cid, type, description, start_date, phone, email, preferred_method, date_requested, received FROM
      project_request WHERE cid = ? and received = '1' ORDER BY date_requested DESC LIMIT ?";
    }

    if ($stmt = $db->prepare($query)) { //Fetching all the records with input credentials
        $stmt->bind_param("ss", $cid, $max); //You need to specify values to each '?' explicitly while using prepared statements
        $stmt->execute();
        $stmt->bind_result($prid, $rcid, $rtype, $description, $start_date, $phone, $email, $preferred_method, $date_requested, $received); // Binding i.e. mapping database results to new variables
        $result = array();
        $count = 0;

        while ($stmt->fetch()) {
            $arr['prid'] = $prid;
            $arr['rcid'] = $rcid;
            $arr['rtype'] = $rtype;
            $arr['description'] = $description;
            $arr['start_date'] = $start_date;
            $arr['phone'] = $phone;
            $arr['email'] = $email;
            $arr['preferred_method'] = $preferred_method;
            $arr['date_requested'] = $date_requested;
            $arr['received'] = $received;

            $result[$count] = $arr;
            $count++;
        }

        $stmt->close();

        return genResult('1', 'Success', $result);
    } else {
        return genResult('3', 'There was an error accessing info.', null);
    }
}

function requestProject($cid, $type, $description, $start_date, $phone, $email, $preferred_method, $db)
{
    $stmt = $db->prepare("INSERT INTO project_request (cid, type, description, start_date, phone, email, preferred_method, date_requested)
VALUES (?,?,?,?,?,?,?,NOW())");
    $stmt->bind_param("sssssss", $cid, $type, $description, $start_date, $phone, $email, $preferred_method); //You need to specify values to each '?' explicitly while using prepared statements
    if ($stmt->execute()) {
        $stmt->close();

        return genResult('1', 'Success', null);
    } else {
        $stmt->close();

        return genResult('3', 'There was an error submitting request. Try again later.', null);
    }
}

function deleteProjectRequest($cid, $prid, $db)
{
    $stmt = $db->prepare("DELETE FROM project_request WHERE prid = ? and cid = ?");

    $stmt->bind_param("ss", $prid, $cid); //You need to specify values to each '?' explicitly while using prepared statements
    if ($stmt->execute()) {
        $stmt->close();

        return genResult('1', 'Success', null);
    } else {
        $stmt->close();

        return genResult('3', 'There was an error submitting request. Try again later.', null);
    }
}

///////////////////////////////////////////////////////
function getTotalClients($db)
{
    if ($stmt = $db->prepare("SELECT count(cid) FROM kts_client") //Fetching all the records with input credentials
    ) {
        $stmt->execute();
        $stmt->bind_result($number);

        $count = 0;
        $result = array();

        while ($stmt->fetch()) {
            $arr['count'] = $number;
            $result[$count] = $arr;
            $count++;
        }

        $stmt->close();

        $status['code'] = 1;
        $status['msg'] = "Success.";
        $status['payload'] = $result[0]['count'];

        return $status;
    } else {
        $status['code'] = 3;
        $status['msg'] = "There was an error accessing info.";
        $status['payload'] = null;

        return $status;
    }
}

function getTimeLogged($id, $type, $option, $db)
{
    if ($type == '1' && $option == 'avg') {
        $query = "SELECT avg(time_logged) FROM current_project where cid = ?";
    } elseif ($type == '2' && $option == 'avg') {
        $query = "SELECT avg(time_logged) FROM current_project where project_leader = ?";
    } elseif ($type == '1' && $option == 'avg_comp') {
        $query = "SELECT avg(time_logged) FROM current_project where cid = ? AND (status = 'Finished' OR 'Completed')";
    } elseif ($type == '2' && $option == 'avg_comp') {
        $query = "SELECT avg(time_logged) FROM current_project where project_leader = ? AND (status = 'Finished' OR 'Completed')";
    } elseif ($type == '1' && $option == 'sum') {
        $query = "SELECT sum(time_logged) FROM current_project where cid = ?";
    } elseif ($type == '2' && $option == 'sum') {
        $query = "SELECT sum(time_logged) FROM current_project where project_leader = ?";
    }

    if ($stmt = $db->prepare($query) //Fetching all the records with input credentials
    ) {
        $stmt->bind_param("s", $id);
        $stmt->execute();
        $stmt->bind_result($number);

        $count = 0;
        $result = array();

        while ($stmt->fetch()) {
            $arr['count'] = $number;
            $result[$count] = $arr;
            $count++;
        }

        $stmt->close();

        $status['code'] = 1;
        $status['msg'] = "Success.";
        $status['payload'] = $result[0]['count'];

        if (empty($status['payload'])) {
            $status['payload'] = 0;
        }

        return $status;
    } else {
        $status['code'] = 3;
        $status['msg'] = "There was an error accessing info.";
        $status['payload'] = null;

        return $status;
    }
}

function getProjectStats($id, $type, $option, $db)
{
    if ($type == '1' && $option == 'in_progress') {
        $query = "SELECT count(cid) FROM current_project where cid = ? AND status <> 'Completed'";
    } elseif ($type == '2' && $option == 'in_progress') {
        $query = "SELECT count(cid) FROM current_project where project_leader = ? AND status <> 'Completed'";
    } elseif ($type == '1' && $option == 'completed') {
        $query = "SELECT count(cid) FROM current_project where cid = ? AND status = 'Completed'";
    } elseif ($type == '2' && $option == 'completed') {
        $query = "SELECT count(cid) FROM current_project where project_leader = ? AND status = 'Completed'";
    } elseif ($type == '1' && $option == 'finished') {
        $query = "SELECT count(cid) FROM current_project where cid = ? AND status = 'Finished'";
    } elseif ($type == '2' && $option == 'finished') {
        $query = "SELECT count(cid) FROM current_project where project_leader = ? AND status = 'Finished'";
    }

    if ($stmt = $db->prepare($query) //Fetching all the records with input credentials
    ) {
        $stmt->bind_param("s", $id);
        $stmt->execute();
        $stmt->bind_result($number);

        $count = 0;
        $result = array();

        while ($stmt->fetch()) {
            $arr['count'] = $number;
            $result[$count] = $arr;
            $count++;
        }

        $stmt->close();

        $status['code'] = 1;
        $status['msg'] = "Success.";
        $status['payload'] = $result[0]['count'];

        return $status;
    } else {
        $status['code'] = 3;
        $status['msg'] = "There was an error accessing info.";
        $status['payload'] = null;

        return $status;
    }
}

function getBalance($id, $type, $option, $db)
{
    if ($type == '1' && $option == 'total') {
        $query = "SELECT sum((amount * qty) + ((amount * qty) * (tax/100)) + shipping) as amount, date_added FROM invoice_item WHERE paid = 0 AND cid = ? GROUP BY date_added";
    } elseif ($type == '2' && $option == 'total') {
        $query = "SELECT sum((amount * qty) + ((amount * qty) * (tax/100)) + shipping) as amount, date_added FROM invoice_item WHERE paid = 0 GROUP BY date_added";
    } elseif ($type == '2' && $option == 'total_pid') {
        $query = "SELECT sum((amount * qty) + ((amount * qty) * (tax/100)) + shipping) as amount, date_added FROM invoice_item WHERE paid = 0 GROUP BY date_added";
    }

    if ($stmt = $db->prepare($query) //Fetching all the records with input credentials
    ) {
        if ($type == '1') {
            $stmt->bind_param("s", $id);
        }
        $stmt->execute();
        $stmt->bind_result($number, $date);

        $count = 0;
        $result = array();

        while ($stmt->fetch()) {
            $arr['amount'] = $number;
            $arr['date'] = $date;
            $result[$count] = $arr;
            $count++;
        }

        $stmt->close();

        $status['code'] = 1;
        $status['msg'] = "Success.";
        $status['payload'] = $result;

        return $status;
    } else {
        $status['code'] = 3;
        $status['msg'] = "There was an error accessing info.";
        $status['payload'] = null;

        return $status;
    }
}

function addPayment($pid, $descrp, $amount, $db)
{
    $stmt = $db->prepare("INSERT INTO item_paid (pid, description, date, amount) VALUES (?,?,now(),?) "); //Fetching all the records with input credentials
    $stmt->bind_param("sss", $pid, $descrp, $amount); //You need to specify values to each '?' explicitly while using prepared statements
    if ($stmt->execute()) {
        return genResult('1', 'Success', null);
    } else {
        return genResult('3', 'payment was not submitted.', null);
    }
}

function getPTypeTotal($id, $type, $db)
{
    $query = '';

    if ($type == 'Website') {
        $query = "SELECT count(project_type) FROM current_project WHERE project_type = 'Website' AND cid = ?";
    } elseif ($type == 'PC/Mobile Application') {
        $query = "SELECT count(project_type) FROM current_project WHERE project_type = 'PC/Mobile Application' AND cid = ?";
    } elseif ($type == 'PC Repair') {
        $query = "SELECT count(project_type) FROM current_project WHERE project_type = 'PC Repair' AND cid = ?";
    } elseif ($type == 'Desktop PC Build') {
        $query = "SELECT count(project_type) FROM current_project WHERE project_type = 'Desktop PC Build' AND cid = ?";
    } elseif ($type == 'Networking Solution') {
        $query = "SELECT count(project_type) FROM current_project WHERE project_type = 'Networking Solution' AND cid = ?";
    } elseif ($type == 'Social Media Management') {
        $query = "SELECT count(project_type) FROM current_project WHERE project_type = 'Social Media Management' AND cid = ?";
    } elseif ($type == 'Social Media Management') {
        $query = "SELECT count(project_type) FROM current_project WHERE project_type = 'Social Media Management' AND cid = ?";
    } elseif ($type == 'Graphic Design') {
        $query = "SELECT count(project_type) FROM current_project WHERE project_type = 'Graphic Design' AND cid = ?";
    } elseif ($type == 'Domain/Hosting') {
        $query = "SELECT count(project_type) FROM current_project WHERE project_type = 'Domain/Hosting' AND cid = ?";
    }

    if ($stmt = $db->prepare($query) //Fetching all the records with input credentials
    ) {
        $stmt->bind_param("s", $id);

        $stmt->execute();
        $stmt->bind_result($number);

        $count = 0;
        $result = array();

        while ($stmt->fetch()) {
            $arr['count'] = $number;
            $result[$count] = $arr;
            $count++;
        }

        $stmt->close();

        $status['code'] = 1;
        $status['msg'] = "Success.";
        $status['payload'] = $result[0]['count'];

        return $status;
    } else {
        $status['code'] = 3;
        $status['msg'] = "There was an error accessing info.";
        $status['payload'] = null;

        return $status;
    }
}

function getPastP($id, $db)
{
    $query = "SELECT start_date, time_logged FROM current_project WHERE datediff(now(), start_date) <= 365 AND cid =? order by start_date ASC";

    if ($stmt = $db->prepare($query) //Fetching all the records with input credentials
    ) {
        $stmt->bind_param("s", $id);

        $stmt->execute();
        $stmt->bind_result($start_date, $time_logged);

        $count = 0;
        $result = array();

        while ($stmt->fetch()) {
            $arr['start_date'] = $start_date;
            $arr['time_logged'] = $time_logged;
            $result[$count] = $arr;
            $count++;
        }

        $stmt->close();

        $status['code'] = 1;
        $status['msg'] = "Success.";
        $status['payload'] = $result;

        return $status;
    } else {
        $status['code'] = 3;
        $status['msg'] = "There was an error accessing info.";
        $status['payload'] = null;

        return $status;
    }
}

/////////////////////////////LOG/////////////////////////////////////////////////////////////
function kts_log($log_msg, $log_filename)
 {

     if (!file_exists($log_filename))
     {
         // create directory/folder uploads.
         mkdir($log_filename, 0755, true);
     }
     $log_file_data = $log_filename.'/log_' . date('Y') . '.log';
     file_put_contents($log_file_data, $log_msg . "\r\n", FILE_APPEND);
 }

/////////////////////////////EMAIL MESSAGES/////////////////////////////////////////////////
function sendConfirmationEmail($sender, $receiver, $subject, $code, $name)
{

    // Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    // More headers
    $headers .= 'From: <' . $sender . '>' . "\r\n";
    //$headers .= 'Cc: myboss@example.com' . "\r\n";
    $message = "
  <!DOCTYPE HTML PUBLIC '-//W3C//DTD XHTML 1.0 Transitional //EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml' xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office'><head>
      <!--[if gte mso 9]><xml>
       <o:OfficeDocumentSettings>
        <o:AllowPNG/>
        <o:PixelsPerInch>96</o:PixelsPerInch>
       </o:OfficeDocumentSettings>
      </xml><![endif]-->
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
      <meta name='viewport' content='width=device-width'>
      <!--[if !mso]><!--><meta http-equiv='X-UA-Compatible' content='IE=edge'><!--<![endif]-->
      <title></title>
      <!--[if !mso]><!-- -->
  	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
  	<!--<![endif]-->

      <style type='text/css' id='media-query'>
        body {
    margin: 0;
    padding: 0; }

  table, tr, td {
    vertical-align: top;
    border-collapse: collapse; }

  .ie-browser table, .mso-container table {
    table-layout: fixed; }

  * {
    line-height: inherit; }

  a[x-apple-data-detectors=true] {
    color: inherit !important;
    text-decoration: none !important; }

  [owa] .img-container div, [owa] .img-container button {
    display: block !important; }

  [owa] .fullwidth button {
    width: 100% !important; }

  [owa] .block-grid .col {
    display: table-cell;
    float: none !important;
    vertical-align: top; }

  .ie-browser .num12, .ie-browser .block-grid, [owa] .num12, [owa] .block-grid {
    width: 600px !important; }

  .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
    line-height: 100%; }

  .ie-browser .mixed-two-up .num4, [owa] .mixed-two-up .num4 {
    width: 200px !important; }

  .ie-browser .mixed-two-up .num8, [owa] .mixed-two-up .num8 {
    width: 400px !important; }

  .ie-browser .block-grid.two-up .col, [owa] .block-grid.two-up .col {
    width: 300px !important; }

  .ie-browser .block-grid.three-up .col, [owa] .block-grid.three-up .col {
    width: 200px !important; }

  .ie-browser .block-grid.four-up .col, [owa] .block-grid.four-up .col {
    width: 150px !important; }

  .ie-browser .block-grid.five-up .col, [owa] .block-grid.five-up .col {
    width: 120px !important; }

  .ie-browser .block-grid.six-up .col, [owa] .block-grid.six-up .col {
    width: 100px !important; }

  .ie-browser .block-grid.seven-up .col, [owa] .block-grid.seven-up .col {
    width: 85px !important; }

  .ie-browser .block-grid.eight-up .col, [owa] .block-grid.eight-up .col {
    width: 75px !important; }

  .ie-browser .block-grid.nine-up .col, [owa] .block-grid.nine-up .col {
    width: 66px !important; }

  .ie-browser .block-grid.ten-up .col, [owa] .block-grid.ten-up .col {
    width: 60px !important; }

  .ie-browser .block-grid.eleven-up .col, [owa] .block-grid.eleven-up .col {
    width: 54px !important; }

  .ie-browser .block-grid.twelve-up .col, [owa] .block-grid.twelve-up .col {
    width: 50px !important; }

  @media only screen and (min-width: 620px) {
    .block-grid {
      width: 600px !important; }
    .block-grid .col {
      vertical-align: top; }
      .block-grid .col.num12 {
        width: 600px !important; }
    .block-grid.mixed-two-up .col.num4 {
      width: 200px !important; }
    .block-grid.mixed-two-up .col.num8 {
      width: 400px !important; }
    .block-grid.two-up .col {
      width: 300px !important; }
    .block-grid.three-up .col {
      width: 200px !important; }
    .block-grid.four-up .col {
      width: 150px !important; }
    .block-grid.five-up .col {
      width: 120px !important; }
    .block-grid.six-up .col {
      width: 100px !important; }
    .block-grid.seven-up .col {
      width: 85px !important; }
    .block-grid.eight-up .col {
      width: 75px !important; }
    .block-grid.nine-up .col {
      width: 66px !important; }
    .block-grid.ten-up .col {
      width: 60px !important; }
    .block-grid.eleven-up .col {
      width: 54px !important; }
    .block-grid.twelve-up .col {
      width: 50px !important; } }

  @media (max-width: 620px) {
    .block-grid, .col {
      min-width: 320px !important;
      max-width: 100% !important;
      display: block !important; }
    .block-grid {
      width: calc(100% - 40px) !important; }
    .col {
      width: 100% !important; }
      .col > div {
        margin: 0 auto; }
    img.fullwidth, img.fullwidthOnMobile {
      max-width: 100% !important; }
    .no-stack .col {
      min-width: 0 !important;
      display: table-cell !important; }
    .no-stack.two-up .col {
      width: 50% !important; }
    .no-stack.mixed-two-up .col.num4 {
      width: 33% !important; }
    .no-stack.mixed-two-up .col.num8 {
      width: 66% !important; }
    .no-stack.three-up .col.num4 {
      width: 33% !important; }
    .no-stack.four-up .col.num3 {
      width: 25% !important; } }

      </style>
  </head>
  <body class='clean-body' style='margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #e2eace'>
    <style type='text/css' id='media-query-bodytag'>
      @media (max-width: 520px) {
        .block-grid {
          min-width: 320px!important;
          max-width: 100%!important;
          width: 100%!important;
          display: block!important;
        }

        .col {
          min-width: 320px!important;
          max-width: 100%!important;
          width: 100%!important;
          display: block!important;
        }

          .col > div {
            margin: 0 auto;
          }

        img.fullwidth {
          max-width: 100%!important;
        }
  			img.fullwidthOnMobile {
          max-width: 100%!important;
        }
        .no-stack .col {
  				min-width: 0!important;
  				display: table-cell!important;
  			}
  			.no-stack.two-up .col {
  				width: 50%!important;
  			}
  			.no-stack.mixed-two-up .col.num4 {
  				width: 33%!important;
  			}
  			.no-stack.mixed-two-up .col.num8 {
  				width: 66%!important;
  			}
  			.no-stack.three-up .col.num4 {
  				width: 33%!important
  			}
  			.no-stack.four-up .col.num3 {
  				width: 25%!important
  			}
      }
    </style>
    <!--[if IE]><div class='ie-browser'><![endif]-->
    <!--[if mso]><div class='mso-container'><![endif]-->
    <table class='nl-container' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #e2eace;width: 100%' cellpadding='0' cellspacing='0'>
  	<tbody>
  	<tr style='vertical-align: top'>
  		<td style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
      <!--[if (mso)|(IE)]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td align='center' style='background-color: #e2eace;'><![endif]-->

      <div style='background-color:transparent;'>
        <div style='Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
          <div style='border-collapse: collapse;display: table;width: 100%;background-color:transparent;'>
            <!--[if (mso)|(IE)]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='background-color:transparent;' align='center'><table cellpadding='0' cellspacing='0' border='0' style='width: 600px;'><tr class='layout-full-width' style='background-color:transparent;'><![endif]-->

                <!--[if (mso)|(IE)]><td align='center' width='600' style=' width:600px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;' valign='top'><![endif]-->
              <div class='col num12' style='min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;'>
                <div style='background-color: transparent; width: 100% !important;'>
                <!--[if (!mso)&(!IE)]><!--><div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;'><!--<![endif]-->


                      <div align='center' class='img-container center  autowidth  fullwidth' style='padding-right: 0px;  padding-left: 0px;'>
  <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 0px; padding-left: 0px;' align='center'><![endif]-->
  <div style='line-height:25px;font-size:1px'>&#160;</div>  <img class='center  autowidth  fullwidth' align='center' border='0' src='https://www.kabtontech.com/wp-content/uploads/2018/01/rounder-up.png' alt='Image' title='Image' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 600px' width='600'>
  <!--[if mso]></td></tr></table><![endif]-->
  </div>


                <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                </div>
              </div>
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
          </div>
        </div>
      </div>    <div style='background-color:transparent;'>
        <div style='Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;' class='block-grid '>
          <div style='border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;'>
            <!--[if (mso)|(IE)]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='background-color:transparent;' align='center'><table cellpadding='0' cellspacing='0' border='0' style='width: 600px;'><tr class='layout-full-width' style='background-color:#FFFFFF;'><![endif]-->

                <!--[if (mso)|(IE)]><td align='center' width='600' style=' width:600px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;' valign='top'><![endif]-->
              <div class='col num12' style='min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;'>
                <div style='background-color: transparent; width: 100% !important;'>
                <!--[if (!mso)&(!IE)]><!--><div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'><!--<![endif]-->


                      <div align='center' class='img-container center  autowidth ' style='padding-right: 0px;  padding-left: 0px;'>
  <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 0px; padding-left: 0px;' align='center'><![endif]-->
    <img class='center  autowidth ' align='center' border='0' src='https://www.kabtontech.com/wp-content/uploads/2018/01/kts-logo-dark.png' alt='Kabton Tech Services - Insight' title='Kabton Tech Services - Insight' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 250px' width='250'>
  <!--[if mso]></td></tr></table><![endif]-->
  </div>



                      <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'><![endif]-->
  <div style='font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:150%;color:#555555; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
  	<div style='font-size:12px;line-height:18px;color:#555555;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 21px;text-align: center'>Welcome</p></div>
  </div>
  <!--[if mso]></td></tr></table><![endif]-->


                <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                </div>
              </div>
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
          </div>
        </div>
      </div>    <div style='background-color:transparent;'>
        <div style='Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;' class='block-grid '>
          <div style='border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;'>
            <!--[if (mso)|(IE)]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='background-color:transparent;' align='center'><table cellpadding='0' cellspacing='0' border='0' style='width: 600px;'><tr class='layout-full-width' style='background-color:#FFFFFF;'><![endif]-->

                <!--[if (mso)|(IE)]><td align='center' width='600' style=' width:600px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;' valign='top'><![endif]-->
              <div class='col num12' style='min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;'>
                <div style='background-color: transparent; width: 100% !important;'>
                <!--[if (!mso)&(!IE)]><!--><div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'><!--<![endif]-->


                      <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'><![endif]-->
  <div style='font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:120%;color:#0D0D0D; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
  	<div style='font-size:12px;line-height:14px;color:#0D0D0D;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 17px;text-align: center'><span style='font-size: 28px; line-height: 33px;'><strong><span style='line-height: 33px; font-size: 28px;'>Hello " . $name . ",</span></strong></span><br><span style='font-size: 28px; line-height: 33px;'>Registration completed</span></p></div>
  </div>
  <!--[if mso]></td></tr></table><![endif]-->



                      <div align='center' class='img-container center  autowidth ' style='padding-right: 0px;  padding-left: 0px;'>
  <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 0px; padding-left: 0px;' align='center'><![endif]-->
    <img class='center  autowidth ' align='center' border='0' src='https://www.kabtontech.com/wp-content/uploads/2018/01/divider.png' alt='Image' title='Image' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 316px' width='316'>
  <!--[if mso]></td></tr></table><![endif]-->
  </div>



                      <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'><![endif]-->
  <div style='font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:150%;color:#555555; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
  	<div style='font-size:12px;line-height:18px;color:#555555;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 21px;text-align: center'>Thanks so much for joining KTS Insight!<span style='color: rgb(168, 191, 111); font-size: 14px; line-height: 21px;'><strong><br></strong></span></p></div>
  </div>
  <!--[if mso]></td></tr></table><![endif]-->



                      <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 10px;'><![endif]-->
  <div style='font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:150%;color:#0D0D0D; padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 10px;'>
  	<div style='font-size:12px;line-height:18px;color:#0D0D0D;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 21px;text-align: center'>TO FINISH SIGNING UP,&#160; ACTIVATE YOUR ACCOUNT</p></div>
  </div>
  <!--[if mso]></td></tr></table><![endif]-->



<a href='http://kabtontech.com/insight/signup/ktssignup.php?confirmation=" . $code . "'>
  <div align='center' class='button-container center' style='padding-right: 10px; padding-left: 10px; padding-top:25px; padding-bottom:10px;'>

    <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0' style='border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;'><tr><td style='padding-right: 10px; padding-left: 10px; padding-top:25px; padding-bottom:10px;' align='center'><v:roundrect xmlns:v='urn:schemas-microsoft-com:vml' xmlns:w='urn:schemas-microsoft-com:office:word' href='' style='height:46pt; v-text-anchor:middle; width:172pt;' arcsize='7%' strokecolor='#EEE9B3' fillcolor='#EEE9B3'><w:anchorlock/><v:textbox inset='0,0,0,0'><center style='color:#0D0D0D; font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif; font-size:16px;'><![endif]-->
      <div style='color: #0D0D0D; background-color: #EEE9B3; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; max-width: 230px; width: 200px;width: auto; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent; padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px; font-family: 'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif; text-align: center; mso-border-alt: none;'>
        <span style='font-size:16px;line-height:32px;'>ACTIVATE MY ACCOUNT</span>
      </div>
    <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->

  </div>
  </a>



                      <table border='0' cellpadding='0' cellspacing='0' width='100%' class='divider' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
      <tbody>
          <tr style='vertical-align: top'>
              <td class='divider_inner' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-right: 10px;padding-left: 10px;padding-top: 30px;padding-bottom: 10px;min-width: 100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
                  <table class='divider_content' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 0px solid transparent;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
                      <tbody>
                          <tr style='vertical-align: top'>
                              <td style='word-break: break-word;border-collapse: collapse !important;vertical-align: top;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
                                  <span></span>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </td>
          </tr>
      </tbody>
  </table>


                      <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 10px;'><![endif]-->
  <div style='font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:150%;color:#0D0D0D; padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 10px;'>
  	<div style='font-size:12px;line-height:18px;color:#0D0D0D;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 21px;text-align: center'>OR ENTER CODE BELOW INTO KTS REGISTRATION FORM</p></div>
  </div>
  <!--[if mso]></td></tr></table><![endif]-->



                      <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'><![endif]-->
  <div style='font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:120%;color:#0D0D0D; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
  	<div style='line-height:14px;font-size:12px;color:#0D0D0D;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;'><p style='margin: 0;line-height: 14px;text-align: center;font-size: 12px'><span style='font-size: 28px; line-height: 33px;'><b>" . $code . "</b></span></p></div>
  </div>
  <!--[if mso]></td></tr></table><![endif]-->


                <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                </div>
              </div>
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
          </div>
        </div>
      </div>    <div style='background-color:transparent;'>
        <div style='Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #525252;' class='block-grid three-up '>
          <div style='border-collapse: collapse;display: table;width: 100%;background-color:#525252;'>
            <!--[if (mso)|(IE)]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='background-color:transparent;' align='center'><table cellpadding='0' cellspacing='0' border='0' style='width: 600px;'><tr class='layout-full-width' style='background-color:#525252;'><![endif]-->

                <!--[if (mso)|(IE)]><td align='center' width='200' style=' width:200px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;' valign='top'><![endif]-->
              <div class='col num4' style='max-width: 320px;min-width: 200px;display: table-cell;vertical-align: top;'>
                <div style='background-color: transparent; width: 100% !important;'>
                <!--[if (!mso)&(!IE)]><!--><div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;'><!--<![endif]-->



  <div align='center' style='padding-right: 0px; padding-left: 0px; padding-bottom: 0px;'>
    <div style='line-height:15px;font-size:1px'>&#160;</div>
    <div style='display: table; max-width:131px;'>
    <!--[if (mso)|(IE)]><table width='131' cellpadding='0' cellspacing='0' border='0'><tr><td style='border-collapse:collapse; padding-right: 0px; padding-left: 0px; padding-bottom: 0px;'  align='center'><table width='100%' cellpadding='0' cellspacing='0' border='0' style='border-collapse:collapse; mso-table-lspace: 0pt;mso-table-rspace: 0pt; width:131px;'><tr><td width='32' style='width:32px; padding-right: 5px;' valign='top'><![endif]-->
      <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px'>
        <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
          <a href='https://www.facebook.com/' title='Facebook' target='_blank'>
            <img src='https://www.kabtontech.com/wp-content/uploads/2018/01/facebook2x.png' alt='Facebook' title='Facebook' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
          </a>
        <div style='line-height:5px;font-size:1px'>&#160;</div>
        </td></tr>
      </tbody></table>
        <!--[if (mso)|(IE)]></td><td width='32' style='width:32px; padding-right: 5px;' valign='top'><![endif]-->
      <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px'>
        <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
          <a href='https://twitter.com/' title='Twitter' target='_blank'>
            <img src='https://www.kabtontech.com/wp-content/uploads/2018/01/twitter2x.png' alt='Twitter' title='Twitter' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
          </a>
        <div style='line-height:5px;font-size:1px'>&#160;</div>
        </td></tr>
      </tbody></table>
        <!--[if (mso)|(IE)]></td><td width='32' style='width:32px; padding-right: 0;' valign='top'><![endif]-->
      <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 0'>
        <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
          <a href='https://plus.google.com/' title='Google+' target='_blank'>
            <img src='https://www.kabtontech.com/wp-content/uploads/2018/01/googleplus2x.png' alt='Google+' title='Google+' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
          </a>
        <div style='line-height:5px;font-size:1px'>&#160;</div>
        </td></tr>
      </tbody></table>
      <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
    </div>
  </div>

                <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                </div>
              </div>
                <!--[if (mso)|(IE)]></td><td align='center' width='200' style=' width:200px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;' valign='top'><![endif]-->
              <div class='col num4' style='max-width: 320px;min-width: 200px;display: table-cell;vertical-align: top;'>
                <div style='background-color: transparent; width: 100% !important;'>
                <!--[if (!mso)&(!IE)]><!--><div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'><!--<![endif]-->


                      <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 0px; padding-left: 0px; padding-top: 20px; padding-bottom: 0px;'><![endif]-->
  <div style='font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:120%;color:#a8bf6f; padding-right: 0px; padding-left: 0px; padding-top: 20px; padding-bottom: 0px;'>
  	<div style='font-size:12px;line-height:14px;color:#a8bf6f;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px;text-align: center'><span style='color: rgb(255, 255, 255); font-size: 12px; line-height: 14px;'><span style='font-size: 12px; line-height: 14px; color: rgb(168, 191, 111);'>Tel.:</span> +1. 876. 304. 7184</span><br></p></div>
  </div>
  <!--[if mso]></td></tr></table><![endif]-->


                <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                </div>
              </div>
                <!--[if (mso)|(IE)]></td><td align='center' width='200' style=' width:200px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;' valign='top'><![endif]-->
              <div class='col num4' style='max-width: 320px;min-width: 200px;display: table-cell;vertical-align: top;'>
                <div style='background-color: transparent; width: 100% !important;'>
                <!--[if (!mso)&(!IE)]><!--><div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'><!--<![endif]-->


                      <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 0px; padding-left: 0px; padding-top: 20px; padding-bottom: 0px;'><![endif]-->
  <div style='font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;line-height:120%;color:#a8bf6f; padding-right: 0px; padding-left: 0px; padding-top: 20px; padding-bottom: 0px;'>
  	<div style='font-size:12px;line-height:14px;color:#a8bf6f;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px;text-align: center'>Email: <span style='color: rgb(255, 255, 255); font-size: 12px; line-height: 14px;'>info@kabtontech.com</span></p></div>
  </div>
  <!--[if mso]></td></tr></table><![endif]-->


                <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                </div>
              </div>
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
          </div>
        </div>
      </div>    <div style='background-color:transparent;'>
        <div style='Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
          <div style='border-collapse: collapse;display: table;width: 100%;background-color:transparent;'>
            <!--[if (mso)|(IE)]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='background-color:transparent;' align='center'><table cellpadding='0' cellspacing='0' border='0' style='width: 600px;'><tr class='layout-full-width' style='background-color:transparent;'><![endif]-->

                <!--[if (mso)|(IE)]><td align='center' width='600' style=' width:600px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;' valign='top'><![endif]-->
              <div class='col num12' style='min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;'>
                <div style='background-color: transparent; width: 100% !important;'>
                <!--[if (!mso)&(!IE)]><!--><div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'><!--<![endif]-->


                      <div align='center' class='img-container center  autowidth  fullwidth' style='padding-right: 0px;  padding-left: 0px;'>
  <!--[if mso]><table width='100%' cellpadding='0' cellspacing='0' border='0'><tr><td style='padding-right: 0px; padding-left: 0px;' align='center'><![endif]-->
    <img class='center  autowidth  fullwidth' align='center' border='0' src='https://www.kabtontech.com/wp-content/uploads/2018/01/rounder-dwn.png' alt='Image' title='Image' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 600px' width='600'>
  <!--[if mso]></td></tr></table><![endif]-->
  </div>



                      <table border='0' cellpadding='0' cellspacing='0' width='100%' class='divider' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
      <tbody>
          <tr style='vertical-align: top'>
              <td class='divider_inner' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-right: 30px;padding-left: 30px;padding-top: 30px;padding-bottom: 30px;min-width: 100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
                  <table class='divider_content' align='center' border='0' cellpadding='0' cellspacing='0' width='100%' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 0px solid transparent;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
                      <tbody>
                          <tr style='vertical-align: top'>
                              <td style='word-break: break-word;border-collapse: collapse !important;vertical-align: top;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%'>
                                  <span></span>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </td>
          </tr>
      </tbody>
  </table>

                <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                </div>
              </div>
            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
          </div>
        </div>
      </div>   <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
  		</td>
    </tr>
    </tbody>
    </table>
    <!--[if (mso)|(IE)]></div><![endif]-->


  </body></html>


  ";

    mail($receiver, $subject, $message, $headers);
}
