<?php
require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if ($type != '3') {
    $data = getHosting($cid, '1', $db) ['payload']; //client
} else {
    $data = getHosting($cid, '2', $db) ['payload']; //employee
}

if (!isset($_SESSION['cid'])) {
    serveLogout();
} elseif ($data == null || $data == "") {
    echo notFound('6'); // no web hosting found
} else {
    echo "
<div class='col-md-12 col-sm-12 col-xs-12'>
  <div class='x_panel'>
    <div class='x_title'>
      <h2>Web Hosting Plans <small>- .$company.</small></h2>
      <!--ul class='nav navbar-right panel_toolbox'>
        <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a>
        </li>
        <li class='dropdown'>
          <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'><i class='fa fa-wrench'></i></a>
          <ul class='dropdown-menu' role='menu'>
            <li><a href='#'>Settings 1</a>
            </li>
            <li><a href='#'>Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class='close-link'><i class='fa fa-close'></i></a>
        </li>
      </ul-->
      <div class='clearfix'></div>
    </div>

    <div class='x_content'>

      <p></p>

      <div class='table-responsive'>
        <table class='table table-striped jambo_table ' id='hosting-table'>
          <thead>
            <tr class='headings'>

              <th class='column-title'>Item </th>
              <th class='column-title'>Website </th>
              <th class='column-title'>Duration (Years) </th>
              <th class='column-title'>Purchase Date </th>
              <th class='column-title'>Expires </th>
              <th class='bulk-actions' colspan='7'>
                <a class='antoo' style='color:#fff; font-weight:500;'>Bulk Actions ( <span class='action-cnt'> </span> ) <i class='fa fa-chevron-down'></i></a>
              </th>
            </tr>
          </thead>

          <tbody>"; ?>

          <?php
    if ($type == '3') {
        $hv = 'hosting-view';
    } else {
        $hv = '';
    }
    $count = 1;
    foreach ($data as $k) {
        if ($count % 2 == 0) {
            echo "<tr class='even pointer $hv' data-chid='" . ktsEncode($k['hid']) . "'>";
        } else {
            echo "<tr class='odd pointer $hv' data-chid='" . ktsEncode($k['hid']) . "'>";
        }

        echo "
              <td class=' view-invoice-details'><a href='#'>" . $count . "</a></td>
              <td class=' '>" . $k['domain'] . "</td>
              <td class=' '>" . $k['duration'] . "</td>
              <td class=' '>" . $k['buy_date'] . "</td>
              <td class=' '>" . $k['expire'] . " </td>
            </tr>";

        $count++;
    }

    echo "
          </tbody>
        </table>
      </div>


    </div>
  </div>
</div>
";
}
?>
