<?php
require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');


if (!isset($_POST['cpid'])) {
    echo notFound('1');
} else {
    $cpid = $_POST['cpid'];
    $cpid2 = ktsDecode($cpid);

    if(isset($_REQUEST['operation']) && $_REQUEST['operation'] == 'old'){
      $op = 'proforma';
      if(!empty($op)) $opString = "Receipt";
      $data = getInvoice($cpid2, '3a', $db, $db2);
      $data3 = getInvoiceItems($cid, $cpid2,'2', $db);
      $op = $_REQUEST['operation'];
    }
    elseif(isset($_REQUEST['operation']) && $_REQUEST['operation'] == 'proforma'){
      $op = 'proforma';
      if(!empty($op)) $opString = "Proforma Invoice";
      $data = getInvoice($cpid2, '4b', $db, $db2);
      $data3 = getInvoiceItems($cid, $cpid2,'3', $db);
      $op = $_REQUEST['operation'];
    }
    else{
      $op = 'invoice';
      if(!empty($op)) $opString = "Invoice";
      $data = getInvoice($cpid2, '3', $db, $db2);
      $data3 = getInvoiceItems($cid, $cpid2, '1', $db);
      $op='';
    }

    //print_r($data);


    //$data = getInvoice($cpid2, '3', $db, $db2);
    $data2 = getClientAddress($cid, $db);

    $data4 = getCurrentProjectDetails($cpid2, $db)['payload'][0];

    $due_date = strtotime($data4['pymnt_due']);
    $legacy_date = strtotime('2019-02-16');

    if($due_date < $legacy_date){
      $due_date = date('F d, Y', strtotime($data[0]['date']. ' + ' .$due_interval));
    }
    else{
      $due_date = date('F d, Y', $due_date);
    }

    echo "
<div class='invoice'>
  <div class='page-title'>
    <div class='title_left'>
      <h3><small></small></h3>
    </div>

    <div class='title_right'>

    </div>
  </div>

  <div class='clearfix'></div>

  <div class='row'>
    <div class='col-md-12'>
      <div class='x_panel'>
        <div class='x_title'>
          <h2>".$opString." <small>- ".$company."</small></h2>

          <div class='clearfix'></div>
        </div>
        <div class='x_content'>

          <section class='content invoice'>";

          if($op == 'old')
          echo "<img class='invoice-back' id='paid-logo' src='images/paid.png' alt='Receipt'>";

          if($op == 'proforma')
          echo "<img class='invoice-back' id='paid-logo' src='images/proforma.png' alt='proforma Invoice'>";


          echo "
            <!-- title row -->
            <div class='row'>
              <div class='col-xs-12 invoice-header'>
                <img class='pull-left' id='invoice-logo' src='images/logoc.png' alt='Kabton Tech Services Logo'>
                <small class='pull-right'>Date Created: ".$data[0]['date']."</small>
              </div>
              <!-- /.col -->
            </div>
            <div class='row spacer2'></div>
            <!-- info row -->
            <div class='row invoice-info'>
              <div class='col-sm-4 invoice-col'>
              From:
                <address>
                                <strong>Kevin Beaton</strong>
                                <br>Kabton Technology Services
                            </address>
              </div>
              <!-- /.col -->
              <div class='col-sm-4 invoice-col'>
                Bill To:
                <address>
                                <strong>".$data[0]['name']."</strong>
                                <br>".$data2[0]['country']."
                            </address>
              </div>
              <!-- /.col -->
              <div class='col-sm-4 invoice-col'>
                Invoice #: <b>".$data[0]['invoice']."</b>
                <br>
                Payment Due: <b>".$due_date."</b>
                <br>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class='row spacer2'></div>
            <!-- Table row -->
            <div class='row'>
              <div class='col-xs-12 table'>
                <table class='table table-striped'>
                  <thead class='item-padding' >
                    <tr>
                    <th class='i-tem' style='width: 59%' ><div class='invoice-item invoice-item-left'>Item</div></th>
                    <th class='i-tem'><div class='invoice-item'>Qty</div></th>
                    <th class='i-tem'><div class='invoice-item'>Rate</div></th>
                    <th class='i-tem'><div class='invoice-item invoice-item-right'>Subtotal</div></th>
                  </tr>
                  </thead>
                  <tbody>
                  "; ?>
                  <?php
                  $tax = 0;
    $shipping = 0;
    $total = 0;
    foreach ($data3 as $v) {
        if ($type != 3) {
            $amount = fxConvert($cid, $v['amount'], $v['date'], $us_dollar, $db)['payload'];
            $tax = fxConvert($cid, $v['tax'], $v['date'], $us_dollar, $db)['payload'];
            $shipping = fxConvert($cid, $v['shipping'], $v['date'], $us_dollar, $db)['payload'];
        } else {
            $amount = $v['amount'];
            $tax = $v['tax'];
            $shipping = $v['shipping'];
        }
        echo "
                    <tr>
                    <td>".$v['description']."</td>
                    <td>".$v['qty']."</td>
                      <td>$".number_format($amount, 2, ".", ",")."</td>
                      <td>$".number_format($amount * $v['qty'], 2, ".", ",")."</td>
                    </tr>";
        $item_total = $amount * $v['qty'];

        $tax = ($tax/100) * $item_total;
        $shipping = $shipping;

        $total += $item_total;
    }

    $currency = '';

    if ($data[0]['currency'] == 'jmd') {
        $currency = 'JM';
    } else {
        $currency = 'US';
    }

    echo
                  "

                  </tbody>
                </table>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class='row spacer2'></div>

            <div class='row'>
              <!-- accepted payments column -->
              <div class='col-xs-6'>
                <p class='lead'>Payment Methods:</p>
                <img id='ncbpic' src='images/ncb.png' alt='National Commercial Bank'>
                <img id='scotiapic' src='images/scotia.png' alt='Scotia Bank'>
                <img id='paypalpic' src='images/paypal.png' alt='Paypal'>
                <!--p class='text-muted well well-sm no-shadow' style='margin-top: 10px;'></p--><hr/>

                Terms:
                <ul>
                <li>A 50% deposit must be made before the start of all software development projects.</li>
                <li>All payments can be made either directly, via National Commercial Bank (#854085786), or Scotia Bank (#430660 Christiana).</li>

                </ul>

                <br>

                Thank you for choosing Kabton Tech Services.

              </div>
              <!-- /.col -->
              <div class='col-xs-6'>
                <p class='lead'>Amount Due:</p>
                <div class='table-responsive'>
                  <table class='table'>
                    <tbody
                      <tr>
                        <th style='width:50%'>Subtotal:</th>
                        <td>".$currency."$".number_format($total, 2, ".", ",")."</td>
                      </tr>"; ?>

                      <?php
                      if ($tax > 0) {
                          echo "
                      <tr>
                        <th>Tax</th>
                        <td>".$currency."$".number_format($tax, 2, ".", ",")."</td>
                      </tr>";
                      } ?>

                    <?php

                    if ($shipping > 0) {
                        echo "
                      <tr>
                        <th>Shipping:</th>
                        <td>".$currency."$".number_format($shipping, 2, ".", ",")."</td>
                      </tr>";
                    }
    echo "
                      <tr>
                        <th>Total:</th>
                        <td>".$currency."$".number_format($total + $tax + $shipping, 2, ".", ",")."</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class='row spacer2'></div>
            <div class='row spacer2'></div>

            <div class='invoice-contact row'><div class='kts-contact-invoice-item'><i class='fa fa-phone kts-invoice-icon'></i>    ".$kts_phone1."</div>  <div class='kts-contact-invoice-item'><i class='fa fa-envelope kts-invoice-icon'></i>    ".$kts_email1."</div>  <div class='kts-contact-invoice-item'><i class='fa fa-desktop kts-invoice-icon'></i>  ".$kts_website."</div></div>

            <div class='row spacer2'></div>
            <div class='row spacer2'></div>

            <!-- this row will not appear when printing -->
            <div class='row no-print'>
              <div class='col-xs-12'>
                <button class='btn btn-default' onclick='window.print();'><i class='fa fa-print'></i> Print</button>
                <button class='btn btn-success pull-right'><i class='fa fa-credit-card'></i> Submit Payment</button>
                <button class='btn btn-primary pull-right' style='margin-right: 5px;'><i class='fa fa-download'></i> Generate PDF</button>
              </div>
            </div>
          </section>

        </div>



      </div>
    </div>
  </div>

";
}
?>
