<?php
require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

$request_search = getProjectRequest($cid, '0', '10', $db);
$requests = null;

if ($request_search['code'] == '1') {
    $requests = $request_search['payload'];
}

?>

<div class='col-md-12 col-sm-12 col-xs-12'>
                <div class='x_panel'>
                  <div class='x_title'>
                    <h2>Request New Project <small></small></h2>

                    <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>
                    <br>

                    <?php
                    $request_count = count($requests);



                     if ($request_count > 0) {
                         echo "

                    <div class='row' id='previously-requested'>
                      <div class='accordion col-md-8 col-sm-5 col-xs-12' id='request-accordian' role='tablist' aria-multiselectable='true'>
                        <div class='panel'>
                          <a class='panel-heading collapsed' role='tab' id='headingOne1' data-toggle='collapse' data-parent='#accordion1' href='#collapseOne1' aria-expanded='false' aria-controls='collapseOne'>
                            <h4 class='panel-title'>Previously Requested Projects  <b>(".$request_count.")</b>   <span class=' fa fa-angle-down' style='float:right'></span></h4>
                          </a>
                          <div id='collapseOne1' class='panel-collapse collapse' role='tabpanel' aria-labelledby='headingOne'>
                            <div class='panel-body'>
                            <table class='table table-striped'>
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Project Type</th>
                                  <th>Date Requested</th>
                                  <th>Status</th>
                                </tr>
                              </thead>
                              <tbody>";

                         $count = 1;
                         foreach ($requests as $r) {
                             echo "

                                  <tr>
                                    <th scope='row'>".$count."</th>
                                    <td>".$r['rtype']."</td>
                                    <td>".$r['date_requested']."</td>
                                    <td>"."In Review"."</td>
                                  </tr>";

                             $count++;
                         }

                         echo "
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    ";
                     }?>

                    <br>
                    <form id='request-form'  class='form-horizontal form-label-left' novalidate=''>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='project-type'>Project Type <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <select id='project-type' name='project-type' required='required' class='form-control'>

                            <option value=''>Choose option</option>

                            <?php foreach ($kts_services as $value) {
                         echo '<option value='.$value.'>'.$value.'</option>';
                     } ?>

                          </select>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='request-date-picker'>Desired Start Date <span class='required'>*</span>
                        </label>
                        <div class='input-group date col-md-4 col-sm-4col-xs-10 request-date-picker' id='request-date-picker'>
                            <input type='text' id='request-date' name='request-date-picker' placeholder='YYYY-MM-DD'class='form-control request-date-picker'>
                            <span class='input-group-addon'>
                               <span class='glyphicon glyphicon-calendar'></span>
                            </span>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>General Description of Project</label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <div class='btn-toolbar editor' data-role='editor-toolbar' data-target='#editor-one'>
                            <div class='btn-group'>
                              <a class='btn dropdown-toggle' data-toggle='dropdown' title='Font'><i class='fa fa-font'></i><b class='caret'></b></a>
                              <ul class='dropdown-menu'>
                              </ul>
                            </div>

                            <div class='btn-group'>
                              <a class='btn dropdown-toggle' data-toggle='dropdown' title='Font Size'><i class='fa fa-text-height'></i>&nbsp;<b class='caret'></b></a>
                              <ul class='dropdown-menu'>
                                <li>
                                  <a data-edit='fontSize 5'>
                                    <p style='font-size:17px'>Huge</p>
                                  </a>
                                </li>
                                <li>
                                  <a data-edit='fontSize 3'>
                                    <p style='font-size:14px'>Normal</p>
                                  </a>
                                </li>
                                <li>
                                  <a data-edit='fontSize 1'>
                                    <p style='font-size:11px'>Small</p>
                                  </a>
                                </li>
                              </ul>
                            </div>

                            <div class='btn-group'>
                              <a class='btn' data-edit='bold' title='Bold (Ctrl/Cmd+B)'><i class='fa fa-bold'></i></a>
                              <a class='btn' data-edit='italic' title='Italic (Ctrl/Cmd+I)'><i class='fa fa-italic'></i></a>
                              <a class='btn' data-edit='strikethrough' title='Strikethrough'><i class='fa fa-strikethrough'></i></a>
                              <a class='btn' data-edit='underline' title='Underline (Ctrl/Cmd+U)'><i class='fa fa-underline'></i></a>
                            </div>

                            <div class='btn-group'>
                              <a class='btn' data-edit='insertunorderedlist' title='Bullet list'><i class='fa fa-list-ul'></i></a>
                              <a class='btn' data-edit='insertorderedlist' title='Number list'><i class='fa fa-list-ol'></i></a>
                              <a class='btn' data-edit='outdent' title='Reduce indent (Shift+Tab)'><i class='fa fa-dedent'></i></a>
                              <a class='btn' data-edit='indent' title='Indent (Tab)'><i class='fa fa-indent'></i></a>
                            </div>

                            <div class='btn-group'>
                              <a class='btn' data-edit='justifyleft' title='Align Left (Ctrl/Cmd+L)'><i class='fa fa-align-left'></i></a>
                              <a class='btn' data-edit='justifycenter' title='Center (Ctrl/Cmd+E)'><i class='fa fa-align-center'></i></a>
                              <a class='btn' data-edit='justifyright' title='Align Right (Ctrl/Cmd+R)'><i class='fa fa-align-right'></i></a>
                              <a class='btn' data-edit='justifyfull' title='Justify (Ctrl/Cmd+J)'><i class='fa fa-align-justify'></i></a>
                            </div>

                            <div class='btn-group'>
                              <a class='btn dropdown-toggle' data-toggle='dropdown' title='Hyperlink'><i class='fa fa-link'></i></a>
                              <div class='dropdown-menu input-append'>
                                <input class='span2' placeholder='URL' type='text' data-edit='createLink' />
                                <button class='btn' type='button'>Add</button>
                              </div>
                              <a class='btn' data-edit='unlink' title='Remove Hyperlink'><i class='fa fa-cut'></i></a>
                            </div>


                            <div class='btn-group'>
                              <a class='btn' data-edit='undo' title='Undo (Ctrl/Cmd+Z)'><i class='fa fa-undo'></i></a>
                              <a class='btn' data-edit='redo' title='Redo (Ctrl/Cmd+Y)'><i class='fa fa-repeat'></i></a>
                            </div>
                          </div>

                          <div id='editor-one' class='editor-wrapper'></div>

                          <textarea name='descr' id='descr' style='display:none;'></textarea>

                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='request-email'>Email <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='request-email' name='email' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='request-phone'>Phone# </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='request-phone' name='phone' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='request_communication'>Primary Communication Method <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <select id='request_communication' name='request_communication' required='required' class='form-control'>

                            <option value=''>Choose option</option>

                            <?php foreach ($communication_options as $value) {
                         echo '<option value='.$value.'>'.$value.'</option>';
                     } ?>

                          </select>
                        </div>
                      </div>
                      <div class='ln_solid'></div>
                      <div class='form-group'>
                        <div class='col-md-6 col-sm-6 col-xs-12 col-md-offset-3'>
                          <button class='btn btn-primary request_reset' type='button'>Cancel</button>
                          <button class='btn btn-primary request_reset' type='button'>Reset</button>
                          <button type='button' class='btn btn-success request_submit'>Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
