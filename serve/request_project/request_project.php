<?php

require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if ($_POST['option'] == 'request_project') {

  //requestProject('7', 'sm', 'sm bro, sm', '2017-12-01', '192388', 'j@h.com', 'text', $db);
    //cid, type, description, start_date, phone, email, preferred_method, date_requested

    if (/*isset($_POST['cid']) && !empty(trim($_POST['cid'])) &&*/ isset($_POST['type']) && !empty(trim($_POST['type']))
    && isset($_POST['description']) && !empty(trim($_POST['description'])) && isset($_POST['start_date']) && !empty(trim($_POST['start_date']))
    && isset($_POST['phone']) && !empty(trim($_POST['phone'])) && isset($_POST['email']) && !empty(trim($_POST['email']))
    && isset($_POST['preferred_method']) && !empty(trim($_POST['preferred_method']))
    ) {



    //$cid = mysqli_real_escape_string($db, $cid);
        $rtype = mysqli_real_escape_string($db, $_POST['type']);
        $description =  mysqli_real_escape_string($db, $_POST['description']);
        $start_date =  mysqli_real_escape_string($db, $_POST['start_date']);
        $phone =  mysqli_real_escape_string($db, $_POST['phone']);
        $email =  mysqli_real_escape_string($db, $_POST['email']);
        $preferred_method =  mysqli_real_escape_string($db, $_POST['preferred_method']);

        //sendMessage($id, $type, $to, $to_type, $mode, $reply_from, $oldmsgid, $oldroot, $subject, $msg, $db)

        $result = requestProject($cid, $rtype, $description, $start_date, $phone, $email, $preferred_method, $db);
        echo json_encode($result);
        return $result;
    } else {
        $result =  genResult('4', 'Request was not successfully sent. Please make sure to fill all required fields.', null);
        echo json_encode($result);
        return $result;
    }
} else {
    $result = genResult('2', 'Message was not successfully sent. (16-8-16)', null);
    echo json_encode($result);
    return $result;
}
