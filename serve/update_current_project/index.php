<?php


require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if ($type != '3' || !isset($_POST['cpid']) || empty($_POST['cpid'])) {
    notFound('1');
    return false;
}

$realcpid = $_POST['cpid'];
$cpid = ktsDecode($_POST['cpid']);
$project_details = getCurrentProjectDetails($cpid, $db)['payload'][0];
$employees = getAllEmployees($db)['payload'];
$clients = getAllClients($db)['payload'];

$invoice = getInvoiceItems($cid, $cpid, '1', $db);

$project_balance = getInvoice($cpid, '3', $db, $db2)[0];
$project_balance = ceil($project_balance['amount']);
$deposit = ceil($project_details['deposit']);
$balance_due =  ceil($project_balance-$deposit);



if ($project_details != null) {
    $the_client = array_search($project_details['cid'], array_column($clients, 'cid'));
    $the_client = $clients[$the_client];

    $the_leader = array_search($project_details['project_leader'], array_column($employees, 'eid'));
    $the_leader = $employees[$the_leader];
} else {
    notFound('1');
    return false;
}


?>

<div class='col-md-12 col-sm-12 col-xs-12'>
                <div class='x_panel'>
                  <div class='x_title'>
                    <h2>Update Project <small></small></h2>

                    <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>
                              <ul class='stats-overview'>
                                <li class='current_project'>
                                  <span class='name'> </span>
                                  <span class='value text-success'> <span class='glyphicon glyphicon-chevron-left' aria-hidden=''true'></span> </span>
                                </li>
                                <li class='edit_current_project' data-cpid='<?php echo $realcpid;?>'>
                                  <span class='name'> </span>
                                  <span class='value text-success'> <span class='glyphicon glyphicon-refresh' aria-hidden=''true'></span> </span>
                                </li>
                                <li class=''>
                                  <span class='name'> </span>
                                  <span class='value text-success'> <span class='glyphicon glyphicon-minus' aria-hidden=''true'></span> </span>
                                </li>
                                <li>
                                  <span class='name'> Project Cost </span>
                                  <span class='value text-success'>$<?php echo number_format($project_balance, 0, ".", ",");?></span>
                                </li>
                                <li>
                                  <span class='name'> Deposit </span>
                                  <span class='value text-success'>$<?php echo number_format($deposit, 0, ".", ","); ?></span>
                                </li>
                                <li class='hidden-phone'>
                                  <span class='name'> Total Balance Due </span>
                                  <span class='value text-success'>$<?php echo number_format($balance_due, 0, ".", ",") ;?></span>
                                </li>
                              </ul>
                    <br>


                    <br>
                    <form id='update-project-form'  class='form-horizontal form-label-left' novalidate=''>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-project-client'>Client <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <select id='update-project-client' name='update-project-client' required='required' class='form-control'>

                            <option value='<?php echo ktsEncode($project_details['cid']);  ?>'><?php echo $the_client['fname']." ".$the_client['lname'] ?></option>

                            <?php foreach ($clients as $value) {
    echo "<option value='".ktsEncode($value['cid'])."'>".$value['fname']." ".$value['lname']." (<b>".$value['name']."</b>)</option>";
} ?>

                          </select>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-project-name'>Project Name <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='update-project-name' name='update-project-name' value='<?php echo $project_details['project_name'];  ?>' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>


                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-project-type'>Project Type <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <select id='update-project-type' name='update-project-type' required='required' class='form-control'>

                            <option value='<?php echo $project_details['project_type'];  ?>'><?php echo $project_details['project_type'];  ?></option>

                            <?php foreach ($kts_services as $value) {
    echo "<option value='".$value."'>".$value."</option>";
} ?>

                          </select>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-project-leader'>Project Leader <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <select id='update-project-leader' name='update-project-leader' required='required' class='form-control'>

                            <option value='<?php echo ktsEncode($the_leader['eid']);  ?>'><?php echo $the_leader['fname']." ".$the_leader['lname'];  ?></option>

                            <?php foreach ($employees as $value) {
    echo '<option value='.ktsEncode($value['eid']).'>'.$value['fname']." ".$value['lname'].'</option>';
} ?>

                          </select>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-start-date-picker'>Desired Start Date <span class='required'>*</span>
                        </label>
                        <div class='input-group date col-md-4 col-sm-4col-xs-10 start-date-picker' id='request-date-picker'>
                            <input type='text' id='update-start-date-picker' value='' name='update-start-date-picker' placeholder='YYYY-MM-DD'class='form-control start-date-picker'>
                            <span class='input-group-addon'>
                               <span class='glyphicon glyphicon-calendar'></span>
                            </span>
                        </div>

                      </div>
                      <script> $('.start-date-picker').val('<?php echo $project_details['start_date'];?>'); </script>


                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='pend-date-picker'>Projected End Date <span class='required'>*</span>
                        </label>
                        <div class='input-group date col-md-4 col-sm-4col-xs-10 pend-date-picker' id='request-date-picker'>
                            <input type='text' id='update-pend-date-picker' name='update-pend-date-picker' placeholder='YYYY-MM-DD'class='form-control pend-date-picker'>
                            <span class='input-group-addon'>
                               <span class='glyphicon glyphicon-calendar'></span>
                            </span>
                        </div>
                      </div>
                      <script> $('.pend-date-picker').val('<?php echo $project_details['projected_end'];?>'); </script>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-project-progress'>Project Progress (%) <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='number' min='0' max='100' id='update-project-progress' name='update-project-progress' value='<?php echo $project_details['progress'];?>' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-project-status'>Project Status <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <select id='update-project-status' name='update-project-status' required='required' class='form-control'>

                            <option  value='<?php echo $project_details['status']; ?>'><?php echo $project_details['status']; ?></option>

                            <?php foreach ($kts_project_status as $value) {
    echo "<option value='".$value."'>".$value."</option>";
} ?>

                          </select>
                        </div>
                      </div>


                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>General Description of Project</label>
                        <div class='col-md-6 col-sm-6 col-xs-12' id='kts-editor-one'>
                          <div class='btn-toolbar editor' data-role='editor-toolbar' data-target='#editor-one'>
                            <div class='btn-group'>
                              <a class='btn dropdown-toggle' data-toggle='dropdown' title='Font'><i class='fa fa-font'></i><b class='caret'></b></a>
                              <ul class='dropdown-menu'>
                              </ul>
                            </div>

                            <div class='btn-group'>
                              <a class='btn dropdown-toggle' data-toggle='dropdown' title='Font Size'><i class='fa fa-text-height'></i>&nbsp;<b class='caret'></b></a>
                              <ul class='dropdown-menu'>
                                <li>
                                  <a data-edit='fontSize 5'>
                                    <p style='font-size:17px'>Huge</p>
                                  </a>
                                </li>
                                <li>
                                  <a data-edit='fontSize 3'>
                                    <p style='font-size:14px'>Normal</p>
                                  </a>
                                </li>
                                <li>
                                  <a data-edit='fontSize 1'>
                                    <p style='font-size:11px'>Small</p>
                                  </a>
                                </li>
                              </ul>
                            </div>

                            <div class='btn-group'>
                              <a class='btn' data-edit='bold' title='Bold (Ctrl/Cmd+B)'><i class='fa fa-bold'></i></a>
                              <a class='btn' data-edit='italic' title='Italic (Ctrl/Cmd+I)'><i class='fa fa-italic'></i></a>
                              <a class='btn' data-edit='strikethrough' title='Strikethrough'><i class='fa fa-strikethrough'></i></a>
                              <a class='btn' data-edit='underline' title='Underline (Ctrl/Cmd+U)'><i class='fa fa-underline'></i></a>
                            </div>

                            <div class='btn-group'>
                              <a class='btn' data-edit='insertunorderedlist' title='Bullet list'><i class='fa fa-list-ul'></i></a>
                              <a class='btn' data-edit='insertorderedlist' title='Number list'><i class='fa fa-list-ol'></i></a>
                              <a class='btn' data-edit='outdent' title='Reduce indent (Shift+Tab)'><i class='fa fa-dedent'></i></a>
                              <a class='btn' data-edit='indent' title='Indent (Tab)'><i class='fa fa-indent'></i></a>
                            </div>

                            <div class='btn-group'>
                              <a class='btn' data-edit='justifyleft' title='Align Left (Ctrl/Cmd+L)'><i class='fa fa-align-left'></i></a>
                              <a class='btn' data-edit='justifycenter' title='Center (Ctrl/Cmd+E)'><i class='fa fa-align-center'></i></a>
                              <a class='btn' data-edit='justifyright' title='Align Right (Ctrl/Cmd+R)'><i class='fa fa-align-right'></i></a>
                              <a class='btn' data-edit='justifyfull' title='Justify (Ctrl/Cmd+J)'><i class='fa fa-align-justify'></i></a>
                            </div>

                            <div class='btn-group'>
                              <a class='btn dropdown-toggle' data-toggle='dropdown' title='Hyperlink'><i class='fa fa-link'></i></a>
                              <div class='dropdown-menu input-append'>
                                <input class='span2' placeholder='URL' type='text' data-edit='createLink' />
                                <button class='btn' type='button'>Add</button>
                              </div>
                              <a class='btn' data-edit='unlink' title='Remove Hyperlink'><i class='fa fa-cut'></i></a>
                            </div>


                            <div class='btn-group'>
                              <a class='btn' data-edit='undo' title='Undo (Ctrl/Cmd+Z)'><i class='fa fa-undo'></i></a>
                              <a class='btn' data-edit='redo' title='Redo (Ctrl/Cmd+Y)'><i class='fa fa-repeat'></i></a>
                            </div>
                          </div>

                          <div id='editor-one' class='editor-wrapper'></div>
                          <script> $('#editor-one').html('<?php echo $project_details['description']; ?>');
                                    $('#editor-one').val('<?php echo $project_details['description']; ?>');</script>

                          <textarea name='descr' id='descr' style='display:none;'></textarea>

                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-project-hours'>Project Hours <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='number' min='0'  id='update-project-hours' name='update-project-hours' value='<?php echo $project_details['hours'];?>' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-project-deposit'>Deposit ($)<span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='number' id='update-project-deposit'  name='update-project-deposit' value='<?php echo $project_details['deposit']; ?>' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>


                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='pymnt-date-picker'>Invoice Payment Date <span class='required'>*</span>
                        </label>
                        <div class='input-group date col-md-4 col-sm-4col-xs-10 pymnt-date-picker' id='request-date-picker'>
                            <input type='text' id='update-pymnt-date-picker' name='update-pymnt-date-picker' placeholder='YYYY-MM-DD'class='form-control pymnt-date-picker'>
                            <span class='input-group-addon'>
                               <span class='glyphicon glyphicon-calendar'></span>
                            </span>
                        </div>
                      </div>
                      <script> $('.pymnt-date-picker').val('<?php echo $project_details['pymnt_due'];?>'); </script>




                      <div class='ln_solid'></div>

                    </form>
                  </div>


                  <div class="x_title">
                    <h2>Billable Items <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" style="display: block;">
                    <table class="table table-hover billable-table">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Description</th>
                          <th>Qty</th>
                          <th>Rate ($)</th>
                          <th>Tax ($)</th>
                          <th>Shipping ($)</th>
                          <th>Subtotal ($)</th>
                          <th>Estimate</th>
                          <th>Paid</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php

                        $count = 1;
                        foreach ($invoice as $v) {
                            echo "

                          <tr data-iiid='".ktsEncode($v['iiid'])."'>
                          <td>".$count."</td>
                          <td><input class='puii-descrip' type='text' value='".$v['description']."' size='150'></td>
                          <td><input class='puii-qty' type='text' min='0' value='".$v['qty']."' size='5'></td>
                          <td><input class='puii-amount' type='text' min='0' value='".$v['amount']."' size='20'></td>
                          <td><input class='puii-tax' type='text' min='0' value='".$v['tax']."' size='10'></td>
                          <td><input class='puii-ship' type='text' min='0' value='".$v['shipping']."' size='10'></td>
                          <td><input class='puii-subtotal' value='".$v['amount'] * $v['qty']."' size='20' disabled></td>";
                          if (trim($v['estimate']) == '1') {
                              echo "
                          <td>
                          <select class='puii-estimate'>
                          <option value='1' size='5'>Yes</option>
                          <option value='0' size='5'>No</option>
                          </select>
                          </td>
                          ";
                        } elseif (trim($v['estimate']) == '0') {
                              echo "
                            <td>
                            <select id='k' class='puii-estimate'>
                            <option value='0' size='5'>No</option>
                            <option value='1' size='5'>Yes</option>
                            </select>
                            </td>
                            ";
                          }
                            if (trim($v['paid']) == '1') {
                                echo "
                            <td>
                            <select class='puii-paid'>
                            <option value='1' size='5'>Yes</option>
                            <option value='0' size='5'>No</option>
                            </select>
                            </td>
                            ";
                            } elseif (trim($v['paid']) == '0') {
                                echo "
                              <td>
                              <select id='k' class='puii-paid'>
                              <option value='0' size='5'>No</option>
                              <option value='1' size='5'>Yes</option>
                              </select>
                              </td>
                              ";
                            }


                            echo "
                            <td><i class='bi_delete fa fa-trash-o' size='2'></i></td>

                          </tr>";

                            $count++;
                        }

                        ?>

                        <tr class='add-item' data-client='<?php echo ktsEncode($project_details['cid']) ?>'>
                        <td>*</td>
                        <td><input class='add-puii-descrip' type='text' value='' size='150'></td>
                        <td><input class='add-puii-qty' type='text' min='0' value='' size='5'></td>
                        <td><input class='add-puii-amount' type='text' min='0' value='' size='20'></td>
                        <td><input class='add-puii-tax' type='text' min='0' value='' size='10'></td>
                        <td><input class='add-puii-ship' type='text' min='0' value='' size='10'></td>
                        <td>
                          <select class='add-puii-curr form-control'>

                            <option  value='jmd' size='5'>jmd</option>

                            <?php foreach ($kts_project_currencies as $value) {
                            echo "<option value='".$value."' size='5'>".$value."</option>";
                        } ?>

                          </select>
                        </td>
                        <td><button type='button' class='add-puii-item btn btn-primary'>Add New Item</button></td>
                        </tr>;
                      </tbody>
                    </table>

                  </div>



                </div>
              </div>








              <div class='modal fade update-project-modal-sm' tabindex='-1' role='dialog' aria-hidden='true'>
                <div class='modal-dialog modal-sm'>
                  <div class='modal-content'>

                    <div class='modal-header'>
                      <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>×</span>
                      </button>
                      <h4 class='update-project-modal-title' id='myModalLabel2'>Update Project Settings</h4>
                    </div>
                    <div class='modal-body'>
                      <h4>Confirm Changes:</h4>
                      <p class='update-project-modal-p1'></p>
                      <p class='update-project-modal-p2'></p>
                    </div>
                    <div class='modal-footer'>
                      <button type='button' class='btn btn-default update-project-modal-cancel' data-dismiss='modal'>Cancel</button>
                      <button type='button' class='btn btn-primary update-project-modal-accept'>Save changes</button>
                    </div>

                  </div>
                </div>
              </div>
              <!-- /modals -->
