<?php
require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');


if ($type != '3'/*must be employee(3)*/ || empty($cid) || !isset($_REQUEST['option']) ||  empty($_REQUEST['option'])
|| !isset($_REQUEST['cpid']) || empty($_REQUEST['cpid']) || !isset($_REQUEST['iiid']) || empty($_REQUEST['iiid'])) {
    $result = genResult('2', 'Operation could not be completed', null);
    echo json_encode($result);
    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_bi_descrp') {
    $iiid = ktsDecode($_REQUEST['iiid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateFAiLURETable($iiid, 'iiid', 'invoice_item', 'description', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_bi_qty') {
    $iiid = ktsDecode($_REQUEST['iiid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($iiid, 'iiid', 'invoice_item', 'qty', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && (!empty($_REQUEST['value']) || trim($_REQUEST['value']) == '0') && $_REQUEST['option'] == 'update_bi_amount') {
    $iiid = ktsDecode($_REQUEST['iiid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($iiid, 'iiid', 'invoice_item', 'amount', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_bi_inum') {
    $iiid = ktsDecode($_REQUEST['iiid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($iiid, 'iiid', 'invoice_item', 'invoice_number', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) /*&& !empty($_REQUEST['value']) */&& $_REQUEST['option'] == 'update_bi_paid') { //empty check removed because value can be '0', which would be a false positive.
    $iiid = ktsDecode($_REQUEST['iiid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($iiid, 'iiid', 'invoice_item', 'paid', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) /*&& !empty($_REQUEST['value'])*/ && $_REQUEST['option'] == 'update_bi_estimate') { //empty check removed because value can be '0', which would be a false positive.
    $iiid = ktsDecode($_REQUEST['iiid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($iiid, 'iiid', 'invoice_item', 'estimate', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && (!empty($_REQUEST['value']) || trim($_REQUEST['value']) == '0') && $_REQUEST['option'] == 'update_bi_tax') {
    $iiid = ktsDecode($_REQUEST['iiid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($iiid, 'iiid', 'invoice_item', 'tax', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && (!empty($_REQUEST['value']) || trim($_REQUEST['value']) == '0') && $_REQUEST['option'] == 'update_bi_ship') {
    $iiid = ktsDecode($_REQUEST['iiid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($iiid, 'iiid', 'invoice_item', 'shipping', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_bi_currency') {
    $iiid = ktsDecode($_REQUEST['iiid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($iiid, 'iiid', 'invoice_item', 'currency', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['cpid']) && !empty($_REQUEST['cpid']) && $_REQUEST['option'] == 'delete_bi') {
    $iiid = ktsDecode($_REQUEST['iiid']);
    $this_cpid = mysqli_real_escape_string($db, ktsDecode(trim($_REQUEST['cpid'])));


    $result = deleteInvoiceItem($iiid, $this_cpid, $db);
    echo json_encode($result);

    return $result;
} else {
    $result = genResult('2', 'Operation unknown--CID '.$cid.'\n Option '.$_REQUEST['option'].'\n CPID '.$_REQUEST['cpid'].'\n IIID  '.$_REQUEST['iiid'].'\n VALUE  '.$_REQUEST['value'], null);
    echo json_encode($result);
    return $result;
}
