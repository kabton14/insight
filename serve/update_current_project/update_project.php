<?php
require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if ($type != '3' || empty($cid) || !isset($_REQUEST['option']) ||  empty($_REQUEST['option'])
|| !isset($_REQUEST['cpid']) || empty($_REQUEST['cpid'])) {
    $result = genResult('2', 'Operation could not be completed', null);
    echo json_encode($result);
    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_client') {
    $cpid = ktsDecode($_REQUEST['cpid']);
    $client = mysqli_real_escape_string($db, ktsDecode(trim($_REQUEST['value'])));


    $result = updateTable($cpid, 'cpid', 'current_project', 'cid', $client, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_pname') {
    $cpid = ktsDecode($_REQUEST['cpid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($cpid, 'cpid', 'current_project', 'project_name', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_ptype') {
    $cpid = ktsDecode($_REQUEST['cpid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($cpid, 'cpid', 'current_project', 'project_type', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_pleader') {
    $cpid = ktsDecode($_REQUEST['cpid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($cpid, 'cpid', 'current_project', 'project_leader', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_pstart') {
    $cpid = ktsDecode($_REQUEST['cpid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($cpid, 'cpid', 'current_project', 'start_date', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_pend') {
    $cpid = ktsDecode($_REQUEST['cpid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($cpid, 'cpid', 'current_project', 'projected_end', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_pprogress') {
    $cpid = ktsDecode($_REQUEST['cpid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($cpid, 'cpid', 'current_project', 'progress', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_pstatus') {
    $cpid = ktsDecode($_REQUEST['cpid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($cpid, 'cpid', 'current_project', 'status', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_pdescrp') {
    $cpid = ktsDecode($_REQUEST['cpid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($cpid, 'cpid', 'current_project', 'description', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && (!empty($_REQUEST['value']) || trim($_REQUEST['value']) == '0') && $_REQUEST['option'] == 'update_pdeposit') {
    $cpid = ktsDecode($_REQUEST['cpid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));

    $pre_result = addPayment($cpid, '', $value, $db);

    if ($pre_result['code'] != '1') {
        echo json_encode($pre_result);

        return $pre_result;
    }


    $result = updateTable($cpid, 'cpid', 'current_project', 'deposit', $value, '0', $db);
    echo json_encode($result);

    return $result;
}
elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_pymnt_due') {
    $cpid = ktsDecode($_REQUEST['cpid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($cpid, 'cpid', 'current_project', 'pymnt_due', $value, '0', $db);
    echo json_encode($result);

    return $result;
}
 elseif (isset($_REQUEST['value']) && (!empty($_REQUEST['value']) || trim($_REQUEST['value']) == '0') && $_REQUEST['option'] == 'update_phours') {
    $cpid = ktsDecode($_REQUEST['cpid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));


    $result = updateTable($cpid, 'cpid', 'current_project', 'time_logged', $value, '0', $db);
    echo json_encode($result);

    return $result;
} else {
    $result = genResult('2', 'Operation unknown', null);
    echo json_encode($result);
    return $result;
}
