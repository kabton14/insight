<?php
require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if ($type != '3' || empty($cid) || !isset($_REQUEST['option']) ||  empty($_REQUEST['option'])
|| !isset($_REQUEST['cpid']) || empty($_REQUEST['cpid']) || !isset($_REQUEST['cid']) || empty($_REQUEST['cid'])) {
    $result = genResult('2', 'Operation could not be completed', null);
    echo json_encode($result);
    return $result;
} elseif ($_REQUEST['option'] == 'add_bi' && isset($_REQUEST['add_descrp']) && !empty($_REQUEST['add_descrp'])
&& isset($_REQUEST['add_qty']) && (!empty($_REQUEST['add_qty']) || trim($_REQUEST['add_qty']) > 0)
&& isset($_REQUEST['add_amount']) && (!empty($_REQUEST['add_amount']) || trim($_REQUEST['add_amount']) >= '0')
&& isset($_REQUEST['add_tax']) && (!empty($_REQUEST['add_tax']) || trim($_REQUEST['add_tax']) >= '0')
&& isset($_REQUEST['add_ship']) && (!empty($_REQUEST['add_ship']) || trim($_REQUEST['add_ship']) >= '0')
&& isset($_REQUEST['add_curr']) && (!empty($_REQUEST['add_curr']) || trim($_REQUEST['add_curr']) >= '0')


) {
    //&& isset($_REQUEST['']) && empty($_REQUEST[''])
    $id = mysqli_real_escape_string($db, ktsDecode(trim($_REQUEST['cid'])));
    $cpid = mysqli_real_escape_string($db, ktsDecode(trim($_REQUEST['cpid'])));

    $add_descrp = mysqli_real_escape_string($db, trim($_REQUEST['add_descrp']));
    $add_qty = mysqli_real_escape_string($db, trim($_REQUEST['add_qty']));
    $add_amount = mysqli_real_escape_string($db, trim($_REQUEST['add_amount']));
    $add_tax = mysqli_real_escape_string($db, trim($_REQUEST['add_tax']));
    $add_ship = mysqli_real_escape_string($db, trim($_REQUEST['add_ship']));
    $add_curr = mysqli_real_escape_string($db, trim($_REQUEST['add_curr']));
    $invoice_number = '1';
    $paid = '0';
    $estimate = '0';



    $result = setInvoiceItem($cpid, $id, $add_descrp, $add_qty, $add_amount, $invoice_number, $paid, $estimate, $add_tax, $add_ship, $add_curr, $db);
    echo json_encode($result);

    return $result;
} else {
    $result = genResult('2', 'Operation unknown', null);
    echo json_encode($result);
    return $result;
}
