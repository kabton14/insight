<?php


require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if ($type != '3') {
    notFound('1');
    return false;
}

$clients = getAllClients($db)['payload'];
?>

<div class='col-md-12 col-sm-12 col-xs-12'>
                <div class='x_panel'>
                  <div class='x_title'>
                    <h2>Create New Web Item <small></small></h2>

                    <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>
                    <br>

                    <form id='create-witem-form'  class='form-horizontal form-label-left' novalidate=''>
                    <div class='form-group'>
                      <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-project-client'>Client <span class='required'>*</span>
                      </label>
                      <div class='col-md-6 col-sm-6'>
                        <select id='create-witem-client' name='create-witem-client' required='required' class='form-control'>

                          <option value=''>Choose Client</option>

                          <?php foreach ($clients as $value) {
    echo "<option class='witem-client' value='".ktsEncode($value['cid'])."'>".$value['lname']." ".$value['fname']." (<b>".$value['name']."</b>)</option>";
} ?>

                        </select>
                      </div>
                    </div>

                    <div class='form-group'>
                      <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-witem-type'>What Do You Want Add<span class='required'>*</span>
                      </label>
                      <div class='col-md-6 col-sm-6'>
                        <input id='toggle-domain' type='checkbox' name='domain' value='parked'>Domain<br>
                        <input id='toggle-hosting' type='checkbox' name='hosting' value='hosting'>Hosting Plan<br>
                      </div>
                    </div>
                  </form>

                    <br>
                    <form id='create-domain-form'  class='form-horizontal form-label-left' novalidate=''>

                      <h2>Create Domain </h2>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-domain-name'>Domain Name <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='create-domain-name' name='create-domain-name' required='required' class='form-control col-md-7 col-xs-12' placeholder='name.tld'>
                        </div>
                      </div>


                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-domain-type'>Domain Type <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <input id='parked' type='checkbox' name='parked' value='parked'> Parked Domain<br>
                          <input id='sub' type='checkbox' name='sub' value='sub'> Sub-Domain<br>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-domain-registrar'>Registrar <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='create-domain-registrar' name='create-domain-registrar' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-domain-account'>Account <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <select id='create-domain-account' name='create-domain-account' required='required' class='form-control'>

                            <option value=''>Choose Account</option>

                            <?php foreach ($hosting_account as $value) {
    echo "<option value='".$value."'>".$value."</option>";
} ?>

                          </select>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-domain-duration'>Duration (yrs) <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='number' min='0' max='100' id='create-domain-duration' name='create-domain-duration' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-domain-purchase-date'>Purchase Date <span class='required'>*</span>
                        </label>
                        <div class='input-group date col-md-4 col-sm-4col-xs-10 start-date-picker' id='request-date-picker'>
                            <input type='text' id='create-domain-purchase-date' name='create-domain-purchase-date' placeholder='YYYY-MM-DD'class='form-control start-date-picker'>
                            <span class='input-group-addon'>
                               <span class='glyphicon glyphicon-calendar'></span>
                            </span>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-domain-expiry-date'>Expiry Date <span class='required'>*</span>
                        </label>
                        <div class='input-group date col-md-4 col-sm-4col-xs-10 start-date-picker' id='request-date-picker'>
                            <input type='text' id='create-domain-expiry-date' name='create-domain-expiry-date' placeholder='YYYY-MM-DD'class='form-control pend-date-picker'>
                            <span class='input-group-addon'>
                               <span class='glyphicon glyphicon-calendar'></span>
                            </span>
                        </div>
                      </div>




                      <div class='ln_solid'></div>
                      <div class='form-group'>
                        <div class='col-md-6 col-sm-6 col-xs-12 col-md-offset-3'>
                          <button class='btn btn-primary create-domain-reset' type='button'>Cancel</button>
                          <button class='btn btn-primary create-domain-reset' type='button'>Reset</button>
                          <button type='button' class='btn btn-success create-domain-submit'>Submit</button>
                        </div>
                      </div>

                    </form>

                    <br>
                    <form id='create-hosting-form'  class='form-horizontal form-label-left' novalidate=''>

                      <h2>Create Hosting Plan </h2>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-website-name'>Website <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select id='create-website-name' name='create-website-name' required='required' class='form-control'>

                            <option value=''>Choose Domain</option>


                          </select>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-hosting-account'>Account <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <select id='create-hosting-account' name='create-hosting-account' required='required' class='form-control'>

                            <option value=''>Choose Account</option>

                            <?php foreach ($hosting_account as $value) {
    echo "<option value='".$value."'>".$value."</option>";
} ?>

                          </select>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-hosting-registrar'>Registrar<span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text'  id='create-hosting-registrar' name='create-hosting-registrar' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-hosting-type'>Website Type<span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <select id='create-hosting-type' name='create-hosting-type' required='required' class='form-control'>

                            <option value=''>Choose Type</option>

                            <?php foreach ($website_type as $value) {
    echo "<option value='".$value."'>".$value."</option>";
} ?>

                          </select>
                        </div>
                      </div>


                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-hosting-duration'>Duration (yrs) <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='number' min='0' max='100' id='create-hosting-duration' name='create-hosting-duration' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-hosting-purchase-date'>Purchase Date <span class='required'>*</span>
                        </label>
                        <div class='input-group date col-md-4 col-sm-4col-xs-10 start-date-picker' id='request-date-picker'>
                            <input type='text' id='create-hosting-purchase-date' name='create-hosting-purchase-date' placeholder='YYYY-MM-DD'class='form-control start-date-picker'>
                            <span class='input-group-addon'>
                               <span class='glyphicon glyphicon-calendar'></span>
                            </span>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-hosting-expiry-date'>Expiry Date <span class='required'>*</span>
                        </label>
                        <div class='input-group date col-md-4 col-sm-4col-xs-10 start-date-picker' id='request-date-picker'>
                            <input type='text' id='create-hosting-expiry-date' name='create-hosting-expiry-date' placeholder='YYYY-MM-DD'class='form-control pend-date-picker'>
                            <span class='input-group-addon'>
                               <span class='glyphicon glyphicon-calendar'></span>
                            </span>
                        </div>
                      </div>




                      <div class='ln_solid'></div>
                      <div class='form-group'>
                        <div class='col-md-6 col-sm-6 col-xs-12 col-md-offset-3'>
                          <button class='btn btn-primary create-hosting-reset' type='button'>Cancel</button>
                          <button class='btn btn-primary create-hosting-reset' type='button'>Reset</button>
                          <button type='button' class='btn btn-success create-hosting-submit'>Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
