<?php

require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if ($_POST['option']=='create_hosting') {
    if (1<2) {
        $hosting_client = mysqli_real_escape_string($db, ktsDecode(trim($_POST['hosting_client'])));
        $hosting_name = mysqli_real_escape_string($db, ktsDecode(trim($_POST['hosting_name'])));
        $hosting_account = mysqli_real_escape_string($db, trim($_POST['hosting_account']));
        $hosting_registrar = mysqli_real_escape_string($db, trim($_POST['hosting_registrar']));
        $hosting_type = mysqli_real_escape_string($db, trim($_POST['hosting_type']));
        $hosting_duration = mysqli_real_escape_string($db, trim($_POST['hosting_duration']));
        $hosting_purchase = mysqli_real_escape_string($db, trim($_POST['hosting_purchase']));
        $hosting_expiry = mysqli_real_escape_string($db, trim($_POST['hosting_expiry']));


        $result = createHosting($hosting_client, $hosting_name, $hosting_account, $hosting_registrar, $hosting_type, $hosting_duration, $hosting_purchase, $hosting_expiry, $db);

        echo json_encode($result);
        return $result;
    } else {
        $result = genResult('2', 'Please ensure all fields are filled.', null);
        echo json_encode($result);
        return $result;
    }
} else {
    $result = genResult('2', 'Unsupported Operation.', null);
    echo json_encode($result);
    return $result;
}
