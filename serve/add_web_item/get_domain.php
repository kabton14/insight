<?php

require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if ($_POST['option']=='get_domain') {
    if (1<2) {
        $client = mysqli_real_escape_string($db, ktsDecode(trim($_POST['client'])));


        $result = getDomain($client, '1', $db);

        if ($result['payload'] != null) {
            foreach ($result['payload'] as &$key) {
                $key['did'] = ktsEncode($key['did']);
            }

            echo json_encode($result);
            return $result;
        } else {
            $result = genResult('2', 'No domains found.', null);
            echo json_encode($result);
            return $result;
        }
    } else {
        $result = genResult('2', 'Please ensure all fields are filled.', null);
        echo json_encode($result);
        return $result;
    }
} else {
    $result = genResult('2', 'Unsupported Operation.', null);
    echo json_encode($result);
    return $result;
}
