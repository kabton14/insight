<?php

require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if ($_POST['option']=='create_domain') {
    if (1<2) {
        $domain_client = mysqli_real_escape_string($db, ktsDecode(trim($_POST['domain_client'])));
        $domain_name = mysqli_real_escape_string($db, trim($_POST['domain_name']));
        $domain_registrar = mysqli_real_escape_string($db, trim($_POST['domain_registrar']));
        $domain_account = mysqli_real_escape_string($db, trim($_POST['domain_account']));
        $domain_parked = mysqli_real_escape_string($db, (int)trim($_POST['domain_parked']));
        $domain_sub = mysqli_real_escape_string($db, (int)trim($_POST['domain_sub']));
        $domain_duration = mysqli_real_escape_string($db, trim($_POST['domain_duration']));
        $domain_purchase = mysqli_real_escape_string($db, trim($_POST['domain_purchase']));
        $domain_expiry = mysqli_real_escape_string($db, trim($_POST['domain_expiry']));

        //$result = createDomain('7', 'xxx.com','sexhost','kts1','0','0','2','2018-04-27','2020-04-27',$db);

        $result = createDomain($domain_client, $domain_name, $domain_registrar, $domain_account, $domain_parked, $domain_sub, $domain_duration, $domain_purchase, $domain_expiry, $db);

        echo json_encode($result);
        return $result;
    } else {
        $result = genResult('2', 'Please ensure all fields are filled.', null);
        echo json_encode($result);
        return $result;
    }
} else {
    $result = genResult('2', 'Unsupported Operation.', null);
    echo json_encode($result);
    return $result;
}
