<?php

$cpid = $_POST['cpid'];
$client = $_POST['client'];

echo "
  <div class='project_upload' data-cpid='".$cpid."'>
    <div class='page-title'>
      <div class='title_left'>
        <h3>File Uploader </h3>
      </div>

      <div class='title_right'>
        <div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'>
          <div class='input-group'>
            <input type='text' class='form-control' placeholder='Search for...'>
            <span class='input-group-btn'>
              <button class='btn btn-default' type='button'>Go!</button>
            </span>
          </div>
        </div>
      </div>
    </div>

    <div class='clearfix'></div>

    <div class='row'>
      <div class='col-md-12 col-sm-12 col-xs-12'>
        <div class='x_panel'>
          <div class='x_title'>
            <h2>File Upload for Project</h2>
            <ul class='nav navbar-right panel_toolbox'>
              <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a>
              </li>
              <li class='dropdown'>
                <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'><i class='fa fa-wrench'></i></a>
                <ul class='dropdown-menu' role='menu'>
                  <li><a href='#'>Settings 1</a>
                  </li>
                  <li><a href='#'>Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class='close-link list_current_project'><i class='fa fa-close'></i></a>
              </li>
            </ul>
            <div class='clearfix'></div>
          </div>
          <div class='x_content'>

          <ul class='stats-overview'>
              <li class='list_current_project'>
                <span class='name'> </span>
                <span class='value text-success'> <span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span> </span>
              </li>
              </ul>

            <p>Drag multiple files to the box below for multi upload or click to select files.</p>
            <form action='' method='post' class='dropzone'>
              <input type='hidden' name='cpid' value='".$cpid."' />
              <input type='hidden' name='client' value='".$client."' />
            </form>
            <br />
            <br />
            <br />
            <br />
          </div>
        </div>
      </div>
    </div>
  </div>

       ";
