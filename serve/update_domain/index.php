<?php

require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if ($type != '3' || !isset($_REQUEST['cdid']) || empty($_REQUEST['cdid'])) {
    notFound('1');
    return false;
}

$cdid = ktsDecode($_REQUEST['cdid']);

$clients = getAllClients($db) ['payload'];
$domain = getDomain($cdid, '3', $db) ['payload'][0];
?>

<div class='col-md-12 col-sm-12 col-xs-12'>
                <div class='x_panel'>
                  <div class='x_title'>
                    <h2>Domain Details <small></small></h2>

                    <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>
                    <br>


                    <br>
                    <form id='update-domain-form' data-cdid='<?php echo ktsEncode($domain['did']); ?>' class='form-horizontal form-label-left' novalidate=''>

                      <h2>Update Domain </h2>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-domain-name'>Domain Name <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='update-domain-name' name='update-domain-name' required='required' value='<?php echo $domain['domain']; ?>' class='form-control col-md-7 col-xs-12' placeholder='name.tld'>
                        </div>
                      </div>


                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-domain-type'>Domain Type <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <input id='update-parked' type='checkbox' name='parked' value='parked' <?php if ($domain['parked'] == '1') {
    echo 'checked';
} else {
    echo '';
} ?> > Parked Domain<br>
                          <input id='update-sub' type='checkbox' name='sub' value='sub' <?php if ($domain['sub_domain'] == '1') {
    echo 'checked';
} else {
    echo '';
} ?> > Sub-Domain<br>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-domain-registrar'>Registrar <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='update-domain-registrar' name='update-domain-registrar' required='required' value='<?php echo $domain['registrar']; ?>'  class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-domain-account'>Account <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <select id='update-domain-account' name='update-domain-account' required='required' value='<?php echo $domain['account']; ?>' class='form-control'>

                            <option value='<?php echo $domain['account']; ?>'><?php echo $domain['account']; ?></option>


                            <?php foreach ($hosting_account as $value) {
    echo "<option value='" . $value . "'>" . $value . "</option>";
} ?>

                          </select>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-domain-duration'>Duration (yrs) <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='number' min='0' max='100' id='update-domain-duration' name='update-domain-duration' required='required' value='<?php echo $domain['duration']; ?>' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-domain-purchase-date'>Purchase Date <span class='required'>*</span>
                        </label>
                        <div class='input-group date col-md-4 col-sm-4col-xs-10 start-date-picker' id='request-date-picker'>
                            <input type='text' id='update-domain-purchase-date' name='update-domain-purchase-date' value='<?php echo $domain['buy_date']; ?>' placeholder='YYYY-MM-DD'class='form-control start-date-picker'>
                            <span class='input-group-addon'>
                               <span class='glyphicon glyphicon-calendar'></span>
                            </span>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-domain-expiry-date'>Expiry Date <span class='required'>*</span>
                        </label>
                        <div class='input-group date col-md-4 col-sm-4col-xs-10 start-date-picker' id='request-date-picker'>
                            <input type='text' id='update-domain-expiry-date' name='update-domain-expiry-date' value='<?php echo $domain['expire']; ?>' placeholder='YYYY-MM-DD'class='form-control pend-date-picker'>
                            <span class='input-group-addon'>
                               <span class='glyphicon glyphicon-calendar'></span>
                            </span>
                        </div>
                      </div>




                      <div class='ln_solid'></div>

                    </form>

                    <br>

                  </div>
                </div>
              </div>


              <!--Modal-->
              <div class='modal fade update-wi-modal-sm' tabindex='-1' role='dialog' aria-hidden='true'>
                <div class='modal-dialog modal-sm'>
                  <div class='modal-content'>

                    <div class='modal-header'>
                      <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>×</span>
                      </button>
                      <h4 class='update-wi-modal-title' id='myModalLabel2'>Update Web Item Settings</h4>
                    </div>
                    <div class='modal-body'>
                      <h4>Confirm Changes:</h4>
                      <p class='update-wi-modal-p1'></p>
                      <p class='update-wi-modal-p2'></p>
                    </div>
                    <div class='modal-footer'>
                      <button type='button' class='btn btn-default update-wi-modal-cancel' data-dismiss='modal'>Cancel</button>
                      <button type='button' class='btn btn-primary update-wi-modal-accept'>Save changes</button>
                    </div>

                  </div>
                </div>
              </div>
              <!-- /modals -->
