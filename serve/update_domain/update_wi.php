<?php
require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if ($type != '3' || empty($cid) || !isset($_REQUEST['option']) || empty($_REQUEST['option']) || (!isset($_REQUEST['cdid']) && !isset($_REQUEST['chid'])) || (empty($_REQUEST['cdid']) && empty($_REQUEST['chid']))) {
    $result = genResult('2', 'Operation could not be completed', null);
    echo json_encode($result);
    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_domain') {
    $cdid = ktsDecode($_REQUEST['cdid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));

    $result = updateTable($cdid, 'did', 'domain', 'domain', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_parked') {
    $cdid = ktsDecode($_REQUEST['cdid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));
    if (trim($value) == 'true') {
        $value = '1';
    } else {
        $value = '0';
    }

    $result = updateTable($cdid, 'did', 'domain', 'parked', $value, '0', $db);

    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_sub') {
    $cdid = ktsDecode($_REQUEST['cdid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));
    if (trim($value) == 'true') {
        $value = '1';
    } else {
        $value = '0';
    }

    $result = updateTable($cdid, 'did', 'domain', 'sub_domain', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_account') {
    $cdid = ktsDecode($_REQUEST['cdid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));

    $result = updateTable($cdid, 'did', 'domain', 'account', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_registrar') {
    $cdid = ktsDecode($_REQUEST['cdid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));

    $result = updateTable($cdid, 'did', 'domain', 'registrar', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_duration') {
    $cdid = ktsDecode($_REQUEST['cdid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));

    $result = updateTable($cdid, 'did', 'domain', 'duration', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_purchase_date') {
    $cdid = ktsDecode($_REQUEST['cdid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));

    $result = updateTable($cdid, 'did', 'domain', 'buy_date', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_expiry') {
    $cdid = ktsDecode($_REQUEST['cdid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));

    $result = updateTable($cdid, 'did', 'domain', 'expire', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_website') {
    $chid = ktsDecode($_REQUEST['chid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));

    //$result = updateTable($chid, 'hid', 'hosting', 'did', $value, '0', $db); Disabled
    $result = genResult('4', 'Feature Disabled', null);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_hosting_account') {
    $chid = ktsDecode($_REQUEST['chid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));

    $result = updateTable($chid, 'hid', 'hosting', 'account', $value, '0', $db);
    echo json_encode($result);

    return $result;
}

//function updateTable($id, $id_column, $table, $column, $value, $multiple, $db){
//$query = "UPDATE ".$table." SET ".$column."=? WHERE ".$id_column."=?";
elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_hosting_registrar') {
    $chid = ktsDecode($_REQUEST['chid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));

    $result = updateTable($chid, 'hid', 'hosting', 'registrar', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_hosting_type') {
    $chid = ktsDecode($_REQUEST['chid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));

    $result = updateTable($chid, 'hid', 'hosting', 'website_type', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_hosting_duration') {
    $chid = ktsDecode($_REQUEST['chid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));

    $result = updateTable($chid, 'hid', 'hosting', 'duration', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_hosting_purchase_date') {
    $chid = ktsDecode($_REQUEST['chid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));

    $result = updateTable($chid, 'hid', 'hosting', 'buy_date', $value, '0', $db);
    echo json_encode($result);

    return $result;
} elseif (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && $_REQUEST['option'] == 'update_hosting_expiry') {
    $chid = ktsDecode($_REQUEST['chid']);
    $value = mysqli_real_escape_string($db, trim($_REQUEST['value']));

    $result = updateTable($chid, 'hid', 'hosting', 'expire', $value, '0', $db);
    echo json_encode($result);

    return $result;
} else {
    $result = genResult('2', 'Operation unknown', null);
    echo json_encode($result);
    return $result;
}
