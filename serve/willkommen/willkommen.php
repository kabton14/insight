<?php

require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if (isset($cid)) {
    if (isset($_REQUEST['option']) && !empty($_REQUEST['option']) && trim($_REQUEST['option']) == "past_projects") {
        $result = getPastP($cid, $db);

        if ($result['code'] == '1') {
            echo json_encode($result);
            return $result;
        } else {
            $result = genResult('2', 'There was an error.', null);
            echo json_encode($result);
            return $result;
        }
    } else {
        $result = genResult('2', 'Unknown operation.', null);
        echo json_encode($result);
        return $result;
    }
} else {
    $result = genResult('2', 'There was an error retreiving records.', null);
    json_encode($result);
    return $result;
}
