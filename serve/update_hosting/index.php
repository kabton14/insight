<?php

require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if ($type != '3' || !isset($_REQUEST['chid']) || empty($_REQUEST['chid'])) {
    notFound('1');
    return false;
}

$chid = ktsDecode($_REQUEST['chid']);

$clients = getAllClients($db) ['payload'];
$hosting = getHosting($chid, '3', $db) ['payload'][0];

?>


<div class='col-md-12 col-sm-12 col-xs-12'>
                <div class='x_panel'>
                  <div class='x_title'>
                    <h2>Hosting Details <small></small></h2>

                    <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>
                    <br>


                    <br>


                    <br>
                    <form id='update-hosting-form'  class='form-horizontal form-label-left' data-chid='<?php echo ktsEncode($hosting['hid']); ?>' novalidate=''>

                      <h2>Update Hosting Plan </h2>
                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-website-name'>Website <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <select id='update-website-name' name='update-website-name' required='required' class='form-control'>

                            <option value='<?php echo ktsEncode($hosting['did']); ?>'><?php echo $hosting['domain']; ?></option>

                            <?php $eligible = getDomain($hosting['cid'], '1', $db) ['payload'];
foreach ($eligible as $key) {
    if (($key['cid'] == $hosting['cid']) && ($key['did'] != $hosting['did'])) {
        echo "<option value='" . ktsEncode($key['did']) . "'>" . $key['domain'] . "</option>";
    }
} ?>


                          </select>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-hosting-account'>Account <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <select id='update-hosting-account' name='update-hosting-account' required='required' class='form-control'>

                            <option value='<?php echo $hosting['account']; ?>'><?php echo $hosting['account']; ?></option>

                            <?php foreach ($hosting_account as $value) {
    echo "<option value='" . $value . "'>" . $value . "</option>";
} ?>

                          </select>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-hosting-registrar'>Registrar<span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text'  id='update-hosting-registrar' name='update-hosting-registrar' value='<?php echo $hosting['registrar']; ?>' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-hosting-type'>Website Type<span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <select id='update-hosting-type' name='update-hosting-type' required='required' class='form-control'>

                            <option value='<?php echo $hosting['website_type']; ?>'><?php echo $hosting['website_type']; ?></option>

                            <?php foreach ($website_type as $value) {
    echo "<option value='" . $value . "'>" . $value . "</option>";
} ?>

                          </select>
                        </div>
                      </div>


                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-hosting-duration'>Duration (yrs) <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='number' min='0' max='100' id='update-hosting-duration' name='update-hosting-duration' value='<?php echo $hosting['duration']; ?>' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-hosting-purchase-date'>Purchase Date <span class='required'>*</span>
                        </label>
                        <div class='input-group date col-md-4 col-sm-4col-xs-10 start-date-picker' id='request-date-picker'>
                            <input type='text' id='update-hosting-purchase-date' name='update-hosting-purchase-date' value='<?php echo $hosting['buy_date']; ?>' placeholder='YYYY-MM-DD'class='form-control start-date-picker'>
                            <span class='input-group-addon'>
                               <span class='glyphicon glyphicon-calendar'></span>
                            </span>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='update-hosting-expiry-date'>Expiry Date <span class='required'>*</span>
                        </label>
                        <div class='input-group date col-md-4 col-sm-4col-xs-10 start-date-picker' id='request-date-picker'>
                            <input type='text' id='update-hosting-expiry-date' name='update-hosting-expiry-date' value='<?php echo $hosting['expire']; ?>' placeholder='YYYY-MM-DD'class='form-control pend-date-picker'>
                            <span class='input-group-addon'>
                               <span class='glyphicon glyphicon-calendar'></span>
                            </span>
                        </div>
                      </div>




                      <div class='ln_solid'></div>

                    </form>
                  </div>
                </div>
              </div>

              <!--Modal-->
              <div class='modal fade update-wi-modal-sm' tabindex='-1' role='dialog' aria-hidden='true'>
                <div class='modal-dialog modal-sm'>
                  <div class='modal-content'>

                    <div class='modal-header'>
                      <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>×</span>
                      </button>
                      <h4 class='update-wi-modal-title' id='myModalLabel2'>Update Web Item Settings</h4>
                    </div>
                    <div class='modal-body'>
                      <h4>Confirm Changes:</h4>
                      <p class='update-wi-modal-p1'></p>
                      <p class='update-wi-modal-p2'></p>
                    </div>
                    <div class='modal-footer'>
                      <button type='button' class='btn btn-default update-wi-modal-cancel' data-dismiss='modal'>Cancel</button>
                      <button type='button' class='btn btn-primary update-wi-modal-accept'>Save changes</button>
                    </div>

                  </div>
                </div>
              </div>
              <!-- /modals -->
