<?php

require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if ($_POST['option'] == 'new_message') {
    if (isset($_POST['subject']) && isset($_POST['message']) && !empty(trim($_POST['subject'])) && !empty(trim($_POST['message']))) {
        $to = '1';
        $to_type = '3';

        if (isset($_POST['receiver']) && !empty(trim($_POST['receiver'])) && $type == '3') {
            $to = mysqli_real_escape_string($db, $_POST['receiver']);
            $exploded_str = explode('-', $to);

            $to = trim($exploded_str[0]);
            $to_type = trim($exploded_str[1]);
        } else {
            $result = genResult('4', 'Please make sure to fill to, subject, and message fields.', null);
            echo json_encode($result);
            return $result;
        }

        $mode = '1';
        $subj = mysqli_real_escape_string($db, $_POST['subject']);
        $msg =  mysqli_real_escape_string($db, $_POST['message']);

        //sendMessage($id, $type, $to, $to_type, $mode, $reply_from, $oldmsgid, $oldroot, $subject, $msg, $db)
        $result = sendMessage($cid, $type, $to, $to_type, $mode, 0, 0, 0, $subj, $msg, $db);
        echo json_encode($result);
        return $result;
    } else {
        $result = genResult('4', 'Please make sure to fill subject and message fields.', null);
        echo json_encode($result);
        return $result;
    }
} elseif ($_POST['option'] == 'reply') {
    if (isset($_POST['msgid']) && !empty(trim($_POST['msgid'])) && isset($_POST['receiver']) && !empty(trim($_POST['receiver']))
    && isset($_POST['receiver_type']) && !empty(trim($_POST['receiver_type'])) && isset($_POST['subject']) && !empty(trim($_POST['subject']))
    && isset($_POST['message']) && !empty(trim($_POST['message']))) {
        $mode = '2';

        $receiver = mysqli_real_escape_string($db, ktsDecode($_POST['receiver']));
        $receiver_type = mysqli_real_escape_string($db, ktsDecode($_POST['receiver_type']));

        $msgid = mysqli_real_escape_string($db, ktsDecode($_POST['msgid']));
        $subj = mysqli_real_escape_string($db, $_POST['subject']);
        $msg =  mysqli_real_escape_string($db, $_POST['message']);

        //sendMessage($id, $type, $to, $to_type, $mode, $reply_from, $oldmsgid, $oldroot, $subject, $msg, $db)
        $result = sendMessage($cid, $type, $receiver, $receiver_type, $mode, 0, $msgid, $msgid, $subj, $msg, $db);
        echo json_encode($result);
        return $result;
    } else {
        $result = genResult('4', 'Message was not successfully sent. Please make sure to fill subject and message fields.', null);
        echo json_encode($result);
        return $result;
    }
} elseif ($_POST['option'] == 'delete') {
    if (isset($_POST['msgid']) && !empty(trim($_POST['msgid']))) {
        $msgid = mysqli_real_escape_string($db, ktsDecode($_POST['msgid']));
        $result = deleteMessage($msgid, $cid, $type, $db, $db2);
        echo json_encode($result);
        return $result;
    } else {
        $result = genResult('4', 'Message was not successfully deleted. Try again later.', null);
        echo json_encode($result);
        return $result;
    }
} else {
    $result = genResult('2', 'There was an error. (16-7-16)', null);
    echo json_encode($result);
    return $result;
}
