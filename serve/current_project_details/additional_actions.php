<?php
require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if ($_POST['option'] == 'send-project-update') {
    if (isset($_POST['project-update-content']) && isset($_POST['cpid'])
&& !empty(trim($_POST['project-update-content'])) && !empty(trim($_POST['cpid']))) {
        $this_cpid = mysqli_real_escape_string($db, ktsDecode(trim($_POST['cpid'])));
        $this_eid = mysqli_real_escape_string($db, trim($cid));
        $this_cupdate = mysqli_real_escape_string($db, trim($_POST['project-update-content']));




        $result = createProjectUpdate($this_cpid, $this_eid, $this_cupdate, $db);
        echo json_encode($result);
        return $result;
    } else {
        $result = genResult('4', 'Please make sure to fill all fields.', null);
        echo json_encode($result);
        return $result;
    }
} else {
    $result = genResult('2', 'Unsupported operation.', null);
    echo json_encode($result);
    return $result;
}
