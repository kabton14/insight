<?php


require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

$avatar = "employee/";
$avatarfrmt = ".png";

$string = "";
$cpid = $_POST['cpid'];

$cpid2 = ktsDecode($cpid);

if (!isset($cpid)) {
    notFound('1');
} else {
    $data = getCurrentProjectDetails($cpid2, $db)['payload'];
    if ($data == null) {
        notFound('1');
        return false;
    }
    $data = $data[0];

    $updates = getProjectUpdates($cpid2, 5, $db)['payload'];

    if ($type == '1' || $type == '2') {
        $dirpath =$_SERVER['DOCUMENT_ROOT']."/client/".$data['cid']."/".$cpid2."/project_file/";
        $dirpath2 =$_SERVER['DOCUMENT_ROOT']."/client/".$data['cid']."/".$cpid2."/update_pic/";

        createProjectPath($dirpath, $dirpath2, 0755);
    }

    $files = listFiles($_SERVER['DOCUMENT_ROOT']."/client/".$data['cid']."/".$cpid2."/project_file/", $_SERVER['HTTP_REFERER']."client/".$data['cid']."/".$cpid2."/project_file/");



    $string .= "
  <div class='damn'>
    <div class='page-title'>
      <div class='title_left'>
        <h3>Project Details <small></small></h3>
      </div>

      <div class='title_right'>
        <!--div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'>
          <div class='input-group'>
            <input type='text' class='form-control' placeholder='Search for...'>
            <span class='input-group-btn'>
              <button class='btn btn-default' type='button'>Go!</button>
            </span>
          </div>
        </div-->
      </div>
    </div>

    <div class='clearfix'></div>

    <div class='row'>
      <div class='col-md-12'>
        <div class='x_panel'>
          <div class='x_title' id='project_title' data-cpid='".ktsEncode($data['cpid'])." data-client='".ktsEncode($data['cid'])."'>
            <h2>".$data['project_name']."</h2>
            <!--ul class='nav navbar-right panel_toolbox'>
              <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a>
              </li>
              <li class='dropdown'>
                <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'><i class='fa fa-wrench'></i></a>
                <ul class='dropdown-menu' role='menu'>
                  <li><a href='#'>Settings 1</a>
                  </li>
                  <li><a href='#'>Settings 2</a>
                  </li>
                </ul>
              </li>
              <li class='current_project'><a class='close-link'><i class='fa fa-close'></i></a>
              </li>
            </ul-->
            <div class='clearfix'></div>
          </div>

          <div class='x_content'>

            <div class='col-md-9 col-sm-9 col-xs-12'>

              <ul class='stats-overview'>
                <li class='current_project'>
                  <span class='name'> </span>
                  <span class='value text-success'> <span class='glyphicon glyphicon-chevron-left' aria-hidden=''true'></span> </span>
                </li>
                <li>
                  <span class='name'> Down Payment </span>
                  <span class='value text-success'> $".$data['deposit']." </span>
                </li>
                <li class='hidden-phone'>
                  <span class='name'> Estimated No. of Days Remaining </span>
                  <span class='value text-success'> ".$data['days_remaining']." Days </span>
                </li>
              </ul>
              <br />

              <div id='echart_mini_pie' data-progress='".$data['progress']."' style='height:350px;'></div>
              <script src='vendors/echarts/dist/echarts.min.js'></script>

              <div>

                <h4>Recent Activity</h4>

                <!-- end of user messages -->
                <ul class='messages'>
"; ?>

<?php
                if ($type == '3') {
                    $string .= "<div id='enter-project-update' class='input-group'>
                            <input type='text' class='form-control project-update-content'>
                            <span class='input-group-btn'>
                                              <button type='button' id='send-project-update' class='btn btn-primary send-update'>Send New Update</button>
                                          </span>
                          </div>";
                }

    if ($updates == null) {
        $string .= "
                <li>
                  <div class='message_wrapper'>
                    <h4 class='heading'>"."No Updates"."</h4>
                    <blockquote class='message'>"."You have no updates as yet."."</blockquote>
                    <br />
                  </div>
                </li>";
    } else {
        foreach ($updates as $x) {
            $string .= "
                  <li>
                    <img src='".$avatar.$x['eid']."/avatar/".$x['eid'].$avatarfrmt."' class='avatar' alt='".$x['fname']."'>
                    <div class='message_date'>
                      <h3 class='date text-info'>".$x['day_added']."</h3>
                      <p class='month'>".$x['month_added']."</p>
                    </div>
                    <div class='message_wrapper'>
                      <h4 class='heading'>".$x['fname']." ".$x['lname']."</h4>
                      <blockquote class='message'>".$x['update_content']."</blockquote>
                      <br />
                      <!--p class='url'>
                        <span class='fs1 text-info' aria-hidden='true' data-icon='?'></span>
                        <a href='#'><i class='fa fa-paperclip'></i> User Acceptance Test.doc </a>
                      </p-->
                    </div>
                  </li>";
        }
    }


    $string .= "
                </ul>
                <!-- end of user messages -->


              </div>


            </div>

            <!-- start project-detail sidebar -->
            <div class='col-md-3 col-sm-3 col-xs-12'>

              <section class='panel'>

                <div class='x_title'>
                  <h2>Project Description</h2>
                  <div class='clearfix'></div>
                </div>
                <div class='panel-body'>
                  <h3 class='green'><i class='fa fa-clipboard'></i> About</h3>

                  <p>".$data['description']."</p>
                  <br />

                  <div class='project_detail'>

                    <p class='title'>Client Company</p>
                    <p>".$company."</p>
                    <p class='title'>Project Leader</p>
                    <p>".$data['pl_fname']." ".$data['pl_lname']."</p>
                  </div>

                  <br />
                  <h5>Project files:</h5>
                  <ul class='list-unstyled project_files'>"; ?>

                  <?php

                  if ($files == false) {
                      $string .="
                    <li ><a target='_blank'><i class='' ></i> "."No files found"."</a>
                    </li>";
                  } else {
                      foreach ($files as $f) {
                          $string .="
                    <li ><a href='".$f['url']."' target='_blank'><i class='".$f['icon']."' ></i> ".$f['name']."</a>
                    </li>";
                      }
                  } ?>
                  <?php

                  $string .= "
                  </ul>
                  <br />

                  <div class='text-center mtop20'>

                    <a href='#' class='btn btn-sm btn-warning' id='uploadfile'>Add files</a>
                    <!--a href='#' class='btn btn-sm btn-primary'>Report contact</a-->


                  </div>
                </div>

              </section>

            </div>
            <!-- end project-detail sidebar -->

          </div>
        </div>
      </div>
    </div>
  </div>


  ";


    echo $string;
}





 ?>

 <!-- Small modal -->

 <div class='modal fade project-update-modal-sm' tabindex='-1' role='dialog' aria-hidden='true'>
   <div class='modal-dialog modal-sm'>
     <div class='modal-content'>

       <div class='modal-header'>
         <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>×</span>
         </button>
         <h4 class='modal-title' id='myModalLabel2'>Send Update</h4>
       </div>
       <div class='modal-body'>
         <h4>Please Confirm...</h4>
         <p>Are you sure you want to send this update?</p>
       </div>
       <div class='modal-footer'>
         <button type='button' class='btn btn-default' data-dismiss='modal'>No</button>
         <button type='button' class='btn btn-primary project-update-modal-accept'>Yes, I'm Sure</button>
       </div>

     </div>
   </div>
 </div>
 <!-- /modals -->
