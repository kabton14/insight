<?php
require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

$data = getClientDetails($cid, $type, $db);

// echo '<script type="text/javascript">',
//      'alert(\'cid:'.$cid.'\n type:'.$type.'\');',
//      '</script>'
// ;

if (!isset($_SESSION['cid'])) {
    serveLogout();
} elseif ($data['payload'] == null || $data['payload'] == "") {
    echo notFound('1'); // not found
} else {
    $this_username;
    $this_email;
    foreach ($data['payload'] as $d) {
        $this_username = $d['user'];
        $this_email = $d['email'];
        $this_currency = $d['currency'];
    }

    echo "
<div class='x_panel'>
  <div class='x_title'>
    <h2>KTS Insight Settings</h2>
    <!--ul class='nav navbar-right panel_toolbox'>
      <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a>
      </li>
      <li class='dropdown'>
        <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'><i class='fa fa-wrench'></i></a>
        <ul class='dropdown-menu' role='menu'>
          <li><a href='#'>Settings 1</a>
          </li>
          <li><a href='#'>Settings 2</a>
          </li>
        </ul>
      </li>
      <li><a class='close-link'><i class='fa fa-close'></i></a>
      </li>
    </ul-->
    <div class='clearfix'></div>
  </div>
  <div class='x_content'>
    <br>

    <form class='form-horizontal form-label-left ' id='settings_form'>
      <div class='form-group  '>
        <div class='kts_setting_upload dropzone glyphicon-plus'></div>


      </div>

      <div class='form-group'>
        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='user-name'>Username<span class='required'>*</span>
        </label>
        <div class='col-md-6 col-sm-6 col-xs-12'>
          <input id='user-name' class='form-control col-md-7 col-xs-12' autocomplete='false' data-validate-length-range='4,15' data-validate-words='1' name='user-name'  required='required' type='text' value='".$this_username."'>
        </div>
      </div>
      <div class='form-group'>
        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='email'>Email <span class='required'>*</span>
        </label>
        <div class='col-md-6 col-sm-6 col-xs-12'>
          <input type='email' id='email' name='email' required='required' class='form-control col-md-7 col-xs-12' value='".$this_email."'>
        </div>
      </div>
      <div class='form-group'>
      <label for='password' class='control-label col-md-3 col-sm-3 col-xs-12'>Password <span class='required'>*</span></label>
      <div class='col-md-6 col-sm-6 col-xs-12'>
        <input id='password' type='password' autocomplete='false' name='password' data-validate-length-range='7,15' class='form-control col-md-7 col-xs-12' required='required'>
      </div>
    </div>

    <div class='form-group'>
      <label class='control-label col-md-3 col-sm-3 col-xs-12 password2'>Repeat Password </label>
      <div class='col-md-6 col-sm-6 col-xs-12 password2'>
        <input type='password' class='form-control' id='password2' name='password2' >
      </div>

        </div>



        <div class='form-group'>
          <label class='control-label col-md-3 col-sm-3 col-xs-12' for='settings-currency'>Preferred Currency<span class='required'></span>
          </label>
          <div class='col-md-6 col-sm-6 col-xs-12'>

            <select id='settings-currency' name='settings-currency' required='required' class='form-control'>

              <option value='".$this_currency."'>".$this_currency."</option>"; ?>

              <?php
              foreach ($kts_currencies as $value) {
                  echo "<option value='".$value."'>".$value."</option>";
              } ?>

<?php

echo "
            </select>
          </div>
        </div>








        <div class='form-group' >
          <label class='control-label col-md-3 col-sm-3 col-xs-12'>Receive Notifications</label>
          <div class='col-md-6 col-sm-6 col-xs-12'>
            <div class='col-md-3 col-sm-3 col-xs-6'>
              <label>
                <input id='settings-sms' type='checkbox' class='js-switch' ".checked($d['sms'])."/> SMS
              </label>
            </div>
            <div class='col-md-6 col-sm-6 col-xs-12'>
              <label>
                <input id='settings-push' type='checkbox' class='js-switch' ".checked($d['push'])."/> Push Notifications (for mobile app)
              </label>
            </div>

          </div>
        </div>


      </div>






      </div>

    </form>
  </div>
</div>


<!-- Small modal -->
<!--button type='button' class='btn btn-primary' data-toggle='modal' data-target='.bs-example-modal-sm'>Small modal</button-->

<div class='modal fade settings-modal-sm' tabindex='-1' role='dialog' aria-hidden='true'>
  <div class='modal-dialog modal-sm'>
    <div class='modal-content'>

      <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>×</span>
        </button>
        <h4 class='settings-modal-title' id='myModalLabel2'>KTM Insight Settings</h4>
      </div>
      <div class='modal-body'>
        <h4>Confirm Changes:</h4>
        <p class='settings-modal-p1'></p>
        <p class='settings-modal-p2'></p>
      </div>
      <div class='modal-footer'>
        <button type='button' class='btn btn-default settings-modal-cancel' data-dismiss='modal'>Cancel</button>
        <button type='button' class='btn btn-primary settings-modal-accept'>Save changes</button>
      </div>

    </div>
  </div>
</div>
<!-- /modals -->
";
}

function checked($check_value)
{
    if ($check_value == 1) {
        return "checked";
    } else {
        return "";
    }
}

?>
