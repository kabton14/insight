<?php
require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');


if (!empty($cid) && isset($_REQUEST['option']) && isset($_REQUEST['user']) && !empty($_REQUEST['option']) && !empty($_REQUEST['user']) && $_REQUEST['option'] == 'user') {
    $user = trim($_REQUEST['user']);

    $result = updateKTS('username', $cid, $user, $user_pattern, $db);
    echo json_encode($result);
    return $result;
} elseif (!empty($cid) && isset($_REQUEST['option']) && isset($_REQUEST['user']) && !empty($_REQUEST['option']) && !empty($_REQUEST['user']) && $_REQUEST['option'] == 'email') {
    $email = trim($_REQUEST['user']);

    $result = updateKTS('email', $cid, $email, $email_pattern, $db);
    echo json_encode($result);
    return $result;
} elseif (!empty($cid) && isset($_REQUEST['option']) && isset($_REQUEST['user']) && !empty($_REQUEST['option']) && !empty($_REQUEST['user']) && $_REQUEST['option'] == 'currency') {
    $value = trim($_REQUEST['user']);

    if (!in_array($value, $kts_currencies)) {
        $status['code'] = 4;
        $status['msg'] = "Unknown Currency.";
        $status['payload'] = null;

        echo json_encode($status);
        return false;
    }

    $result = updateKTS('currency', $cid, $value, $type, $db);
    echo json_encode($result);
    return $result;
} elseif (!empty($cid) && isset($_REQUEST['option']) && isset($_REQUEST['user']) && isset($_REQUEST['user2'])  && !empty($_REQUEST['option']) && !empty($_REQUEST['user']) && !empty($_REQUEST['user2']) && $_REQUEST['option'] == 'password') {
    $pass = trim($_REQUEST['user']);
    $pass2 = trim($_REQUEST['user2']);


    if ($pass != $pass2) {
        $status['code'] = 4;
        $status['msg'] = "Passwords are not the same.";
        $status['payload'] = null;

        echo json_encode($status);
        return false;
    }

    $result = updateKTS('password', $cid, $pass, $password_pattern, $db);
    echo json_encode($result);
    return $result;
} elseif (!empty($cid) && isset($_REQUEST['option']) && isset($_REQUEST['user']) && !empty($_REQUEST['option']) && !empty($_REQUEST['user']) && $_REQUEST['option'] == 'sms') {
    $value = trim($_REQUEST['user']);

    $result = updateKTS('sms', $cid, $value, '', $db);
    echo json_encode($result);
    return $result;
} elseif (!empty($cid) && isset($_REQUEST['option']) && isset($_REQUEST['user']) && !empty($_REQUEST['option']) && !empty($_REQUEST['user']) && $_REQUEST['option'] == 'push') {
    $value = trim($_REQUEST['user']);

    $result = updateKTS('push', $cid, $value, '', $db);
    echo json_encode($result);
    return $result;
} elseif (!empty($cid) && isset($_REQUEST['option']) && isset($_REQUEST['user']) && !empty($_REQUEST['option']) && !empty($_REQUEST['user']) && $_REQUEST['option'] == 'avatar') {
    $value = trim($_REQUEST['user']);

    $result = updateKTS('avatar', $cid, $value, '', $db);
    echo json_encode($result);
    return $result;
} else {
    $status['code'] = 2;
    $status['msg'] = "Could not complete operation. -";
    $status['payload'] = null;

    echo json_encode($status);
    return false;
}
