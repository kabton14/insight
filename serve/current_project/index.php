<?php


require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

$avatar = "/employee/";
$avatarfrmt = ".png";

if ($type == '3') {//employee
  if(isset($_REQUEST['operation']) && $_REQUEST['operation'] == 'old'){
    $data = getCurrentProjects($cid, '3', $db);
  }
  else{
    $data = getCurrentProjects($cid, '2', $db);
  }

} else {
    $data = getCurrentProjects($cid, '1', $db);
}
$data = $data['payload'];

$string = "";
if (!isset($_SESSION['cid'])) {
    serveLogout();
} elseif ($data == null || $data == "") {
    echo notFound('3'); // no projects found
} else {
    ?>

<?php $string .= "

    <div class=''>
     <div class='page-title'>
       <div class='title_left'>
         <h3>Current Projects <small> - ".$company."</small></h3>
       </div>

       <div class='title_right'>
         <!--div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'>
           <div class='input-group'>
             <input type='text' class='form-control' placeholder='Search for...'>
             <span class='input-group-btn'>
               <button class='btn btn-default' type='button'>Go!</button>
             </span>
           </div>
         </div-->
       </div>
     </div>

     <div class='clearfix'></div>

     <div class='row project-list'>
       <div class='col-md-12'>
         <div class='x_panel'>
           <div class='x_title'>
             <h2>Projects</h2>
             <!--ul class='nav navbar-right panel_toolbox'>
               <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a>
               </li>
               <li class='dropdown'>
                 <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'><i class='fa fa-wrench'></i></a>
                 <ul class='dropdown-menu' role='menu'>
                   <li><a href='#'>Settings 1</a>
                   </li>
                   <li><a href='#'>Settings 2</a>
                   </li>
                 </ul>
               </li>
               <li><a class='close-link'><i class='fa fa-close'></i></a>
               </li>
             </ul-->
             <div class='clearfix'></div>
           </div>
           <div class='x_content'>

             <p>List of Projects in Progress:</p>

             <!-- start project list -->
             <table class='table table-striped projects' id='current-projects'>
               <thead>
                 <tr>
                   <th style='width: 1%'>#</th>
                   <th style='width: 20%'>Project Name</th>
                   <th>Team Members</th>
                   <th>Project Progress</th>
                   <th>Status</th>
                   <th style='width: 20%'>Action</th>
                 </tr>
               </thead>
               <tbody>"; ?>

                <?php  //Each Row in Table
                $count = 0;

    foreach ($data as $x) {
        $count++;
        $string .= "<tr class='row_data' data-cpid='".ktsEncode($x['cpid'])."'>
                    <td>".$count."</td>
                    <td>
                      <a>".$x['project_name']."</a>
                      <br />
                      <small>Created ".date_format(date_create($x['start_date']), ' \o\n jS F Y')."</small>
                    </td>

                    <td>
                      <ul class='list-inline'>";



        $employee = getEmployeeAssignment($x['cpid'], '5', $db);
        $employee = $employee['payload'];

        if ($employee == null) {
            $employee = $x['project_leader'];
            $string .=
                      "
                          <li>
                            <img src='".$avatar.$employee."/avatar/".$employee.$avatarfrmt."' class='avatar' alt='TMavatar'>
                          </li>";
        } else {
            foreach ($employee as $em) {
                $string .=
                      "
                          <li>
                            <img src='".$avatar.$em['eid']."/avatar/".$em['eid'].$avatarfrmt."' class='avatar' alt='TMavatar'>
                          </li>";
            }
        }


        $string .= "
                      </ul>
                    </td>
                    <td class='project_progress'>
                      <div class='progress progress_sm'>
                        <div class='progress-bar bg-green' role='progressbar' data-transitiongoal='".$x['progress']."'></div>
                      </div>
                      <small>".$x['progress']."% Complete</small>
                    </td>
                    <td>
                      <button type='button' class='btn btn-success btn-xs'>".$x['status']."</button>
                    </td>
                    <td>
                      <a href='#' class='btn btn-primary btn-xs list_current_project' ><i class='fa fa-folder'></i> View </a>";
        if ($type == '3') {
            $string .= "<a href='#' class='btn btn-info btn-xs edit_current_project'><i class='fa fa-pencil'></i> Edit </a>";
        }

        $string .=
                    "<!--a href='#' class='btn btn-danger btn-xs'><i class='fa fa-trash-o'></i> Delete </a-->
                    </td>
                  </tr>";
    }





    $string .= "</tbody>
            </table>
            <script src='vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'></script>
            <!-- end project list -->

          </div>
        </div>
      </div>
    </div>
  </div>


";

    echo $string;
}
?>
