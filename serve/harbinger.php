<?php
//require ("/../serve/email.php");
//require ('functions.php');
require($_SERVER['DOCUMENT_ROOT'] . "/serve/functions.php");
require($_SERVER['DOCUMENT_ROOT'] . "/serve/email.php");

function notify_invoice($username, $url, $company, $website)
{
    return 'Hi there '. $username.'!,<br><br>You have some payments due. To view or download the invoices, login to your Insight account at:

    <br><br><b>'.$url.'</b><br><br>
    and go the <i>invoices</i> section of the page.

<br><br><i> </i><br><br>


Thank you,<br>
' . $company . '<br>
' . $website;
}

function notify_pass($company, $website)
{
    return 'Hello again,<br><br>We\'re just letting you know that your password has been successfully changed.
    br><br>


Thank you,<br>
' . $company . '<br>
' . $website;
}

function confirm_pass($url, $company, $website)
{
    return 'Hi there!,<br><br>In order to reset your password, please click the link below (so we know it\'s you), and we\'ll be on
    our way!

    <br><br><b>'.$url.'</b><br><br>

<br><br><i>This link expires in 24 hours. If you didn\'t want to reset your password, disregard this message and we\'ll handle things for you. </i><br><br>


Thank you,<br>
' . $company . '<br>
' . $website;
}

function confirm_email($url, $company, $website)
{
    return 'Hi there!,<br><br>In order to reset your email, please click the link below (so we know it\'s you), and we\'ll be on
    our way!

    <br><br><b>'.$url.'</b><br><br>

<br><br><i>This link expires in 24 hours. If you didn\'t want to reset your email, disregard this message and we\'ll handle things for you. </i><br><br>


Thank you,<br>
' . $company . '<br>
' . $website;
}

function domain_msg($name, $domain, $days_left, $date, $company, $website)
{
    return 'Dear ' . $name . ',<br><br>Your domain, <strong>' . $domain . '</strong>, will expire in ' . $days_left . '.
To avoid any downtime or loss of ownership, you will need to contact us via your KTS Insight
account, email or phone to renew your domain name before the expiry date; otherwise your domain
will be allowed to expire on <strong>' . date_format(new DateTime($date), 'l\, jS F Y') . '</strong>

<br><br>An expired domain will enter the redemption period after approximately 30 days. This holding period
lasts an additional 45 - 60 days, during which the domain will become available to be redeemed by the
current registrant for an additional fee, or be purchased by a third party for auction.
If the domain is not redeemed or acquired within the redemption period, it will be submitted to the Registry
for deletion and will be unrecoverable until it is released for anyone to register.<br><br>


Thank you,<br>
' . $company . '<br>
' . $website;
}

function hosting_msg($name, $domain, $days_left, $date, $company, $website)
{
    return 'Your web hosting for <strong>' . $domain . ' </strong> is coming up for renewal.
    Your account is currently set to expire in <strong>' . $days_left . '</strong>.
    Please contact us via your KTS Insight account, email or phone to renew your hosting plan before it expires on <strong>' . date_format(new DateTime($date), 'l\, jS F Y') . '</strong>

<br><br>We\'d hate for you to lose any of your data, so please download backups of all files, emails, and databases to your computer before the account expires.
Upon expiration, your account will be scheduled for deletion by our system and all content associated with your account
will be permanently removed from our servers. Renewing your account in the future won\'t restore your files,
so please make backups now.

<br><br>Thanks,<br>
' . $company . ' Renewals Team <br>
' . $website;
}

if (isset($_REQUEST['kts-testeem']) && !empty($_REQUEST['kts-testeem']) && trim($_REQUEST['kts-testeem']) == 'welcome') {
    echo 'hello!';
} elseif (isset($_REQUEST['kts-testeem']) && !empty($_REQUEST['kts-testeem']) && trim($_REQUEST['kts-testeem']) == 'domain-60') {
    $result2 = getDomain('', '4-60', $db) ['payload'];
    echo json_encode($result2);

    if ($result2 != null) {
        foreach ($result2 as $key) {
            $result3 = getClientDetails($key['cid'], '1', $db) ['payload'][0];
            echo '<br><br>' . json_encode($result3);

            $msg = domain_msg($result3['fname'], $key['domain'], '60 days', $key['expire'], $kts_company, $kts_website);

            sendEmail($kts_normal_email . $kts_url, $result3['email'], 'Domain Expiration', 'Your Domain will expire soon', $msg, $result3['fname']);
            echo '<br><br>' . $msg;
        }
    }
} elseif (isset($_REQUEST['kts-testeem']) && !empty($_REQUEST['kts-testeem']) && trim($_REQUEST['kts-testeem']) == 'domain-30') {
    $result2 = getDomain('', '4-30', $db) ['payload'];
    echo json_encode($result2);

    if ($result2 != null) {
        foreach ($result2 as $key) {
            $result3 = getClientDetails($key['cid'], '1', $db) ['payload'][0];
            echo '<br><br>' . json_encode($result3);

            $msg = domain_msg($result3['fname'], $key['domain'], '30 days', $key['expire'], $kts_company, $kts_website);

            sendEmail($kts_normal_email . $kts_url, $result3['email'], 'Domain Expiration', 'Your Domain will expire soon', $msg, $result3['fname']);
        }
    }
} elseif (isset($_REQUEST['kts-testeem']) && !empty($_REQUEST['kts-testeem']) && trim($_REQUEST['kts-testeem']) == 'domain-7') {
    $result2 = getDomain('', '4-7', $db) ['payload'];
    echo json_encode($result2);

    if ($result2 != null) {
        foreach ($result2 as $key) {
            $result3 = getClientDetails($key['cid'], '1', $db) ['payload'][0];
            echo '<br><br>' . json_encode($result3);

            $msg = domain_msg($result3['fname'], $key['domain'], '7 days', $key['expire'], $kts_company, $kts_website);

            sendEmail($kts_normal_email . $kts_url, $result3['email'], 'Domain Expiration', 'Your Domain will expire soon', $msg, $result3['fname']);
        }
    }
} elseif (isset($_REQUEST['kts-testeem']) && !empty($_REQUEST['kts-testeem']) && trim($_REQUEST['kts-testeem']) == 'domain-1') {
    $result2 = getDomain('', '4-1', $db) ['payload'];
    echo json_encode($result2);

    if ($result2 != null) {
        foreach ($result2 as $key) {
            $result3 = getClientDetails($key['cid'], '1', $db) ['payload'][0];
            echo '<br><br>' . json_encode($result3);

            $msg = domain_msg($result3['fname'], $key['domain'], '1 days', $key['expire'], $kts_company, $kts_website);

            sendEmail($kts_normal_email . $kts_url, 'kabton14@gmail.com', 'Domain Expiration', 'Your Domain will expire soon', $msg, $result3['fname']);
        }
    }
} elseif (isset($_REQUEST['kts-testeemh']) && !empty($_REQUEST['kts-testeemh']) && trim($_REQUEST['kts-testeemh']) == 'hosting-60') {
    $result2 = getHosting('', '4-60', $db) ['payload'];
    echo json_encode($result2);

    if ($result2 != null) {
        foreach ($result2 as $key) {
            $result3 = getClientDetails($key['cid'], '1', $db) ['payload'][0];
            echo '<br><br>' . json_encode($result3);

            $msg = hosting_msg($result3['fname'], $key['domain'], '60 days', $key['expire'], $kts_company, $kts_website);

            sendEmail($kts_normal_email . $kts_url, $result3['email'], 'Hosting Expiration', 'Your hosting plan will expire soon', $msg, $result3['fname']);
        }
    }
} elseif (isset($_REQUEST['kts-testeemh']) && !empty($_REQUEST['kts-testeemh']) && trim($_REQUEST['kts-testeemh']) == 'hosting-30') {
    $result2 = getHosting('', '4-30', $db) ['payload'];
    echo json_encode($result2);

    if ($result2 != null) {
        foreach ($result2 as $key) {
            $result3 = getClientDetails($key['cid'], '1', $db) ['payload'][0];
            echo '<br><br>' . json_encode($result3);

            $msg = hosting_msg($result3['fname'], $key['domain'], '30 days', $key['expire'], $kts_company, $kts_website);

            sendEmail($kts_normal_email . $kts_url, $result3['email'], 'Hosting Expiration', 'Your hosting plan will expire soon', $msg, $result3['fname']);
        }
    }
} elseif (isset($_REQUEST['kts-testeemh']) && !empty($_REQUEST['kts-testeemh']) && trim($_REQUEST['kts-testeemh']) == 'hosting-7') {
    $result2 = getHosting('', '4-7', $db) ['payload'];
    echo json_encode($result2);

    if ($result2 != null) {
        foreach ($result2 as $key) {
            $result3 = getClientDetails($key['cid'], '1', $db) ['payload'][0];
            echo '<br><br>' . json_encode($result3);

            $msg = hosting_msg($result3['fname'], $key['domain'], '7 days', $key['expire'], $kts_company, $kts_website);

            sendEmail($kts_normal_email . $kts_url, $result3['email'], 'Hosting Expiration', 'Your hosting plan will expire soon', $msg, $result3['fname']);
        }
    }
} elseif (isset($_REQUEST['kts-testeemh']) && !empty($_REQUEST['kts-testeemh']) && trim($_REQUEST['kts-testeemh']) == 'hosting-1') {
    $result2 = getHosting('', '4-1', $db) ['payload'];
    echo json_encode($result2);

    if ($result2 != null) {
        foreach ($result2 as $key) {
            $result3 = getClientDetails($key['cid'], '1', $db) ['payload'][0];
            echo '<br><br>' . json_encode($result3);

            $msg = hosting_msg($result3['fname'], $key['domain'], '1 day', $key['expire'], $kts_company, $kts_website);

            sendEmail($kts_normal_email . $kts_url, $result3['email'], 'Hosting Expiration', 'Your hosting plan will expire soon', $msg, $result3['fname']);
        }
    }
} elseif (isset($_REQUEST['kts-testeemi']) && !empty($_REQUEST['kts-testeemi']) && trim($_REQUEST['kts-testeemi']) == 'invoice-30') {
    $result2 = getExpiredInvoice('30', $db)['payload'];
    echo json_encode($result2);

    if ($result2 != null) {
        foreach ($result2 as $key) {
            $result3 = getClientDetails($key['cid'], '1', $db) ['payload'][0];
            echo '<br><br>' . json_encode($result3);

            $msg = notify_invoice($result3['fname'], $kts_insight_url, $kts_company, $kts_website);

            echo "<br> $msg";

            @sendEmail($kts_normal_email . $kts_url, $key['kts_email'], 'Payments Due', 'You have payments to settle', $msg, $result3['fname']);
        }
    }
}
