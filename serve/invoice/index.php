<?php
require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');


if ($type != '3') {
    $data = getInvoice($cid, '1', $db, $db2); //client
} else {



  if(isset($_REQUEST['operation']) && $_REQUEST['operation'] == 'old'){
    $operation = $_REQUEST['operation'];
    $data = getInvoice($cid, '2a', $db, $db2); //employee view- all clients' past invoices (receipts)

  } elseif (isset($_REQUEST['operation']) && $_REQUEST['operation'] == 'proforma'){
    $operation = $_REQUEST['operation'];
    $data = getInvoice($cid, '4b', $db, $db2); //employee view- all clients' proforma invoices

  } else {
    $data = getInvoice($cid, '2', $db, $db2); //employee view
    $operation = '';
  }

}

if (!isset($_SESSION['cid'])) {
    serveLogout();
} elseif (($data == null || $data == "") && $operation == 'old') {
    echo notFound('2b'); // no Receipts found
} elseif (($data == null || $data == "") && $operation == 'proforma') {
    echo notFound('2a'); // no PerformaInvoices found
} elseif ($data == null || $data == "") {
    echo notFound('2'); // no Invoices found
} else {

  kts_log(date('d-M-Y h:i:sa')."--- ".$fname." ".$lname." (company: ".$company.") viewed invoice list.", "invoice_views");
    echo "
<div class='col-md-12 col-sm-12 col-xs-12'>
  <div class='x_panel'>
    <div class='x_title'>
      <h2>Invoices <small>- .$company.</small></h2>
      <!--ul class='nav navbar-right panel_toolbox'>
        <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a>
        </li>
        <li class='dropdown'>
          <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'><i class='fa fa-wrench'></i></a>
          <ul class='dropdown-menu' role='menu'>
            <li><a href='#'>Settings 1</a>
            </li>
            <li><a href='#'>Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class='close-link'><i class='fa fa-close'></i></a>
        </li>
      </ul-->
      <div class='clearfix'></div>
    </div>

    <div class='x_content'>

      <p></p>

      <div class='table-responsive'>
        <table class='table table-striped jambo_table ' id='invoice-table'>
          <thead>
            <tr class='headings'>

              <th class='column-title'>Invoice </th>
              <th class='column-title'>Invoice Date </th>
              <th class='column-title'>Order </th>
              <th class='column-title'>Bill to Name </th>
              <th class='column-title'>Status </th>
              <th class='column-title'>Amount </th>
              <th class='column-title no-link last'><span class='nobr'>Action</span>
              </th>
              <th class='bulk-actions' colspan='7'>
                <a class='antoo' style='color:#fff; font-weight:500;'>Bulk Actions ( <span class='action-cnt'> </span> ) <i class='fa fa-chevron-down'></i></a>
              </th>
            </tr>
          </thead>

          <tbody>"; ?>

          <?php
        $count = 0;

    $curr_cid = null;
    $curr_pid = null;
    $curr_invoice = null;
    $curr_date = null;
    $curr_fake_id = null;
    $curr_name = null;
    $curr_paid = null;
    $curr_amount = null;



    function printRow($pid, $cid, $invoice, $date, $fakeid, $name, $paid, $amount, $denom, $count, $op)
    {
        $row_string = "";


        if($op == 'old'){
          if ($count % 2 == 0) {
              $row_string .=  "<tr class='even pointer view-old-invoice-details' data-cpid='".$pid."'>";
          } else {
              $row_string .=  "<tr class='odd pointer view-old-invoice-details' data-cpid='".$pid."'>";
          }



          $row_string .= "
                <td class=' view-old-invoice-details' data-cpid='".$pid."'><a href='#'>".$invoice."</a></td>
                <td class=' '>".$date."</td>
                <td class=' '>".$fakeid." <i class='success fa fa-long-arrow-up'></i></td>
                <td class=' '>".$name."</td>
                <td class=' '>".$paid."</td>
                <td class='a-right a-right '>".$denom."$ ".number_format($amount, 2, ".", ",")."</td>
                <td class=' last view-old-invoice-details' data-cpid='".$pid."' data-cid='".$cid."'><a href='#'>View</a>
                </td>
              </tr>";
        }
        elseif($op == 'proforma'){
          if ($count % 2 == 0) {
              $row_string .=  "<tr class='even pointer view-proforma-details' data-cpid='".$pid."'>";
          } else {
              $row_string .=  "<tr class='odd pointer view-proforma-details' data-cpid='".$pid."'>";
          }



          $row_string .= "
                <td class=' view-old-invoice-details' data-cpid='".$pid."'><a href='#'>".$invoice."</a></td>
                <td class=' '>".$date."</td>
                <td class=' '>".$fakeid." <i class='success fa fa-long-arrow-up'></i></td>
                <td class=' '>".$name."</td>
                <td class=' '>".$paid."</td>
                <td class='a-right a-right '>".$denom."$ ".number_format($amount, 2, ".", ",")."</td>
                <td class=' last view-old-invoice-details' data-cpid='".$pid."' data-cid='".$cid."'><a href='#'>View</a>
                </td>
              </tr>";
        }
        else{
          if ($count % 2 == 0) {
              $row_string .=  "<tr class='even pointer view-invoice-details' data-cpid='".$pid."'>";
          } else {
              $row_string .=  "<tr class='odd pointer view-invoice-details' data-cpid='".$pid."'>";
          }



          $row_string .= "
                <td class=' view-invoice-details' data-cpid='".$pid."'><a href='#'>".$invoice."</a></td>
                <td class=' '>".$date."</td>
                <td class=' '>".$fakeid." <i class='success fa fa-long-arrow-up'></i></td>
                <td class=' '>".$name."</td>
                <td class=' '>".$paid."</td>
                <td class='a-right a-right '>".$denom."$ ".number_format($amount, 2, ".", ",")."</td>
                <td class=' last view-invoice-details' data-cpid='".$pid."' data-cid='".$cid."'><a href='#'>View</a>
                </td>
              </tr>";
        }





        return $row_string;
    }


    foreach ($data as $k) {
        $denom = 'JM';
        $amount = 0 ;

        if ($type != 3 && $k['currency'] != 'jmd') {
            $amount = fxConvert($cid, $k['amount'], $k['date'], $us_dollar, $db)['payload'];

            $denom = 'US';
        } else {
            $amount = $k['amount'];
        }


        if ($k == reset($data)) {
            $curr_cid = $k['client'];
            $curr_pid = $k['pid'];
            $curr_invoice = $k['invoice'];
            $curr_date = $k['date'];
            $curr_fake_id = $k['fakeid'];
            $curr_name = $k['name'];
            $curr_paid = $k['paid'];
            $curr_amount = $amount;

            if ($k == end($data)) {
                echo printRow($curr_pid, $curr_cid, $curr_invoice, $curr_date, $curr_fake_id, $curr_name, $curr_paid, $curr_amount, $denom, $count, $operation);
                $count++;
            }
        } elseif ($k['client'] == $curr_cid && $k['pid'] == $curr_pid) {
            $curr_amount += $amount;
            $curr_date = $k['date'];

            if ($k == end($data)) {
                echo printRow($curr_pid, $curr_cid, $curr_invoice, $curr_date, $curr_fake_id, $curr_name, $curr_paid, $curr_amount, $denom, $count, $operation);
                $count++;
            }
        } else {
            echo printRow($curr_pid, $curr_cid, $curr_invoice, $curr_date, $curr_fake_id, $curr_name, $curr_paid, $curr_amount, $denom, $count, $operation);
            $count++;

            $curr_cid = $k['client'];
            $curr_pid = $k['pid'];
            $curr_invoice = $k['invoice'];
            $curr_date = $k['date'];
            $curr_fake_id = $k['fakeid'];
            $curr_name = $k['name'];
            $curr_paid = $k['paid'];
            $curr_amount = $amount;

            if ($k == end($data)) {
                echo printRow($curr_pid, $curr_cid, $curr_invoice, $curr_date, $curr_fake_id, $curr_name, $curr_paid, $curr_amount, $denom, $count, $operation);
                $count++;
            }
        }
    }

    echo "
          </tbody>
        </table>
      </div>


    </div>
  </div>
</div>
";
}
?>
