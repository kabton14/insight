<?php


require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if ($type != '3') {
    notFound('1');
    return false;
}

$employees = getAllEmployees($db)['payload'];
$clients = getAllClients($db)['payload'];
?>

<div class='col-md-12 col-sm-12 col-xs-12'>
                <div class='x_panel'>
                  <div class='x_title'>
                    <h2>Create New Project <small></small></h2>

                    <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>
                    <br>


                    <br>
                    <form id='create-project-form'  class='form-horizontal form-label-left' novalidate=''>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-project-client'>Client <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <select id='create-project-client' name='create-project-client' required='required' class='form-control'>

                            <option value=''>Choose Client</option>

                            <?php foreach ($clients as $value) {
    echo "<option value='".ktsEncode($value['cid'])."'>".$value['lname']." ".$value['fname']." (<b>".$value['name']."</b>)</option>";
} ?>

                          </select>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-project-name'>Project Name <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='text' id='create-project-name' name='create-project-name' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>


                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-project-type'>Project Type <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <select id='create-project-type' name='create-project-type' required='required' class='form-control'>

                            <option value=''>Choose option</option>

                            <?php foreach ($kts_services as $value) {
    echo "<option value='".$value."'>".$value."</option>";
} ?>

                          </select>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-project-leader'>Project Leader <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <select id='create-project-leader' name='create-project-leader' required='required' class='form-control'>

                            <option value=''>Choose KTS Employee</option>

                            <?php foreach ($employees as $value) {
    echo '<option value='.ktsEncode($value['eid']).'>'.$value['fname']." ".$value['lname'].'</option>';
} ?>

                          </select>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-start-date-picker'>Desired Start Date <span class='required'>*</span>
                        </label>
                        <div class='input-group date col-md-4 col-sm-4col-xs-10 start-date-picker' id='request-date-picker'>
                            <input type='text' id='create-start-date-picker' name='create-start-date-picker' placeholder='YYYY-MM-DD'class='form-control start-date-picker'>
                            <span class='input-group-addon'>
                               <span class='glyphicon glyphicon-calendar'></span>
                            </span>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-pend-date-picker'>Projected End Date <span class='required'>*</span>
                        </label>
                        <div class='input-group date col-md-4 col-sm-4col-xs-10 start-date-picker' id='request-date-picker'>
                            <input type='text' id='create-pend-date-picker' name='create-pend-date-picker' placeholder='YYYY-MM-DD'class='form-control pend-date-picker'>
                            <span class='input-group-addon'>
                               <span class='glyphicon glyphicon-calendar'></span>
                            </span>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-project-progress'>Project Progress (%) <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='number' min='0' max='100' id='create-project-progress' name='create-project-progress' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-project-status'>Project Status <span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6'>
                          <select id='create-project-status' name='create-project-status' required='required' class='form-control'>

                            <option value=''>Choose Project Status</option>

                            <?php foreach ($kts_project_status as $value) {
    echo "<option value='".$value."'>".$value."</option>";
} ?>

                          </select>
                        </div>
                      </div>


                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12'>General Description of Project</label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <div class='btn-toolbar editor' data-role='editor-toolbar' data-target='#editor-one'>
                            <div class='btn-group'>
                              <a class='btn dropdown-toggle' data-toggle='dropdown' title='Font'><i class='fa fa-font'></i><b class='caret'></b></a>
                              <ul class='dropdown-menu'>
                              </ul>
                            </div>

                            <div class='btn-group'>
                              <a class='btn dropdown-toggle' data-toggle='dropdown' title='Font Size'><i class='fa fa-text-height'></i>&nbsp;<b class='caret'></b></a>
                              <ul class='dropdown-menu'>
                                <li>
                                  <a data-edit='fontSize 5'>
                                    <p style='font-size:17px'>Huge</p>
                                  </a>
                                </li>
                                <li>
                                  <a data-edit='fontSize 3'>
                                    <p style='font-size:14px'>Normal</p>
                                  </a>
                                </li>
                                <li>
                                  <a data-edit='fontSize 1'>
                                    <p style='font-size:11px'>Small</p>
                                  </a>
                                </li>
                              </ul>
                            </div>

                            <div class='btn-group'>
                              <a class='btn' data-edit='bold' title='Bold (Ctrl/Cmd+B)'><i class='fa fa-bold'></i></a>
                              <a class='btn' data-edit='italic' title='Italic (Ctrl/Cmd+I)'><i class='fa fa-italic'></i></a>
                              <a class='btn' data-edit='strikethrough' title='Strikethrough'><i class='fa fa-strikethrough'></i></a>
                              <a class='btn' data-edit='underline' title='Underline (Ctrl/Cmd+U)'><i class='fa fa-underline'></i></a>
                            </div>

                            <div class='btn-group'>
                              <a class='btn' data-edit='insertunorderedlist' title='Bullet list'><i class='fa fa-list-ul'></i></a>
                              <a class='btn' data-edit='insertorderedlist' title='Number list'><i class='fa fa-list-ol'></i></a>
                              <a class='btn' data-edit='outdent' title='Reduce indent (Shift+Tab)'><i class='fa fa-dedent'></i></a>
                              <a class='btn' data-edit='indent' title='Indent (Tab)'><i class='fa fa-indent'></i></a>
                            </div>

                            <div class='btn-group'>
                              <a class='btn' data-edit='justifyleft' title='Align Left (Ctrl/Cmd+L)'><i class='fa fa-align-left'></i></a>
                              <a class='btn' data-edit='justifycenter' title='Center (Ctrl/Cmd+E)'><i class='fa fa-align-center'></i></a>
                              <a class='btn' data-edit='justifyright' title='Align Right (Ctrl/Cmd+R)'><i class='fa fa-align-right'></i></a>
                              <a class='btn' data-edit='justifyfull' title='Justify (Ctrl/Cmd+J)'><i class='fa fa-align-justify'></i></a>
                            </div>

                            <div class='btn-group'>
                              <a class='btn dropdown-toggle' data-toggle='dropdown' title='Hyperlink'><i class='fa fa-link'></i></a>
                              <div class='dropdown-menu input-append'>
                                <input class='span2' placeholder='URL' type='text' data-edit='createLink' />
                                <button class='btn' type='button'>Add</button>
                              </div>
                              <a class='btn' data-edit='unlink' title='Remove Hyperlink'><i class='fa fa-cut'></i></a>
                            </div>


                            <div class='btn-group'>
                              <a class='btn' data-edit='undo' title='Undo (Ctrl/Cmd+Z)'><i class='fa fa-undo'></i></a>
                              <a class='btn' data-edit='redo' title='Redo (Ctrl/Cmd+Y)'><i class='fa fa-repeat'></i></a>
                            </div>
                          </div>

                          <div id='editor-one' class='editor-wrapper'></div>

                          <textarea name='descr' id='descr' style='display:none;'></textarea>

                        </div>
                      </div>

                      <div class='form-group'>
                        <label class='control-label col-md-3 col-sm-3 col-xs-12' for='create-project-deposit'>Deposit ($)<span class='required'>*</span>
                        </label>
                        <div class='col-md-6 col-sm-6 col-xs-12'>
                          <input type='number' min='0' id='create-project-deposit' name='create-project-deposit' required='required' class='form-control col-md-7 col-xs-12'>
                        </div>
                      </div>

                      <


                      <div class='ln_solid'></div>
                      <div class='form-group'>
                        <div class='col-md-6 col-sm-6 col-xs-12 col-md-offset-3'>
                          <button class='btn btn-primary create-project-reset' type='button'>Cancel</button>
                          <button class='btn btn-primary create-project-reset' type='button'>Reset</button>
                          <button type='button' class='btn btn-success create-project-submit'>Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
