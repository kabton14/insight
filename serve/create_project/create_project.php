<?php

require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');


if ($_POST['option']=='create_project') {
    if (1<2) {
        $project_name = mysqli_real_escape_string($db, trim($_POST['project_name']));
        $project_type = mysqli_real_escape_string($db, trim($_POST['project_type']));
        $project_leader = mysqli_real_escape_string($db, ktsDecode(trim($_POST['project_leader'])));
        $project_start_date = mysqli_real_escape_string($db, trim($_POST['project_start_date']));
        $project_end_date = mysqli_real_escape_string($db, trim($_POST['project_end_date']));
        $project_time_logged = '0';
        $project_progress = mysqli_real_escape_string($db, trim($_POST['project_progress']));
        $project_status = mysqli_real_escape_string($db, trim($_POST['project_status']));
        $project_description = mysqli_real_escape_string($db, trim($_POST['project_description']));
        $project_deposit = mysqli_real_escape_string($db, trim($_POST['project_deposit']));
        $client = $_POST['project_client'];
        $client = mysqli_real_escape_string($db, ktsDecode(trim($client)));

        $result = createProject(
        $client,
        $project_name,
        $project_type,
        $project_leader,
        $project_start_date,
        $project_end_date,
        $project_time_logged,
        $project_progress,
    $project_status,
        $project_description,
        $project_deposit,
        $db
    );

        echo json_encode($result);
        return $result;
    } else {
        $result = genResult('2', 'Please ensure all fields are filled.', null);
        echo json_encode($result);
        return $result;
    }
} else {
    $result = genResult('2', 'Unsupported Operation.', null);
    echo json_encode($result);
    return $result;
}
