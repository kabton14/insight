<?php
require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if (isset($_POST['nth'])) {
    $nth = $_POST['nth'];



    $data = getSent($type, $cid, $db);

    echo "

  <div class='mail_heading row'>
    <div class='col-md-8'>
      <div class='btn-group'>
        <button class='btn btn-sm btn-primary' type='button'><i class='fa fa-reply'></i> Reply</button>
        <button class='btn btn-sm btn-default' type='button'  data-placement='top' data-toggle='tooltip' data-original-title='Forward'><i class='fa fa-share'></i></button>
        <button class='btn btn-sm btn-default' type='button' data-placement='top' data-toggle='tooltip' data-original-title='Print'><i class='fa fa-print'></i></button>
        <button class='btn btn-sm btn-default' type='button' data-placement='top' data-toggle='tooltip' data-original-title='Trash'><i class='fa fa-trash-o'></i></button>
      </div>
    </div>
    <div class='col-md-4 text-right'>
      <p class='date'>".date_format(date_create($data[$nth]['date_received']), 'g:ia \o\n l jS F Y') ."</p>
    </div>
    <div class='col-md-12'>
      <h4>".$data[$nth]['subj']."</h4>
    </div>
  </div>
  <div class='sender-info'>
    <div class='row'>
      <div class='col-md-12'>
        <strong>".$data[$nth]['sender_fname']." ".$data[$nth]['sender_lname']."</strong>
        <span>-</span> to
        <strong>me</strong>
        <a class='sender-dropdown'><i class='fa fa-chevron-down'></i></a>
      </div>
    </div>
  </div>
  <div class='view-mail'>
  <br>


    <p>".$data[$nth]['msg']."</p>

  </div>

  <div class='btn-group'>
    <button class='btn btn-sm btn-primary' type='button'><i class='fa fa-reply'></i> Reply</button>
    <button class='btn btn-sm btn-default' type='button'  data-placement='top' data-toggle='tooltip' data-original-title='Forward'><i class='fa fa-share'></i></button>
    <button class='btn btn-sm btn-default' type='button' data-placement='top' data-toggle='tooltip' data-original-title='Print'><i class='fa fa-print'></i></button>
    <button class='btn btn-sm btn-default' type='button' data-placement='top' data-toggle='tooltip' data-original-title='Trash'><i class='fa fa-trash-o'></i></button>
  </div>
";
}
