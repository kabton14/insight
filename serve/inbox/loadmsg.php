<?php
require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

if (isset($_POST['nth'])) {
    $nth = $_POST['nth'];
    $nthd = $nth;
    $nth = ktsDecode($nth);



    $data = getInbox($type, $cid, $db, $db2);

    $msgid = ktsEncode($data[$nth]['msgid']);

    setRead($data[$nth]['msgid'], $cid, $type, $db);

    if ((trim($data[$nth]['sender']) == trim($cid)) && (trim($data[$nth]['sender_type']) == trim($type))) {
        $receiver = ktsEncode('1');
        $receiver_type = ktsEncode('3');
    } else {
        $receiver = ktsEncode($data[$nth]['sender']);
        $receiver_type = ktsEncode($data[$nth]['sender_type']);
    }

    if ($data[$nth]['msg_read'] == '0') {
        setRead(ktsDecode($msgid), ktsDecode($receiver), $type, $db);
    }

    $first_sender = '';
    $first_receiver = '';

    if ((trim($data[$nth]['receiver']) == trim($cid)) && (trim($data[$nth]['receiver_type']) == trim($type))) {
        $first_receiver = 'me';
    } else {
        $first_receiver = trim($data[$nth]['receiver_fname'])." ".trim($data[$nth]['receiver_lname']);
    }

    if ((trim($data[$nth]['sender']) == trim($cid)) && (trim($data[$nth]['sender_type']) == trim($type))) {
        $first_sender = 'me';
    } else {
        $first_sender = trim($data[$nth]['sender_fname'])." ".trim($data[$nth]['sender_lname']);
    }


    echo "
  <div class='mail_heading row'>
    <div class='col-md-8'>
      <div class='btn-group'>
        <button class='btn btn-sm btn-primary kts-reply'  data-msg-index='".$nthd."' data-msg-root='".$msgid."' data-receiver='".$receiver."' data-receiver-type='".$receiver_type."' type='button'><i class='fa fa-reply'></i> Reply</button>
        <button class='btn btn-sm btn-default' type='button'  data-placement='top' data-toggle='tooltip' data-original-title='Forward'><i class='fa fa-share'></i></button>
        <button class='btn btn-sm btn-default kts-mail-print' type='button' data-placement='top' data-toggle='tooltip' data-original-title='Print'><i class='fa fa-print'></i></button>
        <button class='btn btn-sm btn-default kts-mail-delete' type='button' data-placement='top' data-toggle='tooltip' data-original-title='Trash' data-msg-root='".$data[$nth]['msgid']."'><i class='fa fa-trash-o'></i></button>
      </div>
    </div>

    <div class='col-md-12'>
      <h4 class='kts-subject'>".$data[$nth]['subj']."</h4>
    </div>
  </div>
  <div class='sender-info'>
    <div class='row'>
      <div class='col-md-12'>
        <strong class='sender_name'>".$first_sender."</strong>
        <span>-</span> to
        <strong>".$first_receiver."</strong>
        <a class='sender-dropdown'><i class='fa fa-chevron-down'></i></a>
      </div>
      <div class='col-md-12 text-right'>
        <p class='date'><i><small>".date_format(date_create($data[$nth]['date_received']), 'g:ia \o\n l jS F Y') ."</small></i></p>
      </div>
    </div>
  </div>
  <div class='view-mail'>
  <br>


    <p>".$data[$nth]['msg']."</p>

  </div>

  <div class='btn-group'>
    <button class='btn btn-sm btn-primary  kts-reply' data-msg-index='".$nth."' data-msg-root='".$msgid."' data-receiver='".$receiver."' data-receiver-type='".$receiver_type."' type='button'><i class='fa fa-reply'></i> Reply</button>
    <button class='btn btn-sm btn-default' type='button'  data-placement='top' data-toggle='tooltip' data-original-title='Forward'><i class='fa fa-share'></i></button>
    <button class='btn btn-sm btn-default' type='button' data-placement='top' data-toggle='tooltip' data-original-title='Print'><i class='fa fa-print'></i></button>
    <button class='btn btn-sm btn-default kts-mail-delete' type='button' data-placement='top' data-toggle='tooltip' data-original-title='Trash' data-msg-root='".$data[$nth]['msgid']."'><i class='fa fa-trash-o'></i></button>
  </div>
";


    if ($data[$nth]['reply_msgs'] != null) {
        foreach ($data[$nth]['reply_msgs'] as $key) {
            echo "

       <div class='mail_heading row'>
         <div class='col-md-8'>

         </div>

         <div class='col-md-12'>
           <h4>".""."</h4>
         </div>
       </div>
       <div class='sender-info'>
         <div class='row'>
           <div class='col-md-12'>
             <strong>".$key['sender_fname']." ".$key['sender_lname']."</strong>
             <span>-</span> to
             <strong>".$key['receiver_fname']." ".$key['receiver_lname']."</strong>
             <a class='sender-dropdown'><i class='fa fa-chevron-down'></i></a>
           </div>
           <div class='col-md-12 text-right'>
             <p class='date'><i><small>".date_format(date_create($key['date_received']), 'g:ia \o\n l jS F Y') ."</small></i></p>
           </div>
         </div>
       </div>
       <div class='view-mail'>
       <br>


         <p>".$key['msg']."</p>

       </div>


     ";
        }
    }


    echo "";
}




 ?>

 <!-- Small modal -->

 <div class='modal fade delete-msg-modal-sm' tabindex='-1' role='dialog' aria-hidden='true'>
   <div class='modal-dialog modal-sm'>
     <div class='modal-content'>

       <div class='modal-header'>
         <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>×</span>
         </button>
         <h4 class='modal-title' id='myModalLabel2'>Delete Message</h4>
       </div>
       <div class='modal-body'>
         <h4>Please Confirm...</h4>
         <p>Are you sure you want to delete this message?</p>
       </div>
       <div class='modal-footer'>
         <button type='button' class='btn btn-default' data-dismiss='modal'>No</button>
         <button type='button' class='btn btn-primary delete-msg-modal-accept'>Yes, I'm Sure</button>
       </div>

     </div>
   </div>
 </div>
 <!-- /modals -->
