<?php

require($_SERVER['DOCUMENT_ROOT'].'/serve/functions.php');

$data = getInbox($type, $cid, $db, $db2);

if (!isset($_SESSION['cid']) || !isset($_SESSION['type'])) {
    serveLogout();
} elseif ($data == null || $data == "") {
    echo notFound('4'); // no messages found
} else {
    echo "

  <div class='inbox'>

    <div class='page-title'>
      <div class='title_left'>
        <h3>Messages <small></small></h3>
      </div>

      <div class='title_right'>
        <!--div class='col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search'>
          <div class='input-group'>
            <input type='text' class='form-control' placeholder='Search for...'>
            <span class='input-group-btn'>
              <button class='btn btn-default' type='button'>Go!</button>
            </span>
          </div>
        </div-->
      </div>
    </div>

    <div class='clearfix'></div>

    <div class='row'>
      <div class='col-md-12'>
        <div class='x_panel'>
          <div class='x_title'>
            <h2>Inbox<small>User Mail</small></h2>
            <!--ul class='nav navbar-right panel_toolbox'>
              <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a>
              </li>
              <li class='dropdown'>
                <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'><i class='fa fa-wrench'></i></a>
                <ul class='dropdown-menu' role='menu'>
                  <li><a href='#'>Settings 1</a>
                  </li>
                  <li><a href='#'>Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class='close-link'><i class='fa fa-close'></i></a>
              </li>
            </ul-->
            <div class='clearfix'></div>
          </div>
          <div class='x_content'>
            <div class='row'>
              <div class='col-sm-3 mail_list_column'>
                <button id='compose' class='btn btn-sm btn-success btn-block' type='button'>COMPOSE</button>";

    foreach ($data as $d) {
        $nth = ktsEncode($d['n']);
        $nachricht = ktsEncode($d['msgid']);

        echo "
                <a href='#'>
                  <div class='mail_list' data-nth='".$nth."'>
                    <div class='left' data-nth='".$nth."'>";

        if ($d['msg_read'] == null || $d['msg_read'] == 0) {
            echo "<i class='fa fa-circle'></i>";
        } else {
            echo "<i class='fa fa-circle-o'></i>";
        }

        echo "</div>
                    <div class='right' data-nth='".$nth."'>
                      <h3>".$d['sender_fname']." ".$d['sender_lname']." <small>".date_format(date_create($d['date_received']), 'F jS Y')."</small></h3>
                      <p data-nth='".$nth."'>";

        if ($d['replies'] != null || $d['replies'] > 0) {
            echo "<span class='badge'>".$d['replies']."</span>";
        }

        echo $d['subj']."...</p>
                    </div>
                  </div>
                </a>
                ";
    }

    echo "

              </div>
              <!-- /MAIL LIST -->

              <!-- CONTENT MAIL -->
              <div class='col-sm-9 mail_view'>
                <div class='inbox-body'>
                  <div class='mail_heading row'>
                    <div class='col-md-8'>


                    </div>
                    <div class='col-md-12 '>
                    <div class='mail-box'></div>
                      <h4 class='inbox-subject'>To start reading, select one of the available messages.</h4>
                    </div>
                  </div>
                  <div class='sender-info'>
                    <div class='row'>

                    </div>
                  </div>

                  <div class='attachment'>


                  </div>

                </div>

              </div>
              <!-- /CONTENT MAIL -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>




  ";
}
