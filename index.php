<?php
require($_SERVER['DOCUMENT_ROOT'] ."/serve/functions.php");


if (!isset($_SESSION['username'])) { //If user is not logged in then he cannot access the profile page
//echo 'You are not logged in. <a href='login.php'>Click here</a> to log in.';
    header('location:/login');
}

$messages_count = checkNewMessages($cid, $type, $db)['unread_msg_count'];
$data = getInbox($type, $cid, $db, $db2);

$logo = 'images/KTS_Logo_only_white.png';

$client_pic = 'client/'.$cid.'/avatar/';
$initials_pic = 'https://dummyimage.com/128x128/000/fff&text='.$fname[0].''.$lname[0];
$generic_pic = 'images/gen_avatar.png';

$avatar = getAvatar($client_pic, $initials_pic, $generic_pic);


?>
<!DOCTYPE html>
<html lang='en'>
  <head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112690834-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-112690834-1');
</script>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>

    <title>Insight| Kabton Tech Services </title>

    <!-- Bootstrap -->
    <link href='vendors/bootstrap/dist/css/bootstrap.min.css' rel='stylesheet'>
    <!-- Font Awesome -->
    <link href='vendors/font-awesome/css/font-awesome.min.css' rel='stylesheet'>
    <!-- NProgress -->
    <link href='vendors/nprogress/nprogress.css' rel='stylesheet'>
    <!-- iCheck -->
    <link href='vendors/iCheck/skins/flat/green.css' rel='stylesheet'>
>
    <!-- Select2 -->
    <link href="vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="vendors/switchery/dist/switchery.min.css" rel="stylesheet">

    <!--dropzone-->
    <link href='vendors/dropzone/dist/min/dropzone.min.css' rel='stylesheet'>

    <!-- bootstrap-wysiwyg -->
    <link href='vendors/google-code-prettify/bin/prettify.min.css' rel='stylesheet'>

    <!-- bootstrap-datetimepicker -->
    <link href='vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css' rel='stylesheet'>

    <!-- PNotify -->
    <link href='vendors/pnotify/dist/pnotify.css' rel='stylesheet'>
    <link href='vendors/pnotify/dist/pnotify.buttons.css' rel='stylesheet'>
    <link href='ui-pnotify dark ui-pnotify-fade-normal ui-pnotify-in ui-pnotify-fade-in ui-pnotify-movevendors/pnotify/dist/pnotify.nonblock.css' rel='stylesheet'>

    <!-- Pagination -->
    <link href='//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet'>

    <!-- Custom Theme Style -->
    <link href='build/css/custom.min.css' rel='stylesheet'>

    <style>
@import url('https://fonts.googleapis.com/css?family=Open+Sans');
</style>
  </head>

  <!--Start of Tawk.to Script-->
  <script type="text/javascript">
  var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
  (function(){
  var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
  s1.async=true;
  s1.src='https://embed.tawk.to/58fe66894ac4446b24a6bcfe/default';
  s1.charset='UTF-8';
  s1.setAttribute('crossorigin','*');
  s0.parentNode.insertBefore(s1,s0);
  })();
  </script>
  <!--End of Tawk.to Script-->
  <body class='nav-md'>
    <div class='container body'>
      <div class='main_container'>
        <div class='col-md-3 left_col'>
          <div class='left_col scroll-view'>
            <div class='navbar nav_title' style='border: 0;'>
              <a href='/' class='site_title'><img src='<?php echo $logo; ?>'/><span></span> <span>Insight<?php if ($type == '3') {
    echo"  <i>admin</i>";
} else {
    echo "<sup><i>beta</i></sup>";
}?></span></a>
            </div>

            <div class='clearfix'></div>

            <!-- menu profile quick info -->
            <div class='profile clearfix'>
              <div class='profile_pic'>
                <img src='<?php echo $avatar; ?>' alt='...' class='img-circle profile_img'>
              </div>
              <div class='profile_info'>
                <span>Welcome,</span>
                <h2><?php echo $fname.' '.$lname; ?></h2>
              </div>
              <div class='clearfix'></div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id='sidebar-menu' class='main_menu_side hidden-print main_menu'>
              <div class='menu_section'>
                <h3>General</h3>
                <ul class='nav side-menu'>
                  <li class='kts-menu-parent'><a href='/'><i class='fa fa-home'></i> Home </span></a>

                  </li>

                  <li class='kts-menu-parent'><a><i class='fa fa-inbox'></i> Messages
                    <?php if ($messages_count > 0) {
    echo "<span class='badge bg-green message-count-badge'>$messages_count</span>";
}?>
                    <span class='fa fa-chevron-down'></span></a>
                    <div class='kts-menu-wrapper'>
                    <ul class='nav child_menu'>
                      <li class='get-inbox'><a>Inbox</a></li>
                      <li class='create-msg'><a>New </a></li>
                      <!--li class='get-sent'><a>Sent</a></li-->
                    </ul>
                  </div>
                  </li>



                  <li class='kts-menu-parent'><a><i class='fa fa-list-alt'></i> Projects <span class='fa fa-chevron-down'></span></a>
                    <div class='kts-menu-wrapper'>
                    <ul class='nav child_menu'>
                      <li class='current_project'><a>Current Projects</a></li>
                      <?php if ($type == '3') { echo "<li class='past-project'><a>Past Projects</a></li><li class='create_project'><a>Create New Project</a></li>";} ?>
                      <li class='request_project'><a href='#'>Request New Project</a></li>
                    </ul>
                  </div>
                  </li>

                  <li class='kts-menu-parent'><a><i class='fa fa-code'></i> Website Manager <span class='fa fa-chevron-down'></span></a>
                    <div class='kts-menu-wrapper'>
                    <ul class='nav child_menu'>
                      <li class='get-hosting'><a>Web Hosting</a></li>
                      <li class='get-domain'><a>Domains</a></li>
                      <?php if ($type == '3') {
    echo "<li class='create-web-item'><a>Add New</a></li>";
} ?>

                    </ul>
                  </div>
                  </li>



                  <li class='kts-menu-parent'><a><i class='fa fa-list-alt'></i> Invoices <span class='fa fa-chevron-down'></span></a>
                    <div class='kts-menu-wrapper'>
                    <ul class='nav child_menu'>
                      <li class='get-invoice'><a><i class='fa fa-money '></i> Current Invoices </a></li>
                      <li class='get-proforma'><a><i class='fa fa-calculator'></i> Proforma Invoices </a></li>
                      <?php if ($type == '3') { echo "<li class='get-old'><a><i class='fa fa-history '></i>Past Invoices</a></li>";} ?>
                    </ul>
                  </div>
                  </li>

                  <li class='kts-menu-parent'><a><i class='fa fa-users'></i> Contact <span class='fa fa-chevron-down'></span></a>
                    <div class='kts-menu-wrapper'>
                    <ul class='nav child_menu'>
                      <li class='' ><a href='javascript:void(Tawk_API.toggle())'>Live Chat</a></li>
                      <li class=''><a>Contact Info</a></li>
                    </ul>
                  </div>
                  </li>




                </ul>
              </div>
              <div class='menu_section'>

              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class='sidebar-footer hidden-small'>
              <a class='client-settings' data-toggle='tooltip' data-placement='top' title='Settings'>
                <span class='glyphicon glyphicon-cog' aria-hidden='true'></span>
              </a>
              <a data-toggle='tooltip' data-placement='top' title='FullScreen'>
                <span class='glyphicon glyphicon-fullscreen' aria-hidden='true'></span>
              </a>
              <a data-toggle='tooltip' data-placement='top' title='Lock'>
                <span class='glyphicon glyphicon-eye-close' aria-hidden='true'></span>
              </a>
              <a data-toggle='tooltip' data-placement='top' title='Logout' href='logout'>
                <span class='glyphicon glyphicon-off' aria-hidden='true'></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class='top_nav'>
          <div class='nav_menu'>
            <nav>
              <div class='nav toggle'>
                <a id='menu_toggle'><i class='fa fa-bars'></i></a>
              </div>

              <ul class='nav navbar-nav navbar-right'>
                <li class=''>
                  <a href='javascript:;' class='user-profile dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>
                    <img src='<?php echo $avatar; ?>' alt=''><?php echo $fname.' '.$lname; ?>
                    <span class=' fa fa-angle-down'></span>
                  </a>
                  <ul class='dropdown-menu dropdown-usermenu pull-right'>
                    <li class='client-settings'>
                      <a href='javascript:;'>
                        <!--span class='badge bg-red pull-right'>50%</span-->
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href='javascript:;'>Help</a></li>
                    <li><a href='logout'><i class='fa fa-sign-out pull-right'></i> Log Out</a></li>
                  </ul>
                </li>


                <li role='presentation' class='dropdown'>
                  <a href='javascript:;' class='dropdown-toggle info-number' data-toggle='dropdown' aria-expanded='false'>
                    <i class='fa fa-envelope-o'></i>
                    <?php
                    if ($messages_count > 0) {
                        echo "<span class='badge bg-green message-count-badge'> $messages_count </span>";
                    }
                    ?>
                  </a>
                  <ul id='menu1' class='dropdown-menu list-unstyled msg_list' role='menu'>
                    <?php
                    $max = 4;
                    foreach ($data as $d) {
                        if ($max == 0) {
                            break;
                        }
                        echo "
                      <li class='get-inbox'>
                        <a>";
                        if ($d['sender_type'] == '3') {
                            echo "<span class='image'><img src='/insight/".getEmployee($d['sender'], $db)['avatar']."' alt='Sender: ' /></span>";
                        } else {
                            echo "<span class='image'><img src='' alt='Sender: ' /></span>";
                        }
                        echo "  <span>
                            <span>".$d['sender_fname']." ".$d['sender_lname']."</span>
                            <span class='time'>".calcTimeGone($d['date_received'], date("Y-m-d h:i:s"))['payload']."</span>
                          </span>
                          <span class='message'>
                            ".$d['subj']."...
                          </span>
                        </a>
                      </li>";
                        $max--;
                    }


                    ?>

                    <li class='get-inbox'>
                      <div class='text-center'>
                        <a>
                          <strong>See All Messages</strong>
                          <i class='fa fa-angle-right'></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->

          <?php


          if ($type == '3') {
              $total_clients = getTotalClients($db)['payload'];
              $total_employee_project_hours = getTimeLogged($cid, '2', 'sum', $db)['payload'];
              $avg_employee_project_hours = getTimeLogged($cid, '2', 'avg_comp', $db)['payload'];
              $total_employee_project_inprogress = getProjectStats($cid, '2', 'in_progress', $db)['payload'];
              $total_employee_project_completed = getProjectStats($cid, '2', 'completed', $db)['payload'];

              $eBalance = getBalance($cid, '2', 'total', $db)['payload'];

              $total_employee_balance = 0;

              foreach ($eBalance as $eb) {
                  $total_employee_balance += $eb['amount']; //fxConvert($cid, $eb['amount'], $eb['date'], $us_dollar, $db)['payload'];
              }

              $final_employee_balance = number_format($total_employee_balance, 0, ".", ",") ;


              echo "          <div class='row tile_count'>
                        <div class='col-md-2 col-sm-4 col-xs-6 tile_stats_count'>
                          <span class='count_top'><i class='fa fa-user'></i> Total Clients</span>
                          <div class='count'>".$total_clients."</div>
                          <!--span class='count_bottom'><i class='green'>4% </i> From last Week</span-->
                        </div>
                        <div class='col-md-2 col-sm-4 col-xs-6 tile_stats_count'>
                          <span class='count_top'><i class='fa fa-clock-o'></i> Total Project Hours</span>
                          <div class='count'>".$total_employee_project_hours." hrs</div>
                          <!--span class='count_bottom'><i class='green'><i class='fa fa-sort-asc'></i>3% </i> From last Week</span-->
                        </div>
                        <div class='col-md-2 col-sm-4 col-xs-6 tile_stats_count'>
                          <span class='count_top'><i class='fa fa-clock-o'></i> Avg. Project Duration</span>
                          <div class='count'>".number_format($avg_employee_project_hours, 2)." hrs</div>
                          <!--span class='count_bottom'><i class='green'><i class='fa fa-sort-asc'></i>3% </i> From last Week</span-->
                        </div>
                        <div class='col-md-2 col-sm-4 col-xs-6 tile_stats_count'>
                          <span class='count_top'><i class='fa fa-wrench'></i> Total Projects in Progress</span>
                          <div class='count green'>".$total_employee_project_inprogress."</div>
                          <!--span class='count_bottom'><i class='green'><i class='fa fa-sort-asc'></i>34% </i> From last Week</span-->
                        </div>
                        <div class='col-md-2 col-sm-4 col-xs-6 tile_stats_count'>
                          <span class='count_top'><i class='fa fa-check'></i> Total Completed Projects</span>
                          <div class='count'>".$total_employee_project_completed."</div>
                          <!--span class='count_bottom'><i class='red'><i class='fa fa-sort-desc'></i>12% </i> From last Week</span-->
                        </div>
                        <div class='col-md-2 col-sm-4 col-xs-6 tile_stats_count' id='balance-due'>
                          <span class='count_top'><i class='fa fa-money'></i> Total Balance Due</span>
                          <div class='count'>$".$final_employee_balance."</div>
                          <!--span class='count_bottom'><i class='green'><i class='fa fa-sort-asc'></i>34% </i> From last Week</span-->
                        </div>
                      </div>";
          } else {
              $total_client_project_hours = getTimeLogged($cid, '1', 'sum', $db)['payload'];
              $avg_client_project_hours = getTimeLogged($cid, '1', 'avg_comp', $db)['payload'];
              $total_client_project_inprogress = getProjectStats($cid, '1', 'in_progress', $db)['payload'];
              $total_client_project_completed = getProjectStats($cid, '1', 'completed', $db)['payload'];

              $cBalance = getBalance($cid, '1', 'total', $db)['payload'];

              $total_client_balance = 0;

              foreach ($cBalance as $cb) {
                  $total_client_balance += fxConvert($cid, $cb['amount'], $cb['date'], $us_dollar, $db)['payload'];
              }

              $final_client_balance = number_format($total_client_balance, 2, ".", ",") ;



              $website_count = getPTypeTotal($cid, 'Website', $db)['payload'];
              $pcma_count = getPTypeTotal($cid, 'PC/Mobile Application', $db)['payload'];
              $pcrep_count = getPTypeTotal($cid, 'PC Repair', $db)['payload'];
              $deskpc_count = getPTypeTotal($cid, 'Desktop PC Build', $db)['payload'];
              $netsol_count = getPTypeTotal($cid, 'Networking Solution', $db)['payload'];
              $sm_count = getPTypeTotal($cid, 'Social Media Management', $db)['payload'];
              $gd_count = getPTypeTotal($cid, 'Graphic Design', $db)['payload'];
              $dmh_count = getPTypeTotal($cid, 'Domain/Hosting', $db)['payload'];

              $total_count = $website_count + $pcma_count + $pcrep_count + $deskpc_count + $netsol_count + $sm_count + $gd_count + $dmh_count;

              echo "          <div class='row tile_count'>

                        <div class='col-md-2 col-sm-4 col-xs-6 tile_stats_count'>
                          <span class='count_top'><i class='fa fa-check'></i>Completed Projects</span>
                          <div class='count'>".$total_client_project_completed."</div>
                          <!--span class='count_bottom'><i class='red'><i class='fa fa-sort-desc'></i>12% </i> From last Week</span-->
                        </div>
                        <div class='col-md-2 col-sm-4 col-xs-6 tile_stats_count'>
                          <span class='count_top'><i class='fa fa-wrench'></i>Projects in Progress</span>
                          <div class='count green'>".$total_client_project_inprogress."</div>
                          <!--span class='count_bottom'><i class='green'><i class='fa fa-sort-asc'></i>34% </i> From last Week</span-->
                        </div>
                        <div class='col-md-2 col-sm-4 col-xs-6 tile_stats_count'>
                          <span class='count_top'><i class='fa fa-clock-o'></i> Avg. Project Duration</span>
                          <div class='count'>".$avg_client_project_hours." hrs</div>
                          <!--span class='count_bottom'><i class='green'><i class='fa fa-sort-asc'></i>3% </i> From last Week</span-->
                        </div>
                        <div class='col-md-2 col-sm-4 col-xs-6 tile_stats_count' id='balance-due'>
                          <span class='count_top'><i class='fa fa-money'></i> Total Balance Due</span>
                          <div class='count'>$".$final_client_balance."</div>
                          <!--span class='count_bottom'><i class='green'><i class='fa fa-sort-asc'></i>34% </i> From last Week</span-->
                        </div>
                      </div>


                      <!-- /top tiles -->

                      <div class='row'>
                        <div class='col-md-12 col-sm-12 col-xs-12'>
                          <div class='dashboard_graph'>

                            <div class='row x_title'>
                              <div class='col-md-6'>
                                <h3>Your Project History- <small>Projects done with KTS over the past year.</small></h3>
                              </div>
                              <!--div class='col-md-6'>
                                <div id='reportrange' class='pull-right' style='background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc'>
                                  <i class='glyphicon glyphicon-calendar fa fa-calendar'></i>
                                  <span>December 30, 2014 - January 28, 2015</span> <b class='caret'></b>
                                </div>
                              </div-->
                            </div>

                            <div class='col-md-9 col-sm-9 col-xs-12'>
                            <canvas id='project-history'></canvas>
                            </div>
                            <div class='col-md-3 col-sm-3 col-xs-12 bg-white'>
                              <div class='x_title'>
                                <h2>Top Project Types</h2>
                                <div class='clearfix'></div>
                              </div>

                              <div class='col-md-12 col-sm-12 col-xs-6'>
                                <div>
                                  <p>Software/ Website/Hosting</p>
                                  <div class=''>
                                    <div class='progress progress_sm' style='width: ". $ksoft = ($total_count>0 ? ((($pcma_count+$website_count+$dmh_count)/$total_count) * 100) : 0)."%;'>
                                      <div class='progress-bar bg-green' role='progressbar' data-transitiongoal='80'></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class='col-md-12 col-sm-12 col-xs-6'>
                                <div>
                                  <p>PC Build/ Repair/ Networking</p>
                                  <div class=''>
                                    <div class='progress progress_sm' style='width: ". $kpc = ($total_count>0 ? ((($deskpc_count+$pcrep_count+$netsol_count)/$total_count) * 100) : 0)."%;'>
                                      <div class='progress-bar bg-green' role='progressbar' data-transitiongoal='40'></div>
                                    </div>
                                  </div>
                                </div>
                                <div>
                                  <p>Social Media Management/ Design</p>
                                  <div class=''>
                                    <div class='progress progress_sm' style='width: ". $ksm = ($total_count>0 ? ((($sm_count+$gd_count)/$total_count) * 100) : 0)."%;'>
                                      <div class='progress-bar bg-green' role='progressbar' data-transitiongoal='50'></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class='clearfix'></div>
                          </div>
                        </div>

                      </div>
                      <br />


                      ";
          }

          ?>



          <div class="row">


            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                  <h2>Important Updates</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>

                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <h2>You have no updates at the moment.</h2>
                </div>
              </div>
            </div>



            <div class="col-md-8 col-sm-8 col-xs-12">
              <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                  <h2>Quick Settings</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>

                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">
                    <ul class="quick-list">
                      <li class='client-settings'><i class="fa fa-wrench "></i><a href="#">Settings</a>
                      </li>
                      <li class='get-invoice'><i class="fa fa-money"></i><a href="#">Invoices</a>
                      </li>
                      <li><i class="fa fa-comment-o"></i><a href="javascript:void(Tawk_API.toggle())">Live Chat</a> </li>

                      <li class='get-hosting'><i class="fa fa-bar-chart"></i><a href="#">Website Hosting</a> </li>
                      <li class='get-domain'><i class="fa fa-bars"></i><a href="#">Domains</a>
                      </li>
                      <li><i class="fa fa-area-chart"></i><a href="logout">Logout</a>
                      </li>
                    </ul>

                    <div class="sidebar-widget">
                        <h4>Profile Completion</h4>
                        <canvas width="150" height="80" id="chart_gauge_01" class="" style="width: 160px; height: 100px;"></canvas>
                        <div class="goal-wrapper">
                          <span id="gauge-text" class="gauge-value pull-left">0</span>
                          <span class="gauge-value pull-left">%</span>
                          <!--span id="goal-text" class="goal-value pull-right">100%</span-->
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>


          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Kabton's Tech Insights <small>KTS Blog</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>

                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">

                    <?php

      //Read each feed's items
      $entries = array();
      foreach ($insight_feeds as $feed) {
          $xml = simplexml_load_file($feed);
          $entries = array_merge($entries, $xml->xpath("//item"));
      }

      //Sort feed entries by pubDate
      usort($entries, function ($feed1, $feed2) {
          return strtotime($feed2->pubDate) - strtotime($feed1->pubDate);
      });

      ?>

      <ul class='list-unstyled timeline widget'>
      <?php
      //Print all the entries
      foreach ($entries as $entry) {
          ?>
          <li>
          <div class="block">
          <div class="block_content">

          <h2 class="title">
          <a href="<?= $entry->link ?>" target="_blank"><?= $entry->title ?></a>
          </h2>

          <div class="byline">
          <span><?= $entry->dc->creator ?> <?= strftime('%m/%d/%Y %I:%M %p', strtotime($entry->pubDate)) ?></span>
          </div>

          <p class='excerpt'><?= $entry->description ?></p>

          </div>
          </div>
          </li>
          <?php
      }
      ?>
      </ul>


                  </div>
                </div>
              </div>
            </div>



          </div>
        </div>
        <!-- /page content -->

        <!-- compose -->
        <div class='compose col-md-6 col-xs-12'>
          <div class='compose-header'>
            New Message
            <button type='button' class='close compose-close'>
              <span>×</span>
            </button>
          </div>

          <div class='compose-body'>
            <div id='alerts'></div>

            <?php

            if ($type == '3') {
                echo
            "<div class='form-group'>
              <label class='control-label col-md-1' for='compose-recipient'>To:<span class='required'></span>
              </label>
              <div class='col-md-11'>
                <input type='text' id='compose-recipient' required='required' class='form-control col-md-7 col-xs-12'>
              </div>
            </div>";
            }

          ?>


            <div class='form-group'>
              <label class='control-label col-md-1' for='compose-subject'>Subject:<span class='required'></span>
              </label>
              <div class='col-md-11'>
                <input type='text' id='compose-subject' required='required' class='form-control col-md-7 col-xs-12'>
              </div>
            </div>




            <div class='btn-toolbar editor' data-role='editor-toolbar' data-target='#editor'>
              <div class='btn-group'>
                <a class='btn dropdown-toggle' data-toggle='dropdown' title='Font'><i class='fa fa-font'></i><b class='caret'></b></a>
                <ul class='dropdown-menu'>
                </ul>
              </div>

              <div class='btn-group'>
                <a class='btn dropdown-toggle' data-toggle='dropdown' title='Font Size'><i class='fa fa-text-height'></i>&nbsp;<b class='caret'></b></a>
                <ul class='dropdown-menu'>
                  <li>
                    <a data-edit='fontSize 5'>
                      <p style='font-size:17px'>Huge</p>
                    </a>
                  </li>
                  <li>
                    <a data-edit='fontSize 3'>
                      <p style='font-size:14px'>Normal</p>
                    </a>
                  </li>
                  <li>
                    <a data-edit='fontSize 1'>
                      <p style='font-size:11px'>Small</p>
                    </a>
                  </li>
                </ul>
              </div>

              <div class='btn-group'>
                <a class='btn' data-edit='bold' title='Bold (Ctrl/Cmd+B)'><i class='fa fa-bold'></i></a>
                <a class='btn' data-edit='italic' title='Italic (Ctrl/Cmd+I)'><i class='fa fa-italic'></i></a>
                <a class='btn' data-edit='strikethrough' title='Strikethrough'><i class='fa fa-strikethrough'></i></a>
                <a class='btn' data-edit='underline' title='Underline (Ctrl/Cmd+U)'><i class='fa fa-underline'></i></a>
              </div>

              <div class='btn-group'>
                <a class='btn' data-edit='insertunorderedlist' title='Bullet list'><i class='fa fa-list-ul'></i></a>
                <a class='btn' data-edit='insertorderedlist' title='Number list'><i class='fa fa-list-ol'></i></a>
                <a class='btn' data-edit='outdent' title='Reduce indent (Shift+Tab)'><i class='fa fa-dedent'></i></a>
                <a class='btn' data-edit='indent' title='Indent (Tab)'><i class='fa fa-indent'></i></a>
              </div>

              <div class='btn-group'>
                <a class='btn' data-edit='justifyleft' title='Align Left (Ctrl/Cmd+L)'><i class='fa fa-align-left'></i></a>
                <a class='btn' data-edit='justifycenter' title='Center (Ctrl/Cmd+E)'><i class='fa fa-align-center'></i></a>
                <a class='btn' data-edit='justifyright' title='Align Right (Ctrl/Cmd+R)'><i class='fa fa-align-right'></i></a>
                <a class='btn' data-edit='justifyfull' title='Justify (Ctrl/Cmd+J)'><i class='fa fa-align-justify'></i></a>
              </div>

              <div class='btn-group'>
                <a class='btn dropdown-toggle' data-toggle='dropdown' title='Hyperlink'><i class='fa fa-link'></i></a>
                <div class='dropdown-menu input-append'>
                  <input class='span2' placeholder='URL' type='text' data-edit='createLink' />
                  <button class='btn' type='button'>Add</button>
                </div>
                <a class='btn' data-edit='unlink' title='Remove Hyperlink'><i class='fa fa-cut'></i></a>
              </div>

              <!--div class='btn-group'>
                <a class='btn' title='Insert picture (or just drag & drop)' id='pictureBtn'><i class='fa fa-picture-o'></i></a>
                <input type='file' data-role='magic-overlay' data-target='#pictureBtn' data-edit='insertImage' />
              </div-->

              <div class='btn-group'>
                <a class='btn' data-edit='undo' title='Undo (Ctrl/Cmd+Z)'><i class='fa fa-undo'></i></a>
                <a class='btn' data-edit='redo' title='Redo (Ctrl/Cmd+Y)'><i class='fa fa-repeat'></i></a>
              </div>


            </div>
            <label class='control-label col-md-12' for='message'>Message:<span class='required'></span>
            <div id='editor' class='editor-wrapper'></div>
          </div>

          <div class='compose-footer'>
            <button id='compose-send' class='btn btn-sm btn-success' type='button' data-compose-type='1'>Send</button>
          </div>
        </div>


        <!--EO compose-->

        <!-- footer content -->
        <footer>
          <div class='pull-left'>
            Insight v<?php echo $kts_version;?> - Project Viewer by <a href='https://kabtontech.com'>Kabton Tech Services</a>
          </div>
          <div class='clearfix'></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <!--script src='vendors/jquery/dist/jquery.min.js' type="text/javascript"></script-->
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>



    <!-- Bootstrap -->
    <script src='vendors/bootstrap/dist/js/bootstrap.min.js'></script>
    <!-- FastClick -->
    <script src='vendors/fastclick/lib/fastclick.js'></script>
    <!-- NProgress -->
    <script src='vendors/nprogress/nprogress.js'></script>
    <!-- jQuery Smart Wizard -->
    <script src='vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'></script>

    <!-- PNotify -->
    <script src='vendors/pnotify/dist/pnotify.js'></script>
    <script src='vendors/pnotify/dist/pnotify.buttons.js'></script>
    <script src='vendors/pnotify/dist/pnotify.nonblock.js'></script>

    <!--Dropzone-->
    <script src='vendors/dropzone-4.3.0/dist/min/dropzone.min.js'></script>

    <!-- bootstrap-wysiwyg -->
    <script src='vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js'></script>
    <script src='vendors/jquery.hotkeys/jquery.hotkeys.js'></script>
    <script src='vendors/google-code-prettify/src/prettify.js'></script>

    <!-- Chart.js -->
    <script src='vendors/Chart.js/dist/Chart.min.js'></script>
    <!-- gauge.js -->
    <script src='vendors/gauge.js/dist/gauge.min.js'></script>
    <!-- bootstrap-progressbar -->
    <script src='vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'></script>
    <!-- iCheck -->
    <script src='vendors/iCheck/icheck.min.js'></script>
    <!-- Skycons -->
    <script src='vendors/skycons/skycons.js'></script>
    <!-- Flot -->
    <script src='vendors/Flot/jquery.flot.js'></script>
    <script src='vendors/Flot/jquery.flot.pie.js'></script>
    <script src='vendors/Flot/jquery.flot.time.js'></script>
    <script src='vendors/Flot/jquery.flot.stack.js'></script>
    <script src='vendors/Flot/jquery.flot.resize.js'></script>
    <!-- Flot plugins -->
    <script src='vendors/flot.orderbars/js/jquery.flot.orderBars.js'></script>
    <script src='vendors/flot-spline/js/jquery.flot.spline.min.js'></script>
    <script src='vendors/flot.curvedlines/curvedLines.js'></script>
    <!-- DateJS -->
    <script src='vendors/DateJS/build/date.js'></script>
    <!-- JQVMap -->
    <script src='vendors/jqvmap/dist/jquery.vmap.js'></script>
    <script src='vendors/jqvmap/dist/maps/jquery.vmap.world.js'></script>
    <script src='vendors/jqvmap/examples/js/jquery.vmap.sampledata.js'></script>
    <!-- bootstrap-daterangepicker -->
    <script src='vendors/moment/min/moment.min.js'></script>
    <script src='vendors/bootstrap-daterangepicker/daterangepicker.js'></script>

    <!-- bootstrap-datetimepicker -->
    <script src='vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'></script>

    <!-- iCheck -->
    <script src='vendors/iCheck/icheck.min.js'></script>

    <!-- Switchery -->
    <script src='vendors/switchery/dist/switchery.min.js'></script>
    <!-- Select2 -->
    <script src='vendors/select2/dist/js/select2.full.min.js'></script>

    <!-- validator -->
    <script src='vendors/validator/validator.js'></script>
    <script src='vendors/validator/validator2.js'></script>

    <!-- Print -->
    <script src='vendors/printThis/printThis.js'></script>

    <!-- jquery.inputmask -->
    <script src="vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>







    <!-- Custom Theme Scripts -->
    <script src='build/js/custom.min.js'></script>

    <!-- KDeinsleistungen -->
    <script src='build/js/projekt.js'></script>
    <script src='build/js/satoshi.js'></script>
    <script src='build/js/nachricht.js'></script>
    <script src='build/js/website-manage.js'></script>
    <script src='build/js/settings.js'></script>
    <script src='build/js/request_project.js'></script>
    <script src='build/js/update_project.js'></script>
    <script src='build/js/willkommen.js'></script>

    <!-- Pagination >
    <script src="vendors/Datatables/datatables.min.js" type="text/javascript"></script-->


    <!-- Pagination -->
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  </body>
</html>
