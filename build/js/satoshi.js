$(document).on('click', '.get-invoice', function(e) {
  $.ajax({
      method: "POST",
      url: "serve/invoice/index.php",
      //data: {cpid:id},
      beforeSend: function() {
        NProgress.start();
        $(".right_col").fadeOut();
      }
    })
    .done(function(html) {

      $(".right_col").empty();
      $(".right_col").append(html);
      $(".right_col").fadeIn();
      $('html, body').animate({
        scrollTop: 0
      }, 0);

      //$("#project_title").attr('data-cpid', id);
      $('#invoice-table').DataTable();
      NProgress.done();
    });
});

$(document).on('click', '.get-old', function(e) {
  $.ajax({
      method: "POST",
      url: "serve/invoice/index.php",
      data: {
        operation: 'old'
      },
      beforeSend: function() {
        NProgress.start();
        $(".right_col").fadeOut();
      }
    })
    .done(function(html) {

      $(".right_col").empty();
      $(".right_col").append(html);
      $(".right_col").fadeIn();
      $('html, body').animate({
        scrollTop: 0
      }, 0);

      //$("#project_title").attr('data-cpid', id);
      $('#invoice-table').DataTable();
      NProgress.done();
    });
});

$(document).on('click', '.get-proforma', function(e) {
  $.ajax({
      method: "POST",
      url: "serve/invoice/index.php",
      data: {
        operation: 'proforma'
      },
      beforeSend: function() {
        NProgress.start();
        $(".right_col").fadeOut();
      }
    })
    .done(function(html) {

      $(".right_col").empty();
      $(".right_col").append(html);
      $(".right_col").fadeIn();
      $('html, body').animate({
        scrollTop: 0
      }, 0);

      //$("#project_title").attr('data-cpid', id);
      $('#invoice-table').DataTable();
      NProgress.done();
    });
});


$(document).on('click', '.view-invoice-details', function(e) {

  id = $(this).attr('data-cpid');


  $.ajax({
      method: "POST",
      url: "serve/invoice_details/index.php",
      data: {
        cpid: id
      },
      beforeSend: function() {
        NProgress.start();
        $(".right_col").fadeOut();
      }
    })
    .done(function(html) {
      //$( ".right_col" ).fadeOut();
      $(".right_col").empty();
      $(".right_col").append(html);
      $(".right_col").fadeIn();
      $('html, body').animate({
        scrollTop: 0
      }, 0);

      //$("#project_title").attr('data-cpid', id);
      NProgress.done();
    });
});

$(document).on('click', '.view-old-invoice-details', function(e) {

  id = $(this).attr('data-cpid');


  $.ajax({
      method: "POST",
      url: "serve/invoice_details/index.php",
      data: {
        cpid: id,
        operation: 'old'
      },
      beforeSend: function() {
        NProgress.start();
        $(".right_col").fadeOut();
      }
    })
    .done(function(html) {
      //$( ".right_col" ).fadeOut();
      $(".right_col").empty();
      $(".right_col").append(html);
      $(".right_col").fadeIn();
      $('html, body').animate({
        scrollTop: 0
      }, 0);

      //$("#project_title").attr('data-cpid', id);
      NProgress.done();
    });
});

$(document).on('click', '.view-proforma-details', function(e) {

  id = $(this).attr('data-cpid');


  $.ajax({
      method: "POST",
      url: "serve/invoice_details/index.php",
      data: {
        cpid: id,
        operation: 'proforma'
      },
      beforeSend: function() {
        NProgress.start();
        $(".right_col").fadeOut();
      }
    })
    .done(function(html) {
      //$( ".right_col" ).fadeOut();
      $(".right_col").empty();
      $(".right_col").append(html);
      $(".right_col").fadeIn();
      $('html, body').animate({
        scrollTop: 0
      }, 0);

      //$("#project_title").attr('data-cpid', id);
      NProgress.done();
    });
});

$(document).on('click', '#user-name', function(e) {

  if ($('#user-name').val() != kts_setting_user) {
    $('.settings-modal-p1').text('You have made a change to your username.');
    $('.settings-modal-p2').text('Are you sure you want to save this new change?');

    localmodal = $(".settings-modal-sm").modal();
  }


});

$(document).on('click', '.kts-invoice-print', function(e) // wysiwyg for composing messages
  {
    $(".x_panel").printThis({
      debug: false, // show the iframe for debugging
      importCSS: true, // import page CSS
      importStyle: true, // import style tags
      printContainer: true, // grab outer container as well as the contents of the selector
      loadCSS: "path/to/my.css", // path to additional css file - use an array [] for multiple
      pageTitle: "", // add title to print page
      removeInline: false, // remove all inline styles from print elements
      printDelay: 333, // variable print delay; depending on complexity a higher value may be necessary
      header: null, // prefix to html
      footer: null, // postfix to html
      base: false, // preserve the BASE tag, or accept a string for the URL
      formValues: true, // preserve input/form values
      canvas: false, // copy canvas elements (experimental)
      doctypeString: "...", // enter a different doctype for older markup
      removeScripts: false, // remove script tags from print content
      copyTagClasses: false // copy classes from the html & body tag
    });

  });
