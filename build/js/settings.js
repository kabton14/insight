init_validatorb()

var kts_setting_user;
var kts_setting_email;
var kts_setting_pass;
var kts_setting_pass2;
var kts_setting_sms;
var kts_setting_push;
var kts_setting_currency;
var count = 0;





$(document).on('click', '.client-settings', function(e) {
  $.ajax({
      method: "POST",
      url: "serve/settings/index.php",
      //data: {cpid:id},
      beforeSend: function() {
        NProgress.start();
        $(".right_col").fadeOut();
      }
    })
    .done(function(html) {

      $(".right_col").empty();
      $(".right_col").append(html);
      $(".right_col").fadeIn();
      $('html, body').animate({
        scrollTop: 0
      }, 0);

      //$("#project_title").attr('data-cpid', id);
      $('.password2').hide();

      kts_setting_user = $('#user-name').val();
      kts_setting_email = $('#email').val();
      kts_setting_pass = $('#password').val();
      kts_setting_pass2 = $('#password2').val();
      kts_setting_sms = $('#settings-sms')[0].checked;
      kts_setting_push = $('#settings-push')[0].checked;
      kts_setting_currency = $('#settings-currency').val();

      $(".dropzone").dropzone({
        url: "serve/settings/upload.php",
        acceptedFiles: "image/jpeg,image/jpg,image/png,image/gif",
        maxFilesize: 10,
        dictDefaultMessage: 'Press here to upload profile picture.',
        maxFiles: 1,
        init: function() {
          this.on("maxfilesexceeded", function(file) {
            this.removeAllFiles();
            this.addFile(file);
          });
        },
        init: function() {
          this.on("addedfile", function() {
            if (this.files[1] != null) {
              this.removeFile(this.files[0]);
            }
          });
        }
      });
      NProgress.done();
    });
});


$(document).on('blur', '#user-name', function(e) {

  if ($('#user-name').val() != kts_setting_user) {
    $('.settings-modal-p1').text('You have made a change to your username.');
    $('.settings-modal-p2').text('Are you sure you want to save this new change?');

    localmodal = $(".settings-modal-sm").modal();

    localmodal.one('click', '.settings-modal-cancel', function(e) {

      e.stopPropagation();
      $('#user-name').val(kts_setting_user);


    });

    localmodal.one('click', '.settings-modal-accept', function(e) {
      e.stopPropagation();
      $(".settings-modal-sm").modal("hide");
      newkts_setting_user = $('#user-name').val();


      if (settingServerTrip('user', newkts_setting_user, '', 'username')) {
        kts_setting_user = $('#user-name').val();
        $('#user-name').val(kts_setting_user);
      } else {

        e.stopPropagation();
        $('#user-name').val(kts_setting_user);
      }

    });



  }



});

$(document).on('blur', '#settings-currency', function(e) {

  if ($('#settings-currency').val() != kts_setting_currency) {
    $('.settings-modal-p1').text('You have made a change to your currency preference.');
    $('.settings-modal-p2').text('Are you sure you want to save this new change?');

    localmodal = $(".settings-modal-sm").modal();

    localmodal.one('click', '.settings-modal-cancel', function(e) {

      e.stopPropagation();
      $('#settings-currency').val(kts_setting_currency);
      $('#settings-currency').find(" option[value='" + kts_setting_currency + "']").eq(0).text(kts_setting_currency);


    });

    localmodal.one('click', '.settings-modal-accept', function(e) {
      e.stopPropagation();
      $(".settings-modal-sm").modal("hide");
      newkts_setting_currency = $('#settings-currency').val();


      if (settingServerTrip('currency', newkts_setting_currency, '', 'currency preference')) {
        kts_setting_currency = $('#settings-currency').val();
        $('#settings-currency').val(newkts_setting_currency);
        $('#settings-currency').find(" option[value='" + newkts_setting_currency + "']").eq(0).text(newkts_setting_currency);
      } else {

        e.stopPropagation();
        $('#settings-currency').val(kts_setting_currency);
        $('#settings-currency').find(" option[value='" + kts_setting_currency + "']").eq(0).text(kts_setting_currency);
      }

    });



  }



});

$(document).on('blur', '#email', function(e) {

  if ($('#email').val() != kts_setting_email) {
    $('.settings-modal-p1').text('You have made a change to your email.');
    $('.settings-modal-p2').text('Are you sure you want to save this new change?');

    localmodal = $(".settings-modal-sm").modal();

    localmodal.one('click', '.settings-modal-cancel', function(e) {

      e.stopPropagation();
      $('#email').val(kts_setting_email);


    });

    localmodal.one('click', '.settings-modal-accept', function(e) {
      e.stopPropagation();
      $(".settings-modal-sm").modal("hide");
      newkts_setting_email = $('#email').val();


      if (settingServerTrip('email', newkts_setting_email, '', 'email')) {
        kts_setting_email = $('#email').val();
        $('#email').val(kts_setting_email);
      } else {

        e.stopPropagation();
        $('#email').val(kts_setting_email);
      }

    });



  }

});

$(document).on('keyup', '#password', function(e) {

  $('.password2').fadeIn();

});

$(document).on('blur', '#password2', function(e) {

  /*if($('#password').val() != $('#password2').val(){
    alert('password changed');
  }*/

  if ($('#password2').val() != kts_setting_pass2) {
    $('.settings-modal-p1').text('You have made a change to your password.');
    $('.settings-modal-p2').text('Are you sure you want to save this new change?');

    localmodal = $(".settings-modal-sm").modal();

    localmodal.one('click', '.settings-modal-cancel', function(e) {

      e.stopPropagation();
      $('#password').val('fakepassword:-)');
      $('#password2').val('');
      $('.password2').fadeOut();


    });

    localmodal.one('click', '.settings-modal-accept', function(e) {
      e.stopPropagation();
      $(".settings-modal-sm").modal("hide");
      newkts_setting_pass1 = $('#password').val();
      newkts_setting_pass2 = $('#password2').val();


      if (settingServerTrip('password', newkts_setting_pass1, newkts_setting_pass2, 'password')) {
        kts_setting_pass2 = 'fakepassword:-)';
        ('#password2').val('');
      } else {

        e.stopPropagation();
        $('#password').val('fakepassword:-)');
        $('#password2').val('');
        $('.password2').fadeOut();
      }

    });



  }

});



$(document).on('mouseup', '#settings-sms', function(e) {

  //  alert('sweet '+ $('#settings-sms')[0].checked);
  if (1 == 1) {

    $('.settings-modal-p1').text('You have made a change to your notification settings.');
    $('.settings-modal-p2').text('Are you sure you want to save this new change?');

    localmodal = $(".settings-modal-sm").modal();

    localmodal.one('click', '.settings-modal-cancel', function(e) {

      e.stopPropagation();
      $('#settings-sms')[0].checked = kts_setting_sms;


    });

    localmodal.one('click', '.settings-modal-accept', function(e) {
      e.stopPropagation();
      $(".settings-modal-sm").modal("hide");

      newkts_setting_sms = ($('#settings-sms')[0].checked ? 'on' : 'off');

      if (settingServerTrip('sms', newkts_setting_sms, '', 'sms notification')) {
        kts_setting_user = ('#settings-sms')[0].checked;
      } else {

        e.stopPropagation();
        ('#settings-sms')[0].checked = !('#settings-sms')[0].checked;
      }

    });

  }


});


$(document).on('mouseup', '#settings-push', function(e) {

  //  alert('sweet '+ $('#settings-push')[0].checked);
  if (1 == 1) {

    $('.settings-modal-p1').text('You have made a change to your notification settings.');
    $('.settings-modal-p2').text('Are you sure you want to save this new change?');

    localmodal = $(".settings-modal-sm").modal();

    localmodal.one('click', '.settings-modal-cancel', function(e) {

      e.stopPropagation();
      $('#settings-push')[0].checked = kts_setting_sms;


    });

    localmodal.one('click', '.settings-modal-accept', function(e) {
      e.stopPropagation();
      $(".settings-modal-sm").modal("hide");

      newkts_setting_push = ($('#settings-push')[0].checked ? 'on' : 'off');

      if (settingServerTrip('push', newkts_setting_push, '', 'push notification')) {
        kts_setting_user = ('#settings-push')[0].checked;
      } else {

        e.stopPropagation();
        ('#settings-push')[0].checked = !('#settings-push')[0].checked;
      }

    });

  }


});





function settingServerTrip(option, value, value2, description) {

  json = null;


  $.ajax({
      method: "POST",
      url: "serve/settings/ktssettings.php",
      async: false,
      data: {
        'option': option,
        'user': value,
        'user2': value2
      },
      beforeSend: function() {
        NProgress.start();
        console.log("STARThi--->" + count);
        //$( ".right_col" ).fadeOut();
      }

    })
    .done(function(data) {
      json = $.parseJSON(data);
      console.log("DONE--->" + count);

      NProgress.done();




    });

  count++;


  if (json.code == '1') {
    PNotify.removeAll();
    console.log('SUcCESS!');
    new PNotify({
      title: 'Success!',
      text: 'Your ' + description + ' has been updated.',
      type: 'success',
      styling: 'bootstrap3'
    });

    json = null;


    return true;
  } else if (json.code == '4') {
    PNotify.removeAll();
    console.log('FAiLURE!');
    new PNotify({
      title: 'There\'s a problem..',
      text: json.msg,
      type: 'error',
      styling: 'bootstrap3'
    });
    json = null;


    return false;

  } else {
    PNotify.removeAll();
    console.log('FAiLURE!');
    new PNotify({
      title: 'There\'s a problem..',
      text: json.msg, //'There was error updating ' +description+'. Try again later.',
      type: 'error',
      styling: 'bootstrap3'
    });
    json = null;

    return false;
  }





}



function init_validatorb() {
  "undefined" != typeof validator && (console.log("init_validatorb"), validator.message.date = "not a real date", $("form").on("blur", "input[required], input.optional, select.required", validator.checkField).on("change", "select.required", validator.checkField).on("keypress", "input[required][pattern]", validator.keypress), $(".multi.required").on("keyup blur", "input", function() {
    validator.checkField.apply($(this).siblings().last()[0])
  }), $("form").submit(function(a) {
    a.preventDefault();
    var b = !0;
    return validator.checkAll($(this)) || (b = !1), b && this.submit(), !1
  }))
}
