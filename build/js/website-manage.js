$(document).on('click', '.get-domain', function(e) {
	$.ajax({
		method: "POST",
		url: "serve/client_domain/index.php",
		//data: {cpid:id},
		beforeSend: function() {
			NProgress.start();
			$(".right_col").fadeOut();
		}
	}).done(function(html) {
		$(".right_col").empty();
		$(".right_col").append(html);
		$(".right_col").fadeIn();
		$('html, body').animate({
			scrollTop: 0
		}, 0);
		//$("#project_title").attr('data-cpid', id);
		$('#domain-table').DataTable();
		NProgress.done();
	});
});
$(document).on('click', '.get-hosting', function(e) {
	$.ajax({
		method: "POST",
		url: "serve/client_hosting/index.php",
		//data: {cpid:id},
		beforeSend: function() {
			NProgress.start();
			$(".right_col").fadeOut();
		}
	}).done(function(html) {
		$(".right_col").empty();
		$(".right_col").append(html);
		$(".right_col").fadeIn();
		$('html, body').animate({
			scrollTop: 0
		}, 0);
		//$("#project_title").attr('data-cpid', id);
		$('#hosting-table').DataTable();
		NProgress.done();
	});
});
$(document).on('click', '.create-web-item', function(e) {
	$.ajax({
		method: "POST",
		url: "serve/add_web_item/index.php",
		beforeSend: function() {
			NProgress.start();
			$(".right_col").fadeOut();
		}
	}).done(function(html) {
		$(".right_col").empty();
		$(".right_col").append(html);
		$(".right_col").fadeIn();
		$(".progress .progress-bar")[0] && $(".progress .progress-bar").progressbar();
		$('html, body').animate({
			scrollTop: 0
		}, 400);
		$('.start-date-picker').datetimepicker({
			format: 'YYYY-MM-DD'
		});
		$('.pend-date-picker').datetimepicker({
			format: 'YYYY-MM-DD'
		});
		$('#create-hosting-form').hide();
		$('#toggle-domain').prop('checked', true);
		$('#toggle-hosting').prop('checked', false);
		NProgress.done();
	});
});
$(document).on('click', '#toggle-domain', function(e) {
	$('#create-domain-form').toggle('slow')
});
$(document).on('click', '#toggle-hosting', function(e) {
	$('#create-hosting-form').toggle('slow')
});
$(document).on('click', '.create-domain-submit', function(e) {
	domain_client = $('#create-witem-client').val();
	domain_name = $('#create-domain-name').val();
	domain_parked = $('#parked').is(':checked');
	domain_sub = $('#sub').is(':checked');
	domain_registrar = $('#create-domain-registrar').val();
	domain_account = $('#create-domain-account').val();
	domain_duration = $('#create-domain-duration').val();
	domain_purchase = $('#create-domain-purchase-date').val();
	domain_expiry = $('#create-domain-expiry-date').val();
	$.ajax({
		method: "POST",
		url: "serve/add_web_item/create_domain.php",
		data: {
			'option': 'create_domain',
			'domain_client': domain_client,
			'domain_name': domain_name,
			'domain_registrar': domain_registrar,
			'domain_account': domain_account,
			'domain_parked': domain_parked,
			'domain_sub': domain_sub,
			'domain_duration': domain_duration,
			'domain_purchase': domain_purchase,
			'domain_expiry': domain_expiry
		},
		beforeSend: function() {
			NProgress.start();
			//$( ".right_col" ).fadeOut();
		}
	}).done(function(json) {
		json = $.parseJSON(json);
		if(json.code == '1') {
			$('html, body').animate({
				scrollTop: 0
			}, 0);
			resetDomainValues();
			notification('Project Created', 'Your domain was succesfully created.', 'success');
		} else {
			notification('Error', 'Your domain was not succesfully created. Ensure that all required fields are properly filled', 'error');
			$('html, body').animate({
				scrollTop: 0
			}, 0);
		}
		NProgress.done();
	});
});
$(document).on('click', '.create-hosting-submit', function(e) {
	hosting_client = $('#create-witem-client').val();
	hosting_name = $('#create-website-name').val();
	hosting_account = $('#create-hosting-account').val();
	hosting_registrar = $('#create-hosting-registrar').val();
	hosting_type = $('#create-hosting-type').val();
	hosting_duration = $('#create-hosting-duration').val();
	hosting_purchase = $('#create-hosting-purchase-date').val();
	hosting_expiry = $('#create-hosting-expiry-date').val();
	$.ajax({
		method: "POST",
		url: "serve/add_web_item/create_hosting.php",
		data: {
			'option': 'create_hosting',
			'hosting_client': hosting_client,
			'hosting_name': hosting_name,
			'hosting_account': hosting_account,
			'hosting_registrar': hosting_registrar,
			'hosting_type': hosting_type,
			'hosting_duration': hosting_duration,
			'hosting_purchase': hosting_purchase,
			'hosting_expiry': hosting_expiry
		},
		beforeSend: function() {
			NProgress.start();
			//$( ".right_col" ).fadeOut();
		}
	}).done(function(json) {
		json = $.parseJSON(json);
		if(json.code == '1') {
			$('html, body').animate({
				scrollTop: 0
			}, 0);
			resetDomainValues();
			notification('Hosting Created', 'Your hosting plan was succesfully created.', 'success');
		} else {
			notification('Error', 'Your hosting was not succesfully created. Ensure that all required fields are properly filled', 'error');
			$('html, body').animate({
				scrollTop: 0
			}, 0);
		}
		NProgress.done();
	});
});

function resetDomainValues() {
	domain_client = $('#create-witem-client').val('');
	domain_name = $('#create-domain-name').val('');
	//domain_parked = $('#parked').is(':checked');
	//domain_sub = $('#sub').is(':checked');
	$('#parked').prop('checked', false);
	$('#sub').prop('checked', false);
	hosting_account = $('#create-domain-account').val('');
	hosting_registrar = $('#create-domain-registrar').val('');
	domain_duration = $('#create-domain-duration').val('');
	domain_purchase = $('#create-domain-purchase-date').val('');
	domain_expiry = $('#create-domain-expiry-date').val('');
}

function resetHostingValues() {
	hosting_client = $('#create-witem-client').val('');
	hosting_name = $('#create-website-name').val('');
	hosting_account = $('#create-hosting-account').val('');
	hosting_registrar = $('#create-hosting-registrar').val('');
	hosting_type = $('#create-hosting-type').val('');
	hosting_duration = $('#create-hosting-duration').val('');
	hosting_purchase = $('#create-hosting-purchase-date').val('');
	hosting_expiry = $('#create-hosting-expiry-date').val('');
}
$(document).on('change', '#create-witem-client', function(e) {
	client = $('#create-witem-client').val();
	$.ajax({
		method: "POST",
		url: "serve/add_web_item/get_domain.php",
		data: {
			'option': 'get_domain',
			'client': client
		},
		beforeSend: function() {
			NProgress.start();
			//$( ".right_col" ).fadeOut();
		}
	}).done(function(json) {
		$('#create-website-name').empty().append('<option selected="selected" value="">Choose Domain</option>');
		json = $.parseJSON(json);
		if(json.code == '1') {
			json.payload.forEach(function(obj) {
				$('#create-website-name').append($('<option>', {
					value: obj.did,
					text: obj.domain
				}));
			});
			//alert(json.payload[0].domain);
		} else {
			notification('Heads Up', 'Could not retrieve domains for client. To get started, add a new domain or try again later.', 'error');
			$('html, body').animate({
				scrollTop: 0
			}, 0);
		}
		NProgress.done();
	});
});
$(document).on('click', '.create-domain-reset', function(e) {
	resetDomainValues();
});
$(document).on('click', '.create-hosting-reset', function(e) {
	resetHostingValues();
});
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
var cdid;
var chid;
var update_domain;
var update_sub;
var update_parked;
var update_registrar;
var update_account;
var update_duration;
var update_purchase_date;
var update_expiry;
var update_website;
var update_hosting_account;
var update_hosting_registrar;
var update_hosting_type;
var update_hosting_duration;
var update_hosting_purchase_date;
var update_hosting_expiry;
$(document).on('click', '.domain-view', function(e) {
	did = $(this).closest('tr').attr('data-cdid');
	$.ajax({
		method: "POST",
		url: "serve/update_domain/index.php",
		data: {
			'cdid': did
		},
		beforeSend: function() {
			NProgress.start();
			$(".right_col").fadeOut();
		}
	}).done(function(html) {
		$(".right_col").empty();
		$(".right_col").append(html);
		$(".right_col").fadeIn();
		$('html, body').animate({
			scrollTop: 0
		}, 0);
		$('.start-date-picker').datetimepicker({
			format: 'YYYY-MM-DD'
		});
		$('.pend-date-picker').datetimepicker({
			format: 'YYYY-MM-DD'
		});
		cdid = $('#update-domain-form').attr('data-cdid');
		update_domain = {
			value: $('#update-domain-name').val()
		};
		update_parked = {
			value: $('#update-parked')[0].checked
		};
		update_sub = {
			value: $('#update-sub')[0].checked
		};
		update_registrar = {
			value: $('#update-domain-registrar').val()
		};
		update_account = {
			value: $('#update-domain-account').val()
		};
		update_duration = {
			value: $('#update-domain-duration').val()
		};
		update_purchase_date = {
			value: $('#update-domain-purchase-date').val()
		};
		update_expiry = {
			value: $('#update-domain-expiry-date').val()
		};
		//$("#project_title").attr('data-cpid', id);
		NProgress.done();
	});
});
$(document).on('click', '.hosting-view', function(e) {
	hid = $(this).closest('tr').attr('data-chid');
	$.ajax({
		method: "POST",
		url: "serve/update_hosting/index.php",
		data: {
			'chid': hid
		},
		beforeSend: function() {
			NProgress.start();
			$(".right_col").fadeOut();
		}
	}).done(function(html) {
		$(".right_col").empty();
		$(".right_col").append(html);
		$(".right_col").fadeIn();
		$('html, body').animate({
			scrollTop: 0
		}, 0);
		$('.start-date-picker').datetimepicker({
			format: 'YYYY-MM-DD'
		});
		$('.pend-date-picker').datetimepicker({
			format: 'YYYY-MM-DD'
		});
		chid = $('#update-hosting-form').attr('data-chid');
		update_website = {
			value: $('#update-website-name').val()
		};
		update_hosting_account = {
			value: $('#update-hosting-account').val()
		};
		update_hosting_registrar = {
			value: $('#update-hosting-registrar').val()
		};
		update_hosting_type = {
			value: $('#update-hosting-type').val()
		};
		update_hosting_duration = {
			value: $('#update-hosting-duration').val()
		};
		update_hosting_purchase_date = {
			value: $('#update-hosting-purchase-date').val()
		};
		update_hosting_expiry = {
			value: $('#update-hosting-expiry-date').val()
		};
		//$("#project_title").attr('data-cpid', id);
		NProgress.done();
	});
});
$(document).on('blur', '#update-domain-name', function(e) {
	wiUpdater(cdid, $(this), update_domain, '', 'update_domain', 'Domain name');
});
$(document).on('click', '#update-parked', function(e) {
	wiUpdater(cdid, $(this), update_parked, '', 'update_parked', 'Domain parked status');
});
$(document).on('click', '#update-sub', function(e) {
	wiUpdater(cdid, $(this), update_sub, '', 'update_sub', 'Domain sub-domain status');
});
$(document).on('blur', '#update-domain-registrar', function(e) {
	wiUpdater(cdid, $(this), update_registrar, '', 'update_registrar', 'Domain registrar');
});
$(document).on('blur', '#update-domain-account', function(e) {
	wiUpdater(cdid, $(this), update_account, '', 'update_account', 'Domain account');
});
$(document).on('blur', '#update-domain-duration', function(e) {
	wiUpdater(cdid, $(this), update_duration, '', 'update_duration', 'Domain duration');
});
$(document).on('blur', '#update-domain-purchase-date', function(e) {
	wiUpdater(cdid, $(this), update_purchase_date, '', 'update_purchase_date', 'Domain purchase date');
});
$(document).on('blur', '#update-domain-expiry-date', function(e) {
	wiUpdater(cdid, $(this), update_expiry, '', 'update_expiry', 'Domain expiry');
});
////hosting//////
$(document).on('blur', '#update-website-name', function(e) {
	wiUpdater(chid, $(this), update_website, '', 'update_website', 'Hosting domain');
});
$(document).on('blur', '#update-hosting-account', function(e) {
	wiUpdater(chid, $(this), update_hosting_account, '', 'update_hosting_account', 'Hosting account');
});
$(document).on('blur', '#update-hosting-registrar', function(e) {
	wiUpdater(chid, $(this), update_hosting_registrar, '', 'update_hosting_registrar', 'Hosting registrar');
});
$(document).on('blur', '#update-hosting-type', function(e) {
	wiUpdater(chid, $(this), update_hosting_type, '', 'update_hosting_type', 'Hosting type');
});
$(document).on('blur', '#update-hosting-duration', function(e) {
	wiUpdater(chid, $(this), update_hosting_duration, '', 'update_hosting_duration', 'Hosting duration');
});
$(document).on('blur', '#update-hosting-purchase-date', function(e) {
	wiUpdater(chid, $(this), update_hosting_purchase_date, '', 'update_hosting_purchase_date', 'Hosting purchase date');
});
$(document).on('blur', '#update-hosting-expiry-date', function(e) {
	wiUpdater(chid, $(this), update_hosting_expiry, '', 'update_hosting_expiry', 'Hosting expiry date');
});

function wiUpdater(cpid, project_object, init_value, init_valueb, option, descrp) {
	if((project_object.val() != init_value.value && project_object.prop("id") != 'editor-one') || (project_object.html() != init_value.value && project_object.prop("id") == 'editor-one')) {
		isselect = false;
		if(project_object.is('select')) {
			isselect == true;
		}
		$('.update-wi-modal-p1').text('You have made a change to the ' + descrp.toLowerCase() + '.');
		$('.update-wi-modal-p2').text('Are you sure you want to save this new change?');
		localmodal = $(".update-wi-modal-sm").modal();
		localmodal.one('click', '.update-wi-modal-cancel', function(e) {
			e.stopPropagation();
			if(project_object.prop("id") == 'editor-one') {
				project_object.html(init_value.value)
			} else if(isselect) {
				project_object.find("option:selected").text(init_value.value);
			} else if($(project_object).is(':checkbox')) {
				$(project_object)[0].checked = init_value.value;
			} else {
				project_object.val(init_value.value);
			}
			$('body').removeClass('modal-open');
			$('.modal-backdrop').remove();
			$(".update-wi-modal-sm").modal("hide");
		});
		localmodal.one('click', '.update-wi-modal-accept', function(e) {
			e.stopPropagation();
			$('body').removeClass('modal-open');
			$('.modal-backdrop').remove();
			$(".update-wi-modal-sm").modal("hide");
			if(project_object.prop("id") == 'editor-one') {
				newkts_name = project_object.html();
			} else if(isselect) {
				newkts_name_text = project_object.find(" option[value='" + init_value.value + "']").eq(0).text();
			} else if($(project_object).is(':checkbox')) {
				newkts_name = $(project_object)[0].checked;
			} else {
				newkts_name = project_object.val();
			}
			url = "serve/update_domain/update_wi.php";
			if(wiServerTrip(option, cpid, newkts_name, descrp, url)) {
				init_value.value = newkts_name; //project_object.val();
				if(project_object.prop("id") == 'editor-one') {
					init_value.value = newkts_name; //project_object.html();
				}
				if(project_object.prop("id") == 'editor-one') {
					project_object.html(newkts_name)
				} else if(isselect) {
					project_object.find(" option[value='" + newkts_name + "']").eq(0).text();
				} else if($(project_object).is(':checkbox')) {
					$(project_object)[0].checked = newkts_name
				} else {
					project_object.val(newkts_name);
				}
				init_value.value = newkts_name;
			} else {
				e.stopPropagation();
				project_object.val(init_value.value);
				if(project_object.prop("id") == 'editor-one') {
					project_object.html(init_value.value)
				} else if(isselect) {
					project_object.find("option:selected").text(init_value.value);
				} else if($(project_object).is(':checkbox')) {
					$(project_object)[0].checked = init_value.value
				} else {
					project_object.val(init_value.value);
				}
			}
		});
	} else {
		project_object.val(init_value.value);
		if(project_object.prop("id") == 'editor-one') {
			project_object.html(init_value.value)
		}
	}
}

function wiServerTrip(option, cdid, value, description, location) {
	json = null;
	$.ajax({
		method: "POST",
		url: location,
		async: false,
		data: {
			'option': option,
			'cdid': cdid,
			'chid': cdid,
			'value': value
		},
		beforeSend: function() {
			NProgress.start();
			console.log("STARTZ--->" + count + "   VAL->" + value + cdid);
			//$( ".right_col" ).fadeOut();
		}
	}).done(function(data) {
		json = $.parseJSON(data);
		console.log("DONE--->" + count);
		NProgress.done();
	});
	if(json.code == '1') {
		PNotify.removeAll();
		console.log('SUcCESS!');
		new PNotify({
			title: 'Success!',
			text: description + ' has been updated.',
			type: 'success',
			styling: 'bootstrap3'
		});
		json = null;
		return true;
	} else if(json.code == '4') {
		PNotify.removeAll();
		console.log('FAiLURE!');
		new PNotify({
			title: 'There\'s a problem..',
			text: json.msg,
			type: 'error',
			styling: 'bootstrap3'
		});
		json = null;
		return false;
	} else {
		PNotify.removeAll();
		console.log('FAiLURE!');
		new PNotify({
			title: 'There\'s a problem..',
			text: json.msg, //'There was error updating ' +description+'. Try again later.',
			type: 'error',
			styling: 'bootstrap3'
		});
		json = null;
		return false;
	}
}
