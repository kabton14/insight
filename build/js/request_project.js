


$(document).on('click','.request_project',function(e){
    $.ajax({
      method: "POST",
      url: "serve/request_project/index.php",
      beforeSend: function(){
        NProgress.start();
        $( ".right_col" ).fadeOut();
      }

    })
      .done(function( html ) {
        $( ".right_col" ).empty();
        $( ".right_col" ).append( html);
        $( ".right_col" ).fadeIn();

        $(".progress .progress-bar")[0] && $(".progress .progress-bar").progressbar();
        $('html, body').animate({ scrollTop: 0 }, 0);

        NProgress.done();

        init_wysiwyg();

        $('.request-date-picker').datetimepicker({
            format: 'YYYY-MM-DD',
            minDate: moment()
        });

        $('#request-phone').inputmask({"mask": "(999) 999-9999"});
        $("#request-email").inputmask({ alias: "email"});

      });
  });

  $(document).on('click','.request_submit',function(e){

    //$cid = mysqli_real_escape_string($db, ktsDecode($_POST['cid']));
    rtype = $('#project-type').val();
    start_date = $('#request-date').val();
    description =  $('#editor-one').html();
    email =  $('#request-email').val();
    phone =  $('#request-phone').val();
    preferred_method = $('#request_communication').val();

      $.ajax({
        method: "POST",
        url: "serve/request_project/request_project.php",
        data: {option: 'request_project', type: rtype, 'description': description, 'start_date': start_date, 'phone': phone, 'email': email, 'preferred_method': preferred_method  },
        beforeSend: function(){
          NProgress.start();
          //$( ".right_col" ).fadeOut();
        }

      })
        .done(function(json) {

          json = $.parseJSON(json);

          if(json.code == '1'){


          $(".progress .progress-bar")[0] && $(".progress .progress-bar").progressbar();
          $('html, body').animate({ scrollTop: 0 }, 0);
          resetValues();

          notification('Request Submitted', 'Your project request was succesfully submitted. A  member of our team will contact you. (within 24hrs)', 'success');

        }
        else{
          notification('Error', 'Your project request was not succesfully submitted. Ensure that all required fields are properly filled', 'error');
          $(".progress .progress-bar")[0] && $(".progress .progress-bar").progressbar();
          $('html, body').animate({ scrollTop: 0 }, 0);

        }

          NProgress.done();

        });
    });

    $(document).on('click','.request_reset',function(e){
      $('html, body').animate({ scrollTop: 0 }, 400);
      resetValues();
    });


function resetValues(){

  $('#project-type').val('');
  $('#request-date').val('');
  $('#editor-one').html('');
  $('#request-email').val('');
  $('#request-phone').val('');
  $('#request_communication').val('');
}

  function init_wysiwyg() {
      function b(a, b) {
          var c = "";
          "unsupported-file-type" === a ? c = "Unsupported format " + b : console.log("error uploading file", a, b), $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>File upload error</strong> ' + c + " </div>").prependTo("#alerts")
      }
      "undefined" != typeof $.fn.wysiwyg && (console.log("init_wysiwyg"), $(".editor-wrapper").each(function() {
          var a = $(this).attr("id");
          $(this).wysiwyg({
              toolbarSelector: '[data-target="#' + a + '"]',
              fileUploadError: b
          })
      }), window.prettyPrint, prettyPrint())
  }

  function notification(ntitle, ntext, ntype){
    PNotify.removeAll();
    new PNotify({
                                    title: ntitle,
                                    text: ntext,
                                    type: ntype,
                                    styling: 'bootstrap3'
                                });
  }
