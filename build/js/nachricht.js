$(document).on('click','.kts-reply',function(e){    //reply to message

  sender_name = $('.sender_name').text();

  subject = $('.kts-subject').text();
  msg = $('#editor').html();

  if( !$(this).attr('class').includes('compose-close')){
    init_wysiwyg();
  }



  $(".compose").slideToggle();

  $('.compose-header').html("Reply to "+ sender_name +"<button type='button' class= 'close compose-close'> <span>×</span> </button>");
  $('#compose-subject').val(subject);
  $('#compose-send').attr('data-compose-type', '2');



//TODO check if fieds are empty and return modal where applicable.


  });

  $(document).on('click','.kts-mail-delete',function(e){    //delete message
    msgid = $('.kts-reply').attr('data-msg-root');

    localmodal = $(".delete-msg-modal-sm").modal();



    localmodal.on('click', '.delete-msg-modal-accept',function(e){

      e.stopPropagation();
      $('.delete-msg-modal-sm').modal('hide');

          $.ajax({
            method: "POST",
            url: "serve/additional_actions.php",
            data: {option: 'delete', 'msgid': msgid},
            beforeSend: function(){
              NProgress.start();
              //$( ".right_col" ).fadeOut();
            }

          })
            .done(function( data ) {
              json = $.parseJSON(data);

              if(json.code == '1'){
            notification('Success', 'Your message was deleted.', 'success');

            //getMailList(msgindex);
            $('.get-inbox').trigger( "click" );
            msgindex = $('.kts-reply').attr('data-msg-index');
          }
          else if (json.code =='4') {
            notification('Error :(', json.msg, 'error');

          }
          else{
            notification('Error :(', 'There was a problem deleting your message.', 'error');

          }


              $(".progress .progress-bar")[0] && $(".progress .progress-bar").progressbar();
              $('html, body').animate({ scrollTop: 0 }, 0);

              NProgress.done();

            });

          });
    });




$(document).on('click','#compose-send',function(e){     //sendMessage
  type = $(this).attr('data-compose-type');
  recip = $('#compose-recipient').val();
  subj = $('#compose-subject').val();
  msg = $('#editor').html();
  msgid = $('.kts-reply').attr('data-msg-root');
  receiver = $('.kts-reply').attr('data-receiver');
  receiver_type = $('.kts-reply').attr('data-receiver-type');

  //alert(receiver);
  //alert(receiver_type);

  //TODO check if fieds are empty and return modal where applicable.


  if(type == '1'){

    $.ajax({
      method: "POST",
      url: "serve/additional_actions.php",
      data: {option: 'new_message', receiver: recip, subject: subj, message: msg},
      beforeSend: function(){
        NProgress.start();
        //$( ".right_col" ).fadeOut();
      }

    })
      .done(function(data) {
        json = $.parseJSON(data);

        if(json.code == '1'){
      notification('Message Sent!', 'Your message was successfully sent.', 'success');
      clean();
      $(".compose").slideToggle();
      $('.create-msg').removeClass('active');
    }
    else if (json.code == '4') {
      notification('Error :(', json.msg, 'error');

    }
    else{
      notification('Error :(', 'There was a problem sending your message.', 'error');

    }



        //$( ".right_col" ).fadeIn();

        $(".progress .progress-bar")[0] && $(".progress .progress-bar").progressbar();
        $('html, body').animate({ scrollTop: 0 }, 0);

        NProgress.done();

      });
    }
  else if (type == '2') {

    $.ajax({
        method: "POST",
        url: "serve/additional_actions.php",
        data: {option: 'reply', 'msgid': msgid, 'receiver': receiver, 'receiver_type': receiver_type, 'subject': subj, 'message': msg},
        beforeSend: function(){
          NProgress.start();
          //$( ".right_col" ).fadeOut();
        }

      })
        .done(function( data ) {
          json = $.parseJSON(data);

          if(json.code == '1'){
        notification('Message Sent!', 'Your message was successfully sent to one of our team members.', 'success');
        clean();
        $(".compose").slideToggle();
        msgindex = $('.kts-reply').attr('data-msg-index');
        getMailList(msgindex);
      }
      else if (json.code =='4') {
        notification('Error :(', json.msg, 'error');

      }
      else{
        notification('Error :(', 'There was a problem sending your message.', 'error');

      }


          $(".progress .progress-bar")[0] && $(".progress .progress-bar").progressbar();
          $('html, body').animate({ scrollTop: 0 }, 0);

          NProgress.done();

        });

  }
  else {
    notification('Error :(', 'There was a problem sending your message.', 'error');
  }
  });


$(document).on('click','.get-inbox',function(e){ // list inbox items
    $.ajax({
      method: "POST",
      url: "serve/inbox/index.php",
      beforeSend: function(){
        NProgress.start();
        $( ".right_col" ).fadeOut();
      }

    })
      .done(function( html ) {
        $( ".right_col" ).empty();
        $( ".right_col" ).append( html);
        $( ".right_col" ).fadeIn();

        $(".progress .progress-bar")[0] && $(".progress .progress-bar").progressbar();
        $('html, body').animate({ scrollTop: 0 }, 0);

        NProgress.done();

      });
  });

  $(document).on('click','.mail_list',function(e){    //open a specific mail item
    msgindex = $(this).closest('div').attr('data-nth');
    //alert(msgindex);
    getMailList(msgindex);


        $(this).find('.fa-circle').addClass('fa-circle-o').removeClass('fa-circle');

    });

    function getMailList(index){
      $.ajax({
        method: "POST",
        url: "serve/inbox/loadmsg.php",
        data: {nth:index},
        beforeSend: function(){
          NProgress.start();
          $( ".inbox-body" ).fadeOut();
        }

      })
        .done(function( html ) {
          $( ".inbox-body" ).empty();
          $( ".inbox-body" ).append(html);


          $( ".inbox-body" ).fadeIn();

          $(".progress .progress-bar")[0] && $(".progress .progress-bar").progressbar();
          $('html, body').animate({ scrollTop: 0 }, 0);

          $('.message-count-badge').each(function(index) {
            if(count = $(this).text() > 0 ){
              count--;
              $(this).text(count);

            }

            if($(this).text() == 0){
              $(this).hide(400);
            }
          });

          NProgress.done();

        });
    }

    $(document).on('click', '.kts-mail-print', function(e)    // wysiwyg for composing messages
    {
      $(".mail_view").printThis({
    debug: false,               // show the iframe for debugging
    importCSS: true,            // import page CSS
    importStyle: true,         // import style tags
    printContainer: true,       // grab outer container as well as the contents of the selector
    loadCSS: "path/to/my.css",  // path to additional css file - use an array [] for multiple
    pageTitle: "",              // add title to print page
    removeInline: false,        // remove all inline styles from print elements
    printDelay: 333,            // variable print delay; depending on complexity a higher value may be necessary
    header: null,               // prefix to html
    footer: null,               // postfix to html
    base: false ,               // preserve the BASE tag, or accept a string for the URL
    formValues: true,           // preserve input/form values
    canvas: false,              // copy canvas elements (experimental)
    doctypeString: "...",       // enter a different doctype for older markup
    removeScripts: false,       // remove script tags from print content
    copyTagClasses: false       // copy classes from the html & body tag
});

    });


$(document).on('click', '.create-msg, #compose, .compose-close', function(e)    // wysiwyg for composing messages
{
  if( !$(this).attr('class').includes('compose-close')){
    init_wysiwyg();
  }

	$(".compose").slideToggle();
  $('.compose-header').html("New Message <button type='button' class= 'close compose-close'> <span>×</span> </button>");
  $('#compose-subject').val('');
  $('#compose-send').attr('data-compose-type', '1');


});

$(document).on('click', '.compose-close', function(e)    // wysiwyg for composing messages
{
$('.create-msg').removeClass('active');
});





      function init_compose() { //init for above wysiwyg
          "undefined" != typeof $.fn.slideToggle && (console.log("init_compose"), $("#compose, .compose-close").click(function() {
              $(".compose").slideToggle()
          }))
      }
      function init_wysiwyg() {
          function b(a, b) {
              var c = "";
              "unsupported-file-type" === a ? c = "Unsupported format " + b : console.log("error uploading file", a, b), $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>File upload error</strong> ' + c + " </div>").prependTo("#alerts")
          }
          "undefined" != typeof $.fn.wysiwyg && (console.log("init_wysiwyg"), $(".editor-wrapper").each(function() {
              var a = $(this).attr("id");
              $(this).wysiwyg({
                  toolbarSelector: '[data-target="#' + a + '"]',
                  fileUploadError: b
              })
          }), window.prettyPrint, prettyPrint())
      }


function clean(){

  $('#compose-subject').val("");
  $('#compose-recipient').val("");
  $('#editor').html("");

}


function notification(ntitle, ntext, ntype){
  PNotify.removeAll();
  new PNotify({
                                  title: ntitle,
                                  text: ntext,
                                  type: ntype,
                                  styling: 'bootstrap3'
                              });
}
