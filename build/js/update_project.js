var kts_update_client;
var kts_update_client_text;
var kts_update_pname;
var kts_update_ptype;
var kts_update_ptype_text;
var kts_update_pleader;
var kts_update_pleader_text;
var kts_update_pstart;
var kts_update_pend;
var kts_update_pprogress;
var kts_update_pstatus;
var kts_update_pstatus_text;
var kts_update_pdescrp;
var kts_update_phours;
var kts_update_pdeposit;
var kts_update_pymnt_due;

var cpid;
var count = 0;


var bi = [];

$(document).on('click', '.edit_current_project', function(e) {

  if ($(this).is('li')) {
    cpid = $(this).attr('data-cpid'); //current project id from selected row
  } else {
    cpid = $(this).parent().closest('tr').attr('data-cpid'); //current project id from selected row
  }
  if (typeof cpid == "undefined") {
    cpid = $('.project_upload').attr('data-cpid');
  }

  $.ajax({
      method: "POST",
      url: "serve/update_current_project/index.php",
      data: {
        'cpid': cpid
      },
      beforeSend: function() {
        NProgress.start();
        $(".right_col").fadeOut();
      }

    })
    .done(function(html) {
      $(".right_col").empty();
      $(".right_col").append(html);
      $(".right_col").fadeIn();

      $(".progress .progress-bar")[0] && $(".progress .progress-bar").progressbar();
      $('html, body').animate({
        scrollTop: 0
      }, 400);

      init_wysiwyg();

      $('.start-date-picker').datetimepicker({
        format: 'YYYY-MM-DD',
        minDate: moment()
      });
      $('.pend-date-picker').datetimepicker({
        format: 'YYYY-MM-DD',
        minDate: moment()
      });
      $('.pymnt-date-picker').datetimepicker({
        format: 'YYYY-MM-DD'
      });


      kts_update_client = {
        value: $('#update-project-client').val()
      };
      kts_update_client_text = {
        value: $("#update-project-client").find("option:selected").text()
      };
      kts_update_pname = {
        value: $('#update-project-name').val()
      };
      kts_update_ptype = {
        value: $('#update-project-type').val()
      };
      kts_update_ptype_text = {
        value: $("#update-project-type").find("option:selected").text()
      };

      kts_update_pleader = {
        value: $('#update-project-leader').val()
      };
      kts_update_pleader_text = {
        value: $("#update-project-leader").find("option:selected").text()
      };
      kts_update_pstart = {
        value: $('.start-date-picker').val()
      };
      kts_update_pend = {
        value: $('.pend-date-picker').val()
      };
      kts_update_pprogress = {
        value: $('#update-project-progress').val()
      };
      kts_update_pstatus = {
        value: $('#update-project-status').val()
      };
      kts_update_pstatus_text = {
        value: $("#update-project-status").find("option:selected").text()
      };
      kts_update_pdescrp = {
        value: $('#editor-one').html()
      };
      kts_update_phours = {
        value: $('#update-project-hours').val()
      };
      kts_update_pdeposit = {
        value: $('#update-project-deposit').val()
      };
      kts_update_pymnt_due = {
        value: $('#update-pymnt-date-picker').val()
      };



      $('.billable-table > tbody  > tr').each(function() {
        bi.push({
          'iiid': $(this).attr('data-iiid'),
          'descrp': $(this).find('.puii-descrip').val(),
          'qty': $(this).find('.puii-qty').val(),
          'amount': $(this).find('.puii-amount').val(),
          'tax': $(this).find('.puii-tax').val(),
          'ship': $(this).find('.puii-ship').val(),
          'paid': $(this).find('.puii-paid').val(),
          'estimate': $(this).find('.puii-estimate').val(),
          options: getOption($(this))
        });

      });


      NProgress.done();

    });
});


$(document).on('blur', '#update-project-client', function(e) {
  updater(cpid, $(this), kts_update_client, kts_update_client_text, 'update_client', 'Client');
});


$(document).on('blur', '#update-project-name', function(e) {
  updater(cpid, $(this), kts_update_pname, '', 'update_pname', 'Project Name');
});

$(document).on('blur', '#update-project-type', function(e) {
  updater(cpid, $(this), kts_update_ptype, kts_update_pleader_text, 'update_ptype', 'Project Type');
});

$(document).on('blur', '#update-project-leader', function(e) {
  updater(cpid, $(this), kts_update_pleader, kts_update_pleader_text, 'update_pleader', 'Project Leader');
});

$(document).on('blur', '#update-start-date-picker', function(e) {
  updater(cpid, $(this), kts_update_pstart, '', 'update_pstart', 'Project Start');
});

$(document).on('blur', '#update-pend-date-picker', function(e) {
  updater(cpid, $(this), kts_update_pend, '', 'update_pend', 'Project End');
});

$(document).on('blur', '#update-project-progress', function(e) {
  updater(cpid, $(this), kts_update_pprogress, '', 'update_pprogress', 'Project Progress');
});

$(document).on('blur', '#update-project-status', function(e) {
  updater(cpid, $(this), kts_update_pstatus, kts_update_pstatus_text, 'update_pstatus', 'Project Status');
});

$(document).on('click', '#editor-one', function(e) {
  $('#kts-editor-one').focus();
});

$(document).on('blur', '#kts-editor-one', function(e) {
  updater(cpid, $('#editor-one'), kts_update_pdescrp, '', 'update_pdescrp', 'Project Description');
});


$(document).on('blur', '#update-project-deposit', function(e) {
  updater(cpid, $(this), kts_update_pdeposit, '', 'update_pdeposit', 'Project Deposit');
});

$(document).on('blur', '#update-project-hours', function(e) {
  updater(cpid, $(this), kts_update_phours, '', 'update_phours', 'Project Time Log');
});

$(document).on('blur', '#update-pymnt-date-picker', function(e) {
  updater(cpid, $(this), kts_update_pymnt_due, '', 'update_pymnt_due', 'Invoice Due Date');
});




function updater(cpid, project_object, init_value, init_valueb, option, descrp) {


  if ((project_object.val() != init_value.value && project_object.prop("id") != 'editor-one') || (project_object.html() != init_value.value && project_object.prop("id") == 'editor-one')) {
    isselect = false;
    if (project_object.is('select')) {
      isselect == true;
    }

    $('.update-project-modal-p1').text('You have made a change to the ' + descrp.toLowerCase() + '.');
    $('.update-project-modal-p2').text('Are you sure you want to save this new change?');

    localmodal = $(".update-project-modal-sm").modal();

    localmodal.one('click', '.update-project-modal-cancel', function(e) {

      e.stopPropagation();
      project_object.val(init_value.value);
      if (project_object.prop("id") == 'editor-one') {
        project_object.html(init_value.value)
      }

      if (isselect) {
        project_object.find("option:selected").text(init_value.valueb);
      }

      $('body').removeClass('modal-open');
      $('.modal-backdrop').remove();
      $(".update-project-modal-sm").modal("hide");


    });

    localmodal.one('click', '.update-project-modal-accept', function(e) {
      e.stopPropagation();
      $('body').removeClass('modal-open');
      $('.modal-backdrop').remove();
      $(".update-project-modal-sm").modal("hide");


      newkts_name = project_object.val();
      if (project_object.prop("id") == 'editor-one') {
        newkts_name = project_object.html();
      }
      if (isselect) {
        newkts_name_text = project_object.find(" option[value='" + init_value.value + "']").eq(0).text();
      }

      url = "serve/update_current_project/update_project.php";
      if (serverTrip(option, cpid, newkts_name, descrp, url)) {
        init_value.value = newkts_name; //project_object.val();
        if (project_object.prop("id") == 'editor-one') {
          init_value.value = newkts_name; //project_object.html();
        }

        project_object.val(newkts_name);
        if (project_object.prop("id") == 'editor-one') {
          project_object.html(newkts_name)
        }
        if (isselect) {
          project_object.find(" option[value='" + newkts_name + "']").eq(0).text();
        }

      } else {

        e.stopPropagation();
        project_object.val(init_value.value);
        if (project_object.prop("id") == 'editor-one') {
          project_object.html(init_value.value)
        }
        if (isselect) {
          project_object.find("option:selected").text(init_value.valueb);
        }
      }

    });



  } else {

    project_object.val(init_value.value);
    if (project_object.prop("id") == 'editor-one') {
      project_object.html(init_value.value)
    }
  }


}


function serverTrip(option, cpid, value, description, location) {

  json = null;


  $.ajax({
      method: "POST",
      url: location,
      async: false,
      data: {
        'option': option,
        'cpid': cpid,
        'iiid': cpid,
        'value': value
      },
      beforeSend: function() {
        NProgress.start();
        console.log("START--->" + count);
        //$( ".right_col" ).fadeOut();
      }

    })
    .done(function(data) {
      json = $.parseJSON(data);
      console.log("DONE--->" + count);
      NProgress.done();
    });



  if (json.code == '1') {
    PNotify.removeAll();
    console.log('SUcCESS!');
    new PNotify({
      title: 'Success!',
      text: description + ' has been updated.',
      type: 'success',
      styling: 'bootstrap3'
    });

    json = null;


    return true;
  } else if (json.code == '4') {
    PNotify.removeAll();
    console.log('FAiLURE!');
    new PNotify({
      title: 'There\'s a problem..',
      text: json.msg,
      type: 'error',
      styling: 'bootstrap3'
    });
    json = null;


    return false;

  } else {
    PNotify.removeAll();
    console.log('FAiLURE!--'+json.msg);
    new PNotify({
      title: 'There\'s a problem..',
      text: json.msg, //'There was error updating ' +description+'. Try again later.',
      type: 'error',
      styling: 'bootstrap3'
    });
    json = null;

    return false;
  }





}





var curbi;
var new_curbi;
var bi_item;
var bi_item_option;
var bi_isselect;
//var iiid;

// $(document).on('click','.puii-descrip, .puii-qty, .puii-amount, .puii-tax, .puii-ship, .puii-paid',function(e){
//
//   curbi = $(this).val();
//   bi_item = $(this);
//   bi_item_option = getOption(bi_item);
//
// });

$(document).on('blur', '.puii-descrip, .puii-qty, .puii-amount, .puii-tax, .puii-ship, .puii-paid, .puii-estimate', function(e) {

  bi_item = $(this);
  new_value = $(this).val();

  iiid = $(this).closest('tr').attr('data-iiid');
  object = bi.filter(function(e) {
    return e.iiid == iiid;
  });
  bi_class = getClass($(this));
  initial_value = null;

  if (bi_class == '.puii-descrip') {
    initial_value = object[0].descrp;
  } else if (bi_class == '.puii-qty') {
    initial_value = object[0].qty;
  } else if (bi_class == '.puii-amount') {
    initial_value = object[0].amount;
  } else if (bi_class == '.puii-tax') {
    initial_value = object[0].tax;
  } else if (bi_class == '.puii-ship') {
    initial_value = object[0].ship;
  } else if (bi_class == '.puii-paid') {
    initial_value = object[0].paid;
  } else if (bi_class == '.puii-estimate') {
    initial_value = object[0].estimate;
  }




  if (new_value != initial_value) {

    $('.update-project-modal-p1').text('You have made a change.');
    $('.update-project-modal-p2').text('Are you sure you want to save this new change?');

    localmodal = $(".update-project-modal-sm").modal();


    localmodal.one('click', '.update-project-modal-cancel', function(e) {

      e.stopPropagation();

      bi_item.val(initial_value);

      $(".update-project-modal-sm").modal("hide");


    });

    localmodal.one('click', '.update-project-modal-accept', function(e) {
      e.stopPropagation();
      $(".update-project-modal-sm").modal("hide");
      $('body').removeClass('modal-open');
      $('.modal-backdrop').remove();

      item_option = getOption(bi_item);
      url = "serve/update_current_project/update_bi.php";
      //alert("Option: "+item_option.option+"\n ID: "+iiid+"\n Value: "+new_value+"\n Descrp: "+item_option.description+"\n URL: "+url);
      if (serverTrip(item_option.option, iiid, new_value, item_option.description, url)) {



        bi_item.val(new_value);
        $('.edit_current_project').trigger("click");

      } else {

        e.stopPropagation();
        bi_item.val(initial_value);


      }

    });

  }
});



function getOption(item) {
  if (item.hasClass('puii-descrip')) {
    return {
      option: 'update_bi_descrp',
      description: 'Item Description'
    };
  } else if (item.hasClass('puii-qty')) {
    return {
      option: 'update_bi_qty',
      description: 'Item Quantity'
    };
  } else if (item.hasClass('puii-amount')) {
    return {
      option: 'update_bi_amount',
      description: 'Item Amount'
    };
  } else if (item.hasClass('puii-paid')) {
    return {
      option: 'update_bi_paid',
      description: 'Item Paid Status'
    };
  } else if (item.hasClass('puii-estimate')) {
    return {
      option: 'update_bi_estimate',
      description: 'Item Estimate Status'
    };
  } else if (item.hasClass('puii-tax')) {
    return {
      option: 'update_bi_tax',
      description: 'Item Tax'
    };
  } else if (item.hasClass('puii-ship')) {
    return {
      option: 'update_bi_ship',
      description: 'Item Shipping'
    };
  } else {
    return null;
  }
}

function getClass(item) {
  if (item.hasClass('puii-descrip')) {
    return '.puii-descrip';
  } else if (item.hasClass('puii-qty')) {
    return '.puii-qty';
  } else if (item.hasClass('puii-amount')) {
    return '.puii-amount';
  } else if (item.hasClass('puii-paid')) {
    return '.puii-paid';
  } else if (item.hasClass('puii-estimate')) {
    return '.puii-estimate';
  } else if (item.hasClass('puii-tax')) {
    return '.puii-tax';
  } else if (item.hasClass('puii-ship')) {
    return '.puii-ship';
  } else {
    return null;
  }
}
// $('.billable-table > tbody  > tr').each(function() {
// bi.push({'iiid': $(this).attr('data-iiid'), 'descrp': $(this).find('.puii-descrip').val(),
// 'qty': $(this).find('.puii-qty').val(), 'amount':$(this).find('.puii-amount').val(),
// 'tax': $(this).find('.puii-tax').val(), 'ship': $(this).find('.puii-ship').val(),
// 'paid': $(this).find('.puii-paid').val(), options:getOption($(this))});

// });
function getKey(class_name) {
  if (class_name == '.puii-descrip') {
    return 'descrp';
  }
}


$(document).on('click', '.add-puii-item', function(e) {

  cid = $('.add-item').attr('data-client');
  cpid = $('.edit_current_project').attr('data-cpid');

  descrp = $('.add-item').find('.add-puii-descrip').val();
  qty = $('.add-item').find('.add-puii-qty').val();
  amount = $('.add-item').find('.add-puii-amount').val();
  tax = $('.add-item').find('.add-puii-tax').val();
  ship = $('.add-item').find('.add-puii-ship').val();
  curr = $('.add-item').find('.add-puii-curr').val();


  $('.update-project-modal-p1').text('You are adding a billable item.');
  $('.update-project-modal-p2').text('Are you sure you want to save this new change?');

  localmodal = $(".update-project-modal-sm").modal();


  localmodal.one('click', '.update-project-modal-cancel', function(e) {

    e.stopPropagation();

    $('.add-item').find('.add-puii-descrip').val('');
    $('.add-item').find('.add-puii-qty').val('');
    $('.add-item').find('.add-puii-amount').val('');
    $('.add-item').find('.add-puii-tax').val('');
    $('.add-item').find('.add-puii-ship').val('');
    $('.add-item').find('.add-puii-curr').val('');

    $(".update-project-modal-sm").modal("hide");


  });

  localmodal.one('click', '.update-project-modal-accept', function(e) {
    e.stopPropagation();
    $(".update-project-modal-sm").modal("hide");
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();

    json = null;


    $.ajax({
        method: "POST",
        url: "serve/update_current_project/add_bi.php",
        async: false,
        data: {
          'option': 'add_bi',
          'cid': cid,
          'cpid': cpid,
          'add_descrp': descrp,
          'add_qty': qty,
          'add_amount': amount,
          'add_tax': tax,
          'add_ship': ship,
          'add_curr': curr
        },
        beforeSend: function() {
          NProgress.start();
          console.log("START--->" + count);
          //$( ".right_col" ).fadeOut();
        }

      })
      .done(function(data) {
        //console.log(data);
        json = $.parseJSON(data);
        console.log("DONE--->" + count);
        NProgress.done();
        $('.edit_current_project').trigger("click");
      });



    if (json.code == '1') {
      PNotify.removeAll();
      console.log('SUcCESS!');
      new PNotify({
        title: 'Success!',
        text: ' Item has been added.',
        type: 'success',
        styling: 'bootstrap3'
      });

      json = null;


      return true;
    } else if (json.code == '4') {
      PNotify.removeAll();
      console.log('FAiLURE!');
      new PNotify({
        title: 'There\'s a problem..',
        text: json.msg,
        type: 'error',
        styling: 'bootstrap3'
      });
      json = null;


      return false;

    } else {
      PNotify.removeAll();
      console.log('FAiLURE!');
      new PNotify({
        title: 'There\'s a problem..',
        text: json.msg, //'There was error updating ' +description+'. Try again later.',
        type: 'error',
        styling: 'bootstrap3'
      });
      json = null;

      return false;
    }

  });


});


$(document).on('click', '.bi_delete', function(e) {


  cpid = $('.edit_current_project').attr('data-cpid');
  iiid = $(this).closest('tr').attr('data-iiid');


  $('.update-project-modal-p1').text('You are DELETING a billable item.');
  $('.update-project-modal-p2').text('Are you really sure you want to save this new change?');

  localmodal = $(".update-project-modal-sm").modal();


  localmodal.one('click', '.update-project-modal-cancel', function(e) {

    e.stopPropagation();


    $(".update-project-modal-sm").modal("hide");


  });

  localmodal.one('click', '.update-project-modal-accept', function(e) {
    e.stopPropagation();
    $(".update-project-modal-sm").modal("hide");
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();

    json = null;


    $.ajax({
        method: "POST",
        url: "serve/update_current_project/update_bi.php",
        async: false,
        data: {
          'option': 'delete_bi',
          'iiid': iiid,
          'cpid': cpid
        },
        beforeSend: function() {
          NProgress.start();
          console.log("START--->" + count);
          //$( ".right_col" ).fadeOut();
        }

      })
      .done(function(data) {
        json = $.parseJSON(data);
        console.log("DONE--->" + count);
        NProgress.done();
        $('.edit_current_project').trigger("click");
      });



    if (json.code == '1') {
      PNotify.removeAll();
      console.log('SUcCESS!');
      new PNotify({
        title: 'Success!',
        text: ' Item has been deleted.',
        type: 'success',
        styling: 'bootstrap3'
      });

      json = null;


      return true;
    } else if (json.code == '4') {
      PNotify.removeAll();
      console.log('FAiLURE!');
      new PNotify({
        title: 'There\'s a problem..',
        text: json.msg,
        type: 'error',
        styling: 'bootstrap3'
      });
      json = null;


      return false;

    } else {
      PNotify.removeAll();
      console.log('FAiLURE!');
      new PNotify({
        title: 'There\'s a problem..',
        text: json.msg, //'There was error updating ' +description+'. Try again later.',
        type: 'error',
        styling: 'bootstrap3'
      });
      json = null;

      return false;
    }

  });


});
