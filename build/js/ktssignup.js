init_SmartWizardb();
init_validatorb();

$(document).ready(function() {
    $('#signupform')
        .find('[name="phone1"]')
            .intlTelInput({
                utilsScript: '../vendors/intl-tel-input-12.1.0/build/js/utils.js',
                autoPlaceholder: true,
                preferredCountries: ['jm', 'us', 'de', 'gb']
            });

    $('#signupform')
        .find('[name="phone2"]')
            .intlTelInput({
                utilsScript: '../vendors/intl-tel-input-12.1.0/build/js/utils.js',
                autoPlaceholder: true,
                preferredCountries: ['jm', 'us', 'de', 'gb']
            });

});

$('#client-type-on').hide();
$('#client-type-ot').hide();
$(document).on('click','.client-type-i-button',function(e){

  $('#client-type-on').fadeOut();
  $('#client-type-ot').fadeOut();
  $('#organization-name').removeAttr('required');
  $('#organization-type').removeAttr('required');

});

$(document).on('click','.client-type-o-button',function(e){

  //$('#client-type-on').show();
  $('#client-type-on').fadeIn();
  $('#client-type-ot').fadeIn();
  //$('#client-type-ot').fadeIn();
  $('#organization-name').attr('required', 'required');
  $('#organization-type').attr('required', 'required');

});


// Initialize the showStep event
object = $('#wizard_horizontal');
stepDirection = 'forward';


$("#wizard_horizontal").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {

  $(anchorObject).removeClass('selected');
  $(anchorObject).addClass('disabled');

//  $('#signup-anchor li a')[stepNumber].removeClass('selected');

    var json;
    var elmForm = $("#signupform");
     //stepDirection === 'forward' :- this condition allows to do the form validation
    // only on forward navigation, that makes easy navigation on backwards still do the validation when going next
    if(stepDirection === 'forward' && elmForm && stepNumber == 1){

        //elmForm.validator('validate');
        var elmErr = elmForm.children('.has-error');
        if((!validator.checkAll( '#signupform' ) || elmErr && elmErr.length > 0) || ($('#email').val() != $('#email2').val()) || ($('#password').val() != $('#password2').val()) ){
            // Form validation failed
            console.log("whoa nelly!");
            return false;
        }
        else{

          first_name = $('#first-name').val();
          last_name = $('#last-name').val();
          email = $('#email').val();
          confirm_email = $('#email2').val();
          phone1 = $("#phone1").intlTelInput("getNumber");//$('#phone1').val();
          phone2 = $("#phone2").intlTelInput("getNumber");
          address1 = $('#address1').val();
          address2 = $('#address2').val();
          country = $('#country').val();
          client_type = $('input[name=client-type]:checked').val();
          user_name = $('#user-name').val();
          password = $('#password').val();
          password2 = $('#password2').val();
          organization_name = $('#organization-name').val();
          organization_type = $('#organization-type').val();



          $.ajax({
              method: "POST",
              url: "ktssignup.php",
              async: false,
              data: {'first-name': first_name, 'last-name': last_name, 'email': email, 'confirm-email': confirm_email, 'phone1': phone1, 'phone2': phone2,
            'address1': address1, 'address2': address2, 'country': country, 'client-type': client_type, 'organization-name': organization_name, 'organization-type': organization_type, 'user-name': user_name, 'password': password, 'password2': password2 },
              beforeSend: function(){
                NProgress.start();
                //$( ".right_col" ).fadeOut();
              }

            })
              .done(function( data ) {
                console.log('1');
                json = $.parseJSON(data);
                console.log('2');
              //  alert(json.code);
                //$( ".right_col" ).fadeIn();

                $(".progress .progress-bar")[0] && $(".progress .progress-bar").progressbar();
                $('html, body').animate({ scrollTop: 0 }, 0);

                NProgress.done();



              });

//alert(json.msg);

if(json.code == '2' || json.code == '3' || typeof json == "undefined"){
  new PNotify({
                                  title: 'Insight Sign Up Failed :(',
                                  text: 'There was a problem. Please try again later.',
                                  type: 'error',
                                  styling: 'bootstrap3'
                              });

  console.log('KTS sign up failure');
  console.log(json.msg);
  return false;
}
else if (json.code == '4') {
  new PNotify({
                                  title: 'Insight Sign Up Failed :(',
                                  text: json.msg,
                                  type: 'error',
                                  styling: 'bootstrap3'
                              });
  console.log('KTS sign up failure');
  return false;
}
else if (json.code == '1') {
  new PNotify({
                                  title: 'Almost there!',
                                  text: 'Please check your email for a confirmation message.',
                                  type: 'success',
                                  styling: 'bootstrap3',
                                  addclass: 'dark'
                              });
  console.log('KTS sign up success');
  $('.sw-btn-prev').fadeOut(); //Disable previous button
  return true;
}


        }
    }

});



$("#wizard_horizontal").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {



  if(stepDirection === 'forward' && stepNumber == '2'){

    //////////////////MANUAL CONFIRM:
    $(document).on('click', '#step-confirm-go-3' ,function(e){

      successLink = "../images/success.png";
      loginLink = "../login";
      confirmation = $('#step-confirm-3').val();

    $.ajax({
        method: "POST",
        url: "ktssignup.php",
        data: {'confirmation': confirmation},
        beforeSend: function(){
          NProgress.start();
        }

      })
        .done(function( data) {
          json = $.parseJSON(data);

          if(json.code == '1'){

          $(".progress .progress-bar")[0] && $(".progress .progress-bar").progressbar();
          $('html, body').animate({ scrollTop: 0 }, 0);

          NProgress.done();

}
else if (json.code == '2') {

  new PNotify({
                                  title: 'Insight User Confirmation Failed :(',
                                  text: "The confirmation code seems incorrect. Please try again.",
                                  type: 'error',
                                  styling: 'bootstrap3'
                              });
  console.log('KTS sign up failure');
  return false;

}


        });


     });



    /////////////////////FOR POLLING:

    option = 'poll';
    email = $('#email').val();
    user_name = $('#user-name').val();

    interval = setInterval(signUpPoll, 3000);

    function signUpPoll(){
      successLink = "../images/success.png";
      loginLinkz = "../login/?welcome";

    $.ajax({
        method: "POST",
        url: "ktssignup.php",
        data: {'option': option, 'email': email, 'user-name': user_name},
        beforeSend: function(){
          NProgress.start();
          //$( ".right_col" ).fadeOut();
        }

      })
        .done(function( data) {
          json = $.parseJSON(data);

          if(json.code == '1'){

          $(".progress .progress-bar")[0] && $(".progress .progress-bar").progressbar();
          $('html, body').animate({ scrollTop: 0 }, 0);

          NProgress.done();
          $('#step-title-3').hide();
          $('#step-image-3').hide();
          $('#step-form-3').fadeOut();

          $('#step-title-3').text("Congratulations!").fadeIn();
          $('#step-description-3').html("You have successfully confirmed your email. You can now proceed to login <a href='"+loginLinkz +"'>here.</a>").fadeIn();
          $('#step-image-3').html( "<br/><br/><br/><img  src='"+successLink+"' />" ).fadeIn();
          $('#step-login-3').fadeIn();
          clearInterval(interval);
}


        });
      }
  }





});



$("#wizard_horizontal").on("showStep", function(e, object, stepNumber, stepDirection) {
   console.log("You are on step "+stepNumber+" now");
   $('html, body').animate({ scrollTop: 0 },  500, 'swing');

   $(object).removeClass('disabled');
   $(object).addClass('selected');
});







function init_SmartWizardb() {

  $('#wizard_horizontal').smartWizard({
              selected: 0,  // Initial selected step, 0 = first step
              keyNavigation:true, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
              autoAdjustHeight:true, // Automatically adjust content height
              cycleSteps: false, // Allows to cycle the navigation of steps
              backButtonSupport: true, // Enable the back button support
              useURLhash: true, // Enable selection of the step based on url hash
              lang: {  // Language variables
                  next: 'Next',
                  previous: 'Previous'
              },
              toolbarSettings: {
                  toolbarPosition: 'bottom', // none, top, bottom, both
                  toolbarButtonPosition: 'right', // left, right
                  showNextButton: true, // show/hide a Next button
                  showPreviousButton: true, // show/hide a Previous button
                  toolbarExtraButtons: [
  			$("<button id='step-login-3'></button>").text('Login To Insight')
  					      .addClass('btn btn-info')
  					      .on('click', function(){
  						window.location.replace('../login');
  					      })/*,
  			$('<button></button>').text('Cancel')
  					      .addClass('btn btn-danger')
  					      .on('click', function(){
  						alert('Cancel button click');
            })*/
                        ]
              },
              anchorSettings: {
                  anchorClickable: true, // Enable/Disable anchor navigation
                  enableAllAnchors: false, // Activates all anchors clickable all times
                  markDoneStep: true, // add done css
                  enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
              },
              contentURL: null, // content url, Enables Ajax content loading. can set as data data-content-url on anchor
              disabledSteps: [],    // Array Steps disabled
              errorSteps: [],    // Highlight step with errors
              theme: 'dots',
              transitionEffect: 'fade', // Effect on navigation, none/slide/fade
              transitionSpeed: '600'
        });

        $('#step-login-3').hide();
        $('#signup-anchor').removeClass('step-anchor nav nav-tabs');






    /*"undefined" != typeof $.fn.smartWizard && (console.log("init_SmartWizardb"), $("#wizard_horizontal").smartWizard({
        transitionEffect: "fade",
        theme:"arrows"
    }), $(".buttonNext").addClass("btn btn-success"), $(".buttonPrevious").addClass("btn btn-primary"), $(".buttonFinish").addClass("btn btn-default"))
*/
}

function init_validatorb() {
    "undefined" != typeof validator && (console.log("init_validatorb"), validator.message.date = "not a real date", $("form").on("blur", "input[required], input.optional, select.required", validator.checkField).on("change", "select.required", validator.checkField).on("keypress", "input[required][pattern]", validator.keypress), $(".multi.required").on("keyup blur", "input", function() {
        validator.checkField.apply($(this).siblings().last()[0])
    }), $("form").submit(function(a) {
        a.preventDefault();
        var b = !0;
        return validator.checkAll($(this)) || (b = !1), b && this.submit(), !1
    }))
}
