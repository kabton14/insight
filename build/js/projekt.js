  $(document).on('click','.current_project',function(e){
      $.ajax({
        method: "POST",
        url: "serve/current_project/index.php",
        beforeSend: function(){
          NProgress.start();
          $( ".right_col" ).fadeOut();
        }

      })
        .done(function( html ) {
          $( ".right_col" ).empty();
          $( ".right_col" ).append( html);
          $( ".right_col" ).fadeIn();

          $(".progress .progress-bar")[0] && $(".progress .progress-bar").progressbar();
          $('html, body').animate({ scrollTop: 0 }, 400);

          $('#current-projects').DataTable();


          NProgress.done();

        });
    });

    $(document).on('click','.past-project',function(e){
        $.ajax({
          method: "POST",
          url: "serve/current_project/index.php",
          data: {operation:'old'},
          beforeSend: function(){
            NProgress.start();
            $( ".right_col" ).fadeOut();
          }

        })
          .done(function( html ) {
            $( ".right_col" ).empty();
            $( ".right_col" ).append( html);
            $( ".right_col" ).fadeIn();

            $(".progress .progress-bar")[0] && $(".progress .progress-bar").progressbar();
            $('html, body').animate({ scrollTop: 0 }, 400);

            $('#current-projects').DataTable();

            NProgress.done();

          });
      });

    $(document).on('click','.create_project',function(e){
        $.ajax({
          method: "POST",
          url: "serve/create_project/index.php",
          beforeSend: function(){
            NProgress.start();
            $( ".right_col" ).fadeOut();
          }

        })
          .done(function( html ) {
            $( ".right_col" ).empty();
            $( ".right_col" ).append( html);
            $( ".right_col" ).fadeIn();

            $(".progress .progress-bar")[0] && $(".progress .progress-bar").progressbar();
            $('html, body').animate({ scrollTop: 0 }, 400);

            init_wysiwyg();

            $('.start-date-picker').datetimepicker({
                format: 'YYYY-MM-DD',
                minDate: moment()
            });
            $('.pend-date-picker').datetimepicker({
                format: 'YYYY-MM-DD',
                minDate: moment()
            });


            NProgress.done();

          });
      });

      $(document).on('click','.create-project-submit',function(e){

        project_client = $('#create-project-client').val();
        project_name = $('#create-project-name').val();
        project_type = $('#create-project-type').val();
        project_leader =  $('#create-project-leader').val();
        project_start_date =  $('#create-start-date-picker').val();
        project_end_date =  $('#create-pend-date-picker').val();
        project_progress = $('#create-project-progress').val();
        project_status = $('#create-project-status').val();
        project_description = $('#editor-one').html();
        project_deposit = $('#create-project-deposit').val();


          $.ajax({
            method: "POST",
            url: "serve/create_project/create_project.php",
            data: {'option': 'create_project', 'project_client': project_client, 'project_name': project_name, 'project_type': project_type, 'project_leader': project_leader,
            'project_start_date': project_start_date, 'project_end_date': project_end_date, 'project_progress': project_progress,
             'project_status':project_status, 'project_description':project_description, 'project_deposit': project_deposit },
            beforeSend: function(){
              NProgress.start();
              //$( ".right_col" ).fadeOut();
            }

          })
            .done(function(json) {

              json = $.parseJSON(json);

              if(json.code == '1'){


              $('html, body').animate({ scrollTop: 0 }, 0);
              resetProjectValues();

              notification('Project Created', 'Your project was succesfully created.', 'success');

            }
            else{
              notification('Error', 'Your project was not succesfully created. Ensure that all required fields are properly filled', 'error');
              $('html, body').animate({ scrollTop: 0 }, 0);

            }

              NProgress.done();

            });
        });

        $(document).on('click','.create-project-reset',function(e){
          $('html, body').animate({ scrollTop: 0 }, 400);
          resetProjectValues();
        });


    function resetProjectValues(){

      $('#create-project-client').val('');
      $('#create-project-name').val('');
      $('#create-project-type').val('');
      $('#create-project-leader').val('');
      $('#create-start-date-picker').val('');
      $('#create-pend-date-picker').val('');
      $('#create-project-progress').val('');
      $('#create-project-status').val('');
      $('#editor-one').html('');
      $('#create-project-deposit').val('');
    }





   function listProjectDetails(pid){

     $.ajax({
       method: "POST",
       url: "serve/current_project_details/index.php",
       data: {cpid:pid},
       beforeSend: function(){
         NProgress.start();
         $( ".right_col" ).fadeOut();
       }
     })
       .done(function( html ) {
        $( ".right_col" ).empty();
        $( ".right_col" ).append( html);
        $( ".right_col" ).fadeIn();

        $("#project_title").attr('data-cpid', id);

        progress = $('#echart_mini_pie').attr('data-progress');

        init_echarts(progress);
        $('html, body').animate({ scrollTop: 0 }, 400);
        NProgress.done();
      });
   }

   $(document).on('click','.list_current_project',function(e){
    id = $(this).parent().closest('tr').attr('data-cpid'); //current project id from selected row
    if(typeof id == "undefined"){
      id = $('.project_upload').attr('data-cpid');
    }

    listProjectDetails(id);


  });


   $(document).on('click','#send-project-update',function(e){
     cpid = $('#project_title').attr('data-cpid');
     update_content = $('.project-update-content').val();

     $(".project-update-modal-sm").modal();



    $(document).on('click', '.project-update-modal-accept',function(e){



         $.ajax({
           method: "POST",
           url: "serve/current_project_details/additional_actions.php",
           data: {option:'send-project-update', 'cpid':cpid, 'project-update-content': update_content },
           async:false,
           beforeSend: function(){
             NProgress.start();
             $(".project-update-modal-sm").modal('hide');
           }
         })
           .done(function(data) {
             json = $.parseJSON(data);

             if(json.code == '1'){
           notification('Success', 'Your update was sent.', 'success');


           //getMailList(msgindex);

         }
         else if (json.code =='4') {
           notification('Error :(', json.msg, 'error');

         }
         else{
           notification('Error :(', 'There was a problem sending your update.', 'error');

         }


             $(".progress .progress-bar")[0] && $(".progress .progress-bar").progressbar();
             $('html, body').animate({ scrollTop: 0 }, 400);

             $(".project-update-modal-sm").modal('hide');
             $('body').removeClass('modal-open');
             $('.modal-backdrop').remove();
             NProgress.done();
          });

          listProjectDetails(cpid);








        });









   });

   $(document).on('click','#uploadfile',function(e){
    id = $('#project_title').attr('data-cpid'); //current project id from selected row
    client = $('#project_title').attr('data-client'); //current project cid from selected row

    $.ajax({
      method: "POST",
      url: "serve/client_upload/index.php",
      data: {cpid:id, 'client':client},
      dataType: 'html',
      beforeSend: function(){
        NProgress.start();
        $( ".right_col" ).fadeOut();
      }
    })
      .done(function( html ) {
       $( ".right_col" ).empty();
       $( ".right_col" ).append( html);
       $( ".right_col" ).fadeIn();
       $(".dropzone").dropzone({ url: "serve/upload.php", acceptedFiles: "image/jpeg,image/jpg,image/png,image/gif,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/docx,application/pdf,text/plain,application/msword,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", maxFilesize: 50, dictDefaultMessage: 'Click or drop files here to upload',
       maxFiles:15,
    init: function() {
          this.on("maxfilesexceeded", function(file) {
                this.removeAllFiles();
                this.addFile(file);
          });
    } });
       $('html, body').animate({ scrollTop: 0 }, 400);


       NProgress.done();
     });
  });




      function init_echarts(progress) {

          if ("undefined" != typeof echarts) {
              console.log("init_echarts");
              var a = {
                  color: ["#26B99A", "#34495E", "#BDC3C7", "#3498DB", "#9B59B6", "#8abb6f", "#759c6a", "#bfd3b7"],
                  title: {
                      itemGap: 8,
                      textStyle: {
                          fontWeight: "normal",
                          color: "#408829"
                      }
                  },
                  dataRange: {
                      color: ["#1f610a", "#97b58d"]
                  },
                  toolbox: {
                      color: ["#408829", "#408829", "#408829", "#408829"]
                  },
                  tooltip: {
                      backgroundColor: "rgba(0,0,0,0.5)",
                      axisPointer: {
                          type: "line",
                          lineStyle: {
                              color: "#408829",
                              type: "dashed"
                          },
                          crossStyle: {
                              color: "#408829"
                          },
                          shadowStyle: {
                              color: "rgba(200,200,200,0.3)"
                          }
                      }
                  },
                  dataZoom: {
                      dataBackgroundColor: "#eee",
                      fillerColor: "rgba(64,136,41,0.2)",
                      handleColor: "#408829"
                  },
                  grid: {
                      borderWidth: 0
                  },
                  categoryAxis: {
                      axisLine: {
                          lineStyle: {
                              color: "#408829"
                          }
                      },
                      splitLine: {
                          lineStyle: {
                              color: ["#eee"]
                          }
                      }
                  },
                  valueAxis: {
                      axisLine: {
                          lineStyle: {
                              color: "#408829"
                          }
                      },
                      splitArea: {
                          show: !0,
                          areaStyle: {
                              color: ["rgba(250,250,250,0.1)", "rgba(200,200,200,0.1)"]
                          }
                      },
                      splitLine: {
                          lineStyle: {
                              color: ["#eee"]
                          }
                      }
                  },
                  timeline: {
                      lineStyle: {
                          color: "#408829"
                      },
                      controlStyle: {
                          normal: {
                              color: "#408829"
                          },
                          emphasis: {
                              color: "#408829"
                          }
                      }
                  },
                  k: {
                      itemStyle: {
                          normal: {
                              color: "#68a54a",
                              color0: "#a9cba2",
                              lineStyle: {
                                  width: 1,
                                  color: "#408829",
                                  color0: "#86b379"
                              }
                          }
                      }
                  },
                  map: {
                      itemStyle: {
                          normal: {
                              areaStyle: {
                                  color: "#ddd"
                              },
                              label: {
                                  textStyle: {
                                      color: "#c12e34"
                                  }
                              }
                          },
                          emphasis: {
                              areaStyle: {
                                  color: "#99d2dd"
                              },
                              label: {
                                  textStyle: {
                                      color: "#c12e34"
                                  }
                              }
                          }
                      }
                  },
                  force: {
                      itemStyle: {
                          normal: {
                              linkStyle: {
                                  strokeColor: "#408829"
                              }
                          }
                      }
                  },
                  chord: {
                      padding: 4,
                      itemStyle: {
                          normal: {
                              lineStyle: {
                                  width: 1,
                                  color: "rgba(128, 128, 128, 0.5)"
                              },
                              chordStyle: {
                                  lineStyle: {
                                      width: 1,
                                      color: "rgba(128, 128, 128, 0.5)"
                                  }
                              }
                          },
                          emphasis: {
                              lineStyle: {
                                  width: 1,
                                  color: "rgba(128, 128, 128, 0.5)"
                              },
                              chordStyle: {
                                  lineStyle: {
                                      width: 1,
                                      color: "rgba(128, 128, 128, 0.5)"
                                  }
                              }
                          }
                      }
                  },
                  gauge: {
                      startAngle: 225,
                      endAngle: -45,
                      axisLine: {
                          show: !0,
                          lineStyle: {
                              color: [
                                  [.2, "#86b379"],
                                  [.8, "#68a54a"],
                                  [1, "#408829"]
                              ],
                              width: 8
                          }
                      },
                      axisTick: {
                          splitNumber: 10,
                          length: 12,
                          lineStyle: {
                              color: "auto"
                          }
                      },
                      axisLabel: {
                          textStyle: {
                              color: "auto"
                          }
                      },
                      splitLine: {
                          length: 18,
                          lineStyle: {
                              color: "auto"
                          }
                      },
                      pointer: {
                          length: "90%",
                          color: "auto"
                      },
                      title: {
                          textStyle: {
                              color: "#333"
                          }
                      },
                      detail: {
                          textStyle: {
                              color: "auto"
                          }
                      }
                  },
                  textStyle: {
                      fontFamily: "Arial, Verdana, sans-serif"
                  }
              };

              if ($("#echart_pie").length) {
                  var j = echarts.init(document.getElementById("echart_pie"), a);
                  j.setOption({
                      tooltip: {
                          trigger: "item",
                          formatter: "{a} <br/>{b} : {c} ({d}%)"
                      },
                      legend: {
                          x: "center",
                          y: "bottom",
                          data: ["Direct Access", "E-mail Marketing", "Union Ad", "Video Ads", "Search Engine"]
                      },
                      toolbox: {
                          show: !0,
                          feature: {
                              magicType: {
                                  show: !0,
                                  type: ["pie", "funnel"],
                                  option: {
                                      funnel: {
                                          x: "25%",
                                          width: "50%",
                                          funnelAlign: "left",
                                          max: 1548
                                      }
                                  }
                              },
                              restore: {
                                  show: !0,
                                  title: "Restore"
                              },
                              saveAsImage: {
                                  show: !0,
                                  title: "Save Image"
                              }
                          }
                      },
                      calculable: !0,
                      series: [{
                          name: "访问来源",
                          type: "pie",
                          radius: "55%",
                          center: ["50%", "48%"],
                          data: [{
                              value: 335,
                              name: "Direct Access"
                          }, {
                              value: 310,
                              name: "E-mail Marketing"
                          }, {
                              value: 234,
                              name: "Union Ad"
                          }, {
                              value: 135,
                              name: "Video Ads"
                          }, {
                              value: 1548,
                              name: "Search Engine"
                          }]
                      }]
                  });
                  var k = {
                          normal: {
                              label: {
                                  show: !1
                              },
                              labelLine: {
                                  show: !1
                              }
                          }
                      },
                      l = {
                          normal: {
                              color: "rgba(0,0,0,0)",
                              label: {
                                  show: !1
                              },
                              labelLine: {
                                  show: !1
                              }
                          },
                          emphasis: {
                              color: "rgba(0,0,0,0)"
                          }
                      }
              }
              if ($("#echart_mini_pie").length) {
                  var m = echarts.init(document.getElementById("echart_mini_pie"), a);
                  m.setOption({
                      title: {
                          text: "Progress ("+progress+"%)" ,
                          subtext: "KTS Project",
                          sublink: "",
                          x: "center",
                          y: "center",
                          itemGap: 20,
                          textStyle: {
                              color: "rgba(30,144,255,0.8)",
                              fontFamily: "微软雅黑",
                              fontSize: 25,
                              fontWeight: "bolder"
                          }
                      },
                      tooltip: {
                          show: !0,
                          formatter: "{a} <br/>{b} : {c} ({d}%)"
                      },
                      legend: {
                          orient: "vertical",
                          x: 170,
                          y: 45,
                          itemGap: 12,
                          data: ["68%Something #1", "29%Something #2"]
                      },
                      toolbox: {
                          show: !0,
                          feature: {
                              mark: {
                                  show: !0
                              },
                              dataView: {
                                  show: !0,
                                  title: "Text View",
                                  lang: ["Text View", "Close", "Refresh"],
                                  readOnly: !1
                              },
                              restore: {
                                  show: !0,
                                  title: "Restore"
                              },
                              saveAsImage: {
                                  show: !0,
                                  title: "Save Image"
                              }
                          }
                      },
                      series: [{
                          name: "",
                          type: "pie",
                          clockWise: !1,
                          radius: [105, 130],
                          itemStyle: k,
                          data: [{
                              value: (100-progress),
                              name: "Not Completed",
                              itemStyle: 1
                          },
                          {
                              value: progress,
                              name: "Completed"
                          }]
                      }]
                  })
              }

          }
      }
